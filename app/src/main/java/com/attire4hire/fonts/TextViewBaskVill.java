package com.attire4hire.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.w3c.dom.Text;

@SuppressLint("AppCompatCustomView")
public class TextViewBaskVill extends TextView {

    /*
     * Getting Current Class Name
     * */

    private String mTag = TextViewBaskVill.this.getClass().getSimpleName();

    public TextViewBaskVill(Context context) {
        super(context);
        applyCustomFont(context);
    }


    public TextViewBaskVill(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewBaskVill(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TextViewBaskVill(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new BaskVill(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}

