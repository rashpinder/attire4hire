package com.attire4hire.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

public class EdittextRegular extends EditText {
    /*
     * Getting Current Class Name
     * */

    private String mTag = EdittextRegular.this.getClass().getSimpleName();
    public EdittextRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EdittextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EdittextRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public EdittextRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
