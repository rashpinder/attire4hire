package com.attire4hire.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;


public class TextviewSemiBold extends TextView {
    /*
     * Getting Current Class Name
     * */

    private String mTag = TextviewSemiBold.this.getClass().getSimpleName();
    public TextviewSemiBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextviewSemiBold(Context context,  AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextviewSemiBold(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
