package com.attire4hire.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

public class EdittextSemiBold extends EditText {
    /*
     * Getting Current Class Name
     * */

    private String mTag = EdittextSemiBold.this.getClass().getSimpleName();
    public EdittextSemiBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EdittextSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EdittextSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public EdittextSemiBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
