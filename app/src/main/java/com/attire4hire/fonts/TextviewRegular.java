package com.attire4hire.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;

@SuppressLint("AppCompatCustomView")
public class TextviewRegular extends TextView {
    /*
     * Getting Current Class Name
     * */

    private String mTag = TextviewRegular.this.getClass().getSimpleName();
    public TextviewRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextviewRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextviewRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public TextviewRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
