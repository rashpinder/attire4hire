package com.attire4hire.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class ProximaSemiBold {


    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "ProximaNova-Semibold.ttf";


    /*
     * Constructor with Context
     * */
    public ProximaSemiBold(Context context) {
        this.mContext = context;
    }
    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }
}
