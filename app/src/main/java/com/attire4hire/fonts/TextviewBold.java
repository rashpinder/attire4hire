package com.attire4hire.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.w3c.dom.Text;

public class TextviewBold extends TextView {

    /*
     * Getting Current Class Name
     * */

    private String mTag = TextviewBold.this.getClass().getSimpleName();

    public TextviewBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextviewBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextviewBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public TextviewBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
