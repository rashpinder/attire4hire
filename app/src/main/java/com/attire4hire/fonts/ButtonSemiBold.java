package com.attire4hire.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

public class ButtonSemiBold extends Button {
    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonSemiBold.this.getClass().getSimpleName();

    public ButtonSemiBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public ButtonSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public ButtonSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public ButtonSemiBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ProximaSemiBold(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
