package com.attire4hire.retrofitModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyInquiriesPojo {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class AllRequest {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("days")
        @Expose
        private String days;
        @SerializedName("booking_dates")
        @Expose
        private String bookingDates;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("req_handle")
        @Expose
        private String reqHandle;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("product_detail")
        @Expose
        private ProductDetail productDetail;
        @SerializedName("user_detail")
        @Expose
        private UserDetail userDetail;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("booked")
        @Expose
        private List<String> booked = null;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;
        @SerializedName("title")
        @Expose
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getBookingDates() {
            return bookingDates;
        }

        public void setBookingDates(String bookingDates) {
            this.bookingDates = bookingDates;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReqHandle() {
            return reqHandle;
        }

        public void setReqHandle(String reqHandle) {
            this.reqHandle = reqHandle;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public ProductDetail getProductDetail() {
            return productDetail;
        }

        public void setProductDetail(ProductDetail productDetail) {
            this.productDetail = productDetail;
        }

        public UserDetail getUserDetail() {
            return userDetail;
        }

        public void setUserDetail(UserDetail userDetail) {
            this.userDetail = userDetail;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public List<String> getBooked() {
            return booked;
        }

        public void setBooked(List<String> booked) {
            this.booked = booked;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    public class Data {

        @SerializedName("all_requests")
        @Expose
        private List<AllRequest> allRequests = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllRequest> getAllRequests() {
            return allRequests;
        }

        public void setAllRequests(List<AllRequest> allRequests) {
            this.allRequests = allRequests;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

    }

    public class ProductDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("image_id")
        @Expose
        private String imageId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("type_name")
        @Expose
        private String typeName;
        @SerializedName("size_name")
        @Expose
        private String sizeName;
        @SerializedName("color_name")
        @Expose
        private String colorName;
        @SerializedName("brand_name")
        @Expose
        private String brandName;
        @SerializedName("condition_name")
        @Expose
        private String conditionName;
        @SerializedName("ocasion_name")
        @Expose
        private String ocasionName;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("retail_price")
        @Expose
        private String retailPrice;
        @SerializedName("replacement_value")
        @Expose
        private String replacementValue;
        @SerializedName("week_4days")
        @Expose
        private String week4days;
        @SerializedName("week_8days")
        @Expose
        private String week8days;
        @SerializedName("week_12days")
        @Expose
        private String week12days;
        @SerializedName("instant_booking")
        @Expose
        private String instantBooking;
        @SerializedName("open_for_sale")
        @Expose
        private String openForSale;
        @SerializedName("cleaning_free")
        @Expose
        private String cleaningFree;
        @SerializedName("drop_person")
        @Expose
        private String dropPerson;
        @SerializedName("delivery_free")
        @Expose
        private String deliveryFree;
        @SerializedName("image1")
        @Expose
        private String image1;
        @SerializedName("image2")
        @Expose
        private String image2;
        @SerializedName("image3")
        @Expose
        private String image3;
        @SerializedName("image4")
        @Expose
        private String image4;
        @SerializedName("image5")
        @Expose
        private String image5;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getSizeName() {
            return sizeName;
        }

        public void setSizeName(String sizeName) {
            this.sizeName = sizeName;
        }

        public String getColorName() {
            return colorName;
        }

        public void setColorName(String colorName) {
            this.colorName = colorName;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getConditionName() {
            return conditionName;
        }

        public void setConditionName(String conditionName) {
            this.conditionName = conditionName;
        }

        public String getOcasionName() {
            return ocasionName;
        }

        public void setOcasionName(String ocasionName) {
            this.ocasionName = ocasionName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(String retailPrice) {
            this.retailPrice = retailPrice;
        }

        public String getReplacementValue() {
            return replacementValue;
        }

        public void setReplacementValue(String replacementValue) {
            this.replacementValue = replacementValue;
        }

        public String getWeek4days() {
            return week4days;
        }

        public void setWeek4days(String week4days) {
            this.week4days = week4days;
        }

        public String getWeek8days() {
            return week8days;
        }

        public void setWeek8days(String week8days) {
            this.week8days = week8days;
        }

        public String getWeek12days() {
            return week12days;
        }

        public void setWeek12days(String week12days) {
            this.week12days = week12days;
        }

        public String getInstantBooking() {
            return instantBooking;
        }

        public void setInstantBooking(String instantBooking) {
            this.instantBooking = instantBooking;
        }

        public String getOpenForSale() {
            return openForSale;
        }

        public void setOpenForSale(String openForSale) {
            this.openForSale = openForSale;
        }

        public String getCleaningFree() {
            return cleaningFree;
        }

        public void setCleaningFree(String cleaningFree) {
            this.cleaningFree = cleaningFree;
        }

        public String getDropPerson() {
            return dropPerson;
        }

        public void setDropPerson(String dropPerson) {
            this.dropPerson = dropPerson;
        }

        public String getDeliveryFree() {
            return deliveryFree;
        }

        public void setDeliveryFree(String deliveryFree) {
            this.deliveryFree = deliveryFree;
        }

        public String getImage1() {
            return image1;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getImage3() {
            return image3;
        }

        public void setImage3(String image3) {
            this.image3 = image3;
        }

        public String getImage4() {
            return image4;
        }

        public void setImage4(String image4) {
            this.image4 = image4;
        }

        public String getImage5() {
            return image5;
        }

        public void setImage5(String image5) {
            this.image5 = image5;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

    }

    public class UserDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("stripe_account_id")
        @Expose
        private String stripeAccountId;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("profile_pic")
        @Expose
        private String profilePic;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("facebook_id")
        @Expose
        private String facebookId;
        @SerializedName("google_id")
        @Expose
        private String googleId;
        @SerializedName("instagram_id")
        @Expose
        private String instagramId;
        @SerializedName("accesstoken")
        @Expose
        private String accesstoken;
        @SerializedName("email_verification")
        @Expose
        private String emailVerification;
        @SerializedName("phone_verification")
        @Expose
        private String phoneVerification;
        @SerializedName("id_verification")
        @Expose
        private String idVerification;
        @SerializedName("stripe_account_verification")
        @Expose
        private String stripeAccountVerification;
        @SerializedName("address_latitude")
        @Expose
        private String addressLatitude;
        @SerializedName("address_longitude")
        @Expose
        private String addressLongitude;
        @SerializedName("current_latitude")
        @Expose
        private String currentLatitude;
        @SerializedName("current_longitude")
        @Expose
        private String currentLongitude;
        @SerializedName("account_setup")
        @Expose
        private String accountSetup;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("stripe_account_token")
        @Expose
        private String stripeAccountToken;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("accept_terms")
        @Expose
        private String acceptTerms;
        @SerializedName("suspend")
        @Expose
        private String suspend;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("review")
        @Expose
        private Integer review;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getStripeAccountId() {
            return stripeAccountId;
        }

        public void setStripeAccountId(String stripeAccountId) {
            this.stripeAccountId = stripeAccountId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getGoogleId() {
            return googleId;
        }

        public void setGoogleId(String googleId) {
            this.googleId = googleId;
        }

        public String getInstagramId() {
            return instagramId;
        }

        public void setInstagramId(String instagramId) {
            this.instagramId = instagramId;
        }

        public String getAccesstoken() {
            return accesstoken;
        }

        public void setAccesstoken(String accesstoken) {
            this.accesstoken = accesstoken;
        }

        public String getEmailVerification() {
            return emailVerification;
        }

        public void setEmailVerification(String emailVerification) {
            this.emailVerification = emailVerification;
        }

        public String getPhoneVerification() {
            return phoneVerification;
        }

        public void setPhoneVerification(String phoneVerification) {
            this.phoneVerification = phoneVerification;
        }

        public String getIdVerification() {
            return idVerification;
        }

        public void setIdVerification(String idVerification) {
            this.idVerification = idVerification;
        }

        public String getStripeAccountVerification() {
            return stripeAccountVerification;
        }

        public void setStripeAccountVerification(String stripeAccountVerification) {
            this.stripeAccountVerification = stripeAccountVerification;
        }

        public String getAddressLatitude() {
            return addressLatitude;
        }

        public void setAddressLatitude(String addressLatitude) {
            this.addressLatitude = addressLatitude;
        }

        public String getAddressLongitude() {
            return addressLongitude;
        }

        public void setAddressLongitude(String addressLongitude) {
            this.addressLongitude = addressLongitude;
        }

        public String getCurrentLatitude() {
            return currentLatitude;
        }

        public void setCurrentLatitude(String currentLatitude) {
            this.currentLatitude = currentLatitude;
        }

        public String getCurrentLongitude() {
            return currentLongitude;
        }

        public void setCurrentLongitude(String currentLongitude) {
            this.currentLongitude = currentLongitude;
        }

        public String getAccountSetup() {
            return accountSetup;
        }

        public void setAccountSetup(String accountSetup) {
            this.accountSetup = accountSetup;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getStripeAccountToken() {
            return stripeAccountToken;
        }

        public void setStripeAccountToken(String stripeAccountToken) {
            this.stripeAccountToken = stripeAccountToken;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getAcceptTerms() {
            return acceptTerms;
        }

        public void setAcceptTerms(String acceptTerms) {
            this.acceptTerms = acceptTerms;
        }

        public String getSuspend() {
            return suspend;
        }

        public void setSuspend(String suspend) {
            this.suspend = suspend;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Integer getReview() {
            return review;
        }

        public void setReview(Integer review) {
            this.review = review;
        }

    }

}
