package com.attire4hire.extension;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.activity.AccountSetupActivity;
import com.attire4hire.activity.AnotherUserActivity;
import com.attire4hire.activity.BaseActivity;
import com.attire4hire.activity.ChatActivity;
import com.attire4hire.adapters.DetailsSlidingImageAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.chahinem.pageindicator.PageIndicator;
import com.google.gson.JsonObject;
import com.savvi.rangedatepicker.CalendarPickerView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ItemDetailsExtensionActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = ItemDetailsExtensionActivity.this;

    /*
     * Getting the Current Class Name
     */
    String TAG = ItemDetailsExtensionActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;
    @BindView(R.id.mIndicatorCI)
    PageIndicator mIndicatorCI;
    @BindView(R.id.txtNameTV)
    TextviewBold txtNameTV;
    @BindView(R.id.txtPriceTV)
    TextviewSemiBold txtPriceTV;
    @BindView(R.id.loc)
    ImageView loc;
    @BindView(R.id.txtLocationTV)
    TextviewSemiBold txtLocationTV;
    @BindView(R.id.txtSizeTV)
    TextviewSemiBold txtSizeTV;
    @BindView(R.id.txtTypeTV)
    TextviewSemiBold txtTypeTV;
    @BindView(R.id.txtColorTV)
    TextviewSemiBold txtColorTV;
    @BindView(R.id.txtBrandDesignNameTV)
    TextviewSemiBold txtBrandDesignNameTV;
    @BindView(R.id.txtConditionsTV)
    TextviewSemiBold txtConditionsTV;
    @BindView(R.id.txtOccasionTV)
    TextviewSemiBold txtOccasionTV;
    @BindView(R.id.txtRentalPriceTV)
    TextviewSemiBold txtRentalPriceTV;
    @BindView(R.id.txtReplacementValueTV)
    TextviewSemiBold txtReplacementValueTV;
    @BindView(R.id.imgDay4IV)
    ImageView imgDay4IV;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.rlDescriptionRL)
    RelativeLayout rlDescriptionRL;
    @BindView(R.id.rlDeleteRL)
    RelativeLayout rlDeleteRL;
    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.txtUserNameTV)
    TextviewSemiBold txtUserNameTV;
    @BindView(R.id.txTotalReviewsTV)
    TextviewRegular txTotalReviewsTV;
    @BindView(R.id.txtUserLocationTV)
    TextviewRegular txtUserLocationTV;
    @BindView(R.id.rlContactLedgerRL)
    RelativeLayout rlContactLedgerRL;
    @BindView(R.id.txtAddToFavoriteTV)
    TextviewSemiBold txtAddToFavoriteTV;
    @BindView(R.id.txtRequestToBuyTV)
    TextviewSemiBold txtRequestToBuyTV;
    @BindView(R.id.txtSelectDaysTV)
    TextviewSemiBold txtSelectDaysTV;
    @BindView(R.id.txtRentTV)
    TextviewSemiBold txtRentTV;
    @BindView(R.id.txt4DaysTV)
    TextviewRegular txt4DaysTV;
    @BindView(R.id.check_availabilityTv)
    TextviewSemiBold check_availabilityTv;
    @BindView(R.id.ll_add_req)
    LinearLayout ll_add_req;
    @BindView(R.id.rl_reqRent)
    RelativeLayout rl_reqRent;
    @BindView(R.id.ll_rental_fee)
    LinearLayout ll_rental_fee;
    @BindView(R.id.ll_user_details)
    LinearLayout ll_user_details;
    @BindView(R.id.ll_rent)
    LinearLayout ll_rent;
    @BindView(R.id.ll_OpenForSale)
    LinearLayout ll_OpenForSale;
    @BindView(R.id.tv_OpenForSalePrice)
    TextviewSemiBold tv_OpenForSalePrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.mRatingbarRB)
    RatingBar mRatingbarRB;
    @BindView(R.id.userRatings)
    RatingBar userRatings;
    @BindView(R.id.purchasePrice)
    LinearLayout purchasePrice;
    @BindView(R.id.buyRentView)
    View buyRentView;

    /*
     * Initalize Objects...
     * */
    ItemProductModel itemProductModel;
    DetailsSlidingImageAdapter mDetailsSlidingImageAdapter;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<String> mSelectDateList = new ArrayList<>();
    List<Date> mSelectedArrayList;
    //    List<Date> dates;
    String fromDate = "", nextDate = "";
    CalendarPickerView calendarView;
    String strFarvouriteStatus = "", IdVerification = "", Days = "4";

    Date firstDate;
    String firstDateMonthNumber = "";
    List<Date> extensionDates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details_extension);

        ButterKnife.bind(this);

        /* users can't use extension in buy case */
        txtRequestToBuyTV.setVisibility(View.GONE);

        getIntentData();
    }

    @OnClick({R.id.rlCancelRL, R.id.rlDescriptionRL,
            R.id.rlContactLedgerRL, R.id.txtAddToFavoriteTV,
            R.id.txtRentTV, R.id.check_availabilityTv, R.id.ll_user_details})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;
            case R.id.rlDescriptionRL:
                performDescriptionClick();
                break;
            case R.id.rlContactLedgerRL:
                performContactLedgerClick();
                break;
            case R.id.txtAddToFavoriteTV:
                performAddToFavoriteClick();
                break;
            case R.id.txtRentTV:
                performRentToRentClick();
                break;
            case R.id.check_availabilityTv:
                performCalendarClick();
                break;
            case R.id.ll_user_details:
                performUserDetailsClick();
                break;
        }
    }

    private void performRentToRentClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeRequestExtensionAPI();
        }
    }

    private void performUserDetailsClick() {
        startActivity(new Intent(mActivity, AnotherUserActivity.class).putExtra(Attire4hirePrefrences.ANOTHER_USER_ID, itemProductModel.getAnother_User_Id()));
    }

    private void performDescriptionClick() {
        showInfromationDialog(mActivity, "Description", CapitalizedFirstLetter(itemProductModel.getDescription()));
    }

    private void performContactLedgerClick() {
        executeRequestChatApi();
    }

    private void performAddToFavoriteClick() {
        if (strFarvouriteStatus.equals("1")) {
            Add_Favourite("2");
        } else if (strFarvouriteStatus.equals("2")) {
            Add_Favourite("1");
        }
    }

    private void performCalendarClick() {

        if (!Days.equals("")) {
            showCalenderPopup();
        } else {
            showAlertDialog(mActivity, getString(R.string.choose_rental_period));
        }
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            String input = itemProductModel.getBooking_dates();

            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(input);

                String[] strArr = new String[jsonArray.length()];

                for (int i = 0; i < jsonArray.length(); i++) {
                    strArr[i] = jsonArray.getString(i);
                }

                String first_date = strArr[0];

                SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
                Date newDate = spf.parse(first_date);
                spf = new SimpleDateFormat("MMM dd HH:mm:ss yyyy");
                String date = spf.format(newDate);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd HH:mm:ss yyyy");
                firstDate = dateFormat.parse(date);

                firstDateMonthNumber = (String) android.text.format.DateFormat.format("MM", firstDate); // Month in number


                /* get extended dates and show extended dates in pink color */
                Calendar c = Calendar.getInstance();

                String str_dates = itemProductModel.getBooking_dates();

                try {
                    JSONArray mJsonArray = new JSONArray(str_dates);
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        String strDate = mJsonArray.getString(i);
                        Log.e(TAG, "***Single Date**" + strDate);
                        Date mDate = null;
                        try {
                            mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);

                            extensionDates.add(mDate);

                            if (c.getTime().after(mDate)) {
                                String today = c.getTime().toString().substring(0, 10);
                                String datee = mDate.toString().substring(0, 10);

                                if (today.equals(datee)) {
                                } else {
                                    extensionDates.remove(mDate);
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Log.e(TAG, "***Converted Dates**" + extensionDates.size());
                    }

                } catch (Exception e) {
                    e.toString();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Set Details Data
            getDetailsData();
        }
    }

    private void getDetailsData() {
        if (isNetworkAvailable(mActivity)) {

            Days = "4";
            txtSelectDaysTV.setText("4 Days" + "/" + " £" + itemProductModel.getWeek_4days());
            imgDay4IV.setImageResource(R.drawable.ic_check);

            if (mItemProductModel != null)
                mItemProductModel.clear();

            if (mSelectDateList != null)
                mSelectDateList.clear();

            executeDetailsApi();
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void setDataOnWidgets() {
        /*
         * Set ViewPager Images:
         * */
        ArrayList<String> mImagesArrayList = new ArrayList<>();
        if (itemProductModel.getImage1() != null && itemProductModel.getImage1().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage1());
        }
        if (itemProductModel.getImage2() != null && itemProductModel.getImage2().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage2());
        }
        if (itemProductModel.getImage3() != null && itemProductModel.getImage3().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage3());
        }
        if (itemProductModel.getImage4() != null && itemProductModel.getImage4().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage4());
        }
        if (itemProductModel.getImage5() != null && itemProductModel.getImage5().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage5());
        }

        //Set Images
        mDetailsSlidingImageAdapter = new DetailsSlidingImageAdapter(mActivity, mImagesArrayList);
        mViewPagerVP.setAdapter(mDetailsSlidingImageAdapter);
        mIndicatorCI.attachTo(mViewPagerVP);

        /* set data in widgets */
        txtNameTV.setText(CapitalizedFirstLetter(itemProductModel.getName()));
        String strDay41 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_4days()));
        txtPriceTV.setText("£" + strDay41);
        txtSizeTV.setText(itemProductModel.getSize_name());
        txtTypeTV.setText(itemProductModel.getType_name());
        txtColorTV.setText(itemProductModel.getColor_name());

        txtBrandDesignNameTV.setText(itemProductModel.getBrand_name());
        txtConditionsTV.setText(itemProductModel.getCondition_name());
        txtOccasionTV.setText(itemProductModel.getOcasion_name());
        txtRentalPriceTV.setText("£" + itemProductModel.getRetail_price());
        txtLocationTV.setText(itemProductModel.getLocation());

        if (itemProductModel.getAnother_ProfilePic() != null && itemProductModel.getAnother_ProfilePic().contains("http")) {
            Picasso.with(mActivity).load(itemProductModel.getAnother_ProfilePic())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicIV);
        }

        if (itemProductModel.getRatings() != null && itemProductModel.getRatings().length() > 0) {
            mRatingbarRB.setRating(Float.parseFloat(itemProductModel.getRatings()));
        }

        userRatings.setRating(Float.parseFloat(itemProductModel.getUser_Ratings()));

        txtUserNameTV.setText(itemProductModel.getAnother_User_Name() + " " + itemProductModel.getAnother_User_LastName());
        txtUserLocationTV.setText(itemProductModel.getAnother_User_CityName());
        if (itemProductModel.getReview().equals("1") || itemProductModel.getReview().equals("0")) {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Review");
        } else {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Reviews");
        }

        String strDay4 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_4days()));

        txt4DaysTV.setText("4 Days" + " £" + strDay4);

        // if user id matches with the login user
        if (itemProductModel.getAnother_User_Id().equals(getUserID())) {
            rlContactLedgerRL.setVisibility(View.GONE);
            ll_add_req.setVisibility(View.GONE);
            rl_reqRent.setVisibility(View.GONE);
            imgDay4IV.setVisibility(View.GONE);
            check_availabilityTv.setVisibility(View.GONE);
            ll_user_details.setVisibility(View.GONE);
            buyRentView.setVisibility(View.GONE);

            if (itemProductModel.getOpen_for_sale().equals("")) {

                purchasePrice.setVisibility(View.GONE);

            } else if (itemProductModel.getOpen_for_sale().equals("0")) {

                purchasePrice.setVisibility(View.GONE);

            } else {
                purchasePrice.setVisibility(View.VISIBLE);
                txtReplacementValueTV.setText("£" + itemProductModel.getOpen_for_sale());
            }

        } else {
            rlContactLedgerRL.setVisibility(View.VISIBLE);
            ll_user_details.setVisibility(View.VISIBLE);
            ll_add_req.setVisibility(View.VISIBLE);

            if (itemProductModel.getBuyRequestStatus().equals("false")) {

                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    imgDay4IV.setVisibility(View.GONE);
                    check_availabilityTv.setVisibility(View.GONE);
                    rl_reqRent.setVisibility(View.GONE);

                    purchasePrice.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params1);
                } else {
                    imgDay4IV.setVisibility(View.VISIBLE);
                    check_availabilityTv.setVisibility(View.VISIBLE);
                    rl_reqRent.setVisibility(View.VISIBLE);
                    purchasePrice.setVisibility(View.VISIBLE);
                }

            } else {
                if (itemProductModel.getBuy_Option().equals("false")) {
                    imgDay4IV.setVisibility(View.GONE);
                    check_availabilityTv.setVisibility(View.GONE);
                    rl_reqRent.setVisibility(View.GONE);

                    purchasePrice.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params1);
                } else {
                    imgDay4IV.setVisibility(View.VISIBLE);
                    check_availabilityTv.setVisibility(View.VISIBLE);
                    rl_reqRent.setVisibility(View.VISIBLE);
                    purchasePrice.setVisibility(View.VISIBLE);
                }
            }

            manageOpenForSale();
        }

        if (itemProductModel.getFavourite().equals("1")) {
            txtAddToFavoriteTV.setText("Add to Favorites");
        } else if (itemProductModel.getFavourite().equals("2")) {
            txtAddToFavoriteTV.setText("Remove from Favorites");
        }

        //Get All Dates:
        Log.e(TAG, "***All Dates List***" + itemProductModel.getDate());
    }

    /* manage open for sale according to buy request status */
    private void manageOpenForSale() {

        if (itemProductModel.getBuyRequestStatus().equals("false")) {

            purchasePrice.setVisibility(View.GONE);

            if (itemProductModel.getBuy_Option().equals("false")) {
                rl_reqRent.setVisibility(View.GONE);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 20);
                txtAddToFavoriteTV.setLayoutParams(params);
            } else {
                rl_reqRent.setVisibility(View.VISIBLE);
            }
        } else {
            if (itemProductModel.getOpen_for_sale().equals("")) {

                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    rl_reqRent.setVisibility(View.GONE);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    rl_reqRent.setVisibility(View.VISIBLE);
                }

            } else if (itemProductModel.getOpen_for_sale().equals("0")) {


                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    rl_reqRent.setVisibility(View.GONE);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    rl_reqRent.setVisibility(View.VISIBLE);
                }

            } else {
                purchasePrice.setVisibility(View.VISIBLE);
                txtReplacementValueTV.setText("£" + itemProductModel.getOpen_for_sale());
                if (itemProductModel.getBuy_Option().equals("false")) {

                    rl_reqRent.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    rl_reqRent.setVisibility(View.VISIBLE);
                }
            }
        }

        /* open calender automatically when comes from extension */
        showCalenderPopup();
    }

    private void executeDetailsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ITEM_DETAILS_API;
        JSONObject params = new JSONObject();
        try {
            params.put("id", itemProductModel.getId());
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("data")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                for (int i = 0; i < 1; i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);

                    if (!mDataObject.isNull("id"))
                        itemProductModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        itemProductModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        itemProductModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        itemProductModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        itemProductModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        itemProductModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name")) {
                        itemProductModel.setColor_name(mDataObject.getString("color_name"));
                        txtColorTV.setText(mDataObject.getString("color_name"));
                    }
                    if (!mDataObject.isNull("favourite")) {
                        itemProductModel.setFavourite(mDataObject.getString("favourite"));
                        strFarvouriteStatus = mDataObject.getString("favourite");
                    }
                    if (!mDataObject.isNull("rent_request_status"))
                        itemProductModel.setRentRequestStatus(mDataObject.getString("rent_request_status"));

                    if (!mDataObject.isNull("buy_request_status"))
                        itemProductModel.setBuyRequestStatus(mDataObject.getString("buy_request_status"));

                    if (!mDataObject.isNull("buy_option"))
                        itemProductModel.setBuy_Option(mDataObject.getString("buy_option"));

                    if (!mDataObject.isNull("brand_name"))
                        itemProductModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        itemProductModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        itemProductModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        itemProductModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        itemProductModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("location"))
                        itemProductModel.setLocation(mDataObject.getString("location"));
                    if (!mDataObject.isNull("date"))
                        itemProductModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        itemProductModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        itemProductModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        itemProductModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        itemProductModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        itemProductModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        itemProductModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        itemProductModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        itemProductModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        itemProductModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        itemProductModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        itemProductModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        itemProductModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        itemProductModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        itemProductModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        itemProductModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        itemProductModel.setRatings(mDataObject.getString("rating"));

                    JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                    if (!mDataObject_UserDetails.isNull("id"))
                        itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                    if (!mDataObject_UserDetails.isNull("first_name"))
                        itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                    if (!mDataObject_UserDetails.isNull("last_name"))
                        itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                    if (!mDataObject_UserDetails.isNull("email"))
                        itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                    if (!mDataObject_UserDetails.isNull("gender"))
                        itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                    if (!mDataObject_UserDetails.isNull("city_name"))
                        itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                    if (!mDataObject_UserDetails.isNull("phone_no"))
                        itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                    if (!mDataObject_UserDetails.isNull("profile_pic"))
                        itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                    if (!mDataObject_UserDetails.isNull("rating"))
                        itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                    if (!mDataObject_UserDetails.isNull("review"))
                        itemProductModel.setReview(mDataObject_UserDetails.getString("review"));

                    mItemProductModel.add(itemProductModel);
                }

                //Set Data On Widgets
                setDataOnWidgets();
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void showCalenderPopup() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.calender_popup);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        calendarView = alertDialog.findViewById(R.id.calendar_view);
        ImageView nextClick = alertDialog.findViewById(R.id.rightIV);
        ImageView backClick = alertDialog.findViewById(R.id.leftIV);
        TextView clickBT = alertDialog.findViewById(R.id.tv_set_date);

        backClick.setVisibility(View.GONE);
        nextClick.setVisibility(View.GONE);
        clickBT.setVisibility(View.VISIBLE);
        clickBT.setText(getString(R.string.choose_your_rental_dates));

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, 0);

        Date tomorrow = lastYear.getTime();

        /*
         * CLICK PATICULAR DATE
         * */
        calendarView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {

            @Override
            public void onDateSelected(Date date) {
                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                        .withHighlightedDates(mSelectedArrayList)
                        .withSelectedDates(extensionDates);
            }

            @Override
            public void onDateUnselected(Date date) {
                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                        .withHighlightedDates(mSelectedArrayList)
                        .withSelectedDates(extensionDates);
            }
        });

        clickBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        nextClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backClick.setVisibility(View.VISIBLE);
//                c.add(Calendar.MONTH, 1);

                int month = 0, dd = 0, yer = 0;

                Calendar cal = Calendar.getInstance();
                cal.setTime(firstDate);
                month = Integer.parseInt(checkDigit(cal.get(Calendar.MONTH) + 1));
//                dd = checkDigit(cal.get(Calendar.DATE));
//                yer = checkDigit(cal.get(Calendar.YEAR));

                c.add(month, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                calendarView.scrollToDate(resultdate);
            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                c.add(Calendar.MONTH, -1);

                Date resultdate = new Date(c.getTimeInMillis());

                //calender current date
                String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", resultdate); // Week day
                String day = (String) android.text.format.DateFormat.format("dd", resultdate); // day
                String monthString = (String) android.text.format.DateFormat.format("MMM", resultdate); // Month
                String monthNumber = (String) android.text.format.DateFormat.format("MM", resultdate); // Month in number
                String year = (String) android.text.format.DateFormat.format("yyyy", resultdate); // Year

                //calender current month
                int CalenderMonth = Integer.parseInt(firstDateMonthNumber);

                calendarView.scrollToDate(resultdate);
                Log.e("TAG", "mon::" + resultdate);

                //current month
                int month = Calendar.DAY_OF_MONTH;
                int CurrentMonth = month - 1;

                if (CalenderMonth == month) {
                    backClick.setVisibility(View.GONE);
                } else {
                    backClick.setVisibility(View.VISIBLE);
                }
            }
        });

        Log.e(TAG, "**Dates List Size**: " + itemProductModel.getDate());


        /* get all blocked dates */
        if (itemProductModel.getDate() != null && itemProductModel.getDate().length() > 5) {

            mSelectedArrayList = new ArrayList<>();
            try {
                JSONArray mJsonArray = new JSONArray(itemProductModel.getDate());
                for (int i = 0; i < mJsonArray.length(); i++) {
                    String strDate = mJsonArray.getString(i);
                    Log.e(TAG, "***Single Date**" + strDate);
                    Date mDate = null;
                    try {
                        mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);

                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String date = dateFormat.format(mDate);

                        /* get list */
                        JSONArray jsonArray = new JSONArray(itemProductModel.getBooking_dates());
                        List<String> list = new ArrayList<>();
                        for (int k = 0; k < jsonArray.length(); k++) {
                            list.add(jsonArray.getString(k));
                        }

                        /* check if block dates contain any extended date if not then add to list */
                        if (!list.contains(date)) {
                            mSelectedArrayList.add(mDate);
                        }

                        if (c.getTime().after(mDate)) {

                            String today = c.getTime().toString().substring(0, 10);
                            String datee = mDate.toString().substring(0, 10);

                            if (today.equals(datee)) {
//                                Toast.makeText(mActivity, "Today", Toast.LENGTH_SHORT).show();
                            } else {
                                mSelectedArrayList.remove(mDate);
//                                Toast.makeText(mActivity, "Old Date", Toast.LENGTH_SHORT).show();
                            }

                        } else if (c.getTime().equals(mDate)) {
//                            Toast.makeText(mActivity, "Today", Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(mActivity, "New Date", Toast.LENGTH_SHORT).show();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.e(TAG, "***Converted Dates**" + mSelectedArrayList.size());
                }

                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                        .withHighlightedDates(mSelectedArrayList)
                        .withSelectedDates(extensionDates);

            } catch (Exception e) {
                e.toString();
            }

        } else {
            calendarView.init(tomorrow, nextYear.getTime(), dt1)
                    .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                    .withSelectedDates(extensionDates);
        }

        calendarView.scrollToDate(firstDate);

        alertDialog.show();
    }

    private void Add_Favourite(String FavouriteStatus) {
        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();
        params.put("user_id", getUserID());
        params.put("product_id", itemProductModel.getId());
        params.put("favourite", FavouriteStatus);

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.ADD_FAVOURITE, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "**RESPONSE**" + response);

                dismissProgressDialog();
                try {
                    if (response.getString("status").equals("1")) {

                        if (strFarvouriteStatus.equals("1")) {
                            txtAddToFavoriteTV.setText("Remove from Favorites");
                            strFarvouriteStatus = "2";

                        } else if (strFarvouriteStatus.equals("2")) {
                            txtAddToFavoriteTV.setText("Add to Favorites");
                            strFarvouriteStatus = "1";
                        }

                        itemProductModel.setFavourite(strFarvouriteStatus);

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());


            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /* API to generate room_id */
    private void executeRequestChatApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.REQUEST_CHAT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("owner_id", itemProductModel.getAnother_User_Id());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {

                    if (response.getString("status").equals("1")) {
                        try {

                            if (!response.isNull("room_id"))
                                itemProductModel.setRoom_id(response.getString("room_id"));

                            if (!response.isNull("owner_id"))
                                itemProductModel.setOwner_id(response.getString("owner_id"));

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_detail");

                            if (!mDataObject_UserDetails.isNull("id"))
                                itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name"))
                                itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                            if (!mDataObject_UserDetails.isNull("last_name"))
                                itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                            if (!mDataObject_UserDetails.isNull("email"))
                                itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic"))
                                itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                            if (!mDataObject_UserDetails.isNull("rating"))
                                itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                            mItemProductModel.add(itemProductModel);

                            startActivity(new Intent(mActivity, ChatActivity.class).putExtra(ConstantData.MODEL, itemProductModel));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);
                finish();
            }
        });
        alertDialog.show();
    }

    /* API to request extension */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("order_id", itemProductModel.getOrder_id());
        mMap.put("request_id", itemProductModel.getRequest_id());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeRequestExtensionAPI() {
        showProgressDialog(mActivity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.RequestExtension(mParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {

                            if (!jsonObjectMM.isNull("id"))
                                itemProductModel.setRequest_id(jsonObjectMM.getString("id"));

                            Intent intent = new Intent(mActivity, FinalizeYourRequestExtensionActivity.class);
                            intent.putExtra(ConstantData.MODEL, itemProductModel);
                            mActivity.startActivity(intent);
                            finish();

                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}