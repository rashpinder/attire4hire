package com.attire4hire.extension;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.activity.BaseActivity;
import com.attire4hire.activity.CartActivity;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.activity.PaymentActivity;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayForYourItemExtensionActivity extends BaseActivity {
    Activity mActivity = PayForYourItemExtensionActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = PayForYourItemExtensionActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    @BindView(R.id.iv_cart_profile)
    ImageView iv_cart_profile;

    @BindView(R.id.tv_product_name)
    TextviewSemiBold tv_product_name;

    @BindView(R.id.rl_fees_cart)
    RelativeLayout rl_fees_cart;

    @BindView(R.id.tv_fees)
    TextviewBold tv_fees;

    @BindView(R.id.tv_rental_fee)
    TextviewRegular tv_rental_fee;

    @BindView(R.id.tv_service)
    TextviewRegular tv_service;

    @BindView(R.id.tv_total)
    TextviewRegular tv_total;

    @BindView(R.id.tv_proceed_to_pay)
    TextviewSemiBold tv_proceed_to_pay;

    @BindView(R.id.feeTextView)
    TextView feeTextView;

    @BindView(R.id.tv_type)
    TextView tv_type;

    @BindView(R.id.tv_total_top)
    TextView tv_total_top;

    @BindView(R.id.tv_date)
    TextView tv_date;

    TextviewRegular mRentalTV, mCleaningTV, mService, mDeliveryFeeTV;
    String RentalFee = "", ServiceFee = "", TotalFee = "";
    double d_Rental = 0.00, d_Service = 0.00, d_Total = 0.00;
    float f_service = 0, f_Rental = 0;
    ItemProductModel itemProductModel;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    String type = "", RequestId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_for_your_item_extension);

        //Set Butter Knife
        ButterKnife.bind(this);

        getIntentData();

        /* set text views for rental in case of extension */
        tv_type.setText("Rental");
        feeTextView.setText("Rental Fee");
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            //Set Details Data
//            executeOrderApi();
            executeRequestDetailsApi();
        }
    }

    @OnClick({R.id.cancelRL, R.id.tv_proceed_to_pay, R.id.rl_fees_cart, R.id.iv_cart_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.tv_proceed_to_pay:
                performProceedtoPayClick();
                break;

            case R.id.rl_fees_cart:
                if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                    showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_rental), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                    showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_Item_Buy), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                }
                break;

            case R.id.iv_cart_profile:
                performCartProfileClick();
                break;
        }
    }

    /* perform proceed to pay click in case of extension */
    private void performProceedtoPayClick() {
        Intent intent = new Intent(mActivity, PaymentActivity.class);
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PAYMENT_VALUE, "Cart");
        intent.putExtra(ConstantData.PAYMENT_VALUE, "Cart");
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        startActivity(intent);
        finish();
    }

    /* perform item image click */
    private void performCartProfileClick() {
        Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        intent.putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE);
        mActivity.startActivity(intent);
    }

    /*Show popup of fees*/
    private void showFees(Activity mActivity, String strHeading, String strRental, String strCleaning, String strService, String strDelivery) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.fees_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        mRentalTV = alertDialog.findViewById(R.id.tv_rental);
        mCleaningTV = alertDialog.findViewById(R.id.tv_cleaning);
        mService = alertDialog.findViewById(R.id.tv_service);
        mDeliveryFeeTV = alertDialog.findViewById(R.id.tv_delivery_fee);

        mCleaningTV.setVisibility(View.GONE);
        mDeliveryFeeTV.setVisibility(View.GONE);
        mHeading.setText(strHeading);

        String str_rental = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            str_rental = "<b><font color=#b7006d>Rental Fee:</font></b>" + "<font color=#000000> the cost of renting the item for the selected rental period.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            str_rental = "<b><font color=#b7006d>Purchase Price:</font></b>" + "<font color=#000000> the cost of buying your selected item.</font>";
        }

        String str_cleaning = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            mCleaningTV.setVisibility(View.GONE);
            str_cleaning = "<b><font color=#b7006d>Cleaning Fee:</font></b>" + "<font color=#000000> the cost of dry cleaning determined by the lender.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            mCleaningTV.setVisibility(View.GONE);
        }

        String str_service = "<b><font color=#b7006d>Service Fee:</font></b>" + "<font color=#000000> covers our bank fees, the smooth running of the Attires4Hire platform and other services.</font>";
        String str_delivery = "<b><font color=#b7006d>Delivery Fee:</font></b>" + "<font color=#000000> occurs when you select delivery by post.</font>";

        mRentalTV.setText(Html.fromHtml(str_rental));
        mCleaningTV.setText(Html.fromHtml(str_cleaning));
        mService.setText(Html.fromHtml(str_service));
        mDeliveryFeeTV.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * UPDATE READ STATUS API
     * */
    private void executeUpdateReadStatusApi(String request_id) {
        Map<String, String> params = new HashMap();

        params.put("user_id", getUserID());
        params.put("request_id", request_id);

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.UPDATE_READ_STATUS, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    } else {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /* execute get request details API */
    private void executeRequestDetailsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_REQUEST_DETAILS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("request_id", itemProductModel.getRequest_id());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (response.getString("status").equals("1")) {
                try {

                    JSONObject mDataObject = response.getJSONObject("data");

                    if (!mDataObject.isNull("request_id") && !mDataObject.getString("request_id").equals(""))
                        itemProductModel.setRequest_id(mDataObject.getString("request_id"));

                    if (!mDataObject.isNull("booking_dates")) {
                        itemProductModel.setBooking_dates(mDataObject.getString("booking_dates"));

                        String input = mDataObject.getString("booking_dates");

                        JSONArray jsonArray = new JSONArray(input);
                        String[] strArr = new String[jsonArray.length()];

                        for (int i = 0; i < jsonArray.length(); i++) {
                            strArr[i] = jsonArray.getString(i);
                        }

                        String first_date = strArr[0];
                        String last_date = strArr[strArr.length - 1];

                        tv_date.setText(parseDateToddMMyyyy(first_date) + " – " + parseDateToddMMyyyy(last_date));
                    }

                    if (!mDataObject.isNull("delivery_fee") && !mDataObject.getString("delivery_fee").equals(""))
                        itemProductModel.setCart_Delivery_fee(mDataObject.getString("delivery_fee"));
                    if (!mDataObject.isNull("cleaning_fee") && !mDataObject.getString("cleaning_fee").equals(""))
                        itemProductModel.setCart_Cleaning_fee(mDataObject.getString("cleaning_fee"));
                    if (!mDataObject.isNull("rental_fee") && !mDataObject.getString("rental_fee").equals(""))
                        itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));
                    if (!mDataObject.isNull("request_type") && !mDataObject.getString("request_type").equals(""))
                        itemProductModel.setRequest_type(mDataObject.getString("request_type"));

                    if (!mDataObject.isNull("service_fee") && !mDataObject.getString("service_fee").equals(""))
                        itemProductModel.setService_fee(mDataObject.getString("service_fee"));

                    JSONObject mDataObject_ProductDetails = mDataObject.getJSONObject("product_detail");

                    if (!mDataObject_ProductDetails.isNull("id") && !mDataObject_ProductDetails.getString("id").equals(""))
                        itemProductModel.setProduct_id(mDataObject_ProductDetails.getString("id"));
                    if (!mDataObject_ProductDetails.isNull("name") && !mDataObject_ProductDetails.getString("name").equals(""))
                        itemProductModel.setName(mDataObject_ProductDetails.getString("name"));
                    if (!mDataObject_ProductDetails.isNull("user_id") && !mDataObject_ProductDetails.getString("user_id").equals(""))
                        itemProductModel.setUser_id(mDataObject_ProductDetails.getString("user_id"));
                    if (!mDataObject_ProductDetails.isNull("image_id") && !mDataObject_ProductDetails.getString("image_id").equals(""))
                        itemProductModel.setImage_id(mDataObject_ProductDetails.getString("image_id"));
                    if (!mDataObject_ProductDetails.isNull("category_name") && !mDataObject_ProductDetails.getString("category_name").equals(""))
                        itemProductModel.setCategory_name(mDataObject_ProductDetails.getString("category_name"));
                    if (!mDataObject_ProductDetails.isNull("type_name") && !mDataObject_ProductDetails.getString("type_name").equals(""))
                        itemProductModel.setType_name(mDataObject_ProductDetails.getString("type_name"));
                    if (!mDataObject_ProductDetails.isNull("size_name") && !mDataObject_ProductDetails.getString("size_name").equals(""))
                        itemProductModel.setSize_name(mDataObject_ProductDetails.getString("size_name"));

                    if (!mDataObject_ProductDetails.isNull("color_name") && !mDataObject_ProductDetails.getString("color_name").equals(""))
                        itemProductModel.setColor_name(mDataObject_ProductDetails.getString("color_name"));
                    if (!mDataObject_ProductDetails.isNull("brand_name") && !mDataObject_ProductDetails.getString("brand_name").equals(""))
                        itemProductModel.setBrand_name(mDataObject_ProductDetails.getString("brand_name"));
                    if (!mDataObject_ProductDetails.isNull("condition_name") && !mDataObject_ProductDetails.getString("condition_name").equals(""))
                        itemProductModel.setCondition_name(mDataObject_ProductDetails.getString("condition_name"));
                    if (!mDataObject_ProductDetails.isNull("ocasion_name") && !mDataObject_ProductDetails.getString("ocasion_name").equals(""))
                        itemProductModel.setOcasion_name(mDataObject_ProductDetails.getString("ocasion_name"));
                    if (!mDataObject_ProductDetails.isNull("description") && !mDataObject_ProductDetails.getString("description").equals(""))
                        itemProductModel.setDescription(mDataObject_ProductDetails.getString("description"));
                    if (!mDataObject_ProductDetails.isNull("date") && !mDataObject_ProductDetails.getString("date").equals(""))
                        itemProductModel.setDate(mDataObject_ProductDetails.getString("date"));
                    if (!mDataObject_ProductDetails.isNull("retail_price") && !mDataObject_ProductDetails.getString("retail_price").equals(""))
                        itemProductModel.setRetail_price(mDataObject_ProductDetails.getString("retail_price"));
                    if (!mDataObject_ProductDetails.isNull("replacement_value") && !mDataObject_ProductDetails.getString("replacement_value").equals(""))
                        itemProductModel.setReplacement_value(mDataObject_ProductDetails.getString("replacement_value"));
                    if (!mDataObject_ProductDetails.isNull("week_4days") && !mDataObject_ProductDetails.getString("week_4days").equals(""))
                        itemProductModel.setWeek_4days(mDataObject_ProductDetails.getString("week_4days"));
                    if (!mDataObject_ProductDetails.isNull("week_8days") && !mDataObject_ProductDetails.getString("week_8days").equals(""))
                        itemProductModel.setWeek_8days(mDataObject_ProductDetails.getString("week_8days"));
                    if (!mDataObject_ProductDetails.isNull("week_12days") && !mDataObject_ProductDetails.getString("week_12days").equals(""))
                        itemProductModel.setWeek_12days(mDataObject_ProductDetails.getString("week_12days"));
                    if (!mDataObject_ProductDetails.isNull("instant_booking") && !mDataObject_ProductDetails.getString("instant_booking").equals(""))
                        itemProductModel.setInstant_booking(mDataObject_ProductDetails.getString("instant_booking"));
                    if (!mDataObject_ProductDetails.isNull("open_for_sale") && !mDataObject_ProductDetails.getString("open_for_sale").equals(""))
                        itemProductModel.setOpen_for_sale(mDataObject_ProductDetails.getString("open_for_sale"));
                    if (!mDataObject_ProductDetails.isNull("cleaning_free") && !mDataObject_ProductDetails.getString("cleaning_free").equals(""))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));
                    if (!mDataObject_ProductDetails.isNull("drop_person") && !mDataObject_ProductDetails.getString("drop_person").equals(""))
                        itemProductModel.setDrop_person(mDataObject_ProductDetails.getString("drop_person"));
                    if (!mDataObject_ProductDetails.isNull("cleaning_free") && !mDataObject_ProductDetails.getString("cleaning_free").equals(""))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));
                    if (!mDataObject_ProductDetails.isNull("delivery_free") && !mDataObject_ProductDetails.getString("delivery_free").equals(""))
                        itemProductModel.setDelivery_free(mDataObject_ProductDetails.getString("delivery_free"));
                    if (!mDataObject_ProductDetails.isNull("image1") && !mDataObject_ProductDetails.getString("image1").equals(""))
                        itemProductModel.setImage1(mDataObject_ProductDetails.getString("image1"));
                    if (!mDataObject_ProductDetails.isNull("image2") && !mDataObject_ProductDetails.getString("image2").equals(""))
                        itemProductModel.setImage2(mDataObject_ProductDetails.getString("image2"));
                    if (!mDataObject_ProductDetails.isNull("image3") && !mDataObject_ProductDetails.getString("image3").equals(""))
                        itemProductModel.setImage3(mDataObject_ProductDetails.getString("image3"));
                    if (!mDataObject_ProductDetails.isNull("image4") && !mDataObject_ProductDetails.getString("image4").equals(""))
                        itemProductModel.setImage4(mDataObject_ProductDetails.getString("image4"));
                    if (!mDataObject_ProductDetails.isNull("image5") && !mDataObject_ProductDetails.getString("image5").equals(""))
                        itemProductModel.setImage5(mDataObject_ProductDetails.getString("image5"));
                    if (!mDataObject_ProductDetails.isNull("rating") && !mDataObject_ProductDetails.getString("rating").equals(""))
                        itemProductModel.setRatings(mDataObject_ProductDetails.getString("rating"));

                    mItemProductModel.add(itemProductModel);

                    setDataOnWidgets();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (response.getString("status").equals("3")) {
                LogOut();

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void setDataOnWidgets() {
        tv_type.setText("Rental");
        feeTextView.setText("Rental Fee");

        if (itemProductModel.getService_fee() != null && !itemProductModel.getService_fee().equals("")) {
            f_service = Float.parseFloat(itemProductModel.getService_fee());
            d_Service = Double.parseDouble(itemProductModel.getService_fee());
            String s = String.format("%.2f", d_Service);
            tv_service.setText(s);
        } else {
            String s = String.format("%.2f", d_Service);
            tv_service.setText(s);
        }
        if (itemProductModel.getRental_Prize() != null && !itemProductModel.getRental_Prize().equals("")) {
            f_Rental = Float.parseFloat(itemProductModel.getRental_Prize());
            d_Rental = Double.parseDouble(itemProductModel.getRental_Prize());
            String s = String.format("%.2f", d_Rental);
            tv_rental_fee.setText(s);
        } else {
            String s = String.format("%.2f", d_Rental);
            tv_rental_fee.setText(s);
        }

        if (itemProductModel.getService_fee() != null && !itemProductModel.getService_fee().equals("")) {
            f_service = Float.parseFloat(itemProductModel.getService_fee());
            d_Service = Double.parseDouble(itemProductModel.getService_fee());
            String s = String.format("%.2f", d_Service);
            tv_service.setText(s);
        } else {
            String s = String.format("%.2f", d_Service);
            tv_service.setText(s);
        }

        if (itemProductModel.getName() != null && !itemProductModel.getName().equals("")) {
            tv_product_name.setText(CapitalizedFirstLetter(itemProductModel.getName()));
        }

        if (itemProductModel.getImage1() != null && !itemProductModel.getImage1().equals("")) {

            if (itemProductModel.getImage1().contains("http")) {
                Glide.with(mActivity).load(itemProductModel.getImage1())
                        .placeholder(R.drawable.ic_pp_ph)
                        .error(R.drawable.ic_pp_ph)
                        .into(iv_cart_profile);
            }

//            if (itemProductModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(itemProductModel.getImage1())
//                        .placeholder(R.drawable.ic_pp_ph)
//                        .error(R.drawable.ic_pp_ph)
//                        .into(iv_cart_profile);
//            } else {
//                Glide.with(mActivity).load(itemProductModel.getImage1().replace("http://", "https://"))
//                        .placeholder(R.drawable.ic_pp_ph)
//                        .error(R.drawable.ic_pp_ph)
//                        .into(iv_cart_profile);
//            }
        }

        d_Total = d_Rental + d_Service;

        String Total = String.format("%.2f", d_Total);
        tv_total.setText("£" + Total);
        tv_total_top.setText("Total: " + "£" + Total);

        executeUpdateReadStatusApi(itemProductModel.getRequest_id());
    }
}