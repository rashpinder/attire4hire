package com.attire4hire.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.activity.FiltersActivity;
import com.attire4hire.activity.LocationActivity;
import com.attire4hire.activity.RecentSearchActivity;
import com.attire4hire.adapters.FilteredItemsAdapter;
import com.attire4hire.adapters.HorizontalRVAdapter;
import com.attire4hire.adapters.PerformersCornerAdapter;
import com.attire4hire.adapters.SearchAdapter;
import com.attire4hire.interfaces.FavouriteCallBack;
import com.attire4hire.interfaces.PaginationFilterInterface;
import com.attire4hire.interfaces.PaginationPerformersCornerInterface;
import com.attire4hire.interfaces.PaginationSearchInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.savvi.rangedatepicker.CalendarPickerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SearchFragment extends BaseFragment implements PaginationSearchInterface,
        PaginationFilterInterface, PaginationPerformersCornerInterface {
    String TAG = SearchFragment.this.getClass().getSimpleName();

    /*Widgets*/
    Unbinder unbinder;
    @BindView(R.id.dataRV)
    RecyclerView dataRV;
    @BindView(R.id.horizontal_rv)
    RecyclerView horizontalRV;
    HorizontalRVAdapter mRVAdapter;
    @BindView(R.id.et_search)
    EditText recentsearchET;
    @BindView(R.id.searchRL)
    RelativeLayout mSearchRL;
    @BindView(R.id.Iv_cancel)
    ImageView Iv_cancel;

    SearchAdapter mAdapter;
    PerformersCornerAdapter mPerformersCornerAdapter;
    SwipeRefreshLayout swipeToRefresh;
    ProgressBar mProgressBar;
    FilteredItemsAdapter mfilteredItemsAdapter;
    TextView tv_NoData;
    PaginationSearchInterface mInterfaceData;
    PaginationFilterInterface mFilterInterfaceData;
    PaginationPerformersCornerInterface mPerformersCornerInterfaceData;
    private String strLastPage = "FALSE", strItemName = "", strLastFilterPage = "FALSE";
    private int page_no = 1;
    private int filter_page_num = 1;
    private boolean isSwipeRefresh = false;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> mItemPerformersProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> tempArrayList = new ArrayList<>();
    ItemProductModel mModel;
    private ArrayList<String> filtersList = new ArrayList<>();
    String OccasionDate = "", OpenForSale = "", Category = "", Type = "", Brand = "", Colour = "", Condition = "", Size = "",
            PriceRange_start = "", PriceRange_end, Location = "", Occasion = "", Price_Range = "";
    List<Date> mSelectedDateList;
    List<Date> mMultiSelectedDateaList;
    ArrayList<String> mSelectedArrayList = new ArrayList<>();

    /* use 1 if clicked on Performer corner else use 0 */
    public static String strPerformersType = "0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        unbinder = ButterKnife.bind(this, view);

        /* ids */
        mProgressBar = view.findViewById(R.id.mProgressBar);
        tv_NoData = view.findViewById(R.id.tv_NoData);
        mSearchRL = view.findViewById(R.id.searchRL);
        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);

        /* interfaces */
        mInterfaceData = this;
        mFilterInterfaceData = this;
        mPerformersCornerInterfaceData = this;

        /* read and write data from shared prefrences */
        Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.SEARCH_ITEM, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.SEARCH_VALUE, "");
        Attire4hirePrefrences.readString(getActivity(), "status", "");

        /* set top filters adapter */
        setFiltersAdapter();

        /* set SwipeRefreshLayout */
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                if (mItemProductModel != null)
                    mItemProductModel.clear();

                if (mItemPerformersProductModel != null)
                    mItemPerformersProductModel.clear();

                if (tempArrayList != null)
                    tempArrayList.clear();

                recentsearchET.setText("");
                strItemName = "";
                strPerformersType = "0";
                getList(page_no, strItemName);
            }
        });

        return view;
    }

    /* clicks using butterknife */
    @OnClick({R.id.et_search, R.id.Iv_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_search:
                performSearchClick();
                break;

            case R.id.Iv_cancel:
                performCancelClick();
                break;
        }
    }

    /* set recent search intent */
    private void performSearchClick() {
        Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.SEARCH_ITEM, "");
        Intent intent = new Intent(getActivity(), RecentSearchActivity.class);
        intent.putExtra(ConstantData.MODEL, mItemProductModel);
        startActivity(intent);
    }

    /* set click on cancel */
    private void performCancelClick() {
        Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.SEARCH_ITEM, "");
        recentsearchET.setText(null);
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mItemProductModel != null) {
            mItemProductModel.clear();
        }
        if (mItemPerformersProductModel != null) {
            mItemPerformersProductModel.clear();
        }
        strPerformersType = "0";
        getList(page_no, "");
        Iv_cancel.setVisibility(View.GONE);
    }

    /* set products list adapter */
    private void setProductsAdapter() {
        if (dataRV != null) {

            LinearLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 2);

            dataRV.setLayoutManager(linearLayoutManager);
            mAdapter = new SearchAdapter(getContext(), mItemProductModel, favouriteCallBack, mInterfaceData);
            dataRV.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

            dataRV.setItemAnimator(new DefaultItemAnimator());
            dataRV.setNestedScrollingEnabled(false);
        }
    }

    /* set products list adapter */
    private void setPerformersCornerProductsAdapter(ArrayList<ItemProductModel> mItemProductModelList) {

        if (dataRV != null) {

            LinearLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 2);

            dataRV.setLayoutManager(linearLayoutManager);
            mPerformersCornerAdapter = new PerformersCornerAdapter(getContext(), mItemProductModelList,
                    new FavouriteCallBack() {
                        @Override
                        public void favouriteCall(int position, String FavouriteStatus) {
                            Add_Favourite(FavouriteStatus, mItemProductModelList.get(position).getId(), position);
                        }
                    }, mPerformersCornerInterfaceData);
            dataRV.setAdapter(mPerformersCornerAdapter);
            mPerformersCornerAdapter.notifyDataSetChanged();

            dataRV.setItemAnimator(new DefaultItemAnimator());
            dataRV.setNestedScrollingEnabled(false);
        }
    }

    /* interface to favourite and unfavourite products */
    FavouriteCallBack favouriteCallBack = new FavouriteCallBack() {
        @Override
        public void favouriteCall(int position, String FavouriteStatus) {
            Add_Favourite(FavouriteStatus, mItemProductModel.get(position).getId(), position);
        }
    };

    /* execute API to add and remove from favourites API */
    private void Add_Favourite(String FavouriteStatus, String ProductId, int position) {
        showProgressDialog(getActivity());
        Map<String, String> params = new HashMap();
        params.put("user_id", getUserID());
        params.put("product_id", ProductId);
        params.put("favourite", FavouriteStatus);

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.ADD_FAVOURITE, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                dismissProgressDialog();

                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        if (strPerformersType.equals("1")) {
                            mItemPerformersProductModel.get(position).setFavourite(FavouriteStatus);
                        } else {
                            mItemProductModel.get(position).setFavourite(FavouriteStatus);
                        }
                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        showToast(getActivity(), response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /* API to get products list data */
    private void getList(final int page_no, String name) {

        /* clear all filter values and notify adapter when get all products api is being used */
        if (getActivity() != null) {
            ClearFilterValues();
            Attire4hirePrefrences.writeString(getActivity(), ConstantData.DATE_VALUE, "");
        }

        if (filtersList != null) {
            filtersList.clear();
            filtersList.add("Filters");
            filtersList.add("Occasion Date");
            filtersList.add("Performers' Corner");

            mRVAdapter.notifyDataSetChanged();
        }

        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                showProgressDialog(getActivity());
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        Map<String, String> params = new HashMap();
        params.put("name", name);
        params.put("page_no", String.valueOf(page_no));
        params.put("per_page", "16");
        params.put("user_id", getUserID());
        params.put("perform", "0");

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.GET_ALL_PRODUCTS_AND_SEARCH, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                if (!strPerformersType.equals("1")) {
                    parseJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(JSONObject response) {
        tempArrayList.clear();
        try {

            if (response.getString("status").equals("1")) {
                strLastPage = response.getString("last_page");

                JSONArray mJsonArray = response.getJSONArray("data");

                Log.e(TAG, "**Data Array Size**" + mJsonArray.length());

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));

                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("image_id"))
                        mModel.setImage_id(mDataObject.getString("image_id"));

                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));

                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));

                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));

                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));

                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));

                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));

                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));

                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));

                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));

                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));

                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));

                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));

                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));

                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));

                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));

                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));

                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));

                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));

                    if (!mDataObject.isNull("favourite"))
                        mModel.setFavourite(mDataObject.getString("favourite"));

                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));

                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));

                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));

                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));

                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));

                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));

                    if (!mDataObject.getString("user_detail").equals("")) {

                        JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                        if (!mDataObject_UserDetails.isNull("id"))
                            mModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                        if (!mDataObject_UserDetails.isNull("first_name"))
                            mModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                        if (!mDataObject_UserDetails.isNull("last_name"))
                            mModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                        if (!mDataObject_UserDetails.isNull("email"))
                            mModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                        if (!mDataObject_UserDetails.isNull("gender"))
                            mModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                        if (!mDataObject_UserDetails.isNull("city_name"))
                            mModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                        if (!mDataObject_UserDetails.isNull("phone_no"))
                            mModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                        if (!mDataObject_UserDetails.isNull("profile_pic"))
                            mModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                    }

                    if (page_no == 1) {
                        mItemProductModel.add(mModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {
                    /*
                     * Set Adapter
                     * */
                    setProductsAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }

            } else if (response.getString("status").equals("3")) {
                if (getActivity() != null) {
                    LogOut();
                }
            } else if (response.getString("status").equals("0")) {
                setProductsAdapter();
                if (strItemName.equals("")) {
                    showAlertDialog(getActivity(), "No items are available");
                } else {
                    setProductsAdapter();
                    showAlertDialog(getActivity(), getString(R.string.no_search_items));
                }
            } else {
                showAlertDialog(getActivity(), response.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            if (strLastPage.equals("FALSE")) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {

                        if (strPerformersType.equals("1")) {
                            getPerformersCornerList(page_no, strItemName);
                        } else {
                            getList(page_no, strItemName);
                        }
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    /* set top horizontal filters adapter */
    private void setFiltersAdapter() {
        filtersList.add("Filters");
        filtersList.add("Occasion Date");
        filtersList.add("Performers' Corner");

        if (horizontalRV != null) {
            horizontalRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            mRVAdapter = new HorizontalRVAdapter(getContext(), new HorizontalRVAdapter.OccasionDateInterface() {
                @Override
                public void Occasion(int position) {
                    if (!Attire4hirePrefrences.readString(getActivity(), ConstantData.DATE_VALUE, "").equals("search_filter")) {
                        performOccasionDateClick();
                    }
                }

                @Override
                public void Filters(int position) {
                    if (filtersList != null) {
                        filtersList.clear();
                        filtersList.add("Filters");
                    }
                    startActivity(new Intent(getActivity(), FiltersActivity.class));
                }

                @Override
                public void Location(int position) {
                    if (!Attire4hirePrefrences.readString(getActivity(), ConstantData.DATE_VALUE, "").equals("search_filter")) {
                        startActivity(new Intent(getActivity(), LocationActivity.class));
                    }
                }

                @Override
                public void PerformersCorner(int position, TextView mListItemTV) {
                    page_no = 1;
                    if (mItemProductModel != null)
                        mItemProductModel.clear();

                    if (tempArrayList != null)
                        tempArrayList.clear();

                    if (mItemPerformersProductModel != null)
                        mItemPerformersProductModel.clear();

                    strItemName = "";
                    strLastPage = "TRUE";
                    strPerformersType = "1";
                    recentsearchET.setText("");

                    /* make isSwipeRefresh false */
                    isSwipeRefresh = false;

                    getPerformersCornerList(page_no, strItemName);
                }

                @Override
                public void BackToHome(int position, TextView mListItemTV) {
                    page_no = 1;
                    if (mItemProductModel != null)
                        mItemProductModel.clear();

                    if (mItemPerformersProductModel != null)
                        mItemPerformersProductModel.clear();

                    if (tempArrayList != null)
                        tempArrayList.clear();

                    recentsearchET.setText("");
                    strItemName = "";
                    strPerformersType = "0";

                    /* make isSwipeRefresh false */
                    isSwipeRefresh = false;

                    getList(page_no, strItemName);
                }
            }, filtersList);
            horizontalRV.setAdapter(mRVAdapter);
            mRVAdapter.notifyDataSetChanged();
        }
    }

    /* perform click on occasion date */
    private void performOccasionDateClick() {
        showCalenderPopup();
    }

    /*
     * Show Calendar Popup
     *
     * */
    private void showCalenderPopup() {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.calender_popup);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CalendarPickerView calendar = alertDialog.findViewById(R.id.calendar_view);
        ImageView nextClick = alertDialog.findViewById(R.id.rightIV);
        ImageView backClick = alertDialog.findViewById(R.id.leftIV);
        TextView clickBT = alertDialog.findViewById(R.id.tv_set_date);
        clickBT.setText("OK");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        Calendar nextYear = Calendar.getInstance();

        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        //lastYear.add(Calendar.YEAR, 0);
        lastYear.add(Calendar.YEAR, 0);

        Date tomorrow = lastYear.getTime();

        /*
         * CLICK PATICULAR DATE
         * */
        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                if (mSelectedArrayList != null) {
                    mSelectedArrayList.clear();
                }
            }

            @Override
            public void onDateUnselected(Date date) {
                if (mSelectedArrayList != null) {
                    mSelectedArrayList.clear();
                }
            }
        });

        /*
         *`
         * BUTTON TO SHOW SELECTED DATES
         * */
        clickBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date datee = calendar.getSelectedDate();
                mMultiSelectedDateaList = calendar.getSelectedDates();

                for (int i = 0; i < mMultiSelectedDateaList.size(); i++) {
                    String mSelectedDate = convertCalenderTimeToFormat(mMultiSelectedDateaList.get(i).toString());
                    Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                    mSelectedArrayList.add(mSelectedDate);
                }

                Log.e(TAG, "**Selected Dates List Size**" + mSelectedArrayList.size());

                //convert arraylist to string
                OccasionDate = TextUtils.join(",", mSelectedArrayList);
                Attire4hirePrefrences.writeString(getActivity(), ConstantData.OCCASION_DATE, OccasionDate);

                if (Size.equals("") && Condition.equals("") && Colour.equals("") && Brand.equals("") && Category.equals("")
                        && Type.equals("") && Occasion.equals("") && OpenForSale.equals("")
                        && OccasionDate.equals("") && Location.equals("")
                        && PriceRange_start.equals("") && PriceRange_end.equals("")) {

                    ClearFilterValues();

                    if (mItemProductModel != null) {
                        mItemProductModel.clear();
                    }

                    if (tempArrayList != null)
                        tempArrayList.clear();

                    if (mItemPerformersProductModel != null)
                        mItemPerformersProductModel.clear();

                    if (isNetworkAvailable(getActivity())) {
                        page_no = 1;
                        strPerformersType = "0";
                        getList(page_no, strItemName);
                    } else {
                        showToast(getActivity(), getString(R.string.internet_connection_error));
                    }

                } else {
                    if (tempArrayList != null)
                        tempArrayList.clear();

                    if (mItemProductModel != null) {
                        mItemProductModel.clear();
                    }

                    if (mItemPerformersProductModel != null)
                        mItemPerformersProductModel.clear();

                    filter_page_num = 1;

                    if (filtersList != null) {
                        filtersList.clear();
                        filtersList.add("Filters");
                        filtersList.add("Occasion Date");
                        filtersList.add("Performers' Corner");

                        mRVAdapter.notifyDataSetChanged();
                    }

                    if (isNetworkAvailable(getActivity())) {
                        GetItemByChoice(filter_page_num);
                    } else {
                        showToast(getActivity(), getString(R.string.internet_connection_error));
                    }
                }
                alertDialog.dismiss();
            }
        });

        nextClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backClick.setVisibility(View.VISIBLE);
                c.add(Calendar.MONTH, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                calendar.scrollToDate(resultdate);
            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                c.add(Calendar.MONTH, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                String newDtString = sdf.format(resultdate);
                calendar.scrollToDate(resultdate);
                Log.e("TAG", "mon::" + resultdate);
                if (resultdate.getMonth() < 1) {
                    backClick.setVisibility(View.GONE);
                }
            }
        });

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE);

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                .withSelectedDates(mMultiSelectedDateaList);

        //for showing selected dates
        if (!Attire4hirePrefrences.readString(getActivity(), ConstantData.OCCASION_DATE, "").equals("")) {
            OccasionDate = Attire4hirePrefrences.readString(getActivity(), ConstantData.OCCASION_DATE, "");
            //convert string to arraylist
            String[] elements = OccasionDate.split(",");
            List<String> fixedLenghtList = Arrays.asList(elements);
            if (mSelectedArrayList != null) {
                mSelectedArrayList.clear();
            }
            mSelectedArrayList = new ArrayList<String>(fixedLenghtList);
        }

        if (OccasionDate != null) {

            mSelectedDateList = new ArrayList<>();
            try {
                JSONArray mJsonArray = new JSONArray(mSelectedArrayList);
                for (int i = 0; i < mJsonArray.length(); i++) {
                    String strDate = mJsonArray.getString(i);
                    Log.e(TAG, "***Single Date**" + strDate);
                    Date mDate = null;
                    try {
                        mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                        mSelectedDateList.add(mDate);

                        String mSelectedDate = convertCalenderTimeToFormat(mSelectedDateList.get(i).toString());
                        Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                        mSelectedArrayList.add(mSelectedDate);

                        Log.e(TAG, "****Converted Dates***" + mSelectedArrayList.size());

                        calendar.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withSelectedDates(mSelectedDateList);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "***Converted Dates**" + mSelectedDateList.size());
                }
            } catch (Exception e) {
                e.toString();
            }
        }
        alertDialog.show();
    }

    /* method to set all filters values at start */
    private void setFiltersList() {
        if (!Size.equals("")) {
            if (!filtersList.contains("Size")) {
                filtersList.add("Size");
            }
        }
        if (!Condition.equals("")) {
            if (!filtersList.contains("Condition")) {
                filtersList.add("Condition");
            }
        }
        if (!Colour.equals("")) {
            if (!filtersList.contains("Colour")) {
                filtersList.add("Colour");
            }
        }
        if (!Brand.equals("")) {
            if (!filtersList.contains("Brand")) {
                filtersList.add("Brand");
            }
        }
        if (!Category.equals("")) {
            if (!filtersList.contains("Category")) {
                filtersList.add("Category");
            }
        }
        if (!Type.equals("")) {
            if (!filtersList.contains("Product Type")) {
                filtersList.add("Product Type");
            }
        }
        if (!Occasion.equals("")) {
            if (!filtersList.contains("Occasion")) {
                filtersList.add("Occasion");
            }
        }
        if (!OpenForSale.equals("") && !OpenForSale.equals("0")) {
            if (!filtersList.contains("Open For Sale")) {
                filtersList.add("Open For Sale");
            }
        }
        if (!OccasionDate.equals("")) {
            if (!filtersList.contains("Occasion Date")) {
                filtersList.add("Occasion Date");
            }
        }
        if (!Location.equals("")) {
            if (!filtersList.contains("Search by my Location")) {
                filtersList.add("Search by my Location");
            }
        }
        if (!PriceRange_start.equals("") && !PriceRange_end.equals("")) {
            if (!filtersList.contains("Price Range")) {
                filtersList.add("Price Range");
            }
        }
        mRVAdapter.notifyDataSetChanged();
    }

    /* set products adapter */
    private void setFilteredProductsAdapter() {
        if (dataRV != null) {

            LinearLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 2);

            dataRV.setLayoutManager(linearLayoutManager);
            mfilteredItemsAdapter = new FilteredItemsAdapter(getActivity(), mItemProductModel, favouriteCallBack, mFilterInterfaceData);
            dataRV.setAdapter(mfilteredItemsAdapter);
            dataRV.setNestedScrollingEnabled(false);
        }
    }

    /* execute filter products api */
    private void GetItemByChoice(int filter_page_num) {
        recentsearchET.setText("");

        if (filter_page_num == 1) {
            showProgressDialog(getActivity());
        } else if (filter_page_num > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (!PriceRange_start.equals("") && !PriceRange_end.equals("")) {
            Price_Range = PriceRange_start + "," + PriceRange_end;
        }

        String mApiUrl = ConstantData.FILTER_PRODUCT_BY_CHOICE;

        JSONObject params = new JSONObject();
        JSONObject params1 = new JSONObject();

        try {
            params1.put("category", Category);
            params1.put("price_range", Price_Range);
            params1.put("occation_date", OccasionDate);
            params1.put("condition", Condition);
            params1.put("brand", Brand);
            params1.put("size", Size);
            params1.put("product_type", Type);
            params1.put("open_for_sale", OpenForSale);
            params1.put("occasion", Occasion);
            params1.put("location", Location);
            params1.put("color", Colour);

            params.put("page_no", String.valueOf(filter_page_num));
            params.put("user_id", getUserID());
            params.put("filter_choice", params1);

            Log.e(TAG, "**ERROR**" + params.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "**RESPONSE**" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                parseJsonFilterResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonFilterResponse(JSONObject response) {
        dismissProgressDialog();
        tempArrayList.clear();
        try {

            if (response.getString("status").equals("1")) {

                strLastFilterPage = response.getString("last_page");

                JSONArray mJsonArray = response.getJSONArray("data");

                Log.e(TAG, "**Data Array Size**" + mJsonArray.length());

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));

                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("image_id"))
                        mModel.setImage_id(mDataObject.getString("image_id"));

                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));

                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));

                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));

                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));

                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));

                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));

                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));

                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));

                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));

                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));

                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));

                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));

                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));

                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));

                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));

                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));

                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));

                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));

                    if (!mDataObject.isNull("favourite"))
                        mModel.setFavourite(mDataObject.getString("favourite"));

                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));

                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));

                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));

                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));

                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));

                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));

                    if (!mDataObject.getString("user_detail").equals("")) {

                        JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                        if (!mDataObject_UserDetails.isNull("id"))
                            mModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                        if (!mDataObject_UserDetails.isNull("first_name"))
                            mModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                        if (!mDataObject_UserDetails.isNull("last_name"))
                            mModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                        if (!mDataObject_UserDetails.isNull("email"))
                            mModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                        if (!mDataObject_UserDetails.isNull("gender"))
                            mModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                        if (!mDataObject_UserDetails.isNull("city_name"))
                            mModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                        if (!mDataObject_UserDetails.isNull("phone_no"))
                            mModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                        if (!mDataObject_UserDetails.isNull("profile_pic"))
                            mModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                    }

                    if (filter_page_num == 1) {
                        mItemProductModel.add(mModel);
                    } else if (filter_page_num > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (filter_page_num == 1) {

                    /*
                     * Set Adapter
                     * */
                    setFilteredProductsAdapter();

                } else {

                    mfilteredItemsAdapter.notifyDataSetChanged();
                }

            } else {
                setFilteredProductsAdapter();
                if (getActivity() != null) {
                    showAlertDialog(getActivity(), getString(R.string.no_search_items));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    /* clear all filter values */
    private void ClearFilterValues() {
        if (mSelectedArrayList != null) {
            mSelectedArrayList.clear();
        }
        if (mMultiSelectedDateaList != null) {
            mMultiSelectedDateaList.clear();
        }
        OccasionDate = "";
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.SIZE, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.CONDITION, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.COLOUR, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.BRAND, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.ITEM_CATEGORY, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.PRODUCT_TYPE, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.OCCASION_DATE, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.PRICE_RANGE_START, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.PRICE_RANGE_END, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.LOCATION, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.OCCASION, "");
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.OPEN_FOR_SALE, "");
    }

    @Override
    public void onResume() {
        super.onResume();

        /* make isSwipeRefresh false */
        isSwipeRefresh = false;

        /* get all filtered values */
        Size = Attire4hirePrefrences.readString(getActivity(), ConstantData.SIZE, "");
        Condition = Attire4hirePrefrences.readString(getActivity(), ConstantData.CONDITION, "");
        Colour = Attire4hirePrefrences.readString(getActivity(), ConstantData.COLOUR, "");
        Brand = Attire4hirePrefrences.readString(getActivity(), ConstantData.BRAND, "");
        Category = Attire4hirePrefrences.readString(getActivity(), ConstantData.ITEM_CATEGORY, "");
        Type = Attire4hirePrefrences.readString(getActivity(), ConstantData.PRODUCT_TYPE, "");
        Occasion = Attire4hirePrefrences.readString(getActivity(), ConstantData.OCCASION, "");
        OpenForSale = Attire4hirePrefrences.readString(getActivity(), ConstantData.OPEN_FOR_SALE, "");
        OccasionDate = Attire4hirePrefrences.readString(getActivity(), ConstantData.OCCASION_DATE, "");
        Location = Attire4hirePrefrences.readString(getActivity(), ConstantData.LOCATION, "");
        PriceRange_start = Attire4hirePrefrences.readString(getActivity(), ConstantData.PRICE_RANGE_START, "");
        PriceRange_end = Attire4hirePrefrences.readString(getActivity(), ConstantData.PRICE_RANGE_END, "");

        /* get searched item name */
        strItemName = Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.SEARCH_ITEM, "");

        if (!OpenForSale.equals("") && OpenForSale.equals("0")) {
            OpenForSale = "";
        }

        if (!Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.SEARCH_ITEM, "").equals("")) {
            Iv_cancel.setVisibility(View.VISIBLE);
            recentsearchET.setText(strItemName);
        } else {
            Iv_cancel.setVisibility(View.GONE);
            recentsearchET.setText(strItemName);
        }

        if (tempArrayList != null)
            tempArrayList.clear();

        if (mItemProductModel != null)
            mItemProductModel.clear();

        if (mItemPerformersProductModel != null)
            mItemPerformersProductModel.clear();

        /* SEARCH_VALUE == Apply when coming from filter screen and clicked on Apply button */
        if (Attire4hirePrefrences.readString(getActivity(), ConstantData.SEARCH_VALUE, "").equals("Apply")) {

            setFiltersList();

            Attire4hirePrefrences.writeString(getActivity(), ConstantData.DATE_VALUE, "search_filter");

            if (Size.equals("") && Condition.equals("") && Colour.equals("") && Brand.equals("") && Category.equals("")
                    && Type.equals("") && Occasion.equals("") && OpenForSale.equals("")
                    && OccasionDate.equals("") && Location.equals("")
                    && PriceRange_start.equals("") && PriceRange_end.equals("")) {

                ClearFilterValues();

                if (isNetworkAvailable(getActivity())) {
                    page_no = 1;
                    strPerformersType = "0";
                    getList(page_no, strItemName);
                } else {
                    showToast(getActivity(), getString(R.string.internet_connection_error));
                }

            } else {
                if (isNetworkAvailable(getActivity())) {
                    filter_page_num = 1;
                    GetItemByChoice(filter_page_num);
                } else {
                    showToast(getActivity(), getString(R.string.internet_connection_error));
                }
            }

        } else if (Attire4hirePrefrences.readString(getActivity(), ConstantData.SEARCH_VALUE, "").equals("location")) {

            setFiltersList();

            if (Size.equals("") && Condition.equals("") && Colour.equals("") && Brand.equals("") && Category.equals("")
                    && Type.equals("") && Occasion.equals("") && OpenForSale.equals("")
                    && OccasionDate.equals("") && Location.equals("")
                    && PriceRange_start.equals("") && PriceRange_end.equals("")) {

                ClearFilterValues();

                if (isNetworkAvailable(getActivity())) {
                    page_no = 1;
                    strPerformersType = "0";
                    getList(page_no, strItemName);
                } else {
                    showToast(getActivity(), getString(R.string.internet_connection_error));
                }

            } else {
                if (isNetworkAvailable(getActivity())) {
                    filter_page_num = 1;
                    GetItemByChoice(filter_page_num);
                } else {
                    showToast(getActivity(), getString(R.string.internet_connection_error));
                }
            }

        } else {

            ClearFilterValues();

            if (isNetworkAvailable(getActivity())) {
                page_no = 1;

                if (tempArrayList != null)
                    tempArrayList.clear();

                if (mItemProductModel != null)
                    mItemProductModel.clear();

                if (mItemPerformersProductModel != null)
                    mItemPerformersProductModel.clear();

                if (strPerformersType.equals("1")) {
                    getPerformersCornerList(page_no, strItemName);
                } else {
                    getList(page_no, strItemName);
                }

            } else {
                showToast(getActivity(), getString(R.string.internet_connection_error));
            }
        }

        Attire4hirePrefrences.writeString(getActivity(), ConstantData.LOCATION_VALUE, "location");
    }

    @Override
    public void mFilterPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            if (strLastPage.equals("FALSE")) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            ++filter_page_num;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastFilterPage.equals("FALSE")) {

                        GetItemByChoice(filter_page_num);

                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog.isShowing()) {
            dismissProgressDialog();
        }
    }

    /* API to get performers corner products list data */
    private void getPerformersCornerList(final int page_no, String name) {

        if (progressDialog != null && progressDialog.isShowing()) {
            dismissProgressDialog();
        }

        /* clear all filter values and notify adapter when get all products api is being used */
        ClearFilterValues();
        Attire4hirePrefrences.writeString(getActivity(), ConstantData.DATE_VALUE, "");
        if (filtersList != null) {
            filtersList.clear();
            filtersList.add("Filters");
            filtersList.add("Occasion Date");
            filtersList.add("Performers' Corner");

            mRVAdapter.notifyDataSetChanged();
        }

        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                showProgressDialog(getActivity());
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        Map<String, String> params = new HashMap();
        params.put("name", name);
        params.put("page_no", String.valueOf(page_no));
        params.put("per_page", "10");
        params.put("user_id", getUserID());
        params.put("perform", "1");

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.GET_ALL_PRODUCTS_AND_SEARCH, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                parsePerformersCornerJsonResponse(response);

                filtersList.set(2, "Back to Home");
                mRVAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parsePerformersCornerJsonResponse(JSONObject response) {
        tempArrayList.clear();
        try {

            if (response.getString("status").equals("1")) {
                strLastPage = response.getString("last_page");

                JSONArray mJsonArray = response.getJSONArray("data");

                Log.e(TAG, "**Data Array Size**" + mJsonArray.length());

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));

                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("image_id"))
                        mModel.setImage_id(mDataObject.getString("image_id"));

                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));

                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));

                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));

                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));

                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));

                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));

                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));

                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));

                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));

                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));

                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));

                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));

                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));

                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));

                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));

                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));

                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));

                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));

                    if (!mDataObject.isNull("favourite"))
                        mModel.setFavourite(mDataObject.getString("favourite"));

                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));

                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));

                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));

                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));

                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));

                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));

                    if (!mDataObject.getString("user_detail").equals("")) {

                        JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                        if (!mDataObject_UserDetails.isNull("id"))
                            mModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                        if (!mDataObject_UserDetails.isNull("first_name"))
                            mModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                        if (!mDataObject_UserDetails.isNull("last_name"))
                            mModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                        if (!mDataObject_UserDetails.isNull("email"))
                            mModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                        if (!mDataObject_UserDetails.isNull("gender"))
                            mModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                        if (!mDataObject_UserDetails.isNull("city_name"))
                            mModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                        if (!mDataObject_UserDetails.isNull("phone_no"))
                            mModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                        if (!mDataObject_UserDetails.isNull("profile_pic"))
                            mModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                    }

                    if (page_no == 1) {
                        mItemPerformersProductModel.add(mModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemPerformersProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {

                    /*
                     * Set Adapter
                     * */
                    setPerformersCornerProductsAdapter(mItemPerformersProductModel);

                } else {

                    mPerformersCornerAdapter.notifyDataSetChanged();
                }

            } else if (response.getString("status").equals("3")) {
                LogOut();
            } else if (response.getString("status").equals("0")) {
                setPerformersCornerProductsAdapter(mItemPerformersProductModel);
                if (strItemName.equals("")) {
                    showAlertDialog(getActivity(), "No items are available");
                } else {
                    setPerformersCornerProductsAdapter(mItemPerformersProductModel);
                    showAlertDialog(getActivity(), getString(R.string.no_search_items));
                }
            } else {
                showAlertDialog(getActivity(), response.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationPerfInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            if (strLastPage.equals("FALSE")) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {

                        if (strPerformersType.equals("1")) {
                            getPerformersCornerList(page_no, strItemName);
                        } else {
                            getList(page_no, strItemName);
                        }
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }
}