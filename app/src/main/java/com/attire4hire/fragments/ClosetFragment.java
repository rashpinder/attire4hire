package com.attire4hire.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.activity.AccountSetupActivity;
import com.attire4hire.activity.AddItemActivity;
import com.attire4hire.activity.ConfirmedBookingActivity;
import com.attire4hire.activity.Current_BookingsActivity;
import com.attire4hire.activity.DraftActivity;
import com.attire4hire.activity.ListingActivity;
import com.attire4hire.activity.MyInquiriesActivity;
import com.attire4hire.activity.MyRequestsActivity;
import com.attire4hire.activity.OrdersHistoryActivity;
import com.attire4hire.activity.SocialMediaLoginActivity;
import com.attire4hire.adapters.CustomExpandableListAdapter;
import com.attire4hire.interfaces.ExpandableListCallBack;
import com.attire4hire.model.ExpandableListData;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClosetFragment extends BaseFragment {
    /*
     * Initalize Unbinder
     * */
    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.notificationRL)
    RelativeLayout notificationRL;
    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;

    /*
     * Initlaize Objects...
     * */
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    String Total_Count = "";

    /*
     * Initalize Default Constructor
     * For Fragment
     * */
    public ClosetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_closet, container, false);
        //Set ButterKnife
        unbinder = ButterKnife.bind(this, view);
        //Set Expandable List

        setExpandableList();

        return view;
    }

    private void setExpandableList() {
        expandableListView.setGroupIndicator(null);
        expandableListView.setChildIndicator(null);
        expandableListDetail = ExpandableListData.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail, expandableListCallBack);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (groupPosition == 3) {
                    Intent intent = new Intent(getActivity(), OrdersHistoryActivity.class);
                    getActivity().startActivity(intent);
                }

                return false;
            }
        });

//        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//
//                if(groupPosition == 3){
//                    Intent intent = new Intent(getActivity(), OrdersHistoryActivity.class);
//                    getActivity().startActivity(intent);}
//            }
//        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                return false;
            }
        });
    }

    ExpandableListCallBack expandableListCallBack = new ExpandableListCallBack() {

        @Override
        public void ExpandableCall(int listPosition, int expandedListPosition, View view) {
            if (listPosition == 1 && expandedListPosition == 0) {
                Intent intent = new Intent(getActivity(), MyRequestsActivity.class);
                intent.putExtra(ConstantData.REDIRECION_TYPE, ConstantData.CLOSET);
                startActivity(intent);
            } else if (listPosition == 0 && expandedListPosition == 0) {
                Intent intent = new Intent(getActivity(), ListingActivity.class);
                startActivity(intent);

            } else if (listPosition == 0 && expandedListPosition == 1) {

                if (getIdVerification().equals("1") && getPhoneVerification().equals("1") && getEmailVerification().equals("1") && !getStripeAccountID().equals("")) {

                    Intent intent = new Intent(getActivity(), AddItemActivity.class);
                    startActivity(intent);
                } else {
                    if (getIdVerification().equals("1") && getEmailVerification().equals("1") && getPhoneVerification().equals("1") && getStripeAccountID().equals("")) {
                        showVerificationAlertDialog(getActivity(), getString(R.string.verify_stripe));
                    } else
                        showVerificationAlertDialog(getActivity(), getString(R.string.verify_add_item));
                }

            } else if (listPosition == 2 && expandedListPosition == 1) {
                Intent intent = new Intent(getActivity(), ConfirmedBookingActivity.class);
                startActivity(intent);
            } else if (listPosition == 0 && expandedListPosition == 2) {
                Intent intent = new Intent(getActivity(), DraftActivity.class);
                startActivity(intent);
            } else if (listPosition == 2 && expandedListPosition == 0) {
                Intent intent = new Intent(getActivity(), MyInquiriesActivity.class);
                intent.putExtra(ConstantData.REDIRECION_TYPE, ConstantData.CLOSET);
                startActivity(intent);
            } else if (listPosition == 1 && expandedListPosition == 1) {
                Intent intent = new Intent(getActivity(), Current_BookingsActivity.class);
                startActivity(intent);
            }
        }
    };

    @OnClick(R.id.notificationRL)
    public void onViewClicked() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    private void executeProfileApi() {
        showProgressDialog(getActivity());
        String mApiUrl = ConstantData.GET_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("viewed_user", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        startActivity(new Intent(getActivity(), SocialMediaLoginActivity.class));
                    } else {
                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("total_count")) {
                Total_Count = response.getString("total_count");

                if (Total_Count != null && !Total_Count.equals("")) {
                    Intent intent = new Intent("changeCount");
                    intent.putExtra("CountStatus", "editTotalCount");
                    intent.putExtra("total_badge", Total_Count);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                }
            }

            if (!response.isNull("unread_message_count") && !response.getString("unread_message_count").equals("") && !response.getString("unread_message_count").equals("0")) {
                String msg_count = response.getString("unread_message_count");

                Intent intent = new Intent("changeCount");
                intent.putExtra("CountStatus", "editCount");
                intent.putExtra("chat_badge", msg_count);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            }

            if (!response.isNull("user_details")) {
                JSONObject mDataObj = response.getJSONObject("user_details");
                Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.REQUEST_COUNT, response.getString("inquiry_count"));
                Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.INQUIRY_COUNT, response.getString("request_count"));
                Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.CURRENT_BOOKING_COUNT, response.getString("current_booking_count"));
                Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.MY_BOOKING_COUNT, response.getString("my_booking_count"));
                Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.TOTAL_COUNT, response.getString("total_count"));

                if (!mDataObj.isNull("id"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));

                if (!mDataObj.isNull("first_name"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));

                if (!mDataObj.isNull("last_name"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));

                if (!mDataObj.isNull("email"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));

                if (!mDataObj.isNull("gender"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));

                if (!mDataObj.isNull("phone_no"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));

                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals(""))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));

                if (!mDataObj.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObj.getString("email_verification"));

                if (!mDataObj.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PHONE_VERIFICATION, mDataObj.getString("phone_verification"));

                if (!mDataObj.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.ID_VERIFICATION, mDataObj.getString("id_verification"));
            }

            if (!response.isNull("product_datils")) {
                JSONArray mDataArray = response.getJSONArray("product_datils");
                for (int i = 0; i < mDataArray.length(); i++) {
                    JSONObject mDataObject = mDataArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));

                    mItemProductModel.add(mModel);
                }
                setExpandableList();
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mItemProductModel != null) {
            mItemProductModel.clear();
            executeProfileApi();
        }
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AccountSetupActivity.class);
                intent.putExtra(ConstantData.STRIPE, ConstantData.SHOW_STRIPE);
                startActivity(intent);

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}