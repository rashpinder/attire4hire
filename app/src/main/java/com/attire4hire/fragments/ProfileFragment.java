package com.attire4hire.fragments;


import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.activity.LenderReviewsProfileActivity;
import com.attire4hire.activity.RenterReviewProfileActivity;
import com.attire4hire.activity.ReportUserActivity;
import com.attire4hire.activity.SettingsActivity;
import com.attire4hire.adapters.ProfileHorizontalRVAdapter;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {

    /*
     * Initalize TAG
     * */
    String TAG = ProfileFragment.this.getClass().getSimpleName();

    /*
     * WidgetstxtLocationTV
     * */
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.profileRL)
    RelativeLayout profileRL;
    @BindView(R.id.imgSettingIV)
    RelativeLayout imgSettingIV;
    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.txtUserNameTV)
    TextviewSemiBold txtUserNameTV;
    @BindView(R.id.txtLocationTV)
    TextviewRegular txtLocationTV;
    @BindView(R.id.txtItemCountsTV)
    TextviewRegular txtItemCountsTV;
    @BindView(R.id.imgEmailVerifiedIV)
    ImageView imgEmailVerifiedIV;
    @BindView(R.id.imgPhoneNumberVerifiedIV)
    ImageView imgPhoneNumberVerifiedIV;
    @BindView(R.id.imgIdVerifiedIV)
    ImageView imgIdVerifiedIV;
    @BindView(R.id.txtLenderReviewsTV)
    TextviewSemiBold txtLenderReviewsTV;
    @BindView(R.id.txtRenterReviewsTV)
    TextviewSemiBold txtRenterReviewsTV;
    @BindView(R.id.txtReportTV)
    TextView txtReportTV;
    @BindView(R.id.ratingbarRB)
    RatingBar ratingbarRB;

    String mBase64Image = "";
    ItemProductModel mModel;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    /*
     * Initialize Objects...
     * */

    Unbinder unbinder;
    ProfileHorizontalRVAdapter mProfileHorizontalRVAdapter;
    ArrayList<ItemProductModel> mArrayList = new ArrayList<>();

    /*
     * Default Constructor...
     * */
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        Attire4hirePrefrences.writeString(getActivity(), "profile", "");
        CheckVerifications();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!Attire4hirePrefrences.readString(getActivity(), "profile", "").equals("picture")) {
            if (mArrayList != null) {
                mArrayList.clear();
                /* Get Profile Data */
                getProfileData();
            }
        }
    }

    private void CheckVerifications() {
        if (getEmailVerification().equals("1")) {
            imgEmailVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgEmailVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (getPhoneVerification().equals("1")) {
            imgPhoneNumberVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgPhoneNumberVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (getIdVerification().equals("1")) {
            imgIdVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgIdVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }
    }

    private void getProfileData() {
        if (isNetworkAvailable(getActivity()))
            executeProfileApi();
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @OnClick({R.id.imgSettingIV, R.id.imgEmailVerifiedIV, R.id.imgPhoneNumberVerifiedIV, R.id.txtLenderReviewsTV, R.id.txtRenterReviewsTV, R.id.txtReportTV, R.id.imgProfilePicIV,R.id.profileRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSettingIV:
                performSettingClick();
                break;
            case R.id.imgEmailVerifiedIV:
                performEmailVerify();
                break;
            case R.id.imgPhoneNumberVerifiedIV:
                perfromPhoneVerifyClick();
                break;
            case R.id.txtLenderReviewsTV:
                performLenderReviewsClick();
                break;
            case R.id.txtRenterReviewsTV:
                performRenterReviewClick();
                break;
            case R.id.txtReportTV:
                performReportClick();
                break;
            case R.id.imgProfilePicIV:
                perfromChangeProfilePicClick();
                break;
            case R.id.profileRL:
//                perfromChangeProfilePicClick();
                break;
        }
    }

    private void performReportClick() {
        Intent intent = new Intent(getActivity(), ReportUserActivity.class);
        startActivity(intent);
    }

    private void performSettingClick() {
        Attire4hirePrefrences.writeString(getActivity(), "profile", "");
        Intent intent1 = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent1);
    }

    private void performEmailVerify() {

    }

    private void perfromPhoneVerifyClick() {

    }

    private void performLenderReviewsClick() {
        Intent in = new Intent(getActivity(), LenderReviewsProfileActivity.class);
        startActivity(in);
    }

    private void performRenterReviewClick() {
        Intent i = new Intent(getActivity(), RenterReviewProfileActivity.class);
        startActivity(i);
    }

    private void executeProfileApi() {
        showProgressDialog(getActivity());
        String mApiUrl = ConstantData.GET_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("viewed_user", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
                        showToast(getActivity(), response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("user_details")) {
                JSONObject mDataObj = response.getJSONObject("user_details");
                if (!mDataObj.isNull("id"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));

                if (!mDataObj.isNull("first_name"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));

                if (!mDataObj.isNull("last_name"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));

                if (!mDataObj.isNull("email"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));

                if (!mDataObj.isNull("gender"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));

                if (!mDataObj.isNull("city_name"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.CITY, mDataObj.getString("city_name"));

                if (!mDataObj.isNull("phone_no"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));

                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals(""))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));

                if (!mDataObj.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObj.getString("email_verification"));

                if (!mDataObj.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PHONE_VERIFICATION, mDataObj.getString("phone_verification"));

                if (!mDataObj.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.ID_VERIFICATION, mDataObj.getString("id_verification"));

                if (!mDataObj.isNull("paypal_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PAYPAL_VERIFICATION, mDataObj.getString("paypal_verification"));

                if (!mDataObj.isNull("stripe_account_verification"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.STRIPE_VERIFICATION, mDataObj.getString("stripe_account_verification"));

                if (!mDataObj.isNull("stripe_account_id"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.STRIPE_ACCOUNT_ID, mDataObj.getString("stripe_account_id"));

                if (!mDataObj.isNull("rating"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.USER_RATINGS, mDataObj.getString("rating"));

                if (!mDataObj.isNull("google_id"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.GOOGLE_ID, mDataObj.getString("google_id"));

                if (!mDataObj.isNull("rating"))
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.FACEBOOK_ID, mDataObj.getString("facebook_id"));

            }

            if (!response.isNull("unread_message_count") && !response.getString("unread_message_count").equals("") && !response.getString("unread_message_count").equals("0")) {
                String msg_count = response.getString("unread_message_count");

                Intent intent = new Intent("changeCount");
                intent.putExtra("CountStatus", "editCount");
                intent.putExtra("chat_badge", msg_count);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            }

            if (!response.isNull("product_datils")) {
                JSONArray mDataArray = response.getJSONArray("product_datils");
                for (int i = 0; i < mDataArray.length(); i++) {
                    JSONObject mDataObject = mDataArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    mArrayList.add(mModel);
                }
            }

            //Set Data ON Widgets
            setDataOnWidget();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void executeUpdateProfileApi() {

        if (mBase64Image.equals("")) {
            mBase64Image = getUserProfilePicture();
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (RuntimeException ex) {
                Log.e(TAG, ex.toString());
            }
        }

        showProgressDialog(getActivity());
        String mApiUrl = ConstantData.EDIT_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();

        try {
            params.put("user_id", getUserID());
            params.put("first_name", getUserFirstName());
            params.put("last_name", getUserLastName());
            params.put("gender", getUserGender());
            params.put("city_name", getUserCity());
            params.put("phone", getUserPhoneNo());
            params.put("profile_image", mBase64Image);
        } catch (Exception e) {
            e.toString();
        }

        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        parseResponseProfile(response);

                    } else {
                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponseProfile(JSONObject response) {
        try {
            if (!response.isNull("user_detail")) {
                JSONObject mDataObj = response.getJSONObject("user_detail");

                mModel = new ItemProductModel();

                if (!mDataObj.isNull("id")) {
                    mModel.setUser_id(mDataObj.getString("id"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));
                }
                if (!mDataObj.isNull("first_name")) {
                    mModel.setUser_First_Name(mDataObj.getString("first_name"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));
                }
                if (!mDataObj.isNull("last_name")) {
                    mModel.setUser_Last_Name(mDataObj.getString("last_name"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));
                }
                if (!mDataObj.isNull("email")) {
                    mModel.setUser_Email(mDataObj.getString("email"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));
                }
                if (!mDataObj.isNull("gender")) {
                    mModel.setGender(mDataObj.getString("gender"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));
                }
                if (!mDataObj.isNull("city_name")) {
                    mModel.setUser_City(mDataObj.getString("city_name"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.CITY, mDataObj.getString("city_name"));
                }
                if (!mDataObj.isNull("phone_no")) {
                    mModel.setUser_PhoneNo(mDataObj.getString("phone_no"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));
                }
                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals("")) {
                    mModel.setUser_ProfilePic(mDataObj.getString("profile_pic"));
                    Attire4hirePrefrences.writeString(getActivity(), Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));
                }
                mArrayList.add(mModel);
            }

            //Set Data ON Widgets
            GetData();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void GetData() {

//        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
//            Glide.with(getActivity()).load(getUserProfilePicture())
//                    .placeholder(R.drawable.ic_pp_ph)
//                    .error(R.drawable.ic_pp_ph)
//                    .into(imgProfilePicIV);
//        }

        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http") && getUserProfilePicture().contains("https")) {
            Intent intent = new Intent("changeImage");
            intent.putExtra("Status", "editImage");
            intent.putExtra("editImage", mModel.getUser_ProfilePic());
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }
    }

    private void setDataOnWidget() {
        ratingbarRB.setRating(Float.parseFloat(getUserRatings()));
        txtUserNameTV.setText(getUserFirstName() + " " + getUserLastName());
        txtItemCountsTV.setText("Listed Items: " + mArrayList.size());
        txtLocationTV.setText(getUserCity());

        if (getUserProfilePicture() != null && !getUserProfilePicture().equals("") && getUserProfilePicture().contains("http")) {
            Picasso.with(getActivity()).load(getUserProfilePicture())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicIV);
        }
        //Set User Items List
        setmAdapter();
    }

    private void setmAdapter() {
        mRecyclerViewRV.setNestedScrollingEnabled(false);
        mRecyclerViewRV.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mProfileHorizontalRVAdapter = new ProfileHorizontalRVAdapter(getActivity(), mArrayList);
        mRecyclerViewRV.setAdapter(mProfileHorizontalRVAdapter);
        mProfileHorizontalRVAdapter.notifyDataSetChanged();
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    /* Camera Gallery functionality
     * */
    public void perfromChangeProfilePicClick() {
        Attire4hirePrefrences.writeString(getActivity(), "profile", "picture");
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(getActivity(), writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(getActivity(), writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{
                writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCameraGalleryDialog();
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        showCameraGalleryAlertDialog(requireActivity());
//        CropImage.startPickImageActivity(getActivity());
//        ImagePicker.Companion.with(this)
//                .crop()                    //Crop image(Optional), Check Customization for more option
//                .compress(512)            //Final image size will be less than 1 MB(Optional)
//                .maxResultSize(512, 512)    //Final image resolution will be less than 1080 x 1080(Optional)
//                .start();
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /* dhaval2404 cropper */
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            //val fileUri = data?.data
            if (data != null) {

                Uri mFileUri = data.getData();
                Glide.with(getActivity()).load(mFileUri)
                        .placeholder(R.drawable.ic_pp_ph)
                        .error(R.drawable.ic_pp_ph)
                        .into(imgProfilePicIV);
                showImage(mFileUri);

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(getActivity(), ("Image Not Valid"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Task Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    imgProfilePicIV.setImageBitmap(selectedImage);
                    mBase64Image = encodeTobase64(selectedImage);
                    Attire4hirePrefrences.writeString(getActivity(), "profile", "picture");
                    executeUpdateProfileApi();
                    Log.e(TAG, "**Image Base 64**" + mBase64Image);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(getActivity(), "Cropping failed: " + result.getError());
            }
        }
    }

    private void showImage(Uri imageUri) {
        String file = getRealPathFromURI_API19(getActivity(), imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap selectedImage = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        if (decoded != null) {
//            imgProfilePicIV.setImageBitmap(selectedImage);
            mBase64Image = encodeTobase64(selectedImage);
            Attire4hirePrefrences.writeString(getActivity(), "profile", "picture");
            executeUpdateProfileApi();
        }
    }

    public void showCameraGalleryAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtCameraTV = alertDialog.findViewById(R.id.txtCameraTV);
        TextView txtGalleryTV = alertDialog.findViewById(R.id.txtGalleryTV);
        TextView btnCancelTV = alertDialog.findViewById(R.id.btnCancelTV);

        btnCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtCameraTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                takePhotoFromCamera();
            }
        });

        txtGalleryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                choosePhotoFromGallary();
            }
        });
        alertDialog.show();
    }

    private void choosePhotoFromGallary() {
        ImagePicker.Companion.with(this)
                .crop()
                .galleryOnly()//Crop image(Optional), Check Customization for more option
                .compress(512)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(512, 512)    //Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }

    private void takePhotoFromCamera() {
        ImagePicker.Companion.with(this)
                .crop()
                .cameraOnly()//Crop image(Optional), Check Customization for more option
                .compress(512)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(512, 512)    //Final image resolution will be less than 1080 x 1080(Optional)
                .start();
    }
}
