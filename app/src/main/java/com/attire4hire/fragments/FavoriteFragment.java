package com.attire4hire.fragments;


import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.FavoriteAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.interfaces.FavouriteCallBack;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends BaseFragment {
    String TAG = FavoriteFragment.this.getClass().getSimpleName();
    /*Unbinder*/
    Unbinder unbinder;

    /*Widgets*/
    @BindView(R.id.rv_fav)
    RecyclerView favRV;
    @BindView(R.id.tv_NoInquiries)
    TextviewBold tv_NoInquiries;

    FavoriteAdapter mFavAdapter;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    private void FavouritApi() {
        showProgressDialog(getActivity());
        Map<String, String> params = new HashMap();
        params.put("user_id", getUserID());
        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.GET_FAVOURITE, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "**RESPONSE**" + response);
                parseJsonResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(JSONObject response) {
        dismissProgressDialog();
        try {
            if (response.getString("status").equals("1")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                Log.e(TAG, "**Data Array Size**" + mJsonArray.length());

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));

                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("image_id"))
                        mModel.setImage_id(mDataObject.getString("image_id"));

                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));

                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));

                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));

                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));

                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));

                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));

                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));

                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));

                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));

                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));

                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));

                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));

                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));

                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));

                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));

                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));

                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));

                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));

                    if (!mDataObject.isNull("favourite"))
                        mModel.setFavourite(mDataObject.getString("favourite"));

                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));

                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));

                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));

                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));

                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));

                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));

                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    mItemProductModel.add(mModel);
                }

                /*
                 * Set Adapter
                 * */
                setFavouritessAdapter();

            } else if (response.getString("status").equals("3")) {
                LogOut();

            } else {
//                showToast(getActivity(), response.getString("message"));
            }

            if (mItemProductModel.size() > 0) {
                tv_NoInquiries.setVisibility(View.GONE);
                setFavouritessAdapter();
            } else {
                tv_NoInquiries.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    private void setFavouritessAdapter() {
        if (favRV != null) {
            favRV.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mFavAdapter = new FavoriteAdapter(getActivity(), mItemProductModel, favouriteCallBack);
            favRV.setAdapter(mFavAdapter);
            mFavAdapter.notifyDataSetChanged();
        }
    }

    FavouriteCallBack favouriteCallBack = new FavouriteCallBack() {
        @Override
        public void favouriteCall(int position, String FavouriteStatus) {
            Add_Favourite(FavouriteStatus, mItemProductModel.get(position).getId(), position);
        }
    };

    private void Add_Favourite(String FavouriteStatus, String ProductId, int position) {
        showProgressDialog(getActivity());
        Map<String, String> params = new HashMap();
        params.put("user_id", getUserID());
        params.put("product_id", ProductId);
        params.put("favourite", FavouriteStatus);

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.ADD_FAVOURITE, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                dismissProgressDialog();

                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        mItemProductModel.get(position).setFavourite(FavouriteStatus);
                        mItemProductModel.remove(position);

                        if (mItemProductModel.size() > 0) {
                            tv_NoInquiries.setVisibility(View.GONE);
                            mFavAdapter.notifyDataSetChanged();

                        } else {
                            tv_NoInquiries.setVisibility(View.VISIBLE);
                            mFavAdapter.notifyDataSetChanged();
                        }
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
//                        showToast(getActivity(), response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mItemProductModel != null) {
            mItemProductModel.clear();
            FavouritApi();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
