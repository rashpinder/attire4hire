package com.attire4hire.fragments;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.attire4hire.R;
import com.attire4hire.activity.AccountSetupActivity;
import com.attire4hire.activity.SocialMediaLoginActivity;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

public class BaseFragment extends Fragment {

    /*
     * Is User Login Or Not
     * */
    public String IsLogin() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.IS_LOGIN, "");
    }

    /*
     * Get User ID
     * */
    public String getUserID() {
        if (getActivity() != null) {

            return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.USER_ID, "");
        } else
            return null;
    }

    /*
     * Get User Name
     * */
    public String getUserName() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.FIRST_NAME, "") + " " + Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.LAST_NAME, "");
    }

    /*
     * Get User First Name
     * */
    public String getUserFirstName() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.FIRST_NAME, "");
    }

    /*
     * Get User Last Name
     * */
    public String getUserLastName() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.LAST_NAME, "");
    }

    /*
     * Get User Email
     * */
    public String getUserEmail() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.EMAIL, "");
    }

    /*
     * Get User Phone Number
     * */
    public String getUserPhoneNo() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.PHONE_NUMBER, "");
    }

    /*
     * Get User Phone Number
     * */
    public String getUserGender() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.GENDER, "");
    }

    /*
     * Get User City
     * */
    public String getUserCity() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.CITY, "");
    }

    /*
     * Get User Profile Pic
     * */
    public String getUserProfilePicture() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.PROFILE_PIC, "");
    }

    /*
     * Get User Current Latitude
     * */
    public String getCurrentLatitude() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.CURRENT_LOCATION_LATITUDE, "");
    }

    /*
     * Get User Current Latitude
     * */
    public String getCurrentLongitude() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }

    /*
     * Get User Email Verification
     * */
    public String getEmailVerification() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.EMAIL_VERIFICATION, "");
    }

    /*
     * Get User Phone Verification
     * */
    public String getPhoneVerification() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.PHONE_VERIFICATION, "");
    }

    /*
     * Get User Id Verification
     * */
    public String getIdVerification() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.ID_VERIFICATION, "");
    }

    /*
     * Get Request Count
     * */
    public String getRequestCount() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.REQUEST_COUNT, "");
    }

    /*
     * Get Inquiry Count
     * */
    public String getInquiryCount() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.INQUIRY_COUNT, "");
    }

    /*
     * Get Current Count
     * */
    public String getCurrentCount() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.CURRENT_BOOKING_COUNT, "");
    }

    /*
     * Get Confirmed Count
     * */
    public String getConfirmedCount() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.MY_BOOKING_COUNT, "");
    }

    /*
     * Get Total Count
     * */
    public String getTotalCount() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.TOTAL_COUNT, "");
    }

    /*
     * Get User Ratings
     * */
    public String getUserRatings() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.USER_RATINGS, "");
    }

    /*
     * Get PayPal Verification
     * */
    public String getPayPalVerification() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.PAYPAL_VERIFICATION, "");
    }

    /*
     * Get Stripe Verification
     * */
    public String getStripeVerification() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.STRIPE_VERIFICATION, "");
    }

    /*
     * Get Stripe account id
     * */
    public String getStripeAccountID() {
        return Attire4hirePrefrences.readString(getActivity(), Attire4hirePrefrences.STRIPE_ACCOUNT_ID, "");
    }

    Dialog progressDialog;

    /*
     *  Alert Dialog Finish
     * */

    String TAG = BaseFragment.this.getClass().getSimpleName();

    public static void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showToast(Activity mActivity, String strMessage) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), strMessage, Toast.LENGTH_SHORT).show();

        }
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (RuntimeException ex) {
                Log.e(TAG, ex.toString());
            }
        }
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     *
     *  Alert Dialog Logout
     * */
    public void showDialogYesNo(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_custom_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnOK = alertDialog.findViewById(R.id.okTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.cancelTV);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void showFinishAlertDialog(final Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessageTV);
        TextView txtOk = alertDialog.findViewById(R.id.okTV);
        // TextView txtCancel = alertDialog.findViewById(R.id.cancelTV);
        txtMessage.setText(strMessage);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });

        alertDialog.show();
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /*
     * Disconnect with facebook
     * */

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    /*
     * Google Logout
     * */
    public void googleLogOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        GoogleSignInClient mgoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        mgoogleSignInClient.signOut();
    }

    public void LogOut() {
        SharedPreferences preferences = Attire4hirePrefrences.getPreferences(Objects.requireNonNull(getApplicationContext()));
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        disconnectFromFacebook();

        googleLogOut();

        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(getApplicationContext(), SocialMediaLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    //convert Calender Time To Format
    public String convertCalenderTimeToFormat(String input) {//Fri Mar 20 00:00:00 GMT 2020
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("E MMM dd hh:mm:ss z yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
        String goal="";
        if(date!=null) {
            goal = outFormat.format(date);
        }
        return goal;
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String myString) {

        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase();

        return upperString;
    }
}
