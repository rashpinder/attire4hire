package com.attire4hire.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.activity.FinaliseYourRequestActivity;
import com.attire4hire.interfaces.DeleteConversationInterface;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.ChatAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements PaginationInquiriesInterface {
    String TAG = ChatFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    /*Widgets*/
//    @BindView(R.id.chat_rv)
    RecyclerView chatRV;

//    @BindView(R.id.tv_NoChat)
    TextviewBold tv_NoChat;

    //    @BindView(R.id.deleteChatIV)
    ImageView deleteChatIV;

    SwipeRefreshLayout swipeToRefresh;
    ProgressBar mProgressBar;

    ChatAdapter mAdapter;

    String strLastPage = "FALSE";
    int page_no = 1;
    boolean isSwipeRefresh = false;

    PaginationInquiriesInterface mInterfaceData;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> tempArrayList = new ArrayList<>();

    ArrayList<String> selectedList = new ArrayList<>();

    boolean isLoader = false;

    public ChatFragment() {
        // Required empty public constructor
    }

    DeleteConversationInterface deleteConversationInterface = new DeleteConversationInterface() {
        @Override
        public void mDeleteConversationInterface(int position, String roomId, ArrayList<String> SelectedItems) {
            if (SelectedItems.size() > 0) {
                selectedList = SelectedItems;
                deleteChatIV.setVisibility(View.VISIBLE);
            } else {
                deleteChatIV.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        unbinder = ButterKnife.bind(this, view);

        mProgressBar = view.findViewById(R.id.mProgressBar);
        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        deleteChatIV = view.findViewById(R.id.deleteChatIV);
        tv_NoChat = view.findViewById(R.id.tv_NoChat);
        chatRV = view.findViewById(R.id.chat_rv);

        mInterfaceData = this;

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mItemProductModel.clear();
                tempArrayList.clear();
                GetAllChatUsers(page_no);
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @OnClick({R.id.deleteChatIV})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.deleteChatIV:
                performDeleteChatClick();
                break;
        }
    }

    private void performDeleteChatClick() {
        if (isNetworkAvailable(getActivity()))
            showDialogDelete(getActivity());
        else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (selectedList != null) {
            selectedList.clear();
        }
        deleteChatIV.setVisibility(View.GONE);

        if (isNetworkAvailable(getActivity())) {
            if (mItemProductModel != null) {
                mItemProductModel.clear();
                page_no = 1;
                GetAllChatUsers(page_no);
            }

        } else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    private void GetAllChatUsers(final int page_no) {

        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                if (!isLoader) {
                    showProgressDialog(getActivity());
                }
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        String mApiUrl = ConstantData.GET_CHAT_USERS;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("page_no", page_no);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                if (getActivity() != null) {
                    deleteChatIV.setVisibility(View.GONE);
                }

                parseJsonResponse(response.toString());
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(String response) {
        tempArrayList.clear();
        dismissProgressDialog();
        try {

            JSONObject mJsonObject = new JSONObject(response);

            if (mJsonObject.getString("status").equals("1")) {

                JSONArray mJsonArray = mJsonObject.getJSONArray("all_users");

                String chat_badge = mJsonObject.getString("unread_message_count");

                if (chat_badge != null && !chat_badge.equals("")) {
                    Intent intent = new Intent("changeCount");
                    intent.putExtra("CountStatus", "editCount");
                    intent.putExtra("chat_badge", chat_badge);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                }

                strLastPage = mJsonObject.getString("last_page");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();

                    if (!mDataObject.isNull("room_no"))
                        mModel.setRoom_id(mDataObject.getString("room_no"));

                    if (!mDataObject.isNull("sender_id"))
                        mModel.setSender_id(mDataObject.getString("sender_id"));

                    if (!mDataObject.isNull("receiver_id"))
                        mModel.setReceiver_id(mDataObject.getString("receiver_id"));

                    if (!mDataObject.isNull("creation_date"))
                        mModel.setCreation_date(mDataObject.getString("creation_date"));

                    if (!mDataObject.isNull("username"))
                        mModel.setAnother_User_Name(mDataObject.getString("username"));

                    if (!mDataObject.isNull("message"))
                        mModel.setMessage(mDataObject.getString("message"));

                    if (!mDataObject.isNull("profile_pic"))
                        mModel.setAnother_ProfilePic(mDataObject.getString("profile_pic"));

                    if (!mDataObject.isNull("unread_count"))
                        mModel.setUnread_count(mDataObject.getString("unread_count"));

                    if (getActivity() != null) {
                        if (getUserID().equals(mDataObject.getString("sender_id"))) {
                            mModel.setAnother_User_Id(mDataObject.getString("receiver_id"));
                        } else {
                            mModel.setAnother_User_Id(mDataObject.getString("sender_id"));
                        }
                    }

                    if (page_no == 1) {
                        mItemProductModel.add(mModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {
                    /*
                     * Set Adapter
                     * */
                    setAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }

            } else {
                setAdapter();
            }

            if (mItemProductModel.size() > 0) {
                tv_NoChat.setVisibility(View.GONE);
                /*
                 * Set Adapter
                 * */
                setAdapter();
            } else {
                tv_NoChat.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationInquiriesInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            if (strLastPage.equals("FALSE")) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        GetAllChatUsers(page_no);
                    } else {
                        if (mProgressBar.isShown())
                            mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    private void setAdapter() {
        if (chatRV != null) {
            chatRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            mAdapter = new ChatAdapter(getActivity(), mItemProductModel, mInterfaceData, deleteConversationInterface);
            chatRV.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    //    execute delete conversation api
    private void executeDeleteConversation(List<String> list) {

        showProgressDialog(getActivity());
        isLoader = true;
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(list);

        try {
            jsonObject.put("user_id", getUserID());
            jsonObject.put("room_ids", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "**PARAM**" + jsonObject.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.DeleteChat, jsonObject, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        if (mItemProductModel != null) {
                            mItemProductModel.clear();
                        }

                        if (tempArrayList != null) {
                            tempArrayList.clear();
                        }

                        page_no = 1;
                        GetAllChatUsers(page_no);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public void showDialogDelete(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_chat);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        if (selectedList.size() == 1) {
            txtTitleTV.setText(getResources().getString(R.string.delete_chat));
            txtMessageTV.setText(getResources().getString(R.string.delete_msg_chat));
        } else {
            txtTitleTV.setText(getResources().getString(R.string.delete_chats));
            txtMessageTV.setText(getResources().getString(R.string.delete_msg_chats));
        }

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (isNetworkAvailable(mActivity))
                    executeDeleteConversation(selectedList);
                else
                    showToast(mActivity, getString(R.string.internet_connection_error));
            }
        });
        alertDialog.show();
    }
}
