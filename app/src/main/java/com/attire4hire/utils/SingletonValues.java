package com.attire4hire.utils;

public class SingletonValues {

    String email, stripeVerification = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStripeVerification() {
        return stripeVerification;
    }

    public void setStripeVerification(String stripeVerification) {
        this.stripeVerification = stripeVerification;
    }
}
