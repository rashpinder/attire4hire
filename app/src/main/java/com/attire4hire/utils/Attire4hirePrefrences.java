package com.attire4hire.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class Attire4hirePrefrences {
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    public static final String IS_LOGIN = "is_login";
    public static final String LOGIN_TYPE = "login_type";
    public static final String Phone_TYPE = "phone_type";
    public static final String USER_ID = "userId";
    public static final String ANOTHER_USER_ID = "anotheruserId";
    public static final String GOOGLE_ID = "google_id";
    public static final String FACEBOOK_ID = "facebook_id";
    public static final String DROP_OFF = "drop_off";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String CREATED = "created";
    public static final String FACEBOOK_IDD = "facebookId";
    public static final String CURRENT_LOCATION_LATITUDE = "current_latitude";
    public static final String CURRENT_LOCATION_LONGITUDE = "current_longitude";
    public static final String EMAIL_VERIFICATION = "email_verification";
    public static final String PHONE_VERIFICATION = "phone_verification";
    public static final String ID_VERIFICATION = "id_verification";
    public static final String PAYPAL_VERIFICATION = "paypal_verification";
    public static final String STRIPE_VERIFICATION = "stripe_verification";
    public static final String STRIPE_ACCOUNT_ID = "stripe_account_id";
    public static final String PAYPAL_VERIFY = "paypal_verify";
    public static final String USER_RATINGS = "user_ratings";
    public static final String CITY = "city";
    public static final String SEARCH_ITEM = "";

    public static final String NAME_SAERCH = "name";
    public static final String FAVOURITE_STATUS = "favourite";

    public static final String PRODUCT_ID = "productId";
    public static final String FAVOURITE_ITEM = "favourite";

    public static final String REQUEST_COUNT = "requestCount";
    public static final String INQUIRY_COUNT = "inquiryCount";
    public static final String CURRENT_BOOKING_COUNT = "current_booking_count";
    public static final String MY_BOOKING_COUNT = "my_booking_count";
    public static final String TOTAL_COUNT = "total_count";
    public static final String BLOCK = "block";
    public static final String PENALTY_PAYMENT = "penalty_payment";

    public static final String ID_VALID = "id_valid";

    /*
     * Write the String Set
     * */
    public static void writeStringSet(Context context, String key, Set<String> values) {
        getEditor(context).putStringSet(key, values).apply();
    }

    /*
     * Read the String Set
     * */
    public static Set<String> readStringSet(Context context, String key, Set<String> defValue) {
        return getPreferences(context).getStringSet(key, defValue);
    }

    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
