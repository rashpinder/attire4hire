package com.attire4hire.utils;

import android.widget.EditText;

public class ConstantData {

//    public static String BASE_URL = "https://dharmani.com/Attire4Hire/Webservice/"; //Base url
//    public static String BASE_URL = "http://161.97.132.85/Attire4Hire/Webservice/"; //Base url
    public static String BASE_URL = "https://attires4hire.com/Admin/Webservice/";
    public static String publishable_key = "pk_test_51HCpjLIzPahuJsVtQzY68k9wfU5gA9sgB2nNsti2bnDJIkGaiYlVVp1E7BA62Vgknl1twBV4yB5yfTJ4k0JsR69n00Sr8AzNWK"; //stripe publishable key
//    public static String passbase_key = "vh2ekQYNq9sIxy7HyFLFhKnZazuq4vKlkLBknKDDzhV4QF647lnCDnNU3LKIYwJq"; //passbase publishable key
    public static String passbase_key = "tOnoQJi3xYfcYl94sKQGu1DBtO1Am1beyq6RimSrrWwNI3xsxPl1eBp5wHsiRKtv"; //passbase publishable key

    public static final String SocketURL = "https://jaohar-uk.herokuapp.com";

    public static String SIGN_UP = BASE_URL + "Signup.php"; //Used for creating account and registering user in app, details will be saved in the server. API is used in Signup screen.
    public static String SIGN_IN = BASE_URL + "Login.php"; //Used for signing-in the registered user. API is used in Signin screen.
    public static String FACEBOOK_API = BASE_URL + "Facebook_signup.php"; //Used for creating account in the app using facebook account. API is used in Social media login screen.
    public static String FORGOT_PASS_API = BASE_URL + "Forgotpassword.php"; //Used for creating new password if user forgets the current password, new password will be send to user through email. API is used in Forget password screen.
    public static String GOOGLE_API = BASE_URL + "Google_signup.php"; //Used for creating account in the app using google account. API is used in Social media login screen.
    public static String ALL_CATEGORY_API = BASE_URL + "AllCategoryData.php"; // Used for showing category, size , type, colour, brands, conditions, occasion data while adding a new product.
    public static String ADDITEM_API = BASE_URL + "Additem.php"; // Used for adding a new Product
    public static String GET_ITEM_DETAILS_API = BASE_URL + "GetProductListbyid.php"; // Used for displaying product and user details in Item details screen.
    public static String ADDITEM_DRAFT_API = BASE_URL + "AddProductDraft.php"; //Used for adding product in draft listing.
    public static String GET_USER_PRODUCT_LIST = BASE_URL + "GetproductListbyuserId.php"; // Used for showing user's own listing of products.
    public static String GET_DRAFT_ITEMS = BASE_URL + "GetDraftDetailbyid.php"; // Used for displaying draft product listings
    public static String DELETE_DRAFT_ITEMS = BASE_URL + "DeleteDraft.php"; // Used for deleting products from draft listings
    public static String GET_USER_PROFILE = BASE_URL + "GetUserDetailListbyId.php"; // Used for displaying user details for Lender side user details, used in profile screen(fifth tab).
    public static String DELETE_PARTICULARITEM = BASE_URL + "DeleteParticularItem.php"; //Used for deleting user's own product. Used in own item details screen.
    public static String GET_ALL_PRODUCTS_AND_SEARCH = BASE_URL + "GetAllProductListAndSearch.php";//Used for displaying all the products listing on Home screen and also used for displaying searched products listing on the same screen. API is used in Home screen.
    public static String GET_FAVOURITE = BASE_URL + "GetFavouritesbyuser_id.php";//Used for displaying user's favorite products listing in "My favorites".
    public static String ADD_FAVOURITE = BASE_URL + "AddFavourites.php";//Used for adding products to favorites and removing products from favorites.
    public static String CHANGE_PASSWORD = BASE_URL + "ChangePassword.php";// Used for changing current password to a new password.
    public static String GET_INQUIRIES = BASE_URL + "GetAllInquiries.php"; // Used for showing requests in "My Requests" screen.
    public static String UPDATE_REQUEST_STATUS = BASE_URL + "UpdateRequestStatus.php"; //Used for updating request status in "My Requests" screen on click of "Accept" and "Decline" and used for cancelling request from "My Enquiries" screen.
    public static String GET_ALL_REQUESTS = BASE_URL + "GetAllRequests.php"; //Used for showing enquiries in "My Enquiries" screen.
    public static String ADD_REQUEST = BASE_URL + "AddRequest.php";// Used for "Request to buy" and "Request to rent" in item details screen.
    public static String ADD_ACCOUNT_SETUP = BASE_URL + "AddAccountSetup.php"; //Used for adding address details in Accounts settings screen.
    public static String EDIT_DRAFT_ITEMS = BASE_URL + "EditProductDraft.php";
    public static String GET_REQUEST_DETAILS = BASE_URL + "GetRequestDetails.php";// Used for displaying request details in "Finalise your request" screen.
    public static String GET_RECENT_SEARCH = BASE_URL + "GetRecentSearches.php"; //Used for displaying recent searched product names in recent search. API is used in Recent search screen.
    public static String CLEAR_RECENT_SEARCH = BASE_URL + "ClearRecentSearch.php"; //Used for clearing recent searched product names in recent search. API is used in Recent search screen.
    public static String AUTO_COMPLETE_SEARCH = BASE_URL + "AutoCompleteSearch.php";
    public static String GET_USER_PROFILE_DETAILS = BASE_URL + "GetUserProfileById.php"; //Used for showing user details on Edit Profile screen.
    public static String EDIT_USER_PROFILE = BASE_URL + "EditUserprofile.php";//Used for editing user details on Edit Profile screen.
    public static String GET_BRAND = BASE_URL + "Getbrand.php";//Used for showing attire's brand names. API is used in Filters screen for brand part.
    public static String GET_CATEGORY = BASE_URL + "Getcategory.php";//Used for showing attire's categories. API is used in Filters screen for category part.
    public static String GET_COLOR = BASE_URL + "Getcolor.php";//Used for showing attire's colors. API is used in Filters screen for color part.
    public static String GET_SIZE = BASE_URL + "Getsize.php";//Used for showing attire's sizes. API is used in Filters screen for size part.
    public static String GET_TYPE = BASE_URL + "Gettype.php";//Used for showing attire's types. API is used in Filters screen for type part.
    public static String GET_DESIGNER = BASE_URL + "Getdesigner.php";
    public static String GET_CONDITION = BASE_URL + "Getcondition.php"; //Used for showing attire's conditions. API is used in Filters screen for condition part.
    public static String GET_OCCASION = BASE_URL + "Getoccasion.php"; //Used for showing occasions. API is used in Filters screen for occasion part.
    public static String FILTER_PRODUCT_BY_CHOICE = BASE_URL + "FilterProductByChoice.php";//Used for showing filtered products. API is used in Home screen
    public static String ADD_CARD_FOR_PAYMENT = BASE_URL + "AddCardForPayment.php"; //Used for adding stripe card details that will be used for payment.
    public static String RETRIEVE_THE_CARDS = BASE_URL + "ReterieveTheCards.php"; //Used for displaying lists of cards in payment method screen.
    public static String GET_CURRENT_BOOKINGS = BASE_URL + "GetCurrentBookings.php"; //Used for showing current booked products in "Current Bookings" screen.
    public static String GET_MY_BOOKINGS = BASE_URL + "GetMyBookings.php"; //Used for showing comfirmed booking in "My Confirmed Bookings" screen.
    public static String ADD_ORDER = BASE_URL + "AddOrder.php"; //Used for submiting request in "Finalise your request" screen.
    public static String GET_ALL_ORDERS = BASE_URL + "GetAllOrders.php"; //Used for showing products under "Booking History".
    public static String GET_ORDER_DETAILS = BASE_URL + "GetOrderDetails.php"; //Used for showing order details in "Pay for your Item" and "Order confirmation" screen.
    public static String REQUEST_CHAT = BASE_URL + "RequestChat.php"; //Used for requesting chat with another user. Used in Item details and Another user details sceen.
    public static String GET_CHAT_LISTINGS = BASE_URL + "GetChatListing.php";//Used for displaying all chat messages in chat screen.
    public static String ADD_MESSAGE = BASE_URL + "AddMessage.php"; // Used for adding messages in chat screen
    public static String UPDATE_ORDER_STATUS = BASE_URL + "UpdateOrderStatus.php"; //Used for updating all status(Item booked, shipped etc) from Order confirmation screen.
    public static String GET_CHAT_USERS = BASE_URL + "GetChatUsers.php"; // Used for showing all chat users in third tab
    public static String ADD_REVIEW = BASE_URL + "AddReview.php"; // Used for adding reviews to lender and renter corresponding to their products.
    public static String GET_ALL_REVIEWS = BASE_URL + "GetAllReviews.php"; // Used for showing all reviews added by other users in Lender Reviews and Renter Reviews screen.
    public static String CONTACT_US = BASE_URL + "ContactUs.php";
    public static String GET_ACCOUNT_SETUP = BASE_URL + "GetAccountSetup.php"; //Used for displaying address details in account settings screen.
    public static String UPDATE_PHONE_VERIFICATION = BASE_URL + "UpdatePhoneVerification.php";//Used for verifying phone number.
    public static String UPDATE_ID_VERIFICATION = BASE_URL + "UpdateIdVerification.php"; //Used for verifying id using passbase.
    public static String ADD_REPORT = BASE_URL + "AddReport.php"; //Used for reporting user in "Report User" screen.
    public static String EDIT_PRODUCT_ITEM = BASE_URL + "EditProductItem.php"; //Used for editing own product details.
    public static String LOG_OUT = BASE_URL + "Logout.php"; //Used for signing out from app.
    public static String UPDATE_PAYPAL_VERIFICATION = BASE_URL + "UpdatePaypalVerification.php";
    public static String PAYPAL_lOGIN_API = BASE_URL + "PayPalLogin.php";
    public static String RESEND_MAIL = BASE_URL + "ResendMail.php";// Used for resending mail verification incase it was not verified before.
    public static String UPDATE_ORDER_PAYMENT = BASE_URL + "UpdateOrderPayment.php"; //Used for adding card and payment regarding order in payment screen.
    public static String CANCEL_REQUEST = BASE_URL + "CancelRequest.php"; //Used for cancelling order in case rquest for add order is not submitted.
    public static String UPDATE_READ_STATUS = BASE_URL + "UpdateReadStatus.php";//Used for updating read status/badge count for corresponding address.
    public static String UPDATE_ORDER_DETAIL = BASE_URL + "UpdateOrderDetail.php";//Used for updating details in "Pay for your Item" scren.
    public static String REPORT_ORDER = BASE_URL + "ReportOrder.php"; //Used for reporting order from Order confirmation screen.

    public static String DeleteChat = BASE_URL + "DeleteChat.php"; //Used for deleting chat with particular user in chat screen.
    public static String DeleteRequest = BASE_URL + "DeleteRequest.php"; //Used for deleting requests from "My Requests" and "My Enquiries" screens.
    public static String CreateAccount = "http://161.97.132.85/Attire4Hire/Webservice/Stripe/" + "CreateAccount.php";
//    public static String StripeConnect = "https://dharmani.com/Attire4Hire/Webservice/Stripe/" + "StripeConnect.php"; //Used for connecting stripe with the server and app.
    public static String StripeConnect = "https://attires4hire.com/Admin/Webservice/Stripe/" + "StripeConnect.php"; //Used for connecting stripe with the server and app.

    //    public static String ADDITEM_API =BASE_URL +"AddItemImage_android.php";

    /*
     * WebView Links
     * */
    public static String PRIVACY_POLICY = "https://attires4hire.com/privacypolicy";
    public static String TERM_AND_SERVICES = "https://attires4hire.com/termsofservice/";
    public static String ABOUT_US = "https://attires4hire.com/about";
    public static String CONTACT_US_LINK = "https://attires4hire.com/contact-us/";
    public static String HELP = "https://attires4hire.com/faqs/";
    public static String DELIVERY = "https://attires4hire.com/delivery/";
    public static String SIZE_CHART = "https://attires4hire.com/size-chart";
    public static String INVITE_FRIENT = BASE_URL + "invite_frient";
    public static String PAYPAL_LOGIN = "paypal_login";

//    public static String TERM_AND_SERVICES = BASE_URL + "terms&services.html";
//    public static String ABOUT_US = BASE_URL + "AboutUs.html";
//    public static String HELP = BASE_URL + "Help.html";
//    public static String CONTACT_US = BASE_URL + "";
//    public static String DELIVERY = BASE_URL + "Delivery.html";

    /*
     * Google Places Apis
     * */

    public static String getLocationApi(EditText mEditText) {
        String strUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + mEditText.getText().toString().trim() + "&key=" + "AIzaSyBmiFbz7mGbzBg6bEkRtvHovecPdJTKUOY";
        return strUrl.replace(" ", "%20");
    }

    /*
     * Local @param
     * */
    public static String ORDER_ID = "order_id";
    public static String MODEL = "model";
    public static String LIST = "list";
    public static String LINK = "link";
    public static String ADD_ITEM_TYPE = "add_item_type";
    public static String ACTIVITY_DRAFT = "activity_draft";
    public static String ACTIVITY_LIST_ITEM = "activity_list_item";

    public static String FILTER_STATUS = "filter_status";
    public static String FILTER_VALUE = "filter_value";
    public static String SEARCH_VALUE = "search_value";
    public static String DATE_VALUE = "date_value";
    public static String LOCATION_VALUE = "location_value";
    public static String PAYMENT_VALUE = "payment_value";

    public static String OCCASION_DATE = "occasion_date";
    public static String ITEM_CATEGORY = "item_category";
    public static String PRODUCT_TYPE = "product_type";
    public static String BRAND = "brand";
    public static String COLOUR = "color";
    public static String CONDITION = "condition";
    public static String SIZE = "size";
    public static String PRICE_RANGE_START = "price_range_start";
    public static String PRICE_RANGE_END = "price_range_end";
    public static String LOCATION = "location";
    public static String OCCASION = "occasion";
    public static String OPEN_FOR_SALE = "open";
    public static String NOTIFICATION = "notification";
    public static String PENALTY = "penalty";
    public static String USER_TYPE = "user_type";
    public static String PAYPAL_VERIFICATION = "paypal_verification";
    public static String PAYPAL_LINK = "paypal_link";
    public static String P_LINK = "p_link";
    public static String PAYPAL_TRUE = "paypal_true";
    public static String FIRST_DATE = "first_date";
    public static String LAST_DATE = "last_date";
    public static String STRIPE_LINK = "stripe_link";
    public static String S_LINK = "s_link";
    public static String STRIPE = "stripe";
    public static String SHOW_STRIPE = "show_stripe";
    public static String HIDE_STRIPE = "hide_stripe";

    public static int REQUEST_ACTIVITY = 13213;
    public static String TOKEN = "token";
    public static String HIDE_BUTTON = "hide_button";
    public static String HIDE = "hide";
    public static String ITEM_STATUS = "item_status";
    public static String BUY = "buy";
    public static String RENT = "rent";
    public static String IMAGE = "image";
    public static String REDIRECION_TYPE = "redirection_type";
    public static String CLOSET = "closet";

    public static final int VIEW_TYPE_SENDER_MESSAGE = 1;
    public static final int VIEW_TYPE_RECIEVER_MESSAGE = 0;
}