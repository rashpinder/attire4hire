package com.attire4hire.utils;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.attire4hire.volley.LruBitmapCache;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.onesignal.OneSignal;

import java.net.URISyntaxException;

import io.branch.referral.Branch;
import io.socket.client.IO;
import io.socket.client.Socket;

public class Attire4hireApplication extends Application {
    private static final String TAG = "Attire4hire";
    public static final int CONNECTION_TIMEOUT = 50000 * 1000;//120 Seconds    private RequestQueue mRequestQueue;
    private static Attire4hireApplication mInstance;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;
    private static SingletonValues singletonValues;

    public static final String CLIENT_ID = "2630549817020757";
    public static final String CLIENT_SECRET = "54e05ea35b5ac8bfdad913f15bc32d79";
    public static final String CALLBACK_URL = "https://www.dharmani.com/";
    private static final String ONESIGNAL_APP_ID = "36cc7f58-fcc7-4c49-8be6-b58998028523";

    public static synchronized Attire4hireApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        singletonValues = new SingletonValues();
        Fresco.initialize(getApplicationContext());
        FirebaseApp.initializeApp(this);

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);

        // Branch logging for debugging
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(this);
    }

    public static SingletonValues getSingletonValues() {
        return singletonValues;
    }

    /***************
     *Imaplement Volley
     ***************/
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(ConstantData.SocketURL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}