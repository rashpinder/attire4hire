package com.attire4hire.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.model.ChatModel;
import com.attire4hire.utils.ConstantData;

import java.util.ArrayList;

import static com.attire4hire.activity.BaseActivity.decodeEmoji;

public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.MyViewHolder> {
    Context context;
    ArrayList<ChatModel> mChatModel;
    int lastItemPosition = -1;

    public ChatMessagesAdapter(Context context, ArrayList<ChatModel> mChatModel) {
        this.context = context;
        this.mChatModel = mChatModel;
    }

    @NonNull
    @Override
    public ChatMessagesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.chat_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessagesAdapter.MyViewHolder holder, int position) {

        if (mChatModel.get(position).getIntChatType() == ConstantData.VIEW_TYPE_SENDER_MESSAGE) {
            holder.right.setVisibility(View.VISIBLE);
            holder.left.setVisibility(View.GONE);
            holder.rightChat.setText(mChatModel.get(position).getMessage());
            holder.tv_day_Sender.setText(mChatModel.get(position).getCreation_date());

        } else if (mChatModel.get(position).getIntChatType() == ConstantData.VIEW_TYPE_RECIEVER_MESSAGE) {
            holder.right.setVisibility(View.GONE);
            holder.left.setVisibility(View.VISIBLE);
            holder.leftChat.setText(mChatModel.get(position).getMessage());
            holder.tv_day_receiver.setText(mChatModel.get(position).getCreation_date());
        }
    }

    @Override
    public int getItemCount() {
        return mChatModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout right, left;
        TextView rightChat, leftChat, tv_day_receiver, tv_date_receiver, tv_time_receiver, tv_day_Sender,
                tv_date_Sender, tv_time_Sender;
        ImageView sender, receiver;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            right = itemView.findViewById(R.id.right);
            left = itemView.findViewById(R.id.left);
            sender = itemView.findViewById(R.id.sender);
            receiver = itemView.findViewById(R.id.receiver);
            rightChat = itemView.findViewById(R.id.rightChat);
            leftChat = itemView.findViewById(R.id.leftChat);
            tv_day_receiver = itemView.findViewById(R.id.tv_day_receiver);
            tv_date_receiver = itemView.findViewById(R.id.tv_date_receiver);
            tv_time_receiver = itemView.findViewById(R.id.tv_time_receiver);
            tv_day_Sender = itemView.findViewById(R.id.tv_day_Sender);
            tv_date_Sender = itemView.findViewById(R.id.tv_date_Sender);
            tv_date_Sender = itemView.findViewById(R.id.tv_date_Sender);
            tv_time_Sender = itemView.findViewById(R.id.tv_time_Sender);
        }
    }
}
