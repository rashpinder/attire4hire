package com.attire4hire.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.attire4hire.R;
import com.attire4hire.activity.BaseActivity;
import com.attire4hire.model.ImagesModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SlidingImageAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<ImagesModel> modelArrayList;
    AddClick addClick;

    public interface AddClick {
        void Click(int position);
    }

    public SlidingImageAdapter(Context context, ArrayList<ImagesModel> images, AddClick addClick) {
        this.context = context;
        this.modelArrayList = images;
        this.addClick = addClick;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return this.modelArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_sliding_pages, view, false);

        assert imageLayout != null;

        ImageView imageView = imageLayout.findViewById(R.id.image);
        LinearLayout linearLayout = imageLayout.findViewById(R.id.linearLayout);

        ImagesModel mImagesModel = modelArrayList.get(position);

        if (mImagesModel.getImage_path() != null) {
            if (mImagesModel.getImage_path().contains("http")) {
                Glide.with(context).load(mImagesModel.getImage_path())
                        .placeholder(R.drawable.placeholder_img)
                        .error(R.drawable.placeholder_img)
                        .into(imageView);
            }

//            if (mImagesModel.getImage_path().contains("https")) {
//                Glide.with(context).load(mImagesModel.getImage_path())
//                        .placeholder(R.drawable.placeholder_img)
//                        .error(R.drawable.placeholder_img)
//                        .into(imageView);
//            } else {
//                Glide.with(context).load(mImagesModel.getImage_path().replace("http://", "https://"))
//                        .into(imageView);
//            }
            else {
            Bitmap myBitmap = BitmapFactory.decodeFile(mImagesModel.getImage_path());
            imageView.setImageBitmap(myBitmap);}}

//            if (mImagesModel.getImage_path().contains("http")) {
//                imageView.setImageBitmap(((BaseActivity) context).getBitmapFromFilePath(mImagesModel.getImage_path()));
//            }

//            if (mImagesModel.getImage_path().contains("https")) {
//                imageView.setImageBitmap(((BaseActivity) context).getBitmapFromFilePath(mImagesModel.getImage_path()));
//            } else {
//                imageView.setImageBitmap(((BaseActivity) context).getBitmapFromFilePath(mImagesModel.getImage_path().replace("http://", "https://")));
//            }
//        }

        if (modelArrayList.size() == 0) {
            imageView.setBackgroundResource(R.drawable.placeholder_img);
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addClick.Click(position);
            }
        });

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}