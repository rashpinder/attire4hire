package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.CartActivity;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.extension.PayForYourItemExtensionActivity;
import com.attire4hire.interfaces.CancelOrderInterface;
import com.attire4hire.interfaces.DeleteRequestsInterface;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MyInquriesAdapter extends RecyclerView.Adapter<MyInquriesAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ItemProductModel> mItemProductModel;
    PaginationInquiriesInterface paginationInquiriesInterface;
    String status = "";
    CancelOrderInterface mCancelOrderInterface;
    String ITEM_STATUS = "";
    DeleteRequestsInterface deleteRequestsInterface;
    boolean isLongClick = false;
    private int row_index;
    private int count = 0;
    String startDate = "", endDate = "";

    public MyInquriesAdapter(Activity mActivity, ArrayList<ItemProductModel> mItemProductModel, PaginationInquiriesInterface paginationInquiriesInterface,
                             CancelOrderInterface mCancelOrderInterface, DeleteRequestsInterface deleteRequestsInterface) {
        this.mActivity = mActivity;
        this.mItemProductModel = mItemProductModel;
        this.paginationInquiriesInterface = paginationInquiriesInterface;
        this.mCancelOrderInterface = mCancelOrderInterface;
        this.deleteRequestsInterface = deleteRequestsInterface;
    }

    @NonNull
    @Override
    public MyInquriesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inquires_list, parent, false);
        return new MyInquriesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyInquriesAdapter.MyViewHolder holder, int position) {

        if (position >= mItemProductModel.size() - 1) {
            paginationInquiriesInterface.mPaginationInquiriesInterface(true);
        }

        ItemProductModel ItemProductModelList = mItemProductModel.get(position);

        if (ItemProductModelList.getImage1() != null && ItemProductModelList.getImage1().contains("http")) {

            Glide.with(mActivity).load(ItemProductModelList.getImage1())
                    .into(holder.iv_profile);

//            if (ItemProductModelList.getImage1().contains("https")) {
//                Glide.with(mActivity).load(ItemProductModelList.getImage1())
//                        .into(holder.iv_profile);
//            } else {
//                Glide.with(mActivity).load(ItemProductModelList.getImage1().replace("http://", "https://"))
//                        .into(holder.iv_profile);
//            }
        }

        if (ItemProductModelList.getCost() != null && !ItemProductModelList.getCost().equals("")) {
            holder.priceTV.setText("Cost: " + "£" + ItemProductModelList.getCost());
        }

        if (ItemProductModelList.getStart_date() != null && !ItemProductModelList.getStart_date().equals("")) {
            startDate = ItemProductModelList.getStart_date();
        }

        if (ItemProductModelList.getEnd_date() != null && !ItemProductModelList.getEnd_date().equals("")) {
            endDate = ItemProductModelList.getEnd_date();
        }

        if (ItemProductModelList.getRead_status() != null && ItemProductModelList.getRead_status().equals("false")) {
            holder.requestLL.setBackgroundColor(Color.parseColor("#f0f0f0"));
        } else {
            holder.requestLL.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        holder.dateTV.setText("Dates: " + startDate + " - " + endDate);

        if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
            holder.dateTV.setVisibility(View.VISIBLE);
        } else {
            holder.dateTV.setVisibility(View.GONE);
        }

        if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Accepted")) {
            holder.cancelTV.setVisibility(View.VISIBLE);
            holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
            holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
            holder.confirmedTV.setText("Proceed to Pay");
            status = "accepted";
            if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0) {

                if (ItemProductModelList.getExtension().equals("1")) {
                    holder.nameTV.setText("Your request to extend the rental for" + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " has been " + status + ".");
                } else {
                    holder.nameTV.setText("Your request to " + ItemProductModelList.getRequest_type() + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " has been " + status + ".");
                }

            }
            holder.confirmedTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0) {
                        if (ItemProductModelList.getExtension().equals("1")) {
                            Intent intent = new Intent(mActivity, PayForYourItemExtensionActivity.class);
                            intent.putExtra(ConstantData.MODEL, ItemProductModelList);
                            mActivity.startActivity(intent);
                        } else {
                            Intent intent = new Intent(mActivity, CartActivity.class);
                            intent.putExtra(ConstantData.MODEL, ItemProductModelList);
                            mActivity.startActivity(intent);
                        }
                    }

                }
            });

            holder.detailsll.setVisibility(View.VISIBLE);

        } else if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Declined")) {
            holder.cancelTV.setVisibility(View.GONE);
            holder.confirmedTV.setBackgroundResource(R.drawable.black_bg);
            holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
            holder.confirmedTV.setText("Declined");
            status = "declined";
            if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0) {
                if (ItemProductModelList.getExtension().equals("1")) {
                    holder.nameTV.setText("Your request to extend the rental for" + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " has been " + status + ".");
                } else {
                    holder.nameTV.setText("Your request to " + ItemProductModelList.getRequest_type() + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " has been " + status + ".");
                }
            }
            holder.detailsll.setVisibility(View.GONE);

        } else if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Pending")) {
            holder.cancelTV.setVisibility(View.VISIBLE);
            holder.confirmedTV.setBackgroundResource(R.drawable.pending_rec_bg);
            holder.confirmedTV.setTextColor(Color.parseColor("#b7006d"));
            holder.confirmedTV.setText("Pending");
            status = "pending";
            if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0) {
                if (ItemProductModelList.getExtension().equals("1")) {
                    holder.nameTV.setText("Your request to extend the rental for" + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " is " + status + ".");
                } else {
                    holder.nameTV.setText("Your request to " + ItemProductModelList.getRequest_type() + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " is " + status + ".");
                }
            }
            holder.detailsll.setVisibility(View.VISIBLE);

        } else if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Cancelled")) {
            holder.cancelTV.setVisibility(View.GONE);
            holder.confirmedTV.setBackgroundResource(R.drawable.pending_rec_bg);
            holder.confirmedTV.setTextColor(Color.parseColor("#b7006d"));
            holder.confirmedTV.setText("Cancelled");
            status = "cancelled";
            if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0)
                if (ItemProductModelList.getExtension().equals("1")) {
                    holder.nameTV.setText("Your request to extend the rental for" + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " is " + status + ".");
                } else {
                    holder.nameTV.setText("Your request to " + ItemProductModelList.getRequest_type() + " the " + CapitalizedFirstLetter(ItemProductModelList.getName()) + " is " + status + ".");
                }

            holder.detailsll.setVisibility(View.GONE);
        }

        holder.cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCancelOrderInterface.cancelCall(position, holder.cancelTV, holder.confirmedTV, holder.nameTV);
            }
        });

        holder.iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
                    ITEM_STATUS = ConstantData.RENT;
                } else {
                    ITEM_STATUS = ConstantData.BUY;
                }

                mActivity.startActivity(new Intent(mActivity, ItemDetailsActivity.class)
                        .putExtra(ConstantData.MODEL, ItemProductModelList)
                        .putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE)
                        .putExtra(ConstantData.ITEM_STATUS, ITEM_STATUS));
            }
        });

        holder.requestLL.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                isLongClick = true;

                String strStatus = mItemProductModel.get(position).getRequest_status();

                if (strStatus.equals("Cancelled") || strStatus.equals("Declined")) {
                    row_index = position;
                    count = 1;
                    notifyDataSetChanged();
                    deleteRequestsInterface.mDeleteRequestsInterface(position, ItemProductModelList.getRequest_id(), holder.requestLL);
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView confirmedTV, priceTV, dateTV, nameTV, cancelTV;
        ImageView iv_profile;
        LinearLayout requestLL, detailsll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            confirmedTV = itemView.findViewById(R.id.confirmedTV);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            priceTV = itemView.findViewById(R.id.priceTV);
            dateTV = itemView.findViewById(R.id.sizecolorTV);
            nameTV = itemView.findViewById(R.id.nameTV);
            cancelTV = itemView.findViewById(R.id.cancelTV);
            requestLL = itemView.findViewById(R.id.requestLL);
            detailsll = itemView.findViewById(R.id.detailsll);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 1) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
