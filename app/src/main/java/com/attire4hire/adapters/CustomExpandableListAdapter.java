package com.attire4hire.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.interfaces.ExpandableListCallBack;
import com.attire4hire.utils.Attire4hirePrefrences;

import java.util.HashMap;
import java.util.List;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
    /*Widgets*/
    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;
    TextView mBadge;
    ImageView mGroupIV;
    View mViewBottom;
    ExpandableListCallBack expandableListCallBack;
    String RequsetCount = "", CurrentCount = "", InquiryCount = "", ConfirmedCount = "", LendingCount = "", RendingCount = "";
    int request_count = 0, current_count = 0, lending_count = 0, enquiry_count = 0, confirmed_count = 0, renting_count = 0;

    public CustomExpandableListAdapter(Context context, List<String> expandableListTitle, HashMap<String, List<String>> expandableListDetail, ExpandableListCallBack expandableListCallBack) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.expandableListCallBack = expandableListCallBack;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {

        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }

        mBadge = convertView.findViewById(R.id.closet_badge);
        TextView expandedListTextView = convertView
                .findViewById(R.id.expandedListItem);
        mViewBottom = convertView.findViewById(R.id.bottom_view);

        expandedListTextView.setText(expandedListText);

        if (listPosition == 1) {
            if (expandedListPosition == 0) {

                RequsetCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.REQUEST_COUNT, "");
                request_count = Integer.parseInt(RequsetCount);

                if (RequsetCount.equals("0")) {
                    mBadge.setVisibility(View.GONE);
                } else {
                    mBadge.setVisibility(View.VISIBLE);
                    mBadge.setText(RequsetCount);
                }

            } else if (expandedListPosition == 1) {

                CurrentCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.CURRENT_BOOKING_COUNT, "");
                current_count = Integer.parseInt(CurrentCount);

                if (CurrentCount.equals("0")) {
                    mBadge.setVisibility(View.GONE);
                } else {
                    mBadge.setVisibility(View.VISIBLE);
                    mBadge.setText(CurrentCount);
                }

            } else {
                mBadge.setVisibility(View.GONE);
            }

        } else if (listPosition == 2) {
            if (expandedListPosition == 0) {

                InquiryCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.INQUIRY_COUNT, "");
                enquiry_count = Integer.parseInt(InquiryCount);

                if (InquiryCount.equals("0")) {
                    mBadge.setVisibility(View.GONE);
                } else {
                    mBadge.setVisibility(View.VISIBLE);
                    mBadge.setText(InquiryCount);
                }

            } else if (expandedListPosition == 1) {

                ConfirmedCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.MY_BOOKING_COUNT, "");
                confirmed_count = Integer.parseInt(ConfirmedCount);

                if (ConfirmedCount.equals("0")) {
                    mBadge.setVisibility(View.GONE);
                } else {
                    mBadge.setVisibility(View.VISIBLE);
                    mBadge.setText(ConfirmedCount);
                }

            } else {
                mBadge.setVisibility(View.GONE);
            }
        } else {
            mBadge.setVisibility(View.GONE);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableListCallBack.ExpandableCall(listPosition, expandedListPosition, mBadge);
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {

        return this.expandableListTitle.get(listPosition);

    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ImageView mArrowIV;
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_list_group, null);
        }

        mBadge = convertView.findViewById(R.id.closet_badge);

        TextView listTitleTextView = convertView
                .findViewById(R.id.listTitle);

        mGroupIV = convertView.findViewById(R.id.iv_group);
        mArrowIV = convertView.findViewById(R.id.iv_arrow);
        if (listPosition == 3) {
            mArrowIV.setVisibility(View.GONE);
        } else {
            mArrowIV.setVisibility(View.VISIBLE);
        }

        // listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        switch (listPosition) {
            case 0:
                mGroupIV.setImageResource(R.drawable.iv_listings);
                mBadge.setVisibility(View.GONE);
                break;
            case 1:
                mGroupIV.setImageResource(R.drawable.ic_renting);

                RequsetCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.REQUEST_COUNT, "");
                request_count = Integer.parseInt(RequsetCount);

                CurrentCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.CURRENT_BOOKING_COUNT, "");
                current_count = Integer.parseInt(CurrentCount);

                if (request_count != 0 || current_count != 0) {
                    lending_count = request_count + current_count;

                    mBadge.setText(String.valueOf(lending_count));
                    mBadge.setVisibility(View.VISIBLE);
                }else {
                    mBadge.setVisibility(View.GONE);
                }

                break;
            case 2:
                mGroupIV.setImageResource(R.drawable.ic_selling);

                InquiryCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.INQUIRY_COUNT, "");
                enquiry_count = Integer.parseInt(InquiryCount);

                ConfirmedCount = Attire4hirePrefrences.readString(context, Attire4hirePrefrences.MY_BOOKING_COUNT, "");
                confirmed_count = Integer.parseInt(ConfirmedCount);

                if (enquiry_count != 0 || confirmed_count != 0) {
                    renting_count = enquiry_count + confirmed_count;

                    mBadge.setText(String.valueOf(renting_count));
                    mBadge.setVisibility(View.VISIBLE);
                }else {
                    mBadge.setVisibility(View.GONE);
                }

                break;
            case 3:
                mGroupIV.setImageResource(R.drawable.ic_clock);
                mBadge.setVisibility(View.GONE);
                break;
        }
        if (isExpanded) {
            mArrowIV.setImageResource(R.drawable.ic_down);
        } else {
            mArrowIV.setImageResource(R.drawable.ic_next);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;

    }
}