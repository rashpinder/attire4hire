package com.attire4hire.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.activity.OrderConfirmationActivity;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ItemProductModel> mItemProductModel;
    PaginationInquiriesInterface paginationInquiriesInterface;
    String ITEM_STATUS = "";

    public OrdersAdapter(Activity mActivity, ArrayList<ItemProductModel> mItemProductModel, PaginationInquiriesInterface paginationInquiriesInterface) {
        this.mActivity = mActivity;
        this.mItemProductModel = mItemProductModel;
        this.paginationInquiriesInterface = paginationInquiriesInterface;
    }

    @NonNull
    @Override
    public OrdersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_history, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ItemProductModel mModel = mItemProductModel.get(position);

        if (position >= mItemProductModel.size() - 1) {
            paginationInquiriesInterface.mPaginationInquiriesInterface(true);
        }

        if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
            holder.nameTV.setText(CapitalizedFirstLetter(mItemProductModel.get(position).getName()) + "\n" + "Rental");
        } else {
            holder.nameTV.setText(CapitalizedFirstLetter(mItemProductModel.get(position).getName()) + "\n" + "Purchase");
        }

        holder.msgTV.setText(CapitalizedFirstLetter(mItemProductModel.get(position).getDescription()));

        if (mItemProductModel.get(position).getImage1() != null && mItemProductModel.get(position).getImage1().contains("http")) {
            Glide.with(mActivity).load(mItemProductModel.get(position).getImage1())
                    .into(holder.iv_profile);

//            if (mModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(mItemProductModel.get(position).getImage1())
//                        .into(holder.iv_profile);
//            } else {
//                Glide.with(mActivity).load(mModel.getImage1().replace("http://", "https://"))
//                        .into(holder.iv_profile);
//            }
        }
        holder.order_placedDate.setText(mItemProductModel.get(position).getOrder_date());
        holder.dispatchedDate.setText(mItemProductModel.get(position).getDispatch_date());
        holder.delivery_estDate.setText(mItemProductModel.get(position).getDelivery_date());

        holder.ll_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, OrderConfirmationActivity.class);
                intent.putExtra(ConstantData.MODEL, mModel);
                mActivity.startActivity(intent);
            }
        });

        if (mItemProductModel.get(position).getStatus().equals("1")) {
            holder.tv_accept.setText("Item Booked");
        } else if (mItemProductModel.get(position).getStatus().equals("2")) {
            holder.tv_accept.setText("Item Bought");
        } else if (mItemProductModel.get(position).getStatus().equals("3")) {
            holder.tv_accept.setText("Item Shipped");
        } else if (mItemProductModel.get(position).getStatus().equals("4")) {
            holder.tv_accept.setText("Item Received");
        } else if (mItemProductModel.get(position).getStatus().equals("5")) {
            holder.tv_accept.setText("Item Returned");
        } else if (mItemProductModel.get(position).getStatus().equals("6")) {
            if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
                holder.tv_accept.setText("Rental Completed");
            } else {
                holder.tv_accept.setText("Purchase Completed");
            }
        } else if (mItemProductModel.get(position).getStatus().equals("7")) {
            holder.tv_accept.setText("Booking Cancelled");
        }

        holder.iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
                    ITEM_STATUS = ConstantData.RENT;
                } else {
                    ITEM_STATUS = ConstantData.BUY;
                }

                mActivity.startActivity(new Intent(mActivity, ItemDetailsActivity.class).
                        putExtra(ConstantData.MODEL, mModel)
                        .putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE)
                        .putExtra(ConstantData.ITEM_STATUS, ITEM_STATUS));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_profile;
        TextView nameTV, msgTV, order_placedDate, dispatchedDate, delivery_estDate, tv_accept;
        LinearLayout ll_details;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_profile = itemView.findViewById(R.id.iv_profile);
            nameTV = itemView.findViewById(R.id.nameTV);
            msgTV = itemView.findViewById(R.id.msgTV);
            order_placedDate = itemView.findViewById(R.id.order_placedDate);
            dispatchedDate = itemView.findViewById(R.id.dispatchedDate);
            delivery_estDate = itemView.findViewById(R.id.delivery_estDate);
            tv_accept = itemView.findViewById(R.id.tv_accept);
            ll_details = itemView.findViewById(R.id.ll_details);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 1) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
