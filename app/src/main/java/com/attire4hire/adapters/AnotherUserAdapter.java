package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.AnotherUserItemDetailsActivity;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.Random;

public class AnotherUserAdapter extends RecyclerView.Adapter<AnotherUserAdapter.MyViewHolder> {
    private Activity mActivity;
    private ArrayList<ItemProductModel> mArrayList;
    Typeface typeface;
    ArrayList<String> FavouriteItems = new ArrayList<>();
    String Str_Fav = "";

    public AnotherUserAdapter(Activity mActivity, ArrayList<ItemProductModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public AnotherUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_list, parent, false);
        typeface = Typeface.createFromAsset(itemView.getContext().getAssets(), "BaskVill.ttf");

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnotherUserAdapter.MyViewHolder holder, int position) {
        ItemProductModel mModel = mArrayList.get(position);

        holder.itemCardViewCV.setCardBackgroundColor(getRandomColor());

        if (mModel.getImage1() != null && mModel.getImage1().contains("http")) {
            Glide.with(mActivity).load(mModel.getImage1())
                    .into(holder.itemImageRIV);

//            if (mModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(mModel.getImage1())
//                        .into(holder.itemImageRIV);
//            } else {
//                Glide.with(mActivity).load(mModel.getImage1().replace("http", "https"))
//                        .into(holder.itemImageRIV);
//            }
        }
        if (mModel.getBrand_name() != null && mModel.getBrand_name().length() > 0) {
            holder.txtTypeTV.setText(mModel.getBrand_name());
        }
        if (mModel.getName() != null && mModel.getName().length() > 0) {
            holder.txtBrandDesignNameTV.setText(CapitalizedFirstLetter(mModel.getName()));
        }
        if (mModel.getSize_name() != null && mModel.getSize_name().length() > 0) {
            holder.txtPriceTV.setText(mModel.getSize_name());
        }
        if (mModel.getWeek_4days() != null && mModel.getWeek_4days().length() > 0) {
            String strDay4 = String.format("%.2f", Double.parseDouble(mModel.getWeek_4days()));
            holder.txtSizeTV.setText("£" + strDay4);
        }

        holder.itemImageRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AnotherUserItemDetailsActivity.class);
                mIntent.putExtra(ConstantData.TOKEN, "profile");
                mIntent.putExtra(ConstantData.MODEL, mModel);
                mActivity.startActivity(mIntent);
            }
        });

        if (mModel.getRatings() != null && mModel.getRatings().length() > 0)
            holder.mRatingbarRB.setRating(Float.parseFloat(mModel.getRatings()));

        if (mModel.getFavourite().equals("2")) {
            holder.imgFavoriteIV.setImageResource(R.drawable.ic_heart_sel);
            FavouriteItems.add(String.valueOf(position));
        } else {
            holder.imgFavoriteIV.setImageResource(R.drawable.ic_fav_tb);
            FavouriteItems.remove(String.valueOf(position));
        }

        holder.imgFavoriteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FavouriteItems.contains(String.valueOf(position))) {
                    FavouriteItems.remove(String.valueOf(position));
                    holder.imgFavoriteIV.setImageResource(R.drawable.ic_fav_tb);
                    Str_Fav = "1";
                } else {
                    FavouriteItems.add(String.valueOf(position));
                    holder.imgFavoriteIV.setImageResource(R.drawable.ic_heart_sel);
                    Str_Fav = "2";
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView itemImageRIV;
        ImageView imgFavoriteIV;
        TextView txtTypeTV, txtBrandDesignNameTV, txtSizeTV, txtPriceTV;
        RatingBar mRatingbarRB;
        CardView itemCardViewCV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImageRIV = itemView.findViewById(R.id.itemImageRIV);
            imgFavoriteIV = itemView.findViewById(R.id.imgFavoriteIV);
            txtTypeTV = itemView.findViewById(R.id.txtTypeTV);
            txtBrandDesignNameTV = itemView.findViewById(R.id.txtBrandDesignNameTV);
            txtSizeTV = itemView.findViewById(R.id.txtSizeTV);
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            mRatingbarRB = itemView.findViewById(R.id.mRatingbarRB);
            itemCardViewCV = itemView.findViewById(R.id.itemCardViewCV);
        }
    }

    private int getRandomColor() {
        int[] mColorsArray = mActivity.getResources().getIntArray(R.array.androidcolors);
        return mColorsArray[new Random().nextInt(mColorsArray.length)];
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
//        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1).toLowerCase();
        String output = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
        return output;
    }
}