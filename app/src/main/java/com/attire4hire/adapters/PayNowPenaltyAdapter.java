package com.attire4hire.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.OrderConfirmationActivity;
import com.attire4hire.model.PenaltyList;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class PayNowPenaltyAdapter extends RecyclerView.Adapter<PayNowPenaltyAdapter.MyViewHolder> {
    Context context;
    List<PenaltyList.AllOrder> mPenaltyList = new ArrayList<>();

    public PayNowPenaltyAdapter(Context context, List<PenaltyList.AllOrder> mPenaltyList) {
        this.context = context;
        this.mPenaltyList = mPenaltyList;
    }

    @NonNull
    @Override
    public PayNowPenaltyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pay_penalty_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.nameTV.setText(mPenaltyList.get(position).getOrderDetail().getProductDetail().getName());


        if (mPenaltyList.get(position).getOrderDetail().getType().equals("rent")) {
            holder.typeTV.setText("Rental");
        } else {
            holder.typeTV.setText("Purchase");
        }

        holder.datesTv.setText("Dates: " + mPenaltyList.get(position).getOrderDetail().getStartDate() + " - " + mPenaltyList.get(position).getOrderDetail().getEndDate());
        holder.priceTv.setText("£" + mPenaltyList.get(position).getPenalty());

        if (mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1() != null && mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1().contains("http")) {
            Glide.with(context).load(mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1()).into(holder.iv_profile);
//            if (mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1().contains("https")) {
//                Glide.with(context).load(mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1()).into(holder.iv_profile);
//            } else {
//                Glide.with(context).load(mPenaltyList.get(position).getOrderDetail().getProductDetail().getImage1().replace("http://", "https://")).into(holder.iv_profile);
//            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OrderConfirmationActivity.class);
                intent.putExtra(ConstantData.NOTIFICATION, ConstantData.PENALTY);
                intent.putExtra(ConstantData.ORDER_ID, mPenaltyList.get(position).getOrderDetail().getId());
                context.startActivity(intent);
            }
        });

        if (mPenaltyList.size() > 0 && (position == mPenaltyList.size() - 1)) {
            holder.view.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mPenaltyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, typeTV, datesTv, priceTv;
        ImageView iv_profile;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.view);
            nameTV = itemView.findViewById(R.id.nameTV);
            typeTV = itemView.findViewById(R.id.typeTV);
            datesTv = itemView.findViewById(R.id.datesTv);
            priceTv = itemView.findViewById(R.id.priceTv);
            iv_profile = itemView.findViewById(R.id.iv_profile);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 0) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
