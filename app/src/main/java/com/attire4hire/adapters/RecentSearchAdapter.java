package com.attire4hire.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.model.ItemProductModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<String> mRecentItemsList;
    RecentSearchInterface mRecentSearchInterface;
    String name = "";

    public interface RecentSearchInterface {
        void searchInterface(int position, String Name);
    }

    public RecentSearchAdapter(Context mContext, ArrayList<String> mRecentItemsList, RecentSearchInterface mRecentSearchInterface) {
        this.mContext = mContext;
        this.mRecentItemsList = mRecentItemsList;
        this.mRecentSearchInterface = mRecentSearchInterface;
    }

    @NonNull
    @Override
    public RecentSearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent_search_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tv_recent.setText(mRecentItemsList.get(position));

        holder.tv_recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = mRecentItemsList.get(position);
                mRecentSearchInterface.searchInterface(position, name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRecentItemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_recent;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_recent = itemView.findViewById(R.id.tv_recent);
        }
    }

    public void filterList(ArrayList<String> filterdNames) {
        this.mRecentItemsList = filterdNames;
        notifyDataSetChanged();
    }
}
