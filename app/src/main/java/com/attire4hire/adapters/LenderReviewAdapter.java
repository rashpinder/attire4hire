package com.attire4hire.adapters;

import static com.facebook.login.widget.ProfilePictureView.TAG;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ShowLenderActivity;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class LenderReviewAdapter extends RecyclerView.Adapter<LenderReviewAdapter.MyViewHolder> {
    Context context;
    ArrayList<ItemProductModel> mItemProductModel;
    PaginationInquiriesInterface mInterfaceData;

    public LenderReviewAdapter(Context context, ArrayList<ItemProductModel> mItemProductModel, PaginationInquiriesInterface mInterfaceData) {
        this.context = context;
        this.mItemProductModel = mItemProductModel;
        this.mInterfaceData = mInterfaceData;
    }

    @NonNull
    @Override
    public LenderReviewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ItemProductModel itemProductModel = mItemProductModel.get(position);

        if (position >= mItemProductModel.size() - 1) {
            mInterfaceData.mPaginationInquiriesInterface(true);
        }

        holder.nameTV.setText(CapitalizedFirstLetter(itemProductModel.getName()));
        holder.dateTV.setText(itemProductModel.getCreation_date());
        Log.i(TAG, "onBindViewHolder: "+itemProductModel.getCreation_date());
        holder.msgTV.setText(CapitalizedFirstLetter(itemProductModel.getComments()));

        if (itemProductModel.getImage1() != null && itemProductModel.getImage1().contains("http")) {

            Glide.with(context).load(itemProductModel.getImage1())
                    .into(holder.iv_profile);

//            if (itemProductModel.getImage1().contains("https")) {
//                Glide.with(context).load(itemProductModel.getImage1())
//                        .into(holder.iv_profile);
//            } else {
//                Glide.with(context).load(itemProductModel.getImage1().replace("http://", "https://"))
//                        .into(holder.iv_profile);
//            }
        }

        if (itemProductModel.getOverall_exp() != null && itemProductModel.getOverall_exp().length() > 0)
            holder.ratingbarRB.setRating(Float.parseFloat(itemProductModel.getOverall_exp()));

        holder.reviewLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ShowLenderActivity.class).putExtra(ConstantData.MODEL, itemProductModel));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_profile;
        TextView nameTV, dateTV, tv_show_more;
        ReadMoreTextView msgTV;
        RatingBar ratingbarRB;
        LinearLayout reviewLL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_profile = itemView.findViewById(R.id.iv_profile);
            nameTV = itemView.findViewById(R.id.nameTV);
            dateTV = itemView.findViewById(R.id.sizecolorTV);
            msgTV = itemView.findViewById(R.id.msgTV);
            tv_show_more = itemView.findViewById(R.id.tv_show_more);
            ratingbarRB = itemView.findViewById(R.id.ratingbarRB);
            reviewLL = itemView.findViewById(R.id.reviewLL);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 0) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
