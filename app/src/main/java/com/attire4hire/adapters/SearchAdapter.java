package com.attire4hire.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.interfaces.FavouriteCallBack;
import com.attire4hire.interfaces.PaginationSearchInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Random;

import retrofit2.http.Url;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
    Typeface typeface;
    Context mContext;
    ArrayList<ItemProductModel> mItemProductModel;
    FavouriteCallBack favouriteCallBack;
    ArrayList<String> FavouriteItems = new ArrayList<>();
    String Str_Fav = "";
    PaginationSearchInterface mInterfaceData;

    public SearchAdapter(Context mContext, ArrayList<ItemProductModel> mItemProductModel, FavouriteCallBack favouriteCallBack, PaginationSearchInterface mInterfaceData) {
        this.mContext = mContext;
        this.mItemProductModel = mItemProductModel;
        this.favouriteCallBack = favouriteCallBack;
        this.mInterfaceData = mInterfaceData;
    }

    @NonNull
    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list, parent, false);
        typeface = Typeface.createFromAsset(itemView.getContext().getAssets(), "BaskVill.ttf");

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.MyViewHolder holder, int position) {
        holder.itemCardViewCV.setCardBackgroundColor(getRandomColor());

        if (position >= mItemProductModel.size() - 1) {
            mInterfaceData.mPaginationInterface(true);
        }
        ItemProductModel ItemProductModelList = mItemProductModel.get(position);

        if (ItemProductModelList.getImage1() != null && ItemProductModelList.getImage1().contains("http")) {

            Glide.with(mContext).load(ItemProductModelList.getImage1())
                    .into(holder.itemImageRIV);

//            if (ItemProductModelList.getImage1().contains("https")){
//                Glide.with(mContext).load(ItemProductModelList.getImage1())
//                        .into(holder.itemImageRIV);
//            }else {
//                Glide.with(mContext).load(ItemProductModelList.getImage1().replace("http://","https://"))
//                        .into(holder.itemImageRIV);
//            }


        }

        if (ItemProductModelList.getBrand_name() != null && ItemProductModelList.getBrand_name().length() > 0) {
            String text = ItemProductModelList.getBrand_name();
//            if (text.length() > 11) {
//                text = text.substring(0, 11) + "...";
//            }
            holder.txtTypeTV.setText(text);
        }

        if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0)
            holder.txtBrandDesignNameTV.setText(CapitalizedFirstLetter(ItemProductModelList.getName()));

        if (ItemProductModelList.getWeek_4days() != null && ItemProductModelList.getWeek_4days().length() > 0) {
            String strDay4 = String.format("%.2f", Double.parseDouble(ItemProductModelList.getWeek_4days()));
            holder.txtSizeTV.setText("£" + strDay4);
        }

        if (ItemProductModelList.getSize_name() != null && ItemProductModelList.getSize_name().length() > 0)
            holder.txtPriceTV.setText(ItemProductModelList.getSize_name());

        if (ItemProductModelList.getRatings() != null && ItemProductModelList.getRatings().length() > 0)
            holder.mRatingbarRB.setRating(Float.parseFloat(ItemProductModelList.getRatings()));

        holder.mItemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ItemDetailsActivity.class);
                intent.putExtra(ConstantData.TOKEN, "Search");
                intent.putExtra(ConstantData.MODEL, ItemProductModelList);
                mContext.startActivity(intent);
            }
        });

        if (ItemProductModelList.getFavourite().equals("2")) {
            holder.imgFavoriteIV.setImageResource(R.drawable.ic_heart_sel);
            FavouriteItems.add(String.valueOf(position));
        } else {
            holder.imgFavoriteIV.setImageResource(R.drawable.ic_fav_tb);
            FavouriteItems.remove(String.valueOf(position));
        }

        holder.imgFavoriteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FavouriteItems.contains(String.valueOf(position))) {
                    FavouriteItems.remove(String.valueOf(position));
                    holder.imgFavoriteIV.setImageResource(R.drawable.ic_fav_tb);
                    Str_Fav = "1";
                    favouriteCallBack.favouriteCall(position, Str_Fav);
                } else {
                    FavouriteItems.add(String.valueOf(position));
                    holder.imgFavoriteIV.setImageResource(R.drawable.ic_heart_sel);
                    Str_Fav = "2";
                    favouriteCallBack.favouriteCall(position, Str_Fav);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mItemLL;
        CardView itemCardViewCV;
        TextView txtBrandDesignNameTV, txtPriceTV, txtSizeTV, txtTypeTV;
        ImageView itemImageRIV, imgFavoriteIV;
        RatingBar mRatingbarRB;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mItemLL = itemView.findViewById(R.id.ll_item);
            itemCardViewCV = itemView.findViewById(R.id.itemCardViewCV);
            txtBrandDesignNameTV = itemView.findViewById(R.id.txtBrandDesignNameTV);
            itemImageRIV = itemView.findViewById(R.id.itemImageRIV);
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            txtSizeTV = itemView.findViewById(R.id.txtSizeTV);
            txtTypeTV = itemView.findViewById(R.id.txtTypeTV);
            imgFavoriteIV = itemView.findViewById(R.id.imgFavoriteIV);
            mRatingbarRB = itemView.findViewById(R.id.mRatingbarRB);
        }
    }

    private int getRandomColor() {
        int[] mColorsArray = mContext.getResources().getIntArray(R.array.androidcolors);

        return mColorsArray[new Random().nextInt(mColorsArray.length)];
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
//        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1).toLowerCase();
        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
        return upperString;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
