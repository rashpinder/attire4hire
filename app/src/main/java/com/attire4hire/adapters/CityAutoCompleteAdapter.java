package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.attire4hire.R;
import com.attire4hire.model.CityAutoCompleteModel;

import java.util.ArrayList;
import java.util.Arrays;

public class CityAutoCompleteAdapter extends BaseAdapter implements Filterable {

    Context context;

    private final Object mLock = new Object();
    private ArrayList<CityAutoCompleteModel> fullList;
    private ArrayList<CityAutoCompleteModel> mOriginalValues;
    private ArrayFilter mFilter;

    TextView txtCityNameTV, txtCountryNameTV;

    private LayoutInflater mInflater;

    public CityAutoCompleteAdapter(Context context, ArrayList<CityAutoCompleteModel> fullList) {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.fullList = fullList;
    }

    @Override
    public int getCount() {
        return fullList.size();
    }

    @Override
    public CityAutoCompleteModel getItem(int position) {
        return fullList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View rowView =  inflater.inflate(R.layout.item_city_auto_complete, parent, false);
        CityAutoCompleteModel mModel = fullList.get(position);
        txtCityNameTV = (TextView) rowView.findViewById(R.id.txtCityNameTV);
        txtCountryNameTV = (TextView) rowView.findViewById(R.id.txtCountryNameTV);
        txtCityNameTV.setText(getItem(position).getName());
        txtCountryNameTV.setText(getItem(position).getCountry());

        return rowView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    //filter items which does not contain typed text
    private class ArrayFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {

            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<CityAutoCompleteModel>(fullList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                final ArrayList<CityAutoCompleteModel> list;
                synchronized (mLock) {
                    list = new ArrayList<CityAutoCompleteModel>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                final ArrayList<CityAutoCompleteModel> values;

                synchronized (mLock) {
                    values = new ArrayList<CityAutoCompleteModel>(mOriginalValues);
                }
                results.values = values;
                results.count = values.size();

                final int count = values.size();

                final ArrayList<CityAutoCompleteModel> newValues = new ArrayList<CityAutoCompleteModel>();

                for (int i = 0; i < count; i++) {
                    CityAutoCompleteModel item = values.get(i);
                    if (item.getName().toLowerCase().startsWith(prefixString)) {
                        newValues.add(item);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            fullList = (ArrayList<CityAutoCompleteModel>) results.values;

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}