package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.AddItemActivity;
import com.attire4hire.interfaces.DraftCallBacks;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;


public class DraftAdapter extends RecyclerView.Adapter<DraftAdapter.MyViewHolder> {
    private Activity mActivity;
    private ArrayList<ItemProductModel> mArrayList;
    DraftCallBacks mDraftCallBacks;

    public DraftAdapter(Activity mActivity, ArrayList<ItemProductModel> mArrayList, DraftCallBacks mDraftCallBacks) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mDraftCallBacks = mDraftCallBacks;
    }

    @NonNull
    @Override
    public DraftAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_draft_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ItemProductModel mModel = mArrayList.get(position);

        if (mModel.getImage1() != null && mModel.getImage1().contains("http")) {
            Glide.with(mActivity).load(mModel.getImage1())
                    .placeholder(R.drawable.placeholder_img)
                    .error(R.drawable.placeholder_img)
                    .into(holder.imgItemImageIV);

//            if (mModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(mModel.getImage1())
//                        .placeholder(R.drawable.placeholder_img)
//                        .error(R.drawable.placeholder_img)
//                        .into(holder.imgItemImageIV);
//            } else {
//                Glide.with(mActivity).load(mModel.getImage1().replace("http://", "https://"))
//                        .placeholder(R.drawable.placeholder_img)
//                        .error(R.drawable.placeholder_img)
//                        .into(holder.imgItemImageIV);
//            }
        }

        if (mModel.getName() != null && !mModel.getName().equals(""))
            holder.txtNameTV.setText(CapitalizedFirstLetter(mModel.getName()));
        if (mModel.getRetail_price() != null)
            holder.txtItemPriceTV.setText("£" + mModel.getRetail_price());
        if (mModel.getSize_name() != null && mModel.getColor_name() != null)
            holder.txtSizeAndColorTV.setText("Size:" + mModel.getSize_name() + "|" + "Color:" + mModel.getColor_name());

        holder.rlEditRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddItemActivity.class);
                mIntent.putExtra(ConstantData.MODEL, mModel);
                mIntent.putExtra(ConstantData.ADD_ITEM_TYPE, ConstantData.ACTIVITY_DRAFT);
                mActivity.startActivity(mIntent);
            }
        });
        holder.rlDeleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDraftCallBacks.draftDelete(mModel, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtNameTV, txtItemPriceTV, txtSizeAndColorTV;
        RoundedImageView imgItemImageIV;
        RelativeLayout rlEditRL, rlDeleteRL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNameTV = itemView.findViewById(R.id.txtNameTV);
            txtItemPriceTV = itemView.findViewById(R.id.txtItemPriceTV);
            txtSizeAndColorTV = itemView.findViewById(R.id.txtSizeAndColorTV);
            imgItemImageIV = itemView.findViewById(R.id.imgItemImageIV);
            rlEditRL = itemView.findViewById(R.id.rlEditRL);
            rlDeleteRL = itemView.findViewById(R.id.rlDeleteRL);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
//        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1).toLowerCase();
        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
        return upperString;
    }
}
