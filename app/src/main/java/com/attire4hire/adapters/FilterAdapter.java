package com.attire4hire.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.interfaces.FilterCallBack;
import com.attire4hire.model.FiltersModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {
    private Typeface typeface;
    private Context context;
    private ArrayList<FiltersModel> mFiltersModel;
    private String Category = "", Type = "", Brand = "", Color = "", Condition = "", Size = "", Occasion = "";
    private FilterCallBack mFilterCallBack;
    private ArrayList<String> SelectedItems = new ArrayList<>();
    private ArrayList<String> CategoryList = new ArrayList<>();
    private ArrayList<String> TypeList = new ArrayList<>();
    private ArrayList<String> BrandList = new ArrayList<>();
    private ArrayList<String> ColorList = new ArrayList<>();
    private ArrayList<String> ConditionList = new ArrayList<>();
    private ArrayList<String> SizeList = new ArrayList<>();
    private ArrayList<String> OccasionList = new ArrayList<>();

    public FilterAdapter(Context context, ArrayList<FiltersModel> mFiltersModel, FilterCallBack mFilterCallBack) {
        this.context = context;
        this.mFiltersModel = mFiltersModel;
        this.mFilterCallBack = mFilterCallBack;
    }

    @NonNull
    @Override
    public FilterAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_filter, parent, false);
        typeface = Typeface.createFromAsset(itemView.getContext().getAssets(), "BaskVill.ttf");

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterAdapter.MyViewHolder holder, int position) {

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Size")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getSize());

            if (!Attire4hirePrefrences.readString(context, ConstantData.SIZE, "").equals("")) {
                Size = Attire4hirePrefrences.readString(context, ConstantData.SIZE, "");

                //convert string to arraylist
                String[] elements = Size.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                SizeList = new ArrayList<String>(fixedLenghtList);

                if (SizeList.contains(mFiltersModel.get(position).getSize())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Size = Attire4hirePrefrences.readString(context, ConstantData.SIZE, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Condition")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getCondition());

            if (!Attire4hirePrefrences.readString(context, ConstantData.CONDITION, "").equals("")) {
                Condition = Attire4hirePrefrences.readString(context, ConstantData.CONDITION, "");

                //convert string to arraylist
                String[] elements = Condition.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                ConditionList = new ArrayList<String>(fixedLenghtList);

                if (ConditionList.contains(mFiltersModel.get(position).getCondition())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Condition = Attire4hirePrefrences.readString(context, ConstantData.CONDITION, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Color")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getColour());

            if (!Attire4hirePrefrences.readString(context, ConstantData.COLOUR, "").equals("")) {
                Color = Attire4hirePrefrences.readString(context, ConstantData.COLOUR, "");

                //convert string to arraylist
                String[] elements = Color.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                ColorList = new ArrayList<String>(fixedLenghtList);

                if (ColorList.contains(mFiltersModel.get(position).getColour())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Color = Attire4hirePrefrences.readString(context, ConstantData.COLOUR, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Brand")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getBrand());

            if (!Attire4hirePrefrences.readString(context, ConstantData.BRAND, "").equals("")) {
                Brand = Attire4hirePrefrences.readString(context, ConstantData.BRAND, "");

                //convert string to arraylist
                String[] elements = Brand.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                BrandList = new ArrayList<String>(fixedLenghtList);

                if (BrandList.contains(mFiltersModel.get(position).getBrand())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Brand = Attire4hirePrefrences.readString(context, ConstantData.BRAND, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ItemCategory")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getItemCategory());

            if (!Attire4hirePrefrences.readString(context, ConstantData.ITEM_CATEGORY, "").equals("")) {
                Category = Attire4hirePrefrences.readString(context, ConstantData.ITEM_CATEGORY, "");

                //convert string to arraylist
                String[] elements = Category.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                CategoryList = new ArrayList<String>(fixedLenghtList);

                if (CategoryList.contains(mFiltersModel.get(position).getItemCategory())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Category = Attire4hirePrefrences.readString(context, ConstantData.ITEM_CATEGORY, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ProductType")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getProductType());

            if (!Attire4hirePrefrences.readString(context, ConstantData.PRODUCT_TYPE, "").equals("")) {
                Type = Attire4hirePrefrences.readString(context, ConstantData.PRODUCT_TYPE, "");

                //convert string to arraylist
                String[] elements = Type.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                TypeList = new ArrayList<String>(fixedLenghtList);

                if (TypeList.contains(mFiltersModel.get(position).getProductType())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Type = Attire4hirePrefrences.readString(context, ConstantData.PRODUCT_TYPE, "");
        }

        if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Occasion")) {
            holder.tv_filter.setText(mFiltersModel.get(position).getOccasion());

            if (!Attire4hirePrefrences.readString(context, ConstantData.OCCASION, "").equals("")) {
                Occasion = Attire4hirePrefrences.readString(context, ConstantData.OCCASION, "");

                //convert string to arraylist
                String[] elements = Occasion.split(",");
                List<String> fixedLenghtList = Arrays.asList(elements);
                OccasionList = new ArrayList<String>(fixedLenghtList);

                if (OccasionList.contains(mFiltersModel.get(position).getOccasion())) {
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);
                    SelectedItems.add(String.valueOf(position));
                } else {
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
                    SelectedItems.remove(String.valueOf(position));
                }
            }

        } else {
            Occasion = Attire4hirePrefrences.readString(context, ConstantData.OCCASION, "");
        }

        holder.rl_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SelectedItems.contains(String.valueOf(position))) {
                    SelectedItems.remove(String.valueOf(position));
                    holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Size")) {
                        SizeList.remove(mFiltersModel.get(position).getSize());
                        //convert arraylist to string
                        Size = TextUtils.join(",", SizeList);
                        Attire4hirePrefrences.writeString(context, ConstantData.SIZE, Size);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Condition")) {
                        ConditionList.remove(mFiltersModel.get(position).getCondition());
                        //convert arraylist to string
                        Condition = TextUtils.join(",", ConditionList);
                        Attire4hirePrefrences.writeString(context, ConstantData.CONDITION, Condition);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Color")) {
                        ColorList.remove(mFiltersModel.get(position).getColour());
                        //convert arraylist to string
                        Color = TextUtils.join(",", ColorList);
                        Attire4hirePrefrences.writeString(context, ConstantData.COLOUR, Color);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Brand")) {
                        BrandList.remove(mFiltersModel.get(position).getBrand());
                        //convert arraylist to string
                        Brand = TextUtils.join(",", BrandList);
                        Attire4hirePrefrences.writeString(context, ConstantData.BRAND, Brand);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ItemCategory")) {
                        CategoryList.remove(mFiltersModel.get(position).getItemCategory());
                        //convert arraylist to string
                        Category = TextUtils.join(",", CategoryList);
                        Attire4hirePrefrences.writeString(context, ConstantData.ITEM_CATEGORY, Category);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ProductType")) {
                        TypeList.remove(mFiltersModel.get(position).getProductType());
                        //convert arraylist to string
                        Type = TextUtils.join(",", TypeList);
                        Attire4hirePrefrences.writeString(context, ConstantData.PRODUCT_TYPE, Type);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Occasion")) {
                        OccasionList.remove(mFiltersModel.get(position).getOccasion());
                        //convert arraylist to string
                        Occasion = TextUtils.join(",", OccasionList);
                        Attire4hirePrefrences.writeString(context, ConstantData.OCCASION, Occasion);
                    }

                } else {
                    SelectedItems.add(String.valueOf(position));
                    holder.Iv_filter.setImageResource(R.drawable.ic_check);

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Size")) {
                        SizeList.add(mFiltersModel.get(position).getSize());
                        //convert arraylist to string
                        Size = TextUtils.join(",", SizeList);
                        Attire4hirePrefrences.writeString(context, ConstantData.SIZE, Size);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Condition")) {
                        ConditionList.add(mFiltersModel.get(position).getCondition());
                        //convert arraylist to string
                        Condition = TextUtils.join(",", ConditionList);
                        Attire4hirePrefrences.writeString(context, ConstantData.CONDITION, Condition);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Color")) {
                        ColorList.add(mFiltersModel.get(position).getColour());
                        //convert arraylist to string
                        Color = TextUtils.join(",", ColorList);
                        Attire4hirePrefrences.writeString(context, ConstantData.COLOUR, Color);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Brand")) {
                        BrandList.add(mFiltersModel.get(position).getBrand());
                        //convert arraylist to string
                        Brand = TextUtils.join(",", BrandList);
                        Attire4hirePrefrences.writeString(context, ConstantData.BRAND, Brand);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ItemCategory")) {
                        CategoryList.add(mFiltersModel.get(position).getItemCategory());
                        //convert arraylist to string
                        Category = TextUtils.join(",", CategoryList);
                        Attire4hirePrefrences.writeString(context, ConstantData.ITEM_CATEGORY, Category);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("ProductType")) {
                        TypeList.add(mFiltersModel.get(position).getProductType());
                        //convert arraylist to string
                        Type = TextUtils.join(",", TypeList);
                        Attire4hirePrefrences.writeString(context, ConstantData.PRODUCT_TYPE, Type);
                    }

                    if (Attire4hirePrefrences.readString(context, ConstantData.FILTER_VALUE, "").equals("Occasion")) {
                        OccasionList.add(mFiltersModel.get(position).getOccasion());
                        //convert arraylist to string
                        Occasion = TextUtils.join(",", OccasionList);
                        Attire4hirePrefrences.writeString(context, ConstantData.OCCASION, Occasion);
                    }
                }
                mFilterCallBack.CallBack(position, Size, Condition, Color, Brand, Category, Type, Occasion);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFiltersModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_filter;
        RelativeLayout rl_filter;
        ImageView Iv_filter;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_filter = itemView.findViewById(R.id.tv_filter);
            Iv_filter = itemView.findViewById(R.id.Iv_filter);
            rl_filter = itemView.findViewById(R.id.rl_filter);
        }
    }
}
