package com.attire4hire.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ChatActivity;
import com.attire4hire.interfaces.DeleteConversationInterface;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.attire4hire.activity.BaseActivity.decodeEmoji;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    private Context context;
    ArrayList<ItemProductModel> mItemProductModel;
    PaginationInquiriesInterface paginationInquiriesInterface;
    private ArrayList<String> SelectedItems = new ArrayList<>();
    private ArrayList<Integer> SelectedPositions = new ArrayList<>();
    DeleteConversationInterface deleteConversationInterface;
    boolean isLongClick = false;

    public ChatAdapter(Context context, ArrayList<ItemProductModel> mItemProductModel, PaginationInquiriesInterface paginationInquiriesInterface,
                       DeleteConversationInterface deleteConversationInterface) {
        this.context = context;
        this.mItemProductModel = mItemProductModel;
        this.paginationInquiriesInterface = paginationInquiriesInterface;
        this.deleteConversationInterface = deleteConversationInterface;
    }

    @NonNull
    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (position >= mItemProductModel.size() - 1) {
            paginationInquiriesInterface.mPaginationInquiriesInterface(true);
        }

        ItemProductModel itemProductModel = mItemProductModel.get(position);

        holder.nameTV.setText(CapitalizedFirstLetter(itemProductModel.getAnother_User_Name()));
        holder.msgTV.setText(itemProductModel.getMessage());
        holder.timeTV.setText(itemProductModel.getCreation_date());

        if (itemProductModel.getAnother_ProfilePic() != null && itemProductModel.getAnother_ProfilePic().contains("http")) {
            Picasso.with(context).load(itemProductModel.getAnother_ProfilePic())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.iv_profile);
        } else {
            Picasso.with(context).load(R.drawable.ic_pp_ph).into(holder.iv_profile);
        }

        if (itemProductModel.getUnread_count() != null && !itemProductModel.getUnread_count().equals("0")) {
            holder.badge.setText(itemProductModel.getUnread_count());
            holder.badge.setVisibility(View.VISIBLE);
        } else {
            holder.badge.setVisibility(View.GONE);
        }

        holder.ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLongClick == false) {
                    context.startActivity(new Intent(context, ChatActivity.class).putExtra(ConstantData.MODEL, itemProductModel));
                }
                // for click agin on item
                isLongClick = false;
            }
        });

        holder.ll_chat.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                isLongClick = true;

                if (SelectedItems.contains(itemProductModel.getRoom_id())) {
                    SelectedItems.remove(itemProductModel.getRoom_id());
                    holder.selectedIV.setVisibility(View.GONE);
                    holder.mainRLayout.setBackgroundColor(Color.parseColor("#ffffff"));

                } else {
                    SelectedItems.add(itemProductModel.getRoom_id());
                    holder.selectedIV.setVisibility(View.VISIBLE);
                    holder.mainRLayout.setBackgroundColor(Color.parseColor("#f0f0f0"));
                }
                deleteConversationInterface.mDeleteConversationInterface(position, itemProductModel.getRoom_id(), SelectedItems);

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_chat;
        RelativeLayout mainRLayout;
        ImageView iv_profile, selectedIV;
        TextView nameTV, msgTV, timeTV, badge;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ll_chat = itemView.findViewById(R.id.ll_chat);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            nameTV = itemView.findViewById(R.id.nameTV);
            msgTV = itemView.findViewById(R.id.msgTV);
            timeTV = itemView.findViewById(R.id.timeTV);
            badge = itemView.findViewById(R.id.badge);
            mainRLayout = itemView.findViewById(R.id.mainRLayout);
            selectedIV = itemView.findViewById(R.id.selectedIV);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        String output = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
        return output;
    }
}
