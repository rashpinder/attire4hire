package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.model.CityAutoCompleteModel;
import com.attire4hire.model.SpinnerModel;

import java.util.ArrayList;

public class BrandAutoCompleteAdapter extends BaseAdapter implements Filterable {

    Context context;

    private final Object mLock = new Object();
    private ArrayList<SpinnerModel> fullList;
    private ArrayList<SpinnerModel> mOriginalValues;
    private BrandAutoCompleteAdapter.ArrayFilter mFilter;

    TextView txtBrandNameTV;

    private LayoutInflater mInflater;

    public BrandAutoCompleteAdapter(Context context, ArrayList<SpinnerModel> fullList) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.fullList = fullList;
    }

    @Override
    public int getCount() {
        return fullList.size();
    }

    @Override
    public SpinnerModel getItem(int position) {
        return fullList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.item_brand_auto_complete, parent, false);
        SpinnerModel mModel = fullList.get(position);
        txtBrandNameTV = (TextView) rowView.findViewById(R.id.txtBrandNameTV);
        txtBrandNameTV.setText(getItem(position).getName());

        return rowView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new BrandAutoCompleteAdapter.ArrayFilter();
        }
        return mFilter;
    }

    //filter items which does not contain typed text
    private class ArrayFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {

            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<SpinnerModel>(fullList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                final ArrayList<SpinnerModel> list;
                synchronized (mLock) {
                    list = new ArrayList<SpinnerModel>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                final ArrayList<SpinnerModel> values;

                synchronized (mLock) {
                    values = new ArrayList<SpinnerModel>(mOriginalValues);
                }
                results.values = values;
                results.count = values.size();

                final int count = values.size();

                final ArrayList<SpinnerModel> newValues = new ArrayList<SpinnerModel>();

                for (int i = 0; i < count; i++) {
                    SpinnerModel item = values.get(i);
                    if (item.getName().toLowerCase().contains(prefixString)) {
                        newValues.add(item);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            fullList = (ArrayList<SpinnerModel>) results.values;

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
