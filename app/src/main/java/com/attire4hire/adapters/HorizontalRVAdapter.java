package com.attire4hire.adapters;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;

import java.util.ArrayList;

public class HorizontalRVAdapter extends RecyclerView.Adapter<HorizontalRVAdapter.MyViewHolder> {
    private Context context;
    private OccasionDateInterface mOccasionDateInterface;
    private ArrayList<String> filtersList;
    private long mLastClickTab1 = 0;

    public interface OccasionDateInterface {
        void Occasion(int position);

        void Filters(int position);

        void Location(int position);

        void PerformersCorner(int position, TextView mListItemTV);

        void BackToHome(int position, TextView mListItemTV);
    }

    public HorizontalRVAdapter(Context context, OccasionDateInterface mOccasionDateInterface, ArrayList<String> filtersList) {
        this.context = context;
        this.mOccasionDateInterface = mOccasionDateInterface;
        this.filtersList = filtersList;
    }

    @NonNull
    @Override
    public HorizontalRVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_horizontal, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.mListItemTV.setText(filtersList.get(position));

        if (position == 0) {
            holder.mFilter.setVisibility(View.VISIBLE);
        } else {
            holder.mFilter.setVisibility(View.GONE);
        }

        if (filtersList.get(position).equalsIgnoreCase("Performers' Corner") ||
                filtersList.get(position).equalsIgnoreCase("Back to Home")) {
            holder.ll_fil.setBackgroundResource(R.drawable.bg_rec_black);
        } else {
            holder.ll_fil.setBackgroundResource(R.drawable.bg_rec);
        }

        holder.ll_fil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
                    return;
                }
                mLastClickTab1 = SystemClock.elapsedRealtime();

                if (position == 0) {
                    mOccasionDateInterface.Filters(position);
                }
                if (filtersList.get(position).equalsIgnoreCase("Occasion Date")) {
                    mOccasionDateInterface.Occasion(position);
                }
//                if (filtersList.get(position).equalsIgnoreCase("Search by my Location")) {
//                    mOccasionDateInterface.Location(position);
//                }
                if (filtersList.get(position).equalsIgnoreCase("Performers' Corner")) {
                    mOccasionDateInterface.PerformersCorner(position, holder.mListItemTV);
                }
                if (filtersList.get(position).equalsIgnoreCase("Back to Home")) {
                    mOccasionDateInterface.BackToHome(position, holder.mListItemTV);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filtersList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mFilter;
        TextView mListItemTV;
        LinearLayout ll_fil;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mFilter = itemView.findViewById(R.id.iv_filter);
            ll_fil = itemView.findViewById(R.id.ll_fil);
            mListItemTV = itemView.findViewById(R.id.list_item);
        }
    }
}
