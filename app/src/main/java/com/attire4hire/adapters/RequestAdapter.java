package com.attire4hire.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.AnotherUserActivity;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.interfaces.DeleteRequestsInterface;
import com.attire4hire.interfaces.PaginationRequestInterface;
import com.attire4hire.interfaces.RequestStatusCallBack;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.MyViewHolder> {
    Context mContext;
    ArrayList<ItemProductModel> mItemProductModel;
    RequestStatusCallBack requestStatusCallBack;
    PaginationRequestInterface paginationRequestInterface;
    DeleteRequestsInterface deleteRequestsInterface;
    boolean isLongClick = false;
    private int row_index;
    private int count = 0;
    String startDate = "", endDate = "";

    public RequestAdapter(Context mContext, ArrayList<ItemProductModel> mItemProductModel, RequestStatusCallBack requestStatusCallBack,
                          PaginationRequestInterface paginationRequestInterface, DeleteRequestsInterface deleteRequestsInterface) {
        this.mContext = mContext;
        this.mItemProductModel = mItemProductModel;
        this.requestStatusCallBack = requestStatusCallBack;
        this.paginationRequestInterface = paginationRequestInterface;
        this.deleteRequestsInterface = deleteRequestsInterface;
    }

    @NonNull
    @Override
    public RequestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_request_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (position >= mItemProductModel.size() - 1) {
            paginationRequestInterface.mPaginationRequestInterface(true);
        }

        ItemProductModel ItemProductModelList = mItemProductModel.get(position);

        if (ItemProductModelList.getImage1() != null && ItemProductModelList.getImage1().contains("http")) {
            Glide.with(mContext).load(mItemProductModel.get(position).getImage1())
                    .into(holder.iv_profile);

//            if (ItemProductModelList.getImage1().contains("https")) {
//                Glide.with(mContext).load(mItemProductModel.get(position).getImage1())
//                        .into(holder.iv_profile);
//            } else {
//                Glide.with(mContext).load(ItemProductModelList.getImage1().replace("http://", "https://"))
//                        .into(holder.iv_profile);
//            }
        }

        if (ItemProductModelList.getCost() != null && !ItemProductModelList.getCost().equals("")) {
            holder.costTv.setText("Cost: " + "£" + ItemProductModelList.getCost());
        }

        if (ItemProductModelList.getStart_date() != null && !ItemProductModelList.getStart_date().equals("")) {
            startDate = ItemProductModelList.getStart_date();
        }

        if (ItemProductModelList.getEnd_date() != null && !ItemProductModelList.getEnd_date().equals("")) {
            endDate = ItemProductModelList.getEnd_date();
        }

        if (ItemProductModelList.getRead_status() != null && ItemProductModelList.getRead_status().equals("false")) {
            holder.requestLL.setBackgroundColor(Color.parseColor("#f0f0f0"));
        } else {
            holder.requestLL.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        holder.datesTv.setText("Dates: " + startDate + " - " + endDate);

        if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
            holder.datesTv.setVisibility(View.VISIBLE);
        } else {
            holder.datesTv.setVisibility(View.GONE);
        }

        if (ItemProductModelList.getName() != null && ItemProductModelList.getName().length() > 0) {

            String name = CapitalizedFirstLetter(ItemProductModelList.getAnother_User_Name());

            if (ItemProductModelList.getExtension().equals("1")) {
                holder.tv_request.setText(Html.fromHtml("<font color=\"#b7006d\">" + name + "</font>" +
                        " has made a request to extend the rental of" + " your " + CapitalizedFirstLetter(ItemProductModelList.getName()) + "."));
            } else {
                holder.tv_request.setText(Html.fromHtml("<font color=\"#b7006d\">" + name + "</font>" +
                        " has made a request to "
                        + ItemProductModelList.getRequest_type() + " your " + CapitalizedFirstLetter(ItemProductModelList.getName()) + "."));
            }
        }

        if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Pending")) {
            holder.tv_accepted.setVisibility(View.GONE);
            holder.tv_declined.setVisibility(View.GONE);
            holder.tv_cancelled.setVisibility(View.GONE);
            holder.tv_accept.setVisibility(View.VISIBLE);
            holder.tv_decline.setVisibility(View.VISIBLE);

            holder.detailsll.setVisibility(View.VISIBLE);
        } else if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Declined")) {
            holder.tv_accepted.setVisibility(View.GONE);
            holder.tv_cancelled.setVisibility(View.GONE);
            holder.tv_declined.setVisibility(View.VISIBLE);
            holder.tv_accept.setVisibility(View.GONE);
            holder.tv_decline.setVisibility(View.GONE);

            holder.detailsll.setVisibility(View.GONE);
        } else if (ItemProductModelList.getRequest_status() != null && ItemProductModelList.getRequest_status().equalsIgnoreCase("Cancelled")) {
            holder.tv_accepted.setVisibility(View.GONE);
            holder.tv_declined.setVisibility(View.GONE);
            holder.tv_accept.setVisibility(View.GONE);
            holder.tv_decline.setVisibility(View.GONE);
            holder.tv_cancelled.setVisibility(View.VISIBLE);

            holder.detailsll.setVisibility(View.GONE);
        }

        holder.tv_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLongClick == false) {
                    mContext.startActivity(new Intent(mContext, AnotherUserActivity.class).putExtra(Attire4hirePrefrences.ANOTHER_USER_ID, mItemProductModel.get(position).getAnother_User_Id()));
                }
                // for click agin on item
                isLongClick = false;
            }
        });

        holder.iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ItemDetailsActivity.class)
                        .putExtra(ConstantData.MODEL, ItemProductModelList)
                        .putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE));
            }
        });

        holder.requestLL.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                isLongClick = true;

                String strStatus = mItemProductModel.get(position).getRequest_status();

                if (strStatus.equals("Cancelled")) {
                    row_index = position;
                    count = 1;
                    notifyDataSetChanged();
                    deleteRequestsInterface.mDeleteRequestsInterface(position, ItemProductModelList.getRequest_id(),
                            holder.requestLL);
                }
                return true;
            }
        });

        holder.tv_request.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                isLongClick = true;

                String strStatus = mItemProductModel.get(position).getRequest_status();

                if (strStatus.equals("Cancelled") || strStatus.equals("Declined")) {
                    row_index = position;
                    count = 1;
                    notifyDataSetChanged();
                    deleteRequestsInterface.mDeleteRequestsInterface(position, ItemProductModelList.getRequest_id(),
                            holder.requestLL);
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView iv_profile;
        TextviewRegular tv_request, tv_accept, tv_decline, tv_accepted, tv_declined, tv_cancelled, datesTv, costTv;
        LinearLayout requestLL, mainLL, detailsll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_profile = itemView.findViewById(R.id.iv_profile);
            tv_request = itemView.findViewById(R.id.tv_request);
            tv_accept = itemView.findViewById(R.id.tv_accept);
            tv_decline = itemView.findViewById(R.id.tv_decline);
            tv_accepted = itemView.findViewById(R.id.tv_accepted);
            tv_declined = itemView.findViewById(R.id.tv_declined);
            requestLL = itemView.findViewById(R.id.requestLL);
            mainLL = itemView.findViewById(R.id.mainLL);
            tv_cancelled = itemView.findViewById(R.id.tv_cancelled);
            datesTv = itemView.findViewById(R.id.datesTv);
            costTv = itemView.findViewById(R.id.costTv);
            detailsll = itemView.findViewById(R.id.detailsll);

            tv_accept.setOnClickListener(this);
            tv_decline.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.tv_accept:
                    requestStatusCallBack.RequestCall(getLayoutPosition(), "1", tv_accept, tv_decline, tv_declined);
                    break;

                case R.id.tv_decline:
                    requestStatusCallBack.RequestCall(getLayoutPosition(), "2", tv_accept, tv_decline, tv_declined);
                    break;
            }
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 1) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
