package com.attire4hire.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import java.util.ArrayList;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<ItemProductModel> mItemProductModel;
    private int row_index;
    private int count = 0;
    AddCard mAddCard;

    public interface AddCard {
        void addcard(int position, String card_id);
    }

    public CardAdapter(Context mContext, ArrayList<ItemProductModel> mItemProductModel, AddCard mAddCard) {
        this.mContext = mContext;
        this.mItemProductModel = mItemProductModel;
        this.mAddCard = mAddCard;
    }

    @NonNull
    @Override
    public CardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardAdapter.MyViewHolder holder, int position) {

        if (mItemProductModel.get(position).getLast4() != null) {
            holder.tv_cardNumber.setText("ending in ************" + mItemProductModel.get(position).getLast4());
        }

        if (mItemProductModel.get(position).getCardBrand().equals("Visa")) {
            holder.iv_visa_card.setImageResource(R.drawable.ic_visa);
        } else if (mItemProductModel.get(position).getCardBrand().equals("MasterCard")) {
            holder.iv_visa_card.setImageResource(R.drawable.ic_master);
        }

        holder.tv_cardNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                count = 1;
                notifyDataSetChanged();
                mAddCard.addcard(position, mItemProductModel.get(position).getCard_Id());
            }
        });

        if (row_index == position && count == 1) {
            holder.Iv_filter.setImageResource(R.drawable.ic_check);
        } else {
            holder.Iv_filter.setImageResource(R.drawable.ic_uncheck);
        }

        if (Attire4hirePrefrences.readString(mContext, ConstantData.PAYMENT_VALUE, "").equals("Cart")) {
            holder.Iv_filter.setVisibility(View.VISIBLE);

            if (mItemProductModel.size() == 1) {
                holder.Iv_filter.setImageResource(R.drawable.ic_check);
                mAddCard.addcard(position, mItemProductModel.get(position).getCard_Id());
            }

        } else if (Attire4hirePrefrences.readString(mContext, ConstantData.PAYMENT_VALUE, "").equals("Settings")) {
            holder.Iv_filter.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextviewRegular tv_cardNumber;
        ImageView Iv_filter, iv_visa_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_cardNumber = itemView.findViewById(R.id.tv_cardNumber);
            Iv_filter = itemView.findViewById(R.id.Iv_filter);
            iv_visa_card = itemView.findViewById(R.id.iv_visa_card);
        }
    }
}
