package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ItemDetailsProfileActivity;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.Random;

public class ListingAdapter extends RecyclerView.Adapter<ListingAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ItemProductModel> mArrayList = new ArrayList<>();

    public ListingAdapter(Activity mActivity, ArrayList<ItemProductModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public ListingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ItemProductModel mModel = mArrayList.get(position);

        holder.itemCardViewCV.setCardBackgroundColor(getRandomColor());

        if (mModel.getImage1() != null && mModel.getImage1().contains("http")) {
            Glide.with(mActivity).load(mModel.getImage1())
                    .into(holder.itemImageRIV);

//            if (mModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(mModel.getImage1())
//                        .into(holder.itemImageRIV);
//            } else {
//                Glide.with(mActivity).load(mModel.getImage1().replace("http://", "https://"))
//                        .into(holder.itemImageRIV);
//            }
        }

        if (mModel.getBrand_name() != null && mModel.getBrand_name().length() > 0) {
            String text = mModel.getBrand_name();
//            if (text.length() > 11) {
//                text = text.substring(0, 11) + "...";
//            }
            holder.txtTypeTV.setText(text);
        }

        if (mModel.getName() != null && mModel.getName().length() > 0)
            holder.txtBrandDesignNameTV.setText(CapitalizedFirstLetter(mModel.getName()));

        if (mModel.getSize_name() != null && mModel.getSize_name().length() > 0)
            holder.txtPriceTV.setText(mModel.getSize_name());

        if (mModel.getWeek_4days() != null && mModel.getWeek_4days().length() > 0) {
            String strDay4 = String.format("%.2f", Double.parseDouble(mModel.getWeek_4days()));
            holder.txtSizeTV.setText("£" + strDay4);
        }

        if (mModel.getRatings() != null && mModel.getRatings().length() > 0)
            holder.mRatingbarRB.setRating(Float.parseFloat(mModel.getRatings()));

        holder.itemImageRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, ItemDetailsProfileActivity.class);
                mIntent.putExtra(ConstantData.TOKEN, "Listing");
                mIntent.putExtra(ConstantData.MODEL, mModel);
                mActivity.startActivity(mIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView itemImageRIV;
        ImageView imgFavoriteIV;
        TextView txtTypeTV, txtBrandDesignNameTV, txtSizeTV, txtPriceTV;
        RatingBar mRatingbarRB;
        CardView itemCardViewCV;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImageRIV = itemView.findViewById(R.id.itemImageRIV);
            imgFavoriteIV = itemView.findViewById(R.id.imgFavoriteIV);
            txtTypeTV = itemView.findViewById(R.id.txtTypeTV);
            txtBrandDesignNameTV = itemView.findViewById(R.id.txtBrandDesignNameTV);
            txtSizeTV = itemView.findViewById(R.id.txtSizeTV);
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            mRatingbarRB = itemView.findViewById(R.id.mRatingbarRB);
            itemCardViewCV = itemView.findViewById(R.id.itemCardViewCV);
        }
    }

    private int getRandomColor() {
        int[] mColorsArray = mActivity.getResources().getIntArray(R.array.androidcolors);
        return mColorsArray[new Random().nextInt(mColorsArray.length)];
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
//        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1).toLowerCase();
        String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
        return upperString;
    }
}
