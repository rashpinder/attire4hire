package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attire4hire.R;
import com.attire4hire.activity.ItemDetailsActivity;
import com.attire4hire.activity.OrderConfirmationActivity;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ConfirmedBookingAdapter extends RecyclerView.Adapter<ConfirmedBookingAdapter.MyViewHolder> {
    Activity mActivity;
    ArrayList<ItemProductModel> mItemProductModel;
    PaginationInquiriesInterface paginationInquiriesInterface;
    String ITEM_STATUS = "";
    String startDate = "", endDate = "";

    public ConfirmedBookingAdapter(Activity mActivity, ArrayList<ItemProductModel> mItemProductModel, PaginationInquiriesInterface paginationInquiriesInterface) {
        this.mActivity = mActivity;
        this.mItemProductModel = mItemProductModel;
        this.paginationInquiriesInterface = paginationInquiriesInterface;
    }

    @NonNull
    @Override
    public ConfirmedBookingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_confirmed_booking_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ItemProductModel mModel = mItemProductModel.get(position);

        if (position >= mItemProductModel.size() - 1) {
            paginationInquiriesInterface.mPaginationInquiriesInterface(true);
        }

        if (mModel.getRead_status() != null && mModel.getRead_status().equals("false")) {
            holder.mainLL.setBackgroundColor(Color.parseColor("#f0f0f0"));
        } else {
            holder.mainLL.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        holder.typeTV.setVisibility(View.VISIBLE);

        if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
            holder.datesTv.setVisibility(View.VISIBLE);
            holder.priceTV.setText("£" + mItemProductModel.get(position).getTotal_price());
            holder.nameTV.setText(CapitalizedFirstLetter(mItemProductModel.get(position).getName()));
            holder.typeTV.setText("Rental");

            if (mItemProductModel.get(position).getStatus().equals("1")) {
                holder.confirmedTV.setText("Item Booked");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("3")) {
                holder.confirmedTV.setText("Item Shipped");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("4")) {
                holder.confirmedTV.setText("Item Received");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("5")) {
                holder.confirmedTV.setText("Item Returned");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("6")) {
                holder.confirmedTV.setText("Rental Completed");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("7")) {
                holder.confirmedTV.setText("Booking Cancelled");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("0")) {
                holder.confirmedTV.setText("Pending Payment");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(false);
            }

        } else {
            holder.datesTv.setVisibility(View.GONE);
            holder.priceTV.setText("£" + mItemProductModel.get(position).getTotal_price());
            holder.nameTV.setText(CapitalizedFirstLetter(mItemProductModel.get(position).getName()));
            holder.typeTV.setText("Purchase");

            if (mItemProductModel.get(position).getStatus().equals("1")) {
                holder.confirmedTV.setText("Item Booked");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("2")) {
                holder.confirmedTV.setText("Item Bought");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("3")) {
                holder.confirmedTV.setText("Item Shipped");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("4")) {
                holder.confirmedTV.setText("Item Received");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("6")) {
                holder.confirmedTV.setText("Purchase Completed");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("7")) {
                holder.confirmedTV.setText("Booking Cancelled");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(true);
            } else if (mItemProductModel.get(position).getStatus().equals("0")) {
                holder.confirmedTV.setText("Pending Payment");
                holder.confirmedTV.setBackgroundResource(R.drawable.bg_rec_confirm);
                holder.confirmedTV.setTextColor(Color.parseColor("#ffffff"));
                holder.confirmedTV.setEnabled(false);
            }
        }

        holder.sizecolorTV.setText("Size: " + mItemProductModel.get(position).getSize_name() + " | " + "Colour: " + mItemProductModel.get(position).getColor_name());

        if (mItemProductModel.get(position).getCost() != null && !mItemProductModel.get(position).getCost().equals("")) {
            holder.costTv.setText("Cost: " + "£" + mItemProductModel.get(position).getCost());
        }

        if (mItemProductModel.get(position).getStart_date() != null && !mItemProductModel.get(position).getStart_date().equals("")) {
            startDate = mItemProductModel.get(position).getStart_date();
        }

        if (mItemProductModel.get(position).getEnd_date() != null && !mItemProductModel.get(position).getEnd_date().equals("")) {
            endDate = mItemProductModel.get(position).getEnd_date();
        }

        holder.datesTv.setText("Dates: " + startDate + " - " + endDate);

        if (mItemProductModel.get(position).getImage1() != null && mItemProductModel.get(position).getImage1().contains("http")) {
            Glide.with(mActivity).load(mItemProductModel.get(position).getImage1())
                    .into(holder.iv_profile);
//            if (mItemProductModel.get(position).getImage1().contains("https")) {
//                Glide.with(mActivity).load(mItemProductModel.get(position).getImage1())
//                        .into(holder.iv_profile);
//            } else {
//                Glide.with(mActivity).load(mItemProductModel.get(position).getImage1().replace("http://", "https://"))
//                        .into(holder.iv_profile);
//            }
        }

        holder.confirmedTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, OrderConfirmationActivity.class);
                intent.putExtra(ConstantData.MODEL, mModel);
                mActivity.startActivity(intent);
            }
        });

        holder.iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemProductModel.get(position).getRequest_type().equals("rent")) {
                    ITEM_STATUS = ConstantData.RENT;
                } else {
                    ITEM_STATUS = ConstantData.BUY;
                }

                mActivity.startActivity(new Intent(mActivity, ItemDetailsActivity.class)
                        .putExtra(ConstantData.MODEL, mModel)
                        .putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE)
                        .putExtra(ConstantData.ITEM_STATUS, ITEM_STATUS));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemProductModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, priceTV, sizecolorTV, confirmedTV, datesTv, costTv, typeTV;
        ImageView iv_profile;
        LinearLayout detailsll, mainLL;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTV = itemView.findViewById(R.id.nameTV);
            priceTV = itemView.findViewById(R.id.priceTV);
            sizecolorTV = itemView.findViewById(R.id.sizecolorTV);
            confirmedTV = itemView.findViewById(R.id.confirmedTV);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            datesTv = itemView.findViewById(R.id.datesTv);
            costTv = itemView.findViewById(R.id.costTv);
            typeTV = itemView.findViewById(R.id.typeTV);
            detailsll = itemView.findViewById(R.id.detailsll);
            mainLL = itemView.findViewById(R.id.mainLL);
        }
    }

    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String fullname) {
        if (fullname.length() > 1) {
            String upperString = fullname.substring(0, 1).toUpperCase() + fullname.substring(1);
            return upperString;
        } else {
            return fullname;
        }
    }
}
