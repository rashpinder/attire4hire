package com.attire4hire.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.attire4hire.R;
import com.attire4hire.activity.OpenImagesActivity;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class DetailsSlidingImageAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Activity context;
    private ArrayList<String> modelArrayList;

    public DetailsSlidingImageAdapter(Activity context, ArrayList<String> images) {
        this.context = context;
        this.modelArrayList = images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return this.modelArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_sliding_pages, view, false);

        assert imageLayout != null;

        ImageView imageView = imageLayout.findViewById(R.id.image);

        String mImageUrl = modelArrayList.get(position);

        if (mImageUrl.contains("http")) {
            Glide.with(context).load(mImageUrl)
                    .placeholder(null)
                    .error(R.drawable.placeholder_img)
                    .into(imageView);
        }

//        if (mImageUrl.contains("https")) {
//            Glide.with(context).load(mImageUrl)
//                    .placeholder(null)
//                    .error(R.drawable.placeholder_img)
//                    .into(imageView);
//        } else {
//            Glide.with(context).load(mImageUrl.replace("http://", "https://"))
//                    .placeholder(null)
//                    .error(R.drawable.placeholder_img)
//                    .into(imageView);
//        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OpenImagesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ConstantData.IMAGE, mImageUrl);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
            }
        });

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}