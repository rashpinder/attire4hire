package com.attire4hire.FirebaseClasses;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.attire4hire.R;
import com.attire4hire.activity.AccountSetUpUpdateActivity;
import com.attire4hire.activity.ChatActivity;
import com.attire4hire.activity.Current_BookingsActivity;
import com.attire4hire.activity.HomeActivity;
import com.attire4hire.activity.MyInquiriesActivity;
import com.attire4hire.activity.MyRequestsActivity;
import com.attire4hire.activity.OrderConfirmationActivity;
import com.attire4hire.utils.ConstantData;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = MyFirebaseMessagingService.this.getClass().getSimpleName();
    Intent intent;
    String title = "", message = "";
    private static int NOTIFICATION_ID = 6578;
    String user_id = "", notification_type = "", order_id = "", request_id = "", message_count = "",
            total_price = "", AccidentDemageValue = "", orderNo = "", room_id = "", receiver_id = "", instant_booking = "";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        if (remoteMessage.getNotification() != null) {
            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
        }

        user_id = String.valueOf(remoteMessage.getData().get("user_id"));
        notification_type = String.valueOf(remoteMessage.getData().get("notification_type"));
        order_id = String.valueOf(remoteMessage.getData().get("order_id"));
        request_id = String.valueOf(remoteMessage.getData().get("request_id"));
        total_price = String.valueOf(remoteMessage.getData().get("total_price"));
        AccidentDemageValue = String.valueOf(remoteMessage.getData().get("AccidentDemageValue"));
        orderNo = String.valueOf(remoteMessage.getData().get("orderNo"));
        room_id = String.valueOf(remoteMessage.getData().get("room_id"));
        receiver_id = String.valueOf(remoteMessage.getData().get("receiver_id"));
        instant_booking = String.valueOf(remoteMessage.getData().get("instant_booking"));

        if (remoteMessage.getData().get("message_count") != null) {
            message_count = String.valueOf(remoteMessage.getData().get("message_count"));
        }

        if (!message_count.equals("")) {
            Intent intent = new Intent("changeImage");
            intent.putExtra("Status", "message_count");
            intent.putExtra("message_count", message_count);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }

        sendNotification(title, message);
    }

    private void sendNotification(String title, String message) {
        if (notification_type != null) {
            if (notification_type.equalsIgnoreCase("request")) {
                if (instant_booking.equals("1")) {
                    intent = new Intent(this, Current_BookingsActivity.class);
                } else {
                    intent = new Intent(this, MyRequestsActivity.class);
                }
            } else if (notification_type.equalsIgnoreCase("user/Accept/Reject")) {
                intent = new Intent(this, MyInquiriesActivity.class);
            } else if (notification_type.equalsIgnoreCase("order")) {
                intent = new Intent(this, OrderConfirmationActivity.class);
                intent.putExtra(ConstantData.NOTIFICATION, "notification");
                intent.putExtra("order_id", order_id);
            } else if (notification_type.equalsIgnoreCase("chat")) {
                intent = new Intent(this, ChatActivity.class);
                intent.putExtra(ConstantData.NOTIFICATION, "notification");
                intent.putExtra("room_id", room_id);
                intent.putExtra("receiver_id", receiver_id);
            } else if (notification_type.equalsIgnoreCase("cancel")) {
                intent = new Intent(this, MyRequestsActivity.class);
            } else if (notification_type.equalsIgnoreCase("stripe_account_status")) {
                Intent mIntent = new Intent(this, AccountSetUpUpdateActivity.class);
                startActivity(mIntent);
            } else {
                intent = new Intent(this, HomeActivity.class);
            }
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_notification_n)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setPriority(NotificationCompat.PRIORITY_HIGH).setDefaults(NotificationCompat.DEFAULT_ALL);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            } else {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }
        }
    }

    public int generateNotificationId() {
        return (int) System.currentTimeMillis();
    }
}