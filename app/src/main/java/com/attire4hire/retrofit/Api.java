package com.attire4hire.retrofit;

import com.attire4hire.model.PenaltyList;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {

    @FormUrlEncoded
    @POST("GetAllRequests.php")
    Call<JsonObject> getAllInquiries(@Field("user_id") String user_id,
                                     @Field("page_no") int page_no);

    @FormUrlEncoded
    @POST("GetAllInquiries.php")
    Call<JsonObject> getAllRequests(@Field("user_id") String user_id,
                                    @Field("page_no") int page_no);

    @Multipart
    @POST("ReportOrder.php")
    Call<JsonObject> ReportOrder(@Part("user_id") RequestBody user_id,
                                 @Part("order_id") RequestBody order_id,
                                 @Part("reason") RequestBody reason,
                                 @Part("message") RequestBody message,
                                 @Part MultipartBody.Part file);

    @POST("CheckExtension.php")
    Call<JsonObject> CheckExtension(@Body Map<String, String> mParams);

    @POST("RequestExtension.php")
    Call<JsonObject> RequestExtension(@Body Map<String, String> mParams);

    @POST("AddExtensionOrder.php")
    Call<JsonObject> AddExtensionOrder(@Body Map<String, String> mParams);

    @POST("UpdateExtPayment.php")
    Call<JsonObject> UpdateExtPayment(@Body Map<String, String> mParams);

    @POST("PenaltyPayment.php")
    Call<JsonObject> PenaltyPayment(@Body Map<String, String> mParams);

    @FormUrlEncoded
    @POST("PenaltyList.php")
    Call<PenaltyList> getPenaltyList(@Field("user_id") String user_id);

    @POST("GetAllProductListAndSearch.php")
    Call<JsonObject> GetAllProductListAndSearch(@Body Map<String, String> mParams);
}
