package com.attire4hire.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ItemProductModel implements Serializable {
    String id = "";
    String name = "";
    String user_id = "";

    String google_id = "";
    String facebook_id = "";

    String User_First_Name = "";
    String User_Last_Name = "";
    String User_Email = "";
    String Gender = "";
    String User_City = "";
    String User_PhoneNo = "";
    String User_ProfilePic = "";

    String category_name = "";
    String size_name = "";
    String color_name = "";
    String brand_name = "";
    String type_name = "";
    String designer_name = "";
    String condition_name = "";
    String ocasion_name = "";
    String description = "";
    String date = "";
    String retail_price = "";
    String replacement_value = "";
    String week_4days = "";
    String week_8days = "";
    String week_12days = "";
    String instant_booking = "";
    String open_for_sale = "";
    String cleaning_free = "";
    String drop_person = "";
    String delivery_free = "";
    String image1 = "";
    String image2 = "";
    String image3 = "";
    String image4 = "";
    String image5 = "";
    String location = "";

    String image_id = "";
    String favourite = "";

    //MyInquiries
    String extension = "";
    String Request_id = "";
    String product_id = "";
    String request_type = "";
    String request_status = "";
    String creation_date = "";
    String Another_User_Id = "";
    String Another_User_Name = "";
    String Another_User_LastName = "";
    String Another_User_Email = "";
    String Another_User_Gender = "";
    String Another_User_CityName = "";
    String Another_User_PhoneNo = "";
    String Another_ProfilePic = "";
    String Another_Id_Verification = "";
    String Another_Email_Verification = "";
    String Another_Phone_Verification = "";
    String Draft_Id = "";
    String Ratings = "";
    String User_Ratings = "";

    String Rental_Prize = "";
    String Service_fee = "";
    String Cart_Delivery_fee = "";
    String Cart_Cleaning_fee = "";
    String Recent_Search_Data = "";

    String Email_Verification = "";
    String Phone_Verification = "";
    String Id_Verification = "";
    String BuyRentRequestStatus = "";
    String RentRequestStatus = "";
    String BuyRequestStatus = "";

    String CardBrand = "";
    String CardCountry = "";
    String Card_Id = "";
    String exp_month = "";
    String exp_year = "";
    String last4 = "";
    String card_holder_name = "";

    String order_number = "";
    String transaction_number = "";
    String accidental_damage = "";
    String damage_fee = "";
    String total_price = "";
    String status = "";
    String delivery_type = "";
    String allow_payment = "";

    String order_date = "";
    String dispatch_date = "";
    String delivery_date = "";
    String return_date = "";
    String completed_date = "";

    String order_id = "";
    String room_id = "";
    String owner_id = "";
    String sender_id = "";
    String receiver_id = "";
    String message = "";
    String review_id = "";
    String reviewed_user = "";
    String overall_exp = "";
    String communication = "";
    String item_as_desc = "";
    String item_arrive_on_time = "";
    String comments = "";
    String Buy_Option = "";
    String lender_rating_status = "";
    String renter_rating_status = "";
    String pick_up = "";
    String unread_count = "";
    String review = "";

    String sort_code = "";
    String street_address = "";
    String street_address2 = "";
    String account_number = "";
    String city = "";
    String state = "";
    String country = "";
    String zipcode = "";
    String authenticationkey = "";
    String message_time = "";
    String booking_dates = "";
    String book_date = "";
    String txn_no = "";
    String txn_no2 = "";
    String cost = "";
    String start_date = "";
    String end_date = "";
    String hour_diff = "";
    String receive_hour_diff = "";
    String report_status = "";
    String read_status = "";

    String product_owner_id = "";
    String report_claim = "";

    ArrayList<ImagesModel> ImagesArrayList = new ArrayList<>();
    ArrayList<String> mSelectedArrayList = new ArrayList<>();

    ArrayList<String> mCardsList = new ArrayList<>();
    ArrayList<String> mRecentItemsList = new ArrayList<>();

    public ArrayList<String> getmSelectedArrayList() {
        return mSelectedArrayList;
    }

    public void setmSelectedArrayList(ArrayList<String> mSelectedArrayList) {
        this.mSelectedArrayList = mSelectedArrayList;
    }

    public ArrayList<ImagesModel> getImagesArrayList() {
        return ImagesArrayList;
    }

    public void setImagesArrayList(ArrayList<ImagesModel> imagesArrayList) {
        ImagesArrayList = imagesArrayList;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getSize_name() {
        return size_name;
    }

    public void setSize_name(String size_name) {
        this.size_name = size_name;
    }

    public String getColor_name() {
        return color_name;
    }

    public void setColor_name(String color_name) {
        this.color_name = color_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getDesigner_name() {
        return designer_name;
    }

    public void setDesigner_name(String designer_name) {
        this.designer_name = designer_name;
    }

    public String getCondition_name() {
        return condition_name;
    }

    public void setCondition_name(String condition_name) {
        this.condition_name = condition_name;
    }

    public String getOcasion_name() {
        return ocasion_name;
    }

    public void setOcasion_name(String ocasion_name) {
        this.ocasion_name = ocasion_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRetail_price() {
        return retail_price;
    }

    public void setRetail_price(String retail_price) {
        this.retail_price = retail_price;
    }

    public String getReplacement_value() {
        return replacement_value;
    }

    public void setReplacement_value(String replacement_value) {
        this.replacement_value = replacement_value;
    }

    public String getWeek_4days() {
        return week_4days;
    }

    public void setWeek_4days(String week_4days) {
        this.week_4days = week_4days;
    }

    public String getWeek_8days() {
        return week_8days;
    }

    public void setWeek_8days(String week_8days) {
        this.week_8days = week_8days;
    }

    public String getWeek_12days() {
        return week_12days;
    }

    public void setWeek_12days(String week_12days) {
        this.week_12days = week_12days;
    }

    public String getInstant_booking() {
        return instant_booking;
    }

    public void setInstant_booking(String instant_booking) {
        this.instant_booking = instant_booking;
    }

    public String getOpen_for_sale() {
        return open_for_sale;
    }

    public void setOpen_for_sale(String open_for_sale) {
        this.open_for_sale = open_for_sale;
    }

    public String getCleaning_free() {
        return cleaning_free;
    }

    public void setCleaning_free(String cleaning_free) {
        this.cleaning_free = cleaning_free;
    }

    public String getDrop_person() {
        return drop_person;
    }

    public void setDrop_person(String drop_person) {
        this.drop_person = drop_person;
    }

    public String getDelivery_free() {
        return delivery_free;
    }

    public void setDelivery_free(String delivery_free) {
        this.delivery_free = delivery_free;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getFavourite() {
        return favourite;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public String getRequest_id() {
        return Request_id;
    }

    public void setRequest_id(String request_id) {
        Request_id = request_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getAnother_User_Id() {
        return Another_User_Id;
    }

    public void setAnother_User_Id(String another_User_Id) {
        Another_User_Id = another_User_Id;
    }

    public String getAnother_User_Name() {
        return Another_User_Name;
    }

    public void setAnother_User_Name(String another_User_Name) {
        Another_User_Name = another_User_Name;
    }

    public String getAnother_User_LastName() {
        return Another_User_LastName;
    }

    public void setAnother_User_LastName(String another_User_LastName) {
        Another_User_LastName = another_User_LastName;
    }

    public String getAnother_User_Email() {
        return Another_User_Email;
    }

    public void setAnother_User_Email(String another_User_Email) {
        Another_User_Email = another_User_Email;
    }

    public String getAnother_User_Gender() {
        return Another_User_Gender;
    }

    public void setAnother_User_Gender(String another_User_Gender) {
        Another_User_Gender = another_User_Gender;
    }

    public String getAnother_User_CityName() {
        return Another_User_CityName;
    }

    public void setAnother_User_CityName(String another_User_CityName) {
        Another_User_CityName = another_User_CityName;
    }

    public String getAnother_User_PhoneNo() {
        return Another_User_PhoneNo;
    }

    public void setAnother_User_PhoneNo(String another_User_PhoneNo) {
        Another_User_PhoneNo = another_User_PhoneNo;
    }

    public String getAnother_ProfilePic() {
        return Another_ProfilePic;
    }

    public void setAnother_ProfilePic(String another_ProfilePic) {
        Another_ProfilePic = another_ProfilePic;
    }

    public String getDraft_Id() {
        return Draft_Id;
    }

    public void setDraft_Id(String draft_Id) {
        Draft_Id = draft_Id;
    }

    public String getRatings() {
        return Ratings;
    }

    public void setRatings(String ratings) {
        Ratings = ratings;
    }

    public String getUser_Ratings() {
        return User_Ratings;
    }

    public void setUser_Ratings(String user_Ratings) {
        User_Ratings = user_Ratings;
    }

    public String getRental_Prize() {
        return Rental_Prize;
    }

    public void setRental_Prize(String rental_Prize) {
        Rental_Prize = rental_Prize;
    }

    public String getService_fee() {
        return Service_fee;
    }

    public void setService_fee(String service_fee) {
        Service_fee = service_fee;
    }

    public String getCart_Delivery_fee() {
        return Cart_Delivery_fee;
    }

    public void setCart_Delivery_fee(String cart_Delivery_fee) {
        Cart_Delivery_fee = cart_Delivery_fee;
    }

    public String getCart_Cleaning_fee() {
        return Cart_Cleaning_fee;
    }

    public void setCart_Cleaning_fee(String cart_Cleaning_fee) {
        Cart_Cleaning_fee = cart_Cleaning_fee;
    }

    public ArrayList<String> getmCardsList() {
        return mCardsList;
    }

    public void setmCardsList(ArrayList<String> mCardsList) {
        this.mCardsList = mCardsList;
    }

    public String getRecent_Search_Data() {
        return Recent_Search_Data;
    }

    public void setRecent_Search_Data(String recent_Search_Data) {
        Recent_Search_Data = recent_Search_Data;
    }

    public ArrayList<String> getmRecentItemsList() {
        return mRecentItemsList;
    }

    public void setmRecentItemsList(ArrayList<String> mRecentItemsList) {
        this.mRecentItemsList = mRecentItemsList;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUser_First_Name() {
        return User_First_Name;
    }

    public void setUser_First_Name(String user_First_Name) {
        User_First_Name = user_First_Name;
    }

    public String getUser_Last_Name() {
        return User_Last_Name;
    }

    public void setUser_Last_Name(String user_Last_Name) {
        User_Last_Name = user_Last_Name;
    }

    public String getUser_Email() {
        return User_Email;
    }

    public void setUser_Email(String user_Email) {
        User_Email = user_Email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getUser_City() {
        return User_City;
    }

    public void setUser_City(String user_City) {
        User_City = user_City;
    }

    public String getUser_PhoneNo() {
        return User_PhoneNo;
    }

    public void setUser_PhoneNo(String user_PhoneNo) {
        User_PhoneNo = user_PhoneNo;
    }

    public String getUser_ProfilePic() {
        return User_ProfilePic;
    }

    public void setUser_ProfilePic(String user_ProfilePic) {
        User_ProfilePic = user_ProfilePic;
    }

    public String getEmail_Verification() {
        return Email_Verification;
    }

    public void setEmail_Verification(String email_Verification) {
        Email_Verification = email_Verification;
    }

    public String getPhone_Verification() {
        return Phone_Verification;
    }

    public void setPhone_Verification(String phone_Verification) {
        Phone_Verification = phone_Verification;
    }

    public String getId_Verification() {
        return Id_Verification;
    }

    public void setId_Verification(String id_Verification) {
        Id_Verification = id_Verification;
    }

    public String getBuyRentRequestStatus() {
        return BuyRentRequestStatus;
    }

    public void setBuyRentRequestStatus(String buyRentRequestStatus) {
        BuyRentRequestStatus = buyRentRequestStatus;
    }

    public String getCardBrand() {
        return CardBrand;
    }

    public void setCardBrand(String cardBrand) {
        CardBrand = cardBrand;
    }

    public String getCardCountry() {
        return CardCountry;
    }

    public void setCardCountry(String cardCountry) {
        CardCountry = cardCountry;
    }

    public String getCard_Id() {
        return Card_Id;
    }

    public void setCard_Id(String card_Id) {
        Card_Id = card_Id;
    }

    public String getExp_month() {
        return exp_month;
    }

    public void setExp_month(String exp_month) {
        this.exp_month = exp_month;
    }

    public String getExp_year() {
        return exp_year;
    }

    public void setExp_year(String exp_year) {
        this.exp_year = exp_year;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getCard_holder_name() {
        return card_holder_name;
    }

    public void setCard_holder_name(String card_holder_name) {
        this.card_holder_name = card_holder_name;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getTransaction_number() {
        return transaction_number;
    }

    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    public String getAccidental_damage() {
        return accidental_damage;
    }

    public void setAccidental_damage(String accidental_damage) {
        this.accidental_damage = accidental_damage;
    }

    public String getDamage_fee() {
        return damage_fee;
    }

    public void setDamage_fee(String damage_fee) {
        this.damage_fee = damage_fee;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAllow_payment() {
        return allow_payment;
    }

    public void setAllow_payment(String allow_payment) {
        this.allow_payment = allow_payment;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getDispatch_date() {
        return dispatch_date;
    }

    public void setDispatch_date(String dispatch_date) {
        this.dispatch_date = dispatch_date;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getReviewed_user() {
        return reviewed_user;
    }

    public void setReviewed_user(String reviewed_user) {
        this.reviewed_user = reviewed_user;
    }

    public String getOverall_exp() {
        return overall_exp;
    }

    public void setOverall_exp(String overall_exp) {
        this.overall_exp = overall_exp;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getItem_as_desc() {
        return item_as_desc;
    }

    public void setItem_as_desc(String item_as_desc) {
        this.item_as_desc = item_as_desc;
    }

    public String getItem_arrive_on_time() {
        return item_arrive_on_time;
    }

    public void setItem_arrive_on_time(String item_arrive_on_time) {
        this.item_arrive_on_time = item_arrive_on_time;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getBuy_Option() {
        return Buy_Option;
    }

    public void setBuy_Option(String buy_Option) {
        Buy_Option = buy_Option;
    }

    public String getLender_rating_status() {
        return lender_rating_status;
    }

    public void setLender_rating_status(String lender_rating_status) {
        this.lender_rating_status = lender_rating_status;
    }

    public String getRenter_rating_status() {
        return renter_rating_status;
    }

    public void setRenter_rating_status(String renter_rating_status) {
        this.renter_rating_status = renter_rating_status;
    }

    public String getPick_up() {
        return pick_up;
    }

    public void setPick_up(String pick_up) {
        this.pick_up = pick_up;
    }

    public String getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(String unread_count) {
        this.unread_count = unread_count;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getSort_code() {
        return sort_code;
    }

    public void setSort_code(String sort_code) {
        this.sort_code = sort_code;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getStreet_address2() {
        return street_address2;
    }

    public void setStreet_address2(String street_address2) {
        this.street_address2 = street_address2;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAuthenticationkey() {
        return authenticationkey;
    }

    public void setAuthenticationkey(String authenticationkey) {
        this.authenticationkey = authenticationkey;
    }

    public String getAnother_Id_Verification() {
        return Another_Id_Verification;
    }

    public void setAnother_Id_Verification(String another_Id_Verification) {
        Another_Id_Verification = another_Id_Verification;
    }

    public String getAnother_Email_Verification() {
        return Another_Email_Verification;
    }

    public void setAnother_Email_Verification(String another_Email_Verification) {
        Another_Email_Verification = another_Email_Verification;
    }

    public String getAnother_Phone_Verification() {
        return Another_Phone_Verification;
    }

    public void setAnother_Phone_Verification(String another_Phone_Verification) {
        Another_Phone_Verification = another_Phone_Verification;
    }

    public String getMessage_time() {
        return message_time;
    }

    public void setMessage_time(String message_time) {
        this.message_time = message_time;
    }

    public String getRentRequestStatus() {
        return RentRequestStatus;
    }

    public void setRentRequestStatus(String rentRequestStatus) {
        RentRequestStatus = rentRequestStatus;
    }

    public String getBuyRequestStatus() {
        return BuyRequestStatus;
    }

    public void setBuyRequestStatus(String buyRequestStatus) {
        BuyRequestStatus = buyRequestStatus;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getBooking_dates() {
        return booking_dates;
    }

    public void setBooking_dates(String booking_dates) {
        this.booking_dates = booking_dates;
    }

    public String getBook_date() {
        return book_date;
    }

    public void setBook_date(String book_date) {
        this.book_date = book_date;
    }

    public String getTxn_no() {
        return txn_no;
    }

    public void setTxn_no(String txn_no) {
        this.txn_no = txn_no;
    }

    public String getProduct_owner_id() {
        return product_owner_id;
    }

    public void setProduct_owner_id(String product_owner_id) {
        this.product_owner_id = product_owner_id;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getHour_diff() {
        return hour_diff;
    }

    public void setHour_diff(String hour_diff) {
        this.hour_diff = hour_diff;
    }

    public String getReturn_date() {
        return return_date;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }

    public String getReceive_hour_diff() {
        return receive_hour_diff;
    }

    public void setReceive_hour_diff(String receive_hour_diff) {
        this.receive_hour_diff = receive_hour_diff;
    }

    public String getReport_status() {
        return report_status;
    }

    public void setReport_status(String report_status) {
        this.report_status = report_status;
    }

    public String getCompleted_date() {
        return completed_date;
    }

    public void setCompleted_date(String completed_date) {
        this.completed_date = completed_date;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTxn_no2() {
        return txn_no2;
    }

    public void setTxn_no2(String txn_no2) {
        this.txn_no2 = txn_no2;
    }

    public String getReport_claim() {
        return report_claim;
    }

    public void setReport_claim(String report_claim) {
        this.report_claim = report_claim;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
