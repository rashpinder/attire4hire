package com.attire4hire.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PenaltyList {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class AllOrder {

        @SerializedName("order_detail")
        @Expose
        private OrderDetail orderDetail;
        @SerializedName("penalty")
        @Expose
        private String penalty;

        public OrderDetail getOrderDetail() {
            return orderDetail;
        }

        public void setOrderDetail(OrderDetail orderDetail) {
            this.orderDetail = orderDetail;
        }

        public String getPenalty() {
            return penalty;
        }

        public void setPenalty(String penalty) {
            this.penalty = penalty;
        }

    }

    public class Data {

        @SerializedName("all_orders")
        @Expose
        private List<AllOrder> allOrders = null;
        @SerializedName("total_penalty")
        @Expose
        private Integer totalPenalty;

        public List<AllOrder> getAllOrders() {
            return allOrders;
        }

        public void setAllOrders(List<AllOrder> allOrders) {
            this.allOrders = allOrders;
        }

        public Integer getTotalPenalty() {
            return totalPenalty;
        }

        public void setTotalPenalty(Integer totalPenalty) {
            this.totalPenalty = totalPenalty;
        }
    }

    public class OrderDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("request_id")
        @Expose
        private String requestId;
        @SerializedName("order_number")
        @Expose
        private String orderNumber;
        @SerializedName("transaction_number")
        @Expose
        private String transactionNumber;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("accidental_damage")
        @Expose
        private String accidentalDamage;
        @SerializedName("rental_fee")
        @Expose
        private String rentalFee;
        @SerializedName("cleaning_fee")
        @Expose
        private String cleaningFee;
        @SerializedName("service_fee")
        @Expose
        private String serviceFee;
        @SerializedName("delivery_fee")
        @Expose
        private String deliveryFee;
        @SerializedName("damage_fee")
        @Expose
        private String damageFee;
        @SerializedName("total_price")
        @Expose
        private String totalPrice;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("allow_payment")
        @Expose
        private String allowPayment;
        @SerializedName("dispatch_date")
        @Expose
        private String dispatchDate;
        @SerializedName("delivery_date")
        @Expose
        private String deliveryDate;
        @SerializedName("return_date")
        @Expose
        private String returnDate;
        @SerializedName("completed_date")
        @Expose
        private String completedDate;
        @SerializedName("delivery_type")
        @Expose
        private String deliveryType;
        @SerializedName("payment_received")
        @Expose
        private String paymentReceived;
        @SerializedName("payment_transferred")
        @Expose
        private String paymentTransferred;
        @SerializedName("book_date")
        @Expose
        private String bookDate;
        @SerializedName("txn_no")
        @Expose
        private String txnNo;
        @SerializedName("extension")
        @Expose
        private String extension;
        @SerializedName("related_order")
        @Expose
        private String relatedOrder;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("hour_diff")
        @Expose
        private Integer hourDiff;
        @SerializedName("product_detail")
        @Expose
        private ProductDetail productDetail;
        @SerializedName("user_detail")
        @Expose
        private String userDetail;
        @SerializedName("order_date")
        @Expose
        private String orderDate;
        @SerializedName("receive_hour_diff")
        @Expose
        private Integer receiveHourDiff;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getTransactionNumber() {
            return transactionNumber;
        }

        public void setTransactionNumber(String transactionNumber) {
            this.transactionNumber = transactionNumber;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getAccidentalDamage() {
            return accidentalDamage;
        }

        public void setAccidentalDamage(String accidentalDamage) {
            this.accidentalDamage = accidentalDamage;
        }

        public String getRentalFee() {
            return rentalFee;
        }

        public void setRentalFee(String rentalFee) {
            this.rentalFee = rentalFee;
        }

        public String getCleaningFee() {
            return cleaningFee;
        }

        public void setCleaningFee(String cleaningFee) {
            this.cleaningFee = cleaningFee;
        }

        public String getServiceFee() {
            return serviceFee;
        }

        public void setServiceFee(String serviceFee) {
            this.serviceFee = serviceFee;
        }

        public String getDeliveryFee() {
            return deliveryFee;
        }

        public void setDeliveryFee(String deliveryFee) {
            this.deliveryFee = deliveryFee;
        }

        public String getDamageFee() {
            return damageFee;
        }

        public void setDamageFee(String damageFee) {
            this.damageFee = damageFee;
        }

        public String getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAllowPayment() {
            return allowPayment;
        }

        public void setAllowPayment(String allowPayment) {
            this.allowPayment = allowPayment;
        }

        public String getDispatchDate() {
            return dispatchDate;
        }

        public void setDispatchDate(String dispatchDate) {
            this.dispatchDate = dispatchDate;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getReturnDate() {
            return returnDate;
        }

        public void setReturnDate(String returnDate) {
            this.returnDate = returnDate;
        }

        public String getCompletedDate() {
            return completedDate;
        }

        public void setCompletedDate(String completedDate) {
            this.completedDate = completedDate;
        }

        public String getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(String deliveryType) {
            this.deliveryType = deliveryType;
        }

        public String getPaymentReceived() {
            return paymentReceived;
        }

        public void setPaymentReceived(String paymentReceived) {
            this.paymentReceived = paymentReceived;
        }

        public String getPaymentTransferred() {
            return paymentTransferred;
        }

        public void setPaymentTransferred(String paymentTransferred) {
            this.paymentTransferred = paymentTransferred;
        }

        public String getBookDate() {
            return bookDate;
        }

        public void setBookDate(String bookDate) {
            this.bookDate = bookDate;
        }

        public String getTxnNo() {
            return txnNo;
        }

        public void setTxnNo(String txnNo) {
            this.txnNo = txnNo;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getRelatedOrder() {
            return relatedOrder;
        }

        public void setRelatedOrder(String relatedOrder) {
            this.relatedOrder = relatedOrder;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getHourDiff() {
            return hourDiff;
        }

        public void setHourDiff(Integer hourDiff) {
            this.hourDiff = hourDiff;
        }

        public ProductDetail getProductDetail() {
            return productDetail;
        }

        public void setProductDetail(ProductDetail productDetail) {
            this.productDetail = productDetail;
        }

        public String getUserDetail() {
            return userDetail;
        }

        public void setUserDetail(String userDetail) {
            this.userDetail = userDetail;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public Integer getReceiveHourDiff() {
            return receiveHourDiff;
        }

        public void setReceiveHourDiff(Integer receiveHourDiff) {
            this.receiveHourDiff = receiveHourDiff;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

    }

    public class ProductDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("image_id")
        @Expose
        private String imageId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("type_name")
        @Expose
        private String typeName;
        @SerializedName("size_name")
        @Expose
        private String sizeName;
        @SerializedName("color_name")
        @Expose
        private String colorName;
        @SerializedName("brand_name")
        @Expose
        private String brandName;
        @SerializedName("condition_name")
        @Expose
        private String conditionName;
        @SerializedName("ocasion_name")
        @Expose
        private String ocasionName;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("retail_price")
        @Expose
        private String retailPrice;
        @SerializedName("replacement_value")
        @Expose
        private String replacementValue;
        @SerializedName("week_4days")
        @Expose
        private String week4days;
        @SerializedName("week_8days")
        @Expose
        private String week8days;
        @SerializedName("week_12days")
        @Expose
        private String week12days;
        @SerializedName("instant_booking")
        @Expose
        private String instantBooking;
        @SerializedName("open_for_sale")
        @Expose
        private String openForSale;
        @SerializedName("cleaning_free")
        @Expose
        private String cleaningFree;
        @SerializedName("drop_person")
        @Expose
        private String dropPerson;
        @SerializedName("delivery_free")
        @Expose
        private String deliveryFree;
        @SerializedName("image1")
        @Expose
        private String image1;
        @SerializedName("image2")
        @Expose
        private String image2;
        @SerializedName("image3")
        @Expose
        private String image3;
        @SerializedName("image4")
        @Expose
        private String image4;
        @SerializedName("image5")
        @Expose
        private String image5;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("disable")
        @Expose
        private String disable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getSizeName() {
            return sizeName;
        }

        public void setSizeName(String sizeName) {
            this.sizeName = sizeName;
        }

        public String getColorName() {
            return colorName;
        }

        public void setColorName(String colorName) {
            this.colorName = colorName;
        }

        public String getBrandName() {
            return brandName;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public String getConditionName() {
            return conditionName;
        }

        public void setConditionName(String conditionName) {
            this.conditionName = conditionName;
        }

        public String getOcasionName() {
            return ocasionName;
        }

        public void setOcasionName(String ocasionName) {
            this.ocasionName = ocasionName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getRetailPrice() {
            return retailPrice;
        }

        public void setRetailPrice(String retailPrice) {
            this.retailPrice = retailPrice;
        }

        public String getReplacementValue() {
            return replacementValue;
        }

        public void setReplacementValue(String replacementValue) {
            this.replacementValue = replacementValue;
        }

        public String getWeek4days() {
            return week4days;
        }

        public void setWeek4days(String week4days) {
            this.week4days = week4days;
        }

        public String getWeek8days() {
            return week8days;
        }

        public void setWeek8days(String week8days) {
            this.week8days = week8days;
        }

        public String getWeek12days() {
            return week12days;
        }

        public void setWeek12days(String week12days) {
            this.week12days = week12days;
        }

        public String getInstantBooking() {
            return instantBooking;
        }

        public void setInstantBooking(String instantBooking) {
            this.instantBooking = instantBooking;
        }

        public String getOpenForSale() {
            return openForSale;
        }

        public void setOpenForSale(String openForSale) {
            this.openForSale = openForSale;
        }

        public String getCleaningFree() {
            return cleaningFree;
        }

        public void setCleaningFree(String cleaningFree) {
            this.cleaningFree = cleaningFree;
        }

        public String getDropPerson() {
            return dropPerson;
        }

        public void setDropPerson(String dropPerson) {
            this.dropPerson = dropPerson;
        }

        public String getDeliveryFree() {
            return deliveryFree;
        }

        public void setDeliveryFree(String deliveryFree) {
            this.deliveryFree = deliveryFree;
        }

        public String getImage1() {
            return image1;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getImage3() {
            return image3;
        }

        public void setImage3(String image3) {
            this.image3 = image3;
        }

        public String getImage4() {
            return image4;
        }

        public void setImage4(String image4) {
            this.image4 = image4;
        }

        public String getImage5() {
            return image5;
        }

        public void setImage5(String image5) {
            this.image5 = image5;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }
    }
}