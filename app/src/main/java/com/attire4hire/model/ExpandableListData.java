package com.attire4hire.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListData {
    public static LinkedHashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<>();

        List<String> mylistings = new ArrayList<String>();
        mylistings.add("My Listed Items");
        mylistings.add("List an Item");
        mylistings.add("Draft Items");
    
        List<String> lending = new ArrayList<String>();
        lending.add("Booking Enquiries");
        lending.add("Current Bookings");
       
        List<String> renting = new ArrayList<String>();
        renting.add("My Enquiries");
        renting.add("My Confirmed Bookings");

        List<String> history = new ArrayList<String>();

        expandableListDetail.put("My Listings", mylistings);
        expandableListDetail.put("Lending / Selling", lending);
        expandableListDetail.put("Renting / Buying", renting);
        expandableListDetail.put("Booking History", history);

        return expandableListDetail;
    }
}
