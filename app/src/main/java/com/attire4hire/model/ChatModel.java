package com.attire4hire.model;

import java.io.Serializable;

public class ChatModel implements Serializable, Comparable<ChatModel> {

    private int intChatType;
    String msg_id = "";
    String room_id = "";
    String owner_id = "";
    String message = "";
    String read_status = "";

    String message_id = "";
    String sender_id = "";
    String receiver_id = "";
    String creation_date = "";
    String receiver_pic = "";
    String id = "";
    String receiver_name = "";

    public int getIntChatType() {
        return intChatType;
    }

    public void setIntChatType(int intChatType) {
        this.intChatType = intChatType;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getReceiver_pic() {
        return receiver_pic;
    }

    public void setReceiver_pic(String receiver_pic) {
        this.receiver_pic = receiver_pic;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    @Override
    public int compareTo(ChatModel o) {
        return 0;
    }

//    @Override
//    public int compareTo(ChatModel f) {
//        if (Double.parseDouble(creation_date)  > Double.parseDouble(f.getCreation_date())) {
//            return 0;
//        } else if (Double.parseDouble(creation_date) < Double.parseDouble(f.getCreation_date())) {
//            return -1;
//        } else {
//            return 1;
//        }
//    }
}
