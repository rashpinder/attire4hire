package com.attire4hire.model;

import java.io.Serializable;

public class ImagesModel implements Serializable {
    String Image_path = "";
    String Image_name = "";

    public String getImage_path() {
        return Image_path;
    }

    public void setImage_path(String image_path) {
        Image_path = image_path;
    }

    public String getImage_name() {
        return Image_name;
    }

    public void setImage_name(String image_name) {
        Image_name = image_name;
    }
}
