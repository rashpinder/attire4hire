package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.RecentSearchAdapter;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.attire4hire.fragments.SearchFragment.strPerformersType;

public class RecentSearchActivity extends BaseActivity {
    Activity mActivity = RecentSearchActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = RecentSearchActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.recent_search_rv)
    RecyclerView recentSearchRV;
    @BindView(R.id.et_recent_search)
    EdittextRegular et_recent_search;
    @BindView(R.id.iv_filter)
    ImageView filterIV;
    @BindView(R.id.tv_search)
    TextviewSemiBold tv_search;
    @BindView(R.id.Iv_cancel)
    ImageView Iv_cancel;
    @BindView(R.id.ll_recent)
    RelativeLayout ll_recent;
    @BindView(R.id.tv_clear)
    TextviewSemiBold tv_clear;

    String strItemName = "";
    RecentSearchAdapter mAdapter;
    ArrayList<String> mRecentItemsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_search);
        ButterKnife.bind(this);

        if (getIdVerification().equals("1") && getEmailVerification().equals("1") && getPhoneVerification().equals("1")) {
        } else {
            showVerificationAlertDialog(mActivity, "Verify your identity to rent, lend, buy and sell items.");
        }

        et_recent_search.setText("");
        strItemName = Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, "");

        ClearFilterValues();

        et_recent_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do something

                    onBackPressed();
                }
                return false;
            }
        });

        executeRecentSearchApi();

        if (mRecentItemsList != null) {
            RecentSearchAdapter();
            SearchItem();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void ClearFilterValues() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.SEARCH_VALUE, "");

        Attire4hirePrefrences.writeString(mActivity, ConstantData.SIZE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.CONDITION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.COLOUR, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.BRAND, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.ITEM_CATEGORY, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRODUCT_TYPE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION_DATE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_START, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_END, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.LOCATION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OPEN_FOR_SALE, "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /* clear performers' corner data */
        strPerformersType = "0";

        if (et_recent_search.getText().toString().trim().equals("")) {

            if (strItemName.equals("")) {
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, "");
            } else {
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, strItemName);
            }

        } else {

            Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, et_recent_search.getText().toString().trim());
        }
    }

    @OnClick({R.id.iv_filter, R.id.tv_search, R.id.Iv_cancel, R.id.et_recent_search, R.id.tv_clear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_filter:

                et_recent_search.setText("");
                strItemName = Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, "");

                Intent intent = new Intent(mActivity, FiltersActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.tv_search:

                if (et_recent_search.getText().toString().trim().equals("")) {
                    Toast.makeText(mActivity, "Write something for searching!", Toast.LENGTH_SHORT).show();
                } else {
                    SearchName();
                }
                break;

            case R.id.Iv_cancel:
                performCancelClick();
                break;

            case R.id.et_recent_search:

                break;

            case R.id.tv_clear:
                executeClearRecentSearchApi();
                break;
        }
    }

    private void performCancelClick() {

        et_recent_search.setText("");
        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, "");

        if (mRecentItemsList != null) {
            mRecentItemsList.clear();
        }

        executeRecentSearchApi();
    }

    private void SearchName() {
        strItemName = et_recent_search.getText().toString().trim();
        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, et_recent_search.getText().toString().trim());
        onBackPressed();
    }

    private void executeRecentSearchApi() {

        if (mRecentItemsList != null) {
            mRecentItemsList.clear();
        }

        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_RECENT_SEARCH;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("staus");
                    String strMessage = response.getString("message");

                    if (strStatus.equals("1")) {

//                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();

                        JSONArray mJsonArray = new JSONArray(response.getString("data"));

                        for (int i = 0; i < mJsonArray.length(); i++) {

                            String Items = mJsonArray.getString(i);
                            mRecentItemsList.add(Items);
                        }

                        RecentSearchAdapter();

                    } else {
                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().

                addToRequestQueue(jsonRequest);

    }

    private void executeClearRecentSearchApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.CLEAR_RECENT_SEARCH;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("staus");
                    String strMessage = response.getString("message");

                    if (strStatus.equals("1")) {

//                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();

                        mRecentItemsList.clear();
                        mAdapter.notifyDataSetChanged();

//                        showAlertDialog(mActivity, getString(R.string.item_search_clear));

//                        RecentSearchAdapter();

                    } else {
                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().

                addToRequestQueue(jsonRequest);

    }

    private void executeRecentSearchClickApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.AUTO_COMPLETE_SEARCH;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("search_text", "");
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("staus");
                    String strMessage = response.getString("message");

                    if (strStatus.equals("1")) {

                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();

                        JSONArray mJsonArray = new JSONArray(response.getString("data"));

                        for (int i = 0; i < mJsonArray.length(); i++) {

                            String Items = mJsonArray.getString(i);
//                            mItemsList.add(Items);
                        }


                    } else {
                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);

    }

    private void RecentSearchAdapter() {
        if (recentSearchRV != null) {
            recentSearchRV.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
            mAdapter = new RecentSearchAdapter(mActivity, mRecentItemsList, new RecentSearchAdapter.RecentSearchInterface() {
                @Override
                public void searchInterface(int position, String Name) {
                    strItemName = Name;
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.SEARCH_ITEM, strItemName);
                    onBackPressed();
                }
            });
            recentSearchRV.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void SearchItem() {
        et_recent_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

//                ll_recent.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

//                getIntentData();

                //after the change calling the method and passing the search input
                if (editable.toString().isEmpty()) {

                    mAdapter.filterList(mRecentItemsList);

                } else {

                    filter(editable.toString());
                }
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<String> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (String s : mRecentItemsList) {
            //if the existing elements contains the search input
            if (s.contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        mAdapter.filterList(filterdNames);
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);
                finish();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
