package com.attire4hire.activity;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.DetailsSlidingImageAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.chahinem.pageindicator.PageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ItemDetailsProfileActivity extends BaseActivity {

    /*
     * Initlaize Activity
     * */
    Activity mActivity = ItemDetailsProfileActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ItemDetailsProfileActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;
    @BindView(R.id.mIndicatorCI)
    PageIndicator mIndicatorCI;
    @BindView(R.id.txtNameTV)
    TextviewBold txtNameTV;
    @BindView(R.id.txtPriceTV)
    TextviewSemiBold txtPriceTV;
    @BindView(R.id.loc)
    ImageView loc;
    @BindView(R.id.txtLocationTV)
    TextviewSemiBold txtLocationTV;
    @BindView(R.id.txtSizeTV)
    TextviewSemiBold txtSizeTV;
    @BindView(R.id.txtTypeTV)
    TextviewSemiBold txtTypeTV;
    @BindView(R.id.txtColorTV)
    TextviewSemiBold txtColorTV;
    @BindView(R.id.txtBrandDesignNameTV)
    TextviewSemiBold txtBrandDesignNameTV;
    @BindView(R.id.txtConditionsTV)
    TextviewSemiBold txtConditionsTV;
    @BindView(R.id.txtOccasionTV)
    TextviewSemiBold txtOccasionTV;
    @BindView(R.id.txtRentalPriceTV)
    TextviewSemiBold txtRentalPriceTV;
    @BindView(R.id.txtReplacementValueTV)
    TextviewSemiBold txtReplacementValueTV;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.rlDescriptionRL)
    RelativeLayout rlDescriptionRL;
    @BindView(R.id.rlDeleteRL)
    RelativeLayout rlDeleteRL;
    @BindView(R.id.txt4DaysTV)
    TextviewRegular txt4DaysTV;
    @BindView(R.id.txt8DaysTV)
    TextviewRegular txt8DaysTV;
    @BindView(R.id.txt12DaysTV)
    TextviewRegular txt12DaysTV;

    @BindView(R.id.mRatingbarRB)
    RatingBar mRatingbarRB;

    /*
     * Initalize Objects...
     * */
    ItemProductModel mItemProductModel;
    DetailsSlidingImageAdapter mDetailsSlidingImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details_profile);

        ButterKnife.bind(this);

        getIntentData();

        rlDeleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mActivity, rlDeleteRL);
                popup.getMenuInflater().inflate(R.menu.option_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
//                        Toast.makeText(mActivity, "You Clicked:"+item.getTitle(),Toast.LENGTH_SHORT).show();
//                        return true;

                        switch (item.getItemId()) {
                            case R.id.delete:
                                //your code
                                // EX : call intent if you want to swich to other activity
                                showConfirmDialog();
                                return true;
                            case R.id.edit:
                                //your code
                                Intent mIntent = new Intent(mActivity, AddItemEditActivity.class);
                                mIntent.putExtra(ConstantData.MODEL, mItemProductModel);
                                mIntent.putExtra(ConstantData.ADD_ITEM_TYPE, "AddItem");
                                mActivity.startActivity(mIntent);
                                finish();
                                return true;
                            default:
//                                return super.onOptionsItemSelected(item);
                                return true;
                        }
                    }
                });
                popup.show();//showing popup menu
            }
        });//closing the setOnClickListener method
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mItemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            //Set Details Data
            getDetailsData();
        }
    }

    private void getDetailsData() {
        if (isNetworkAvailable(mActivity)) {

            executeDetailsApi();

        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void setDataOnWidgets() {
        /*
         * Set ViewPager Images:
         * */
        ArrayList<String> mImagesArrayList = new ArrayList<>();
        if (mItemProductModel.getImage1() != null && mItemProductModel.getImage1().contains("http")) {
            mImagesArrayList.add(mItemProductModel.getImage1());
        }
        if (mItemProductModel.getImage2() != null && mItemProductModel.getImage2().contains("http")) {
            mImagesArrayList.add(mItemProductModel.getImage2());
        }
        if (mItemProductModel.getImage3() != null && mItemProductModel.getImage3().contains("http")) {
            mImagesArrayList.add(mItemProductModel.getImage3());
        }
        if (mItemProductModel.getImage4() != null && mItemProductModel.getImage4().contains("http")) {
            mImagesArrayList.add(mItemProductModel.getImage4());
        }
        if (mItemProductModel.getImage5() != null && mItemProductModel.getImage5().contains("http")) {
            mImagesArrayList.add(mItemProductModel.getImage5());
        }

        //Set Images
        mDetailsSlidingImageAdapter = new DetailsSlidingImageAdapter(mActivity, mImagesArrayList);
        mViewPagerVP.setAdapter(mDetailsSlidingImageAdapter);
        mIndicatorCI.attachTo(mViewPagerVP);

        txtNameTV.setText(CapitalizedFirstLetter(mItemProductModel.getName()));
        String strDay41 = String.format("%.2f", Double.parseDouble(mItemProductModel.getWeek_4days()));
        txtPriceTV.setText("£" + strDay41);
        txtSizeTV.setText(mItemProductModel.getSize_name());
        txtTypeTV.setText(mItemProductModel.getType_name());
        txtColorTV.setText(mItemProductModel.getColor_name());

        txtBrandDesignNameTV.setText(mItemProductModel.getBrand_name());
        txtConditionsTV.setText(mItemProductModel.getCondition_name());
        txtOccasionTV.setText(mItemProductModel.getOcasion_name());
        txtRentalPriceTV.setText("£" + mItemProductModel.getRetail_price());
        txtReplacementValueTV.setText("£" + mItemProductModel.getReplacement_value());
        txtLocationTV.setText(mItemProductModel.getLocation());

        String strDay4 = String.format("%.2f", Double.parseDouble(mItemProductModel.getWeek_4days()));
        String strDay8 = String.format("%.2f", Double.parseDouble(mItemProductModel.getWeek_8days()));
        String strDay12 = String.format("%.2f", Double.parseDouble(mItemProductModel.getWeek_12days()));

        txt4DaysTV.setText("4 Days" + " £" + strDay4);
        txt8DaysTV.setText("8 Days" + " £" + strDay8);
        txt12DaysTV.setText("12 Days" + " £" + strDay12);

        if (mItemProductModel.getRatings() != null && mItemProductModel.getRatings().length() > 0)
            mRatingbarRB.setRating(Float.parseFloat(mItemProductModel.getRatings()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick({R.id.rlCancelRL, R.id.rlDescriptionRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.rlDescriptionRL:
                performDescriptionClick();
                break;
        }
    }

    private void performDescriptionClick() {
        showInformationDialog(mActivity, "Description", CapitalizedFirstLetter(mItemProductModel.getDescription()));
    }

    private void showConfirmDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_draft_item);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //Execute api
                if (isNetworkAvailable(mActivity))
                    executeDeteleApi();
                else
                    showToast(mActivity, getString(R.string.internet_connection_error));
            }
        });
        alertDialog.show();
    }

    public void showInformationDialog(Activity mActivity, String strHeading, String strDescriptions) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_information);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        ImageView imgDismissIV = alertDialog.findViewById(R.id.imgDismissIV);
        TextView txtHeadingTV = alertDialog.findViewById(R.id.txtHeadingTV);
        TextView txtDescriptionTV = alertDialog.findViewById(R.id.txtDescriptionTV);


        txtHeadingTV.setText(strHeading);
        txtDescriptionTV.setText(strDescriptions);

        imgDismissIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void executeDeteleApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.DELETE_PARTICULARITEM;
        JSONObject params = new JSONObject();
        try {
            params.put("id", mItemProductModel.getId());
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("status");
                    String strMessage = response.getString("message");
                    if (strStatus.equals("1")) {
                        showAlertDialogFinish(mActivity, "Your item has been deleted.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeDetailsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ITEM_DETAILS_API;
        JSONObject params = new JSONObject();
        try {
            params.put("id", mItemProductModel.getId());
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("data")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                for (int i = 0; i < 1; i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);

                    if (!mDataObject.isNull("id"))
                        mItemProductModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mItemProductModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mItemProductModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mItemProductModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mItemProductModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mItemProductModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name")) {
                        mItemProductModel.setColor_name(mDataObject.getString("color_name"));
                        txtColorTV.setText(mDataObject.getString("color_name"));
                    }

                    if (!mDataObject.isNull("brand_name"))
                        mItemProductModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mItemProductModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mItemProductModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mItemProductModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mItemProductModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("location"))
                        mItemProductModel.setLocation(mDataObject.getString("location"));
                    if (!mDataObject.isNull("date"))
                        mItemProductModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mItemProductModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mItemProductModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mItemProductModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mItemProductModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mItemProductModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mItemProductModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mItemProductModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mItemProductModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mItemProductModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mItemProductModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mItemProductModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mItemProductModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mItemProductModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mItemProductModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mItemProductModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        mItemProductModel.setRatings(mDataObject.getString("rating"));
                }

                //Set Data On Widgets
                setDataOnWidgets();
            }
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
