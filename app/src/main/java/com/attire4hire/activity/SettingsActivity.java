package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends BaseActivity {
    /*
     * Initialize Activity
     * */
    Activity mActivity = SettingsActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SettingsActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.txtSignOutTV)
    TextviewRegular txtSignOutTV;
    @BindView(R.id.txtNameTV)
    TextviewSemiBold txtNameTV;
    @BindView(R.id.txtEmailTV)
    TextviewRegular txtEmailTV;
    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;
    @BindView(R.id.rlProfileVerificationRL)
    RelativeLayout rlProfileVerificationRL;
    @BindView(R.id.rlPaymentMethodRL)
    RelativeLayout rlPaymentMethodRL;
    @BindView(R.id.rlAccountSettingRL)
    RelativeLayout rlAccountSettingRL;
    @BindView(R.id.rlDeliveryOptionRL)
    RelativeLayout rlDeliveryOptionRL;
    @BindView(R.id.rlInviteFriendsRL)
    RelativeLayout rlInviteFriendsRL;
    @BindView(R.id.rlAboutUsRL)
    RelativeLayout rlAboutUsRL;
    @BindView(R.id.rlContactUsRL)
    RelativeLayout rlContactUsRL;
    @BindView(R.id.rlTermConditionRL)
    RelativeLayout rlTermConditionRL;
    @BindView(R.id.rlHelpRL)
    RelativeLayout rlHelpRL;
    @BindView(R.id.rlChangePasswordRL)
    RelativeLayout rlChangePasswordRL;
    @BindView(R.id.rlAddPaypalRL)
    RelativeLayout rlAddPaypalRL;
    @BindView(R.id.rlSizeChartRL)
    RelativeLayout rlSizeChartRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Set Data
        setWidgetData();
    }

    private void setWidgetData() {
        if (getUserEmail() != null && getUserEmail().length() > 0)
            txtEmailTV.setText(getUserEmail());

        if (getUserName() != null && getUserName().length() > 0)
            txtNameTV.setText(getUserFirstName() + " " + getUserLastName());

        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http"))
            Picasso.with(mActivity).load(getUserProfilePicture())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicIV);

        else
            imgProfilePicIV.setImageResource(R.drawable.ic_pp_ph);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.rlCancelRL, R.id.txtSignOutTV, R.id.imgEditIV, R.id.rlProfileVerificationRL, R.id.rlPaymentMethodRL,
            R.id.rlAccountSettingRL, R.id.rlDeliveryOptionRL, R.id.rlInviteFriendsRL, R.id.rlAboutUsRL,
            R.id.rlContactUsRL, R.id.rlTermConditionRL, R.id.rlHelpRL, R.id.rlChangePasswordRL,
            R.id.rlAddPaypalRL, R.id.rlSizeChartRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;
            case R.id.txtSignOutTV:
                performLogoutClick();
                break;
            case R.id.imgEditIV:
                perfromEditProfileClick();
                break;
            case R.id.rlProfileVerificationRL:
                performVerificationClick();
                break;
            case R.id.rlPaymentMethodRL:
                performPaymentMethodClick();
                break;
            case R.id.rlAccountSettingRL:
                performAccountSettingClick();
                break;
            case R.id.rlDeliveryOptionRL:
                perfromDeliveryClick();
                break;
            case R.id.rlInviteFriendsRL:
                perfromInviteCFriendClick();
                break;
            case R.id.rlAboutUsRL:
                performAboutUsClick();
                break;
            case R.id.rlContactUsRL:
                performContactUs();
                break;
            case R.id.rlTermConditionRL:
                performTermConditionClick();
                break;
            case R.id.rlHelpRL:
                performHelpClick();
                break;
            case R.id.rlChangePasswordRL:
                perfromChangePasswordClick();
                break;
            case R.id.rlAddPaypalRL:
                performAddPaypalClick();
                break;
            case R.id.rlSizeChartRL:
                performSizeChartClick();
        }
    }

    private void performSizeChartClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.SIZE_CHART);
        startActivity(mIntent);
    }

    private void performAddPaypalClick() {
        if (isNetworkAvailable(mActivity))
            excutePaypalLogInApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
//        startActivity(new Intent(mActivity, PaypalActivity.class));
    }

    private void perfromChangePasswordClick() {
        if (Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.LOGIN_TYPE, "").equalsIgnoreCase("social")) {
            if (getFacebookID() != null && !getFacebookID().equals("")) {
                showAlertDialog(mActivity, "You are unable to change your password as you are logged in via your Facebook account.");
            } else {
                showAlertDialog(mActivity, "You are unable to change your password as you are logged in via your Google account.");
            }
        } else {
            Intent mIntent = new Intent(mActivity, ChangePasswordActivity.class);
            startActivity(mIntent);
        }
    }

    /*logout*/
    private void performLogoutClick() {
        showDialogLogoutt(mActivity);
    }

    public void showDialogLogoutt(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtLogoutTV = alertDialog.findViewById(R.id.txtLogoutTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtLogoutTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (isNetworkAvailable(mActivity))
                    executeLogOutApi();
                else
                    showToast(mActivity, getString(R.string.internet_connection_error));
            }
        });
        alertDialog.show();
    }

    private void perfromEditProfileClick() {
        startActivity(new Intent(mActivity, EditProfileActivity.class));
    }

    private void performVerificationClick() {
        startActivity(new Intent(mActivity, AccountSetUpUpdateActivity.class));
    }

    private void performPaymentMethodClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PAYMENT_VALUE, "Settings");
        startActivity(new Intent(mActivity, PaymentActivity.class).putExtra(ConstantData.PAYMENT_VALUE, "Settings"));
    }

    private void performAccountSettingClick() {
        startActivity(new Intent(mActivity, AccountSettingsActivity.class));
    }

    private void perfromDeliveryClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.DELIVERY);
        startActivity(mIntent);
    }

    private void perfromInviteCFriendClick() {
        shareAppLink();
    }

    private void performAboutUsClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.ABOUT_US);
        startActivity(mIntent);
    }

    private void performContactUs() {
//        Intent mIntent = new Intent(mActivity, ContactUsActivity.class);
//        startActivity(mIntent);
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.CONTACT_US_LINK);
        startActivity(mIntent);
    }

    private void performTermConditionClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.TERM_AND_SERVICES);
        startActivity(mIntent);
    }

    private void performHelpClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.HELP);
        startActivity(mIntent);
    }

    private void executeLogOutApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.LOG_OUT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

//                        showToast(mActivity, response.getString("message"));
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        SharedPreferences preferences = Attire4hirePrefrences.getPreferences(Objects.requireNonNull(getApplicationContext()));
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();

                        disconnectFromFacebook();

                        googleLogOut();

                        FirebaseAuth.getInstance().signOut();

                        Intent intent = new Intent(getApplicationContext(), SocialMediaLoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    //paypal log in
    private void excutePaypalLogInApi() {

        showProgressDialog(mActivity);

        String strURL = ConstantData.PAYPAL_lOGIN_API + "?user_id=" + getUserID();

        Log.e(TAG, "*****Response****" + strURL);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    parseJsonResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void parseJsonResponse(String response) {
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                String login_url = mJsonObjectt.getString("login_url");

                Intent mIntent = new Intent(mActivity, OpenPaYpalLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.PAYPAL_LINK);
                mIntent.putExtra(ConstantData.P_LINK, login_url);
                startActivity(mIntent);

            } else {
                showToast(mActivity, mJsonObjectt.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
