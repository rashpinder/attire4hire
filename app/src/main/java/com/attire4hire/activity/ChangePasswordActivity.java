package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.ButtonBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity {
    /*
     * Initialize Activity
     * */
    Activity mActivity = ChangePasswordActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ChangePasswordActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.editCurrentPasswordET)
    EdittextRegular editCurrentPasswordET;
    @BindView(R.id.editNewPasswordET)
    EdittextRegular editNewPasswordET;
    @BindView(R.id.editConfirmNewPasswordET)
    EdittextRegular editConfirmNewPasswordET;
    @BindView(R.id.submitBT)
    ButtonBold submitBT;
    @BindView(R.id.CurrentPasswordEyeIV)
    ImageView CurrentPasswordEyeIV;
    @BindView(R.id.NewPasswordEyeIV)
    ImageView NewPasswordEyeIV;
    @BindView(R.id.ConfirmPasswordEyeIV)
    ImageView ConfirmPasswordEyeIV;

    int ConfirmPasswordCount = 0, NewPasswordCount = 0, CurrentPasswordCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        editCurrentPasswordET.setTypeface(Typeface.DEFAULT);
        editCurrentPasswordET.setTransformationMethod(new PasswordTransformationMethod());
        editCurrentPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        editNewPasswordET.setTypeface(Typeface.DEFAULT);
        editNewPasswordET.setTransformationMethod(new PasswordTransformationMethod());
        editNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        editConfirmNewPasswordET.setTypeface(Typeface.DEFAULT);
        editConfirmNewPasswordET.setTransformationMethod(new PasswordTransformationMethod());
        editConfirmNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    @OnClick({R.id.rlCancelRL, R.id.submitBT, R.id.CurrentPasswordEyeIV, R.id.NewPasswordEyeIV, R.id.ConfirmPasswordEyeIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;
            case R.id.submitBT:
                performSubmitClick();
                break;

            case R.id.CurrentPasswordEyeIV:
                performCurrentPasswordEyeClick();
                break;

            case R.id.NewPasswordEyeIV:
                performNewPasswordEyeClick();
                break;

            case R.id.ConfirmPasswordEyeIV:
                performConfirmPasswordEyeClick();
                break;
        }
    }

    private void performCurrentPasswordEyeClick() {
        if (CurrentPasswordCount == 0) {
            CurrentPasswordEyeIV.setImageResource(R.drawable.eye);
            editCurrentPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            editCurrentPasswordET.setSelection(editCurrentPasswordET.getText().length());
            CurrentPasswordCount++;

        } else if (CurrentPasswordCount == 1) {
            CurrentPasswordEyeIV.setImageResource(R.drawable.eye_hide);
            editCurrentPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editCurrentPasswordET.setSelection(editCurrentPasswordET.getText().length());
            CurrentPasswordCount = 0;
        }
    }

    private void performNewPasswordEyeClick() {
        if (NewPasswordCount == 0) {
            NewPasswordEyeIV.setImageResource(R.drawable.eye);
            editNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            editNewPasswordET.setSelection(editNewPasswordET.getText().length());
            NewPasswordCount++;

        } else if (NewPasswordCount == 1) {
            NewPasswordEyeIV.setImageResource(R.drawable.eye_hide);
            editNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editNewPasswordET.setSelection(editNewPasswordET.getText().length());
            NewPasswordCount = 0;
        }
    }

    private void performConfirmPasswordEyeClick() {
        if (ConfirmPasswordCount == 0) {
            ConfirmPasswordEyeIV.setImageResource(R.drawable.eye);
            editConfirmNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            editConfirmNewPasswordET.setSelection(editConfirmNewPasswordET.getText().length());
            ConfirmPasswordCount++;

        } else if (ConfirmPasswordCount == 1) {
            ConfirmPasswordEyeIV.setImageResource(R.drawable.eye_hide);
            editConfirmNewPasswordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editConfirmNewPasswordET.setSelection(editConfirmNewPasswordET.getText().length());
            ConfirmPasswordCount = 0;
        }
    }

    private void performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeChangePasswordApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void executeChangePasswordApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.CHANGE_PASSWORD;
        JSONObject params = new JSONObject();
        try {
            params.put("id", getUserID());
            params.put("old_password", editCurrentPasswordET.getText().toString().trim());
            params.put("new_password", editNewPasswordET.getText().toString().trim());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        showAlertDialog1(mActivity, getString(R.string.password_changed));

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public void showAlertDialog1(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    /*
     * Add validations
     * */
    private Boolean isValidate() {
        boolean flag = true;
        if (editCurrentPasswordET.getText().toString().trim().equals("") ) {
            showAlertDialog(mActivity, getString(R.string.please_enter_current_password));
            flag = false;
        }  else if (editCurrentPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_password));
            flag = false;
        }else if (editNewPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password));
            flag = false;
        } else if (editNewPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_password));
            flag = false;
        }else if (editConfirmNewPasswordET.getText().toString().trim().equals("") || !editNewPasswordET.getText().toString().trim().equals(editConfirmNewPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_mismatched));
            flag = false;
        }
        return flag;
    }
}