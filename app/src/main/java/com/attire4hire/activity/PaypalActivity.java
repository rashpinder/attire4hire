package com.attire4hire.activity;

import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaypalActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = PaypalActivity.this;

    /*
     * Getting the Current Class Name
     */
    String TAG = PaypalActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;

    @BindView(R.id.et_mail)
    EditText et_mail;

    @BindView(R.id.tv_submit)
    TextView tv_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.rlCancelRL, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.tv_submit:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        if (validate()) {
            if (isNetworkAvailable(mActivity))
                executeUpdatePayPalVerificationApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    //paypal verification
    private void executeUpdatePayPalVerificationApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_PAYPAL_VERIFICATION;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("paypal_id", et_mail.getText().toString().trim());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        showAlertDialogFinish(mActivity, response.getString("message"));

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /*
     * Add validations
     * */
    private Boolean validate() {
        boolean flag = true;
        if (et_mail.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(et_mail.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        }
        return flag;
    }
}
