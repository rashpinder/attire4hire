package com.attire4hire.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.attire4hire.R;
import com.attire4hire.fonts.ButtonBold;
import com.attire4hire.fonts.EdittextRegular;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.FirebaseUser;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MobileVerificationActivity extends BaseActivity {
    Activity mActivity = MobileVerificationActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = MobileVerificationActivity.this.getClass().getSimpleName();

    CountryCodePicker ccp;

    @BindView(R.id.phoneNumberET)
    EdittextRegular phoneNumberET;

    @BindView(R.id.continueBtn)
    ButtonBold continueBtn;

    @BindView(R.id.backIV)
    ImageView backIV;

    String phoneNumber = "", countryCode = "";

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseAuthSettings firebaseAuthSettings = auth.getFirebaseAuthSettings();
    FirebaseUser user = auth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);

        ButterKnife.bind(this);

        /*Set Country Code Picker*/
        setCountryCodePicker();
    }

    private void setCountryCodePicker() {
        ccp = findViewById(R.id.ccp);
        ccp.enableHint(false);
        ccp.setTextColor(getResources().getColor(R.color.white));
        ccp.registerPhoneNumberTextView(phoneNumberET);
        ccp.setDefaultCountryUsingNameCodeAndApply("GB");
        Log.e(TAG, "Code== " + ccp.getSelectedCountryCode());

        countryCode = ccp.getSelectedCountryCodeWithPlus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });
    }

    private void performContinueClick() {
        phoneNumber = countryCode + phoneNumberET.getText().toString().trim();

        if (phoneNumber.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phoneNumber));
        } else if (phoneNumberET.length() < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_phone_number));
        } else {
            Intent intent = new Intent(mActivity, OtpVerificationActivity.class);
            intent.putExtra("Phone", phoneNumber);
            startActivity(intent);
            finish();
        }
    }

    @OnClick({R.id.continueBtn, R.id.backIV})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.continueBtn:
                performContinueClick();
                break;

            case R.id.backIV:
                onBackPressed();
                break;
        }
    }
}
