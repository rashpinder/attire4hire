package com.attire4hire.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.CityAutoCompleteAdapter;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.model.CityAutoCompleteModel;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.attire4hire.utils.WordUtils;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    Activity mActivity = EditProfileActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    private ImageView back;
    private ImageView profile_pic, addProfilePic;
    private EdittextRegular firstName, lastName, et_gender;
    TextviewRegular userNumber;
    private File photoFile;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private String imagepath, gender;
    private Activity activity;
    private RelativeLayout cancelRL,profileRL;
    private TextviewRegular tv_save, tv_edit;
    private Spinner genderSpinner;
    private AutoCompleteTextView cityET;
    private ImageView imgClearIV;
    Bitmap mBitmapInsurance = null;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    CityAutoCompleteAdapter mCityAutoCompleteAdapter;
    ArrayList<CityAutoCompleteModel> mCitiesArrayList = new ArrayList<>();
    ItemProductModel mModel;
    String mBase64Image = "", phoneNumber = "";

    TextView itemTextView;
    View v;
    private static final int PICK_IMAGE = 100;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        activity = EditProfileActivity.this;

        intIds();
        performActions();

        // ***Get Profile Data***
        executeProfileApi();

        setCityAutoComplete();
    }


    public void showCameraGalleryAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtCameraTV = alertDialog.findViewById(R.id.txtCameraTV);
        TextView txtGalleryTV = alertDialog.findViewById(R.id.txtGalleryTV);
        TextView btnCancelTV = alertDialog.findViewById(R.id.btnCancelTV);

        btnCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtCameraTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                takePhotoFromCamera();
            }
        });

        txtGalleryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                choosePhotoFromGallary();
            }
        });
        alertDialog.show();
    }




    public void takePhoto(Activity view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(),  "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        imageUri = Uri.fromFile(photo);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
//                    String path = saveImage(bitmap);
//                    Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    profile_pic.setImageBitmap(bitmap);
                    mBase64Image = encodeTobase64(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == TAKE_PICTURE) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            profile_pic.setImageBitmap(thumbnail);
            mBase64Image = encodeTobase64(thumbnail);
//            saveImage(thumbnail);
//            Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }



//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        /* dhaval2404 cropper */
//        if (resultCode == Activity.RESULT_OK) {
//            //Image Uri will not be null for RESULT_OK
//            //val fileUri = data?.data
//            if (data != null) {
//
//                Uri mFileUri = data.getData();
//
//                showImage(mFileUri);
//
//            } else if (resultCode == ImagePicker.RESULT_ERROR) {
//                Toast.makeText(mActivity, ("Image Not Valid"), Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show();
//            }
//        }
//
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        }
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//
//                try {
//                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
//                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
//                    profile_pic.setImageBitmap(selectedImage);
//                    mBase64Image = encodeTobase64(selectedImage);
//
//                    Log.e(TAG, "**Image Base 64**" + mBase64Image);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                showToast(mActivity, "Cropping failed: " + result.getError());
//            }
//        }
//    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "attire");
        if (!wallpaperDirectory.exists()) {  // have the object build the directory structure, if needed.
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case TAKE_PICTURE:
//                if (resultCode == Activity.RESULT_OK) {
//                    Uri selectedImage = data.getData();
////                    getContentResolver().notifyChange(selectedImage, null);
////                    ImageView imageView = (ImageView) findViewById(R.id.ImageView);
//                    ContentResolver cr = getContentResolver();
//                    Bitmap bitmap;
//                    try {
//                        bitmap = android.provider.MediaStore.Images.Media
//                                .getBitmap(cr, selectedImage);
//                        mBase64Image = encodeTobase64(bitmap);
//                        profile_pic.setImageBitmap(bitmap);
//                        Toast.makeText(this, selectedImage.toString(),
//                                Toast.LENGTH_LONG).show();
//                    } catch (Exception e) {
//                        Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
//                                .show();
//                        Log.e("Camera", e.toString());
//                    }
//                }
//            case PICK_IMAGE:
//                if (resultCode == RESULT_OK){
//                    imageUri = data.getData();
//                    showImage(imageUri);
////                    profile_pic.setImageURI(imageUri);
//        }
//        }
//    }



    private void intIds() {
        cancelRL = findViewById(R.id.cancelRL);
        profile_pic = findViewById(R.id.profile_pic);
        addProfilePic = findViewById(R.id.addProfilePic);
        profileRL = findViewById(R.id.profileRL);
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        userNumber = findViewById(R.id.userNumber);
        cancelRL = findViewById(R.id.cancelRL);
        tv_save = findViewById(R.id.tv_save);
        tv_edit = findViewById(R.id.tv_edit);
        cityET = findViewById(R.id.cityET);
        genderSpinner = findViewById(R.id.genderSpinner);
        imgClearIV = findViewById(R.id.imgClearIV);
        et_gender = findViewById(R.id.et_gender);
    }

    private void performActions() {
        cancelRL.setOnClickListener(this);
        profileRL.setOnClickListener(this);
        userNumber.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        tv_edit.setOnClickListener(this);

//        genderSpinner.setEnabled(false);
//        addProfilePic.setEnabled(false);
//        firstName.setEnabled(false);
//        userNumber.setEnabled(false);
//        lastName.setEnabled(false);
//        cityET.setEnabled(false);
        cityET.setTextColor(getResources().getColor(R.color.white));

        userNumber.setClickable(true);

        firstName.addTextChangedListener(new GenericTextWatcher1(firstName));
        lastName.addTextChangedListener(new GenericTextWatcher1(lastName));
        cityET.addTextChangedListener(new GenericTextWatcher1(cityET));

        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.Phone_TYPE, "");
    }

    public class GenericTextWatcher1 implements TextWatcher {

        private View view;

        private GenericTextWatcher1(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {

                case R.id.firstName:
                    try {
                        String inputString = "" + firstName.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + firstName.getText().toString())) {
                            firstName.setText("" + firstLetterCapString);
                            firstName.setSelection(firstName.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.lastName:
                    try {
                        String inputString = "" + lastName.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + lastName.getText().toString())) {
                            lastName.setText("" + firstLetterCapString);
                            lastName.setSelection(lastName.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_city:
                    try {
                        String inputString = "" + cityET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + cityET.getText().toString())) {
                            cityET.setText("" + firstLetterCapString);
                            cityET.setSelection(cityET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /*
     * Gender adapter
     * */
    private void setGenderAdapter() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "ProximaNova-Semibold.ttf");

        ArrayList<String> mStatusArry = new ArrayList<>();

        mStatusArry.add("Gender");
        mStatusArry.add("Male");
        mStatusArry.add("Female");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArry) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);

                    TextView itemTextView = v.findViewById(R.id.itemTV);

                    if (position == 0) {
                        itemTextView.setVisibility(View.INVISIBLE);
                    }

                    String strName = mStatusArry.get(position);
                    itemTextView.setText(strName);
                    itemTextView.setTypeface(font);
                }
                return v;
            }
        };
        genderSpinner.setAdapter(adapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = mStatusArry.get(position);
                if (position == 0) {
                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.white));
                } else {
                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.white));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mModel.getGender() != null && mModel.getGender().length() > 0) {
            for (int i = 0; i < mStatusArry.size(); i++) {
                if (mModel.getGender().equals(mStatusArry.get(i))) {
                    genderSpinner.setSelection(i);
                }
            }
        }
    }

    /*Set Cities Auto Complete*/
    private void setCityAutoComplete() {
        imgClearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearText();
            }
        });

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                CityAutoCompleteModel mModel = new CityAutoCompleteModel();
                if (!mDataObj.isNull("country"))
                    mModel.setCountry(mDataObj.getString("country"));
                if (!mDataObj.isNull("geonameid"))
                    mModel.setGeonameid(mDataObj.getString("geonameid"));
                if (!mDataObj.isNull("name"))
                    mModel.setName(mDataObj.getString("name"));
                if (!mDataObj.isNull("subcountry"))
                    mModel.setSubcountry(mDataObj.getString("subcountry"));

                mCitiesArrayList.add(mModel);
            }

            Log.e(TAG, "***Array Size***" + mCitiesArrayList.size());
        } catch (Exception e) {
            e.toString();
        }

        mCityAutoCompleteAdapter = new CityAutoCompleteAdapter(this, mCitiesArrayList);

        cityET.setThreshold(0);

        cityET.setAdapter(mCityAutoCompleteAdapter);

        cityET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (cityET.getText().toString().length() != 0) {
                    if (hasFocus) {
                        cityET.showDropDown();
                    }
                }
            }
        });

        cityET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                cityET.showDropDown();
                return false;
            }
        });

        cityET.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                //do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    imgClearIV.setVisibility(View.VISIBLE);
                    if (cityET.hasFocus()) {
                        cityET.showDropDown();
                    }
                } else {
                    imgClearIV.setVisibility(View.GONE);
                }
            }
        });

        cityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                CityAutoCompleteModel mModel = (CityAutoCompleteModel) arg0.getItemAtPosition(arg2);
                cityET.setText(mModel.getName());
                cityET.setSelection(cityET.getText().toString().trim().length());
                HideKey();
            }
        });
    }

    private void clearText() {
        cityET.setText("");
        imgClearIV.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;

//            case R.id.addProfilePic:
//                perfromChangeProfilePicClick();
//                break;

            case R.id.profileRL:
                perfromChangeProfilePicClick();
                break;

            case R.id.userNumber:
                startActivity(new Intent(mActivity, MobileVerificationActivity.class));
                break;

            case R.id.tv_edit:
                addProfilePic.setVisibility(View.VISIBLE);
                addProfilePic.setEnabled(true);
                firstName.setEnabled(true);
                lastName.setEnabled(true);
                cityET.setEnabled(true);
                userNumber.setEnabled(true);
                cityET.setTextColor(getResources().getColor(R.color.white));
                tv_edit.setVisibility(View.GONE);
                tv_save.setVisibility(View.VISIBLE);
                genderSpinner.setEnabled(true);
                // ***City Auto Complete***
//                setCityAutoComplete();
                break;

            case R.id.tv_save:
                PerformSaveProfileClick();
                break;
        }
    }

    private void executeProfileApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_USER_PROFILE_DETAILS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("user_details")) {
                JSONObject mDataObj = response.getJSONObject("user_details");

                mModel = new ItemProductModel();

                if (!mDataObj.isNull("id")) {
                    mModel.setUser_id(mDataObj.getString("id"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));
                }
                if (!mDataObj.isNull("first_name")) {
                    mModel.setUser_First_Name(mDataObj.getString("first_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));
                }
                if (!mDataObj.isNull("last_name")) {
                    mModel.setUser_Last_Name(mDataObj.getString("last_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));
                }
                if (!mDataObj.isNull("email")) {
                    mModel.setUser_Email(mDataObj.getString("email"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));
                }
                if (!mDataObj.isNull("gender")) {
                    mModel.setGender(mDataObj.getString("gender"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));
                }
                if (!mDataObj.isNull("city_name")) {
                    mModel.setUser_City(mDataObj.getString("city_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CITY, mDataObj.getString("city_name"));
                }
                if (!mDataObj.isNull("phone_no")) {
                    mModel.setUser_PhoneNo(mDataObj.getString("phone_no"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));
                }
                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals("")) {
                    mModel.setUser_ProfilePic(mDataObj.getString("profile_pic"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));
                }
                mItemProductModel.add(mModel);
            }

            //Set Data ON/ Widgets
            GetData();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void GetData() {

        if (mModel.getUser_First_Name() != null) {
            firstName.setText(mModel.getUser_First_Name());
        }
        if (mModel.getUser_Last_Name() != null) {
            lastName.setText(mModel.getUser_Last_Name());
        }
        if (mModel.getUser_City() != null) {
            cityET.setText(mModel.getUser_City());
        }
        if (mModel.getUser_City() != null) {
            et_gender.setText(mModel.getGender());
        }
        if (mModel.getUser_PhoneNo() != null) {
            userNumber.setText(mModel.getUser_PhoneNo());
        }

        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
            Glide.with(mActivity).load(getUserProfilePicture())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .into(profile_pic);
        }

//        if (mModel.getUser_ProfilePic() != null && mModel.getUser_ProfilePic().contains("http") && mModel.getUser_ProfilePic().contains("https")) {
        if (mModel.getUser_ProfilePic() != null && mModel.getUser_ProfilePic().contains("http")) {
            Intent intent = new Intent("changeImage");
            intent.putExtra("Status", "editImage");
            intent.putExtra("editImage", mModel.getUser_ProfilePic());
            LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
        }

        // ***Gender Adapter***
        setGenderAdapter();
    }

    private void executeUpdateProfileApi() {

        if (mBase64Image.equals("")) {
            mBase64Image = getUserProfilePicture();
        }

        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.EDIT_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();

        try {
            params.put("user_id", getUserID());
            params.put("first_name", firstName.getText().toString().trim());
            params.put("last_name", lastName.getText().toString().trim());
            params.put("gender", gender);
            params.put("city_name", cityET.getText().toString().trim());
            params.put("phone", userNumber.getText().toString().trim());
            params.put("profile_image", mBase64Image);
        } catch (Exception e) {
            e.toString();
        }

        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        parseResponseProfile(response);

                    } else {
                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponseProfile(JSONObject response) {
        try {
            if (!response.isNull("user_detail")) {
                JSONObject mDataObj = response.getJSONObject("user_detail");

                mModel = new ItemProductModel();

                if (!mDataObj.isNull("id")) {
                    mModel.setUser_id(mDataObj.getString("id"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));
                }
                if (!mDataObj.isNull("first_name")) {
                    mModel.setUser_First_Name(mDataObj.getString("first_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));
                }
                if (!mDataObj.isNull("last_name")) {
                    mModel.setUser_Last_Name(mDataObj.getString("last_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));
                }
                if (!mDataObj.isNull("email")) {
                    mModel.setUser_Email(mDataObj.getString("email"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));
                }
                if (!mDataObj.isNull("gender")) {
                    mModel.setGender(mDataObj.getString("gender"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));
                }
                if (!mDataObj.isNull("city_name")) {
                    mModel.setUser_City(mDataObj.getString("city_name"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CITY, mDataObj.getString("city_name"));
                }
                if (!mDataObj.isNull("phone_no")) {
                    mModel.setUser_PhoneNo(mDataObj.getString("phone_no"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));
                }
                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals("")) {
                    mModel.setUser_ProfilePic(mDataObj.getString("profile_pic"));
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));
                }
                mItemProductModel.add(mModel);
            }

            //Set Data ON Widgets
            GetData();

            addProfilePic.setVisibility(View.VISIBLE);
//            addProfilePic.setEnabled(false);
//            firstName.setEnabled(false);
//            lastName.setEnabled(false);
//            cityET.setEnabled(false);
//            userNumber.setEnabled(false);
            cityET.setTextColor(getResources().getColor(R.color.white));
            tv_save.setVisibility(View.VISIBLE);
//            tv_edit.setVisibility(View.VISIBLE);
//            genderSpinner.setEnabled(false);

            showAlertDialogFinish(mActivity, getString(R.string.profile_update));

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void PerformSaveProfileClick() {
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeUpdateProfileApi();
            }
        }
    }

    private Boolean validate() {
        boolean flag = true;
        if (firstName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (lastName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_surname));
            flag = false;
        } else if (gender.equals("Gender") || gender.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_gender));
            flag = false;
        } else if (cityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city));
            flag = false;
        } else if (userNumber.getText().toString().trim().length() < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_phone_number));
            flag = false;
        }
        return flag;

    }

    @Override
    protected void onResume() {
        super.onResume();

        GetUserNumber();
    }

    private void GetUserNumber() {
        if (getUserPhoneNo() != null) {
            phoneNumber = getUserPhoneNo();
            userNumber.setText(phoneNumber);
        } else {

        }
    }

    /* Camera Gallery functionality
     * */
    public void perfromChangeProfilePicClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        showCameraGalleryAlertDialog(mActivity);
//        CropImage.startPickImageActivity(mActivity);
//        ImagePicker.Companion.with(this)
//                .crop()                    //Crop image(Optional), Check Customization for more option
//                .compress(720)            //Final image size will be less than 1 MB(Optional)
//                .maxResultSize(720, 720)    //Final image resolution will be less than 1080 x 1080(Optional)
//                .start();
    }


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE);
    }

    private void takePhotoFromCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, TAKE_PICTURE);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }
    



    private void showImage(Uri imageUri) {
        String file = getRealPathFromURI_API19(mActivity, imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap selectedImage = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        if (decoded != null) {
            profile_pic.setImageBitmap(selectedImage);
            mBase64Image = encodeTobase64(selectedImage);
        }
    }

    private void HideKey() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
