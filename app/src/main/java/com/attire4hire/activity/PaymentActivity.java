package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.CardAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class PaymentActivity extends BaseActivity implements View.OnClickListener {
    Activity mActivity = PaymentActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = PaymentActivity.this.getClass().getSimpleName();

    /*Widgets*/
    RelativeLayout cancelRL, addRL;
    TextView mMakePayTV;
    RecyclerView rv_Card;
    TextviewBold tv_NoRequest;
    RelativeLayout ll_payment;
    LinearLayout llNoDataFountTV;
    Button btnAddCard;

    ArrayList<ItemProductModel> mmItemProductModel = new ArrayList<>();
    CardAdapter mAdapter;
    String Payment = "", mCard = "", Card_id = "";
    ItemProductModel itemProductModel;
    String payment_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        initialization();
        getIntentData();
        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mmItemProductModel != null) {
            mmItemProductModel.clear();
            mCard = "";
            executeRetrieveCardApi();
        }
    }

    private void getIntentData() {
        if (getIntent() != null) {

            Payment = getIntent().getStringExtra(ConstantData.PAYMENT_VALUE);

            if (Payment.equalsIgnoreCase("Cart")) {
                mMakePayTV.setVisibility(View.VISIBLE);
                ((RelativeLayout.LayoutParams) ll_payment.getLayoutParams()).setMargins(0, 0, 0, 30);
                itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
                payment_type = "Cart";
            } else if (Payment.equalsIgnoreCase("Settings")) {
                mMakePayTV.setVisibility(View.GONE);
                ((RelativeLayout.LayoutParams) ll_payment.getLayoutParams()).setMargins(0, 0, 0, 0);
                payment_type = "Settings";
            } else if (Payment.equalsIgnoreCase("Penalty")) {
                mMakePayTV.setVisibility(View.VISIBLE);
                ((RelativeLayout.LayoutParams) ll_payment.getLayoutParams()).setMargins(0, 0, 0, 30);
//                itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
                payment_type = "Penalty";
            }
        }
    }

    /*set data*/
    private void setData() {
        cancelRL.setOnClickListener(this);
        mMakePayTV.setOnClickListener(this);
        addRL.setOnClickListener(this);
        btnAddCard.setOnClickListener(this);
    }

    /*initialization*/
    private void initialization() {
        cancelRL = findViewById(R.id.cancelRL);
        mMakePayTV = findViewById(R.id.tv_make_pay);
        addRL = findViewById(R.id.addRL);
        rv_Card = findViewById(R.id.rv_Card);
        tv_NoRequest = findViewById(R.id.tv_NoRequest);
        ll_payment = findViewById(R.id.ll_payment);
        llNoDataFountTV = findViewById(R.id.llNoDataFountTV);
        btnAddCard = findViewById(R.id.btnAddCard);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.tv_make_pay:
                perfomMakePaymentClick();
                break;

            case R.id.addRL:
                startActivity(new Intent(mActivity, Card_Activity.class));
                break;

            case R.id.btnAddCard:
                performAddClick();
                break;
        }
    }

    private void performAddClick() {
        startActivity(new Intent(mActivity, Card_Activity.class));
    }

    private void perfomMakePaymentClick() {
        if (!mCard.equals("")) {

            if (payment_type.equals("Penalty")) {
                executePenaltyPaymentAPI();
            } else {
                if (itemProductModel.getExtension().equals("1")) {
                    executeUpdateExtPaymentAPI();
                } else {
                    executeUpdateOrderPayment();
                }
            }
        } else {
            showAlertDialog(mActivity, "Please select your payment card.");
        }
    }

    /* execute update order API */
    private void executeUpdateOrderPayment() {
        showProgressDialog1(mActivity);
        String mApiUrl = ConstantData.UPDATE_ORDER_PAYMENT;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", itemProductModel.getOrder_id());
            params.put("card_id", Card_id);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog1();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog1();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        dismissProgressDialog1();
        try {

            if (response.getString("status").equals("1")) {
                try {

                    showCardTextDialog(mActivity, response.getString("message"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (response.getString("status").equals("3")) {

                showCardDialog(mActivity, response.getString("message"));

            } else {

                showCardDialog(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    public void showCardTextDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                Intent intent = new Intent(mActivity, OrderConfirmationActivity.class);
                intent.putExtra(ConstantData.MODEL, itemProductModel);
                startActivity(intent);
                finish();
            }
        });
        alertDialog.show();
    }

    public void showCardDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void SetCardAdapter() {
        rv_Card.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        mAdapter = new CardAdapter(mActivity, mmItemProductModel, new CardAdapter.AddCard() {
            @Override
            public void addcard(int position, String card_id) {
                mCard = String.valueOf(position);
                Card_id = card_id;
            }
        });
        rv_Card.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void executeRetrieveCardApi() {
        showProgressDialog1(mActivity);
        String mApiUrl = ConstantData.RETRIEVE_THE_CARDS;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog1();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("status");
                    String strMessage = response.getString("message");

                    if (strStatus.equals("1")) {

                        parseJsonResponse(response);

//                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();

                    } else if (strStatus.equals("3")) {
                        LogOut();

                    } else {
//                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
                    }

                    if (mmItemProductModel.size() > 0) {

                        ll_payment.setVisibility(View.VISIBLE);
//                        tv_NoRequest.setVisibility(View.GONE);
                        llNoDataFountTV.setVisibility(View.GONE);

                        /*
                         * Set Adapter
                         * */
                        SetCardAdapter();

                    } else {
                        ll_payment.setVisibility(View.GONE);
//                        tv_NoRequest.setVisibility(View.VISIBLE);
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog1();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(JSONObject response) {
        try {
            JSONArray mJsonArray = response.getJSONArray("data");

            Log.e(TAG, "**Data Array Size**" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObject = mJsonArray.getJSONObject(i);
                ItemProductModel model = new ItemProductModel();

                if (!mDataObject.isNull("card_id")) {
                    model.setCard_Id(mDataObject.getString("card_id"));
                }
                if (!mDataObject.isNull("brand"))
                    model.setCardBrand(mDataObject.getString("brand"));

                if (!mDataObject.isNull("country"))
                    model.setCardCountry(mDataObject.getString("country"));

                if (!mDataObject.isNull("exp_month"))
                    model.setExp_month(mDataObject.getString("exp_month"));

                if (!mDataObject.isNull("exp_year"))
                    model.setExp_year(mDataObject.getString("exp_year"));

                if (!mDataObject.isNull("last4"))
                    model.setLast4(mDataObject.getString("last4"));

                if (!mDataObject.isNull("card_holder_name"))
                    model.setCard_holder_name(mDataObject.getString("card_holder_name"));

                mmItemProductModel.add(model);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog1(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog1() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (RuntimeException ex) {

            }
        }
    }

    /* API to Update Extension Payment */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("order_id", itemProductModel.getOrder_id());
        mMap.put("card_id", Card_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateExtPaymentAPI() {
        showProgressDialog(mActivity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.UpdateExtPayment(mParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {
                            showCardTextDialog(mActivity, jsonObjectMM.getString("message"));
                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    /* API for Penalty Payment */
    private Map<String, String> mPenaltyParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("card_id", Card_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePenaltyPaymentAPI() {
        showProgressDialog(mActivity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.PenaltyPayment(mPenaltyParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {
                            showHomeAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        } else if (jsonObjectMM.getString("status").equals("3")) {
                            showHomeAlertDialog(mActivity, getString(R.string.failed_to_make_the_payment));
                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}
