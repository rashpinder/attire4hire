package com.attire4hire.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.attire4hire.R;
import com.attire4hire.utils.ConstantData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenLinkActivity extends AppCompatActivity {
    /*
     * Initialize Activity
     * */
    Activity mActivity = OpenLinkActivity.this;
    /*
     * Get Activity Name
     * */
    String TAG = OpenLinkActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.mWebViewWV)
    WebView mWebViewWV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;

    /*
     * Initlize Objects
     * */
    String strLinkUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);

        //butter knife
        ButterKnife.bind(this);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            strLinkUrl = getIntent().getStringExtra(ConstantData.LINK);
            Log.e(TAG, "**Link**" + strLinkUrl);
            if (strLinkUrl.equals(ConstantData.TERM_AND_SERVICES)) {
                txtTitleTV.setText(getString(R.string.terms_of_service));
            } else if (strLinkUrl.equals(ConstantData.DELIVERY)) {
                txtTitleTV.setText(getString(R.string.delivery_options));
            } else if (strLinkUrl.equals(ConstantData.ABOUT_US)) {
                txtTitleTV.setText(getString(R.string.about_us));
            } else if (strLinkUrl.equals(ConstantData.HELP)) {
                txtTitleTV.setText(getString(R.string.FAQ));
            } else if (strLinkUrl.equals(ConstantData.CONTACT_US_LINK)) {
                txtTitleTV.setText(getString(R.string.contact_us));
            } else if (strLinkUrl.equals(ConstantData.PRIVACY_POLICY)) {
                txtTitleTV.setText(getString(R.string.privacy_policy));
            } else if (strLinkUrl.equals(ConstantData.SIZE_CHART)) {
                txtTitleTV.setText(getString(R.string.size_chart));
            }
            loadUrlInWebView(strLinkUrl);
        }
    }

    private void loadUrlInWebView(String strLinkUrl) {
        mWebViewWV.setWebViewClient(new MyWebClient());
        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.loadUrl(strLinkUrl);
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            mProgressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }
    }

    // To handle "Back" key press event for WebView to go back to previous screen.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebViewWV.canGoBack()) {
            mWebViewWV.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
