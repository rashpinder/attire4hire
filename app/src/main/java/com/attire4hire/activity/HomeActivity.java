package com.attire4hire.activity;

import static com.attire4hire.fragments.SearchFragment.strPerformersType;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fragments.ChatFragment;
import com.attire4hire.fragments.ClosetFragment;
import com.attire4hire.fragments.FavoriteFragment;
import com.attire4hire.fragments.ProfileFragment;
import com.attire4hire.fragments.SearchFragment;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity {

    Activity mActivity = HomeActivity.this;

    /*Getting current class Name*/
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.iv_chat)
    ImageView chatIV;
    @BindView(R.id.iv_profile)
    CircleImageView profileIV;
    @BindView(R.id.iv_closet)
    ImageView closetIV;
    @BindView(R.id.iv_fav)
    ImageView favIV;
    @BindView(R.id.iv_search)
    ImageView SearchIV;
    @BindView(R.id.closet_badge)
    TextviewRegular closet_badge;
    @BindView(R.id.chat_badge)
    TextviewRegular chat_badge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        SetProfilePic();

        SearchIV.setImageResource(R.drawable.ic_search_sel);

        if (getIntent().getStringExtra(ConstantData.REDIRECION_TYPE) != null
                && Objects.equals(getIntent().getStringExtra(ConstantData.REDIRECION_TYPE), ConstantData.CLOSET)) {
            switchFragment(new ClosetFragment(), "", false, null);
        } else {
            switchFragment(new SearchFragment(), "", false, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isNetworkAvailable(mActivity)) {
            executeProfileApi();
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    @OnClick({R.id.iv_chat, R.id.iv_profile, R.id.iv_closet, R.id.iv_fav, R.id.iv_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_chat:
                if (isNetworkAvailable(mActivity)) {
                    executeProfileApi();
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error));
                switchFragment(new ChatFragment(), "", false, null);
                chatIV.setImageResource(R.drawable.ic_chat_sel);
                closetIV.setImageResource(R.drawable.ic_closet_tb);
                favIV.setImageResource(R.drawable.ic_fav_tb);
                SearchIV.setImageResource(R.drawable.ic_search_tb);
                profileIV.setBorderColor(getResources().getColor(R.color.lightGray));
                profileIV.setBorderWidth(1);
                break;

            case R.id.iv_profile:
//                if (isNetworkAvailable(mActivity)) {
//                    executeProfileApi();
//                } else
//                    showToast(mActivity, getString(R.string.internet_connection_error));
                switchFragment(new ProfileFragment(), "", false, null);
                closetIV.setImageResource(R.drawable.ic_closet_tb);
                chatIV.setImageResource(R.drawable.ic_mssg_tb);
                favIV.setImageResource(R.drawable.ic_fav_tb);
                SearchIV.setImageResource(R.drawable.ic_search_tb);
                profileIV.setBorderColor(getResources().getColor(R.color.darkPink));
                profileIV.setBorderWidth(1);
                break;

            case R.id.iv_closet:
                if (isNetworkAvailable(mActivity)) {
                    executeProfileApi();
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error));
                switchFragment(new ClosetFragment(), "", false, null);
                closetIV.setImageResource(R.drawable.ic_closet_sel);
                chatIV.setImageResource(R.drawable.ic_mssg_tb);
                favIV.setImageResource(R.drawable.ic_fav_tb);
                SearchIV.setImageResource(R.drawable.ic_search_tb);
                profileIV.setBorderColor(getResources().getColor(R.color.lightGray));
                profileIV.setBorderWidth(1);
                break;

            case R.id.iv_fav:
                if (isNetworkAvailable(mActivity)) {
                    executeProfileApi();
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error));
                switchFragment(new FavoriteFragment(), "", false, null);
                favIV.setImageResource(R.drawable.ic_heart);
                chatIV.setImageResource(R.drawable.ic_mssg_tb);
                closetIV.setImageResource(R.drawable.ic_closet_tb);
                SearchIV.setImageResource(R.drawable.ic_search_tb);
                profileIV.setBorderColor(getResources().getColor(R.color.lightGray));
                break;

            case R.id.iv_search:
                if (isNetworkAvailable(mActivity)) {
                    executeProfileApi();
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error));

                /* clear performers' corner data */
                strPerformersType = "0";
                switchFragment(new SearchFragment(), "", false, null);
                SearchIV.setImageResource(R.drawable.ic_search_sel);
                chatIV.setImageResource(R.drawable.ic_mssg_tb);
                closetIV.setImageResource(R.drawable.ic_closet_tb);
                favIV.setImageResource(R.drawable.ic_fav_tb);
                profileIV.setBorderColor(getResources().getColor(R.color.lightGray));
                profileIV.setBorderWidth(1);
                break;
        }
    }

    private void SetProfilePic() {
//        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http") && getUserProfilePicture().contains("https")) {
        if (getUserProfilePicture() != null && getUserProfilePicture().contains("http")) {
            Picasso.with(mActivity).load(getUserProfilePicture())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(profileIV);
        }

        if (getIntent() != null) {

            LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String status = intent.getStringExtra("Status");
                    if (status.equals("editImage")) {
                        Picasso.with(context).load(intent.getStringExtra("editImage")).
                                placeholder(R.drawable.ic_pp_ph)
                                .error(R.drawable.ic_pp_ph)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(profileIV);
                    } else if (status.equals("message_count")) {
                        chat_badge.setText(intent.getStringExtra("message_count"));
                        executeProfileApi();
                    }
                }
            }, new IntentFilter("changeImage"));

            LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String status = intent.getStringExtra("CountStatus");
                    if (status.equalsIgnoreCase("editCount")) {
                        if (intent.getStringExtra("chat_badge") != null && !intent.getStringExtra("chat_badge").equals("") && !intent.getStringExtra("chat_badge").equals("0")) {
                            chat_badge.setVisibility(View.VISIBLE);
                            chat_badge.setText(intent.getStringExtra("chat_badge"));
                        } else {
                            chat_badge.setVisibility(View.GONE);
                        }
                    } else if (status.equalsIgnoreCase("editTotalCount")) {

                        if (intent.getStringExtra("total_badge") != null && !intent.getStringExtra("total_badge").equals("") && !intent.getStringExtra("total_badge").equals("0")) {
                            closet_badge.setVisibility(View.VISIBLE);
                            closet_badge.setText(intent.getStringExtra("total_badge"));
                        } else {
                            closet_badge.setVisibility(View.GONE);
                        }
                    }
                }
            }, new IntentFilter("changeCount"));
        }
    }

    /*to switch between fragments*/
    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void executeProfileApi() {
//        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("viewed_user", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
//                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        startActivity(new Intent(mActivity, SocialMediaLoginActivity.class));
                    } else {
                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }

//                    executecreateUserStripeAccountAPI();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("user_details")) {
                JSONObject mDataObj = response.getJSONObject("user_details");

                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.REQUEST_COUNT, response.getString("inquiry_count"));
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.INQUIRY_COUNT, response.getString("request_count"));
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CURRENT_BOOKING_COUNT, response.getString("current_booking_count"));
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.MY_BOOKING_COUNT, response.getString("my_booking_count"));
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.TOTAL_COUNT, response.getString("total_count"));
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PENALTY_PAYMENT, response.getString("penalty_amt"));

                if (!response.isNull("total_count") && !response.getString("total_count").equals("") && !response.getString("total_count").equals("0")) {
                    closet_badge.setVisibility(View.VISIBLE);
                    closet_badge.setText(response.getString("total_count"));
                } else {
                    closet_badge.setVisibility(View.GONE);
                }

                if (!response.isNull("unread_message_count") && !response.getString("unread_message_count").equals("") && !response.getString("unread_message_count").equals("0")) {
                    chat_badge.setVisibility(View.VISIBLE);
                    chat_badge.setText(response.getString("unread_message_count"));
                } else {
                    chat_badge.setVisibility(View.GONE);
                }

                if (!mDataObj.isNull("id"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));

                if (!mDataObj.isNull("first_name"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));

                if (!mDataObj.isNull("last_name"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));

                if (!mDataObj.isNull("email"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));

                if (!mDataObj.isNull("gender"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));

                if (!mDataObj.isNull("phone_no"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));

                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals(""))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));

                if (!mDataObj.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObj.getString("email_verification"));

                if (!mDataObj.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, mDataObj.getString("phone_verification"));

                if (!mDataObj.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, mDataObj.getString("id_verification"));

                if (!mDataObj.isNull("paypal_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFICATION, mDataObj.getString("paypal_verification"));

                if (!mDataObj.isNull("stripe_account_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.STRIPE_VERIFICATION, mDataObj.getString("stripe_account_verification"));

                if (!mDataObj.isNull("stripe_account_id"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.STRIPE_ACCOUNT_ID, mDataObj.getString("stripe_account_id"));

                if (!mDataObj.isNull("block"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.BLOCK, mDataObj.getString("block"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
