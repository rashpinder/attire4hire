package com.attire4hire.activity;

import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsActivity extends BaseActivity {
    /*
     * Initialize Activity
     * */
    Activity mActivity = ContactUsActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ContactUsActivity.this.getClass().getSimpleName();

    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    @BindView(R.id.tv_submit)
    TextviewRegular tv_submit;

    @BindView(R.id.et_Name)
    EdittextRegular et_Name;

    @BindView(R.id.et_mail)
    EdittextRegular et_mail;

    @BindView(R.id.et_message)
    EdittextRegular et_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        ButterKnife.bind(this);

        et_Name.setText(getUserName());
        et_mail.setText(getUserEmail());

        //Capitalize first letter
        //for title
        et_message.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_message.getText().toString().length() == 1 && et_message.getTag().toString().equals("true")) {
                    et_message.setTag("false");
                    et_message.setText(et_message.getText().toString().toUpperCase());
                    et_message.setSelection(et_message.getText().toString().length());
                }
                if (et_message.getText().toString().length() == 0) {
                    et_message.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick({R.id.cancelRL, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.tv_submit:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeContactUsApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    /*
     * Add validations
     * */
    private Boolean isValidate() {
        boolean flag = true;
        if (et_Name.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        } else if (et_mail.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(et_mail.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (et_message.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_message));
            flag = false;
        }
        return flag;
    }

    private void executeContactUsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.CONTACT_US;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("textmessage", et_message.getText().toString());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        showAlertDialogFinish(mActivity, "Your email has been sent.");

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}
