package com.attire4hire.activity;

import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddEmailActivity extends BaseActivity {
    Activity mActivity = AddEmailActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = AddEmailActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.emailLL)
    LinearLayout emailLL;
    @BindView(R.id.emailView)
    View emailView;
    @BindView(R.id.submitBT)
    Button submitBT;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_email);

        //butter knife
        ButterKnife.bind(this);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {

        }
    }

    @OnClick({R.id.submitBT, R.id.rlCancelRL})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitBT:
                onForgetPassClick();
                break;
            case R.id.rlCancelRL:
                onBackClick();
                break;
        }
    }

    private void onBackClick() {
        onBackPressed();
    }

    private void onForgetPassClick() {
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeResendMailApi();
            }
        }
    }

    public Boolean validate() {
        boolean flag = true;
        if (emailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(emailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (emailET.getText().toString().contains(" ")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        }
        return flag;
    }

    private void executeResendMailApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.RESEND_MAIL;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("email", emailET.getText().toString().trim());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        showAlertDialogFinish(mActivity, response.getString("message"));
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}