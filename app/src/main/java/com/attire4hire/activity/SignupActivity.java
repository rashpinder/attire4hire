package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.utils.WordUtils;
import com.attire4hire.adapters.CityAutoCompleteAdapter;
import com.attire4hire.fonts.ButtonBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.model.CityAutoCompleteModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends BaseActivity {
    Activity mActivity = SignupActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SignupActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.userNameET)
    EdittextRegular userNameET;
    @BindView(R.id.surName_et)
    EdittextRegular surnameET;
    @BindView(R.id.userNameLL)
    LinearLayout userNameLL;
    @BindView(R.id.firstNameView)
    View userNameView;
    @BindView(R.id.emailET)
    EdittextRegular emailET;
    @BindView(R.id.emailLL)
    LinearLayout emailLL;
    @BindView(R.id.emailView)
    View emailView;
    @BindView(R.id.genderLL)
    LinearLayout genderLL;
    @BindView(R.id.genderView)
    View genderView;
    @BindView(R.id.cityET)
    AutoCompleteTextView cityET;
    @BindView(R.id.cityLL)
    LinearLayout cityLL;
    @BindView(R.id.cityView)
    View cityView;
    @BindView(R.id.phoneNumberET)
    TextviewRegular phoneNumberET;
    @BindView(R.id.phoneNumberLL)
    LinearLayout phoneNumberLL;
    @BindView(R.id.phoneNumberView)
    View phoneNumberView;
    @BindView(R.id.passET)
    EdittextRegular passET;
    @BindView(R.id.passwordLL)
    LinearLayout passwordLL;
    @BindView(R.id.passView)
    View passView;
    @BindView(R.id.signUpBT)
    ButtonBold signUpBT;
    @BindView(R.id.headerLL)
    LinearLayout headerLL;
    @BindView(R.id.alreadyHaveAcoountET)
    TextviewRegular alreadyHaveAcoountET;
    @BindView(R.id.userNameIV)
    ImageView userNameIV;
    @BindView(R.id.emailIV)
    ImageView emailIV;
    @BindView(R.id.genderIV)
    ImageView genderIV;
    @BindView(R.id.cityIV)
    ImageView cityIV;
    @BindView(R.id.phoneNumberIV)
    ImageView phoneNumberIV;
    @BindView(R.id.passIV)
    ImageView passIV;
    @BindView(R.id.logoIV)
    LinearLayout logoIV;
    @BindView(R.id.genderSpinner)
    Spinner genderSpinner;
    @BindView(R.id.imgClearIV)
    ImageView imgClearIV;
    @BindView(R.id.termsCB)
    CheckBox termsCB;
    @BindView(R.id.termsTV)
    TextView termsTV;
    @BindView(R.id.eyeIV)
    ImageView eyeIV;

    boolean IsTermsChecked = false;

    String gender = "", phoneNumber = "", termsChecked = "", Device_Token = "";

    CityAutoCompleteAdapter mCityAutoCompleteAdapter;
    ArrayList<CityAutoCompleteModel> mCitiesArrayList = new ArrayList<>();
    int eyeCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        //for capitalized first letter
        userNameET.addTextChangedListener(new GenericTextWatcher1(userNameET));
        surnameET.addTextChangedListener(new GenericTextWatcher1(surnameET));
        cityET.addTextChangedListener(new GenericTextWatcher1(cityET));
        passET.addTextChangedListener(new GenericTextWatcher1(passET));

        //password typeface
        passET.setTypeface(Typeface.DEFAULT);
        passET.setTransformationMethod(new PasswordTransformationMethod());
        passET.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        getPushToken();

        /*Set Spannable Text*/
        spannabeText();
        /*Call gender adapter */
        setGenderAdapter();
        /*Set Cities Auto Complete*/
        setCityAutoComplete();

        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, "");

        termsCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                IsTermsChecked = isChecked;
                if (IsTermsChecked) {
                    termsChecked = "1";
                } else {
                    termsChecked = "";
                }
            }
        });

        addClicksToTermsAndPrivacyString();
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    Device_Token = task.getResult();
                    Log.e(TAG, "**Push Token**" + Device_Token);

                });
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.I_have_read));

        ClickableSpan teremsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.TERM_AND_SERVICES);
                startActivity(mIntent);

            }
        };

        // Character starting from 41 - 57 is Terms and condition.
        // Character starting from 62 - 76 is privacy policy.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.PRIVACY_POLICY);
                startActivity(mIntent);

            }
        };

        SpanString.setSpan(teremsAndCondition, 41, 57, 0);
        SpanString.setSpan(privacy, 62, 76, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 41, 57, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 62, 76, 0);
        SpanString.setSpan(new UnderlineSpan(), 41, 57, 0);
        SpanString.setSpan(new UnderlineSpan(), 62, 76, 0);

        termsTV.setMovementMethod(LinkMovementMethod.getInstance());
        termsTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        termsTV.setSelected(true);
    }

    public class GenericTextWatcher1 implements TextWatcher {

        private View view;

        private GenericTextWatcher1(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {

                case R.id.userNameET:
                    try {
                        String inputString = "" + userNameET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + userNameET.getText().toString())) {
                            userNameET.setText("" + firstLetterCapString);
                            userNameET.setSelection(userNameET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.surName_et:
                    try {
                        String inputString = "" + surnameET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + surnameET.getText().toString())) {
                            surnameET.setText("" + firstLetterCapString);
                            surnameET.setSelection(surnameET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.cityET:
                    try {
                        String inputString = "" + cityET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + cityET.getText().toString())) {
                            cityET.setText("" + firstLetterCapString);
                            cityET.setSelection(cityET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.passET:
                    try {
                        String inputString = "" + passET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + passET.getText().toString())) {
                            passET.setText("" + firstLetterCapString);
                            passET.setSelection(passET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        GetUserNumber();
    }

    private void GetUserNumber() {

        if (getUserPhoneNo() != null) {
            phoneNumber = getUserPhoneNo();
            phoneNumberET.setText(phoneNumber);
        } else {

        }
    }

    private void setCityAutoComplete() {
        imgClearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearText();
            }
        });

        if (mCitiesArrayList != null) {
            mCitiesArrayList.clear();
        }

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                CityAutoCompleteModel mModel = new CityAutoCompleteModel();
                if (!mDataObj.isNull("country"))
                    mModel.setCountry(mDataObj.getString("country"));
                if (!mDataObj.isNull("geonameid"))
                    mModel.setGeonameid(mDataObj.getString("geonameid"));
                if (!mDataObj.isNull("name"))
                    mModel.setName(mDataObj.getString("name"));
                if (!mDataObj.isNull("subcountry"))
                    mModel.setSubcountry(mDataObj.getString("subcountry"));

                mCitiesArrayList.add(mModel);
            }

            Log.e(TAG, "***Array Size***" + mCitiesArrayList.size());
        } catch (Exception e) {
            e.toString();
        }

        mCityAutoCompleteAdapter = new CityAutoCompleteAdapter(this, mCitiesArrayList);

        cityET.setThreshold(0);

        cityET.setAdapter(mCityAutoCompleteAdapter);

        cityET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus)
//                    cityET.showDropDown();
            }
        });

        cityET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                cityET.showDropDown();
                return false;
            }
        });

        cityET.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                //do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    imgClearIV.setVisibility(View.VISIBLE);
                    cityET.showDropDown();
                } else {
                    imgClearIV.setVisibility(View.GONE);
                }
            }
        });

        cityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                CityAutoCompleteModel mModel = (CityAutoCompleteModel) arg0.getItemAtPosition(arg2);
                cityET.setText(mModel.getName());
                cityET.setSelection(cityET.getText().toString().trim().length());
            }
        });
    }

    private void clearText() {
        cityET.setText("");
        imgClearIV.setVisibility(View.GONE);
    }

    /*
     * Gender adapter
     * */
    private void setGenderAdapter() {
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Regular.ttf");
        String[] mStatusArry = getResources().getStringArray(R.array.gender);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArry) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner_gender, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemGenderTV);
                if (position == 0) {
                    itemTextView.setVisibility(View.INVISIBLE);
                }
                itemTextView.setText(mStatusArry[position]);
                itemTextView.setTypeface(font);
//                itemTextView.setTextSize(14);
                return v;
            }
        };

        genderSpinner.setAdapter(adapter);

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                gender = mStatusArry[position];
                if (position == 0) {
//                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorGray));
                } else {
//                    ((TextView) view).setTextSize(14);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.white));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*
     * Sign up Api
     * */
    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();
        params.put("first_name", userNameET.getText().toString());
        params.put("last_name", surnameET.getText().toString());
        params.put("email", emailET.getText().toString().trim());
        params.put("gender", gender);
        params.put("city_name", cityET.getText().toString().trim());
        params.put("phone", phoneNumberET.getText().toString());
        params.put("password", passET.getText().toString().trim());
        params.put("address_latitude", "");
        params.put("address_longitude", "");
        params.put("current_latitude", getCurrentLatitude());
        params.put("current_longitude", getCurrentLongitude());
        params.put("device_token", Device_Token);
        params.put("device_type", "1");

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.SIGN_UP, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {

                JSONObject data = response.getJSONObject("data");

                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.IS_LOGIN, "true");

                if (!data.isNull("id")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, data.getString("id"));
                }
                if (!data.isNull("first_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, data.getString("first_name"));
                }
                if (!data.isNull("last_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, data.getString("last_name"));
                }
                if (!data.isNull("email")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, data.getString("email"));
                }
                if (!data.isNull("gender")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, data.getString("gender"));
                }
                if (!data.isNull("phone_no")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, data.getString("phone_no"));
                }
                if (!data.isNull("password")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PASSWORD, data.getString("password"));
                }
                if (!data.isNull("role")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ROLE, data.getString("role"));
                }
                if (!data.isNull("profile_pic") && !data.getString("profile_pic").equals("")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, data.getString("profile_pic"));
                }
                if (!data.isNull("created")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CREATED, data.getString("created"));
                }

                if (!data.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, data.getString("email_verification"));

                if (!data.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, data.getString("phone_verification"));

                if (!data.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, data.getString("id_verification"));

                Intent intent = new Intent(mActivity, HomeActivity.class);
                startActivity(intent);
                finish();

            } else {
                showAlertDialog(mActivity, response.getString("message"));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void spannabeText() {
        String str_signin = "<font color=#FFFFFF>Already have an account? </font>" + "<b><font color=#FFFFFF>SIGN IN!</font></b>";
        SpannableString ss = new SpannableString((Html.fromHtml(str_signin)));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(mActivity, SigninActivity.class);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                if (alreadyHaveAcoountET.isPressed()) {
                    ds.setColor(getResources().getColor(R.color.darkPink));
                } else {
                    ds.setColor(getResources().getColor(R.color.white));
                }
                alreadyHaveAcoountET.invalidate();
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.white));
                //  dontHaveAccountTV.setHighlightColor(Color.TRANSPARENT);
                alreadyHaveAcoountET.setBackground(getDrawable(R.drawable.ripple_effect));
            }
        };
        ss.setSpan(clickableSpan, 25, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        alreadyHaveAcoountET.setText(ss);
        alreadyHaveAcoountET.setMovementMethod(LinkMovementMethod.getInstance());

    }

    /*
     * Add validations
     * */
    private Boolean validate() {
        boolean flag = true;
        if (userNameET.getText().toString().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (surnameET.getText().toString().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_surname));
            flag = false;
        } else if (emailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(emailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (emailET.getText().toString().contains(" ")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (gender.equals("Gender")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_gender));
            flag = false;
        } else if (cityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city));
            flag = false;
        } else if (phoneNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phoneNumber));
            flag = false;
        } else if (passET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (passET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_password));
            flag = false;
        } else if (phoneNumberET.getText().toString().trim().length() < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_phone_number));
            flag = false;
        } else if (termsChecked.equals("")) {
            showAlertDialog(mActivity, getString(R.string.terms_conditions));
            flag = false;
        }
        return flag;

    }

    @OnClick({R.id.signUpBT, R.id.phoneNumberET, R.id.termsTV, R.id.eyeIV/*, R.id.cityLL, R.id.cityIV, R.id.cityET*/})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signUpBT:
                performSignUpClick();
                break;

            case R.id.phoneNumberET:
                startActivity(new Intent(mActivity, MobileVerificationActivity.class));
                break;

            case R.id.termsTV:
//                performTermsClick();
                break;

            case R.id.eyeIV:
                performEyeClick();
                break;

         /* case R.id.cityLL:
                performCityClick();
                break;
            case R.id.cityIV:
                performCityClick();
                break;
            case R.id.cityET:
                performCityClick();
                break;*/
        }
    }

    private void performEyeClick() {

        if (eyeCount == 0) {
            eyeIV.setImageResource(R.drawable.eye);
            passET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passET.setSelection(passET.getText().length());
            eyeCount++;

        } else if (eyeCount == 1) {
            eyeIV.setImageResource(R.drawable.eye_hide);
            passET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passET.setSelection(passET.getText().length());
            eyeCount = 0;
        }
    }

    private void performTermsClick() {
        Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
        mIntent.putExtra(ConstantData.LINK, ConstantData.TERM_AND_SERVICES);
        startActivity(mIntent);
    }

    private void performSignUpClick() {
//        Log.e(TAG, "Code== " + ccp.getSelectedCountryNameCode());
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }
    }

    private void performCityClick() {
        Intent mIntent = new Intent(mActivity, CitiesSearchActivity.class);
        startActivityForResult(mIntent, ConstantData.REQUEST_ACTIVITY);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, SocialMediaLoginActivity.class));
        finish();
    }

    public void showAlertDialog1(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_signup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, HomeActivity.class);
                startActivity(intent);
                finish();

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
