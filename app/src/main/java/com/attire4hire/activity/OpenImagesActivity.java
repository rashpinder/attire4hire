package com.attire4hire.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.attire4hire.R;
import com.attire4hire.helper.TouchImageView;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenImagesActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = OpenImagesActivity.this;

    /*
     * Getting the Current Class Name
     */
    String TAG = OpenImagesActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.ImageView)
    TouchImageView ImageView;

    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    String strImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_images);

        ButterKnife.bind(this);

        setStatusBar(mActivity);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {

            strImage = getIntent().getStringExtra(ConstantData.IMAGE);

            if (strImage.contains("http")) {
                Glide.with(mActivity).load(strImage)
                        .placeholder(null)
                        .error(R.drawable.placeholder_img)
                        .into(ImageView);
            }

//            if (strImage.contains("https")) {
//                Glide.with(mActivity).load(strImage)
//                        .placeholder(null)
//                        .error(R.drawable.placeholder_img)
//                        .into(ImageView);
//            } else {
//                Glide.with(mActivity).load(strImage.replace("http://", "https://"))
//                        .placeholder(null)
//                        .error(R.drawable.placeholder_img)
//                        .into(ImageView);
//            }
        }
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }
}