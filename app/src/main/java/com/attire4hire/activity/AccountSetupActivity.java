package com.attire4hire.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.EdittextBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.passbase.passbase_sdk.PassbaseSDK;
import com.passbase.passbase_sdk.PassbaseSDKListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSetupActivity extends BaseActivity {

    Activity mActivity = AccountSetupActivity.this;

    /**
     * Getting the Current Class Name
     */

    String TAG = AccountSetupActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.tv_submit_acc)
    TextView tv_submit_acc;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.et_sort_code)
    EdittextRegular mSortCode;
    @BindView(R.id.et_acc_no)
    EdittextRegular mAccNo;
    @BindView(R.id.et_add_1)
    EdittextRegular mAddress1ET;
    @BindView(R.id.et_add_2)
    EdittextRegular mAddress2ET;
    @BindView(R.id.et_city)
    EdittextRegular mCityET;
    @BindView(R.id.et_country)
    EdittextRegular mCountryET;
    @BindView(R.id.et_postcode)
    EdittextRegular mPostcodeET;
    @BindView(R.id.et_bank_acc)
    EdittextBold mBankAccET;
    @BindView(R.id.et_state)
    EdittextRegular et_state;
    @BindView(R.id.rl_passbase_btn)
    RelativeLayout verificationButton;
    @BindView(R.id.rl_number_btn)
    RelativeLayout rl_number_btn;
    @BindView(R.id.rl_email_btn)
    RelativeLayout rl_email_btn;
    @BindView(R.id.iv_next_id)
    ImageView iv_next_id;
    @BindView(R.id.iv_next_phone)
    ImageView iv_next_phone;
    @BindView(R.id.iv_next_email)
    ImageView iv_next_email;
    @BindView(R.id.rl_paypal_btn)
    RelativeLayout rl_paypal_btn;
    @BindView(R.id.iv_next_paypal)
    ImageView iv_next_paypal;

    @BindView(R.id.rl_stripe_btn)
    RelativeLayout rl_stripe_btn;

    @BindView(R.id.iv_next_stripe)
    ImageView iv_next_stripe;

    @BindView(R.id.stripe_view)
    View stripe_view;

    String Auth_Key = "";
    String mAddress2 = "";

    SwipeRefreshLayout swipeToRefresh;
    boolean isSwipeRefresh = false;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setup);

        //Set Butter Knife
        ButterKnife.bind(this);

        //set stripe verification 0 to hide popup
        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "");
        Attire4hireApplication.getSingletonValues().setStripeVerification("0");

        swipeToRefresh = findViewById(R.id.swipeToRefresh);

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                executeProfileApi();
            }
        });

        pass_base();

        dismissProgressDialog();

        CheckVerification();

        getIntentData();
    }

    private void getIntentData() {
        rl_stripe_btn.setVisibility(View.GONE);
        stripe_view.setVisibility(View.GONE);

        if (getIntent() != null) {

            if (getIntent().getStringExtra(ConstantData.STRIPE) != null) {

                if (getIntent().getStringExtra(ConstantData.STRIPE).equals(ConstantData.SHOW_STRIPE)) {

                    rl_stripe_btn.setVisibility(View.VISIBLE);
                    stripe_view.setVisibility(View.VISIBLE);

                }
            }
        }
    }

    private void CheckVerification() {
        if (getIdVerification().equalsIgnoreCase("1")) {
            verificationButton.setEnabled(false);
            iv_next_id.setImageResource(R.drawable.ic_check);
        } else {
            verificationButton.setEnabled(true);
            iv_next_id.setImageResource(R.drawable.ic_next);
        }

        if (getPhoneVerification().equalsIgnoreCase("1")) {
            rl_number_btn.setEnabled(false);
            iv_next_phone.setImageResource(R.drawable.ic_check);
        } else {
            rl_number_btn.setEnabled(true);
            iv_next_phone.setImageResource(R.drawable.ic_next);
        }

        if (getEmailVerification().equalsIgnoreCase("1")) {
            rl_email_btn.setEnabled(false);
            iv_next_email.setImageResource(R.drawable.ic_check);
        } else {
            rl_email_btn.setEnabled(true);
            iv_next_email.setImageResource(R.drawable.ic_next);
        }

        if (getStripeVerification().equalsIgnoreCase("1")) {
            rl_stripe_btn.setEnabled(false);
            iv_next_stripe.setImageResource(R.drawable.ic_check);
        } else {
            rl_stripe_btn.setEnabled(true);
            iv_next_stripe.setImageResource(R.drawable.ic_next);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        dismissProgressDialog();

        if (mItemProductModel != null) {
            mItemProductModel.clear();
            executeProfileApi();
        }

        if (Attire4hireApplication.getSingletonValues().getStripeVerification().equals("1")) {
            showStripeAlertDialog(mActivity, getString(R.string.your_details_are_now_being_reviewed_by_stripe));
        }
    }

    private void showStripeAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIdVerification().equalsIgnoreCase("1") && getPhoneVerification().equalsIgnoreCase("1") && getEmailVerification().equalsIgnoreCase("1") /* && getPayPalVerification().equalsIgnoreCase("1") */) {
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.putExtra(ConstantData.REDIRECION_TYPE, ConstantData.CLOSET);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                } else {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    public String writeCamera = Manifest.permission.CAMERA;

    private void pass_base() {
        PassbaseSDK passbaseRef = new PassbaseSDK(this);

        // Initialization and prefilled email
        passbaseRef.initialize(ConstantData.passbase_key);
        passbaseRef.setPrefillUserEmail(getUserEmail());
//        passbaseRef.setPrefillUserEmail(getUserEmail());
        // Handling verifications via callbacks
        passbaseRef.callback(new PassbaseSDKListener() {
            @Override
            public void onSubmitted(@NotNull String s) {

            }

            @Override
            public void onStart() {
                System.out.println("MainActivity onStart");
            }

            @Override
            public void onFinish(@Nullable String identityAccessKey) {
                dismissProgressDialog();
                System.out.println("MainActivity onFinish: " + identityAccessKey);
                executeIdVerificationApi(identityAccessKey);
            }

            @Override
            public void onError(@NotNull String errorCode) {
                dismissProgressDialog();
                System.out.println("MainActivity onError: " + errorCode);
            }
        });

        verificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (checkPermission()) {
//                    Intent mIntent = new Intent(mActivity, OpenPassbaseLinkActivity.class);
//                    mIntent.putExtra(ConstantData.LINK, "https://verify.passbase.com/dharmani-76f56e11");
//                    startActivity(mIntent);
//                } else {
//                    requestPermission();
//                }
                Attire4hireApplication.getSingletonValues().setStripeVerification("0");
                showProgressDialog(mActivity);
                passbaseRef.startVerification();
            }
        });

//        PassbaseButton verificationButton = this.findViewById(R.id.passbaseVerificationButton);
//        verificationButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                passbaseRef.startVerification();
//            }
//        });
    }

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                    mIntent.putExtra(ConstantData.LINK, "https://verify.passbase.com/dharmani-76f56e11");
                    startActivity(mIntent);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

//        passbaseRef = new Passbase(this);
//
//        final Pair<String, String>[] additionalAttributes = new Pair[]{new Pair("userId", "a2ab4-f2jc4-k2dd4-1fa3x")};
//        // 2. Initialize object here with own API key
//        passbaseRef.initialize(ConstantData.passbase_key,
//                getUserEmail(), new Pair[]{});
//
//        // Add here the callbacks
//        passbaseRef.onCompletePassbaseVerification(new Function1<String, Unit>() {
//            @Override
//            public Unit invoke(String s) {
//                dismissProgressDialog();
//                Log.e("AccountSetupActivity>>>", "AccountSetupActivity onCompletePassbase:::::::;;" + s);
//                executeIdVerificationApi(s);
//                return null;
//            }
//        });
//
//        passbaseRef.onCancelPassbaseVerification(new Function0<Unit>() {
//            @Override
//            public Unit invoke() {
//                dismissProgressDialog();
//                Log.e("AccountSetupActivity>>>", "AccountSetupActivity onCancelPassbase");
//                return null;
//            }
//        });

    @OnClick({R.id.rlCancelRL, R.id.tv_submit_acc, R.id.rl_number_btn, R.id.rl_paypal_btn, R.id.rl_email_btn, R.id.rl_stripe_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.rlCancelRL:
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "");
                onBackPressed();
                break;

            case R.id.tv_submit_acc:
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "");
                performSubmitClick();
                break;

            case R.id.rl_number_btn:
                Attire4hireApplication.getSingletonValues().setStripeVerification("0");
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "");
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.Phone_TYPE, "AccountSetUp");
                startActivity(new Intent(mActivity, MobileVerificationActivity.class));
                break;

            case R.id.rl_paypal_btn:
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "1");
                performAddPaypalClick();
                break;

            case R.id.rl_email_btn:
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFY, "");
                if (getUserEmail() != null && !getUserEmail().equals("")) {
                    AddEmailVerification();
                } else {
                    Intent intent = new Intent(mActivity, AddEmailActivity.class);
                    startActivity(intent);
                }
                break;

            case R.id.rl_stripe_btn:
                Attire4hireApplication.getSingletonValues().setStripeVerification("0");
                performAddStripeClick();
                break;
        }
    }

    private void performAddStripeClick() {
        if (isNetworkAvailable(mActivity))
            executeStripeLogInApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    //stripe log in
    private void executeStripeLogInApi() {

        showProgressDialog(mActivity);

        String strURL = ConstantData.StripeConnect + "?user_id=" + getUserID();

        Log.e(TAG, "*****Response****" + strURL);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {

                    JSONObject mJsonObjectt = new JSONObject(response);

                    if (mJsonObjectt.getString("status").equals("1")) {

                        String login_url = mJsonObjectt.getString("login_url");

                        Intent mIntent = new Intent(mActivity, OpenPaYpalLinkActivity.class);
                        mIntent.putExtra(ConstantData.LINK, ConstantData.STRIPE_LINK);
                        mIntent.putExtra(ConstantData.S_LINK, login_url);
                        startActivity(mIntent);

                    } else {
                        showToast(mActivity, mJsonObjectt.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void AddEmailVerification() {
        if (isNetworkAvailable(mActivity))
            executeResendMailApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeResendMailApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.RESEND_MAIL;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("email", getUserEmail());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        showAlertDialog(mActivity, response.getString("message"));
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void performAddPaypalClick() {
        if (isNetworkAvailable(mActivity))
            excutePaypalLogInApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeIdVerificationApi(String Token) {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_ID_VERIFICATION;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("token", Token);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        showVerificationAlertDialog(mActivity, getString(R.string.thankyou_for_verify));

                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void performSubmitClick() {

    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                callHomeActivityIntent(mActivity);
            }
        });
        alertDialog.show();
    }

    private void executeProfileApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("viewed_user", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("user_details")) {
                JSONObject mDataObj = response.getJSONObject("user_details");
                if (!mDataObj.isNull("id"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, mDataObj.getString("id"));

                if (!mDataObj.isNull("first_name"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObj.getString("first_name"));

                if (!mDataObj.isNull("last_name"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObj.getString("last_name"));

                if (!mDataObj.isNull("email"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, mDataObj.getString("email"));

                if (!mDataObj.isNull("gender"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, mDataObj.getString("gender"));

                if (!mDataObj.isNull("city_name"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CITY, mDataObj.getString("city_name"));

                if (!mDataObj.isNull("phone_no"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, mDataObj.getString("phone_no"));

                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals(""))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObj.getString("profile_pic"));

                if (!mDataObj.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObj.getString("email_verification"));

                if (!mDataObj.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, mDataObj.getString("phone_verification"));

                if (!mDataObj.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, mDataObj.getString("id_verification"));

                if (!mDataObj.isNull("paypal_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PAYPAL_VERIFICATION, mDataObj.getString("paypal_verification"));

                if (!mDataObj.isNull("stripe_account_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.STRIPE_VERIFICATION, mDataObj.getString("stripe_account_verification"));

                if (!mDataObj.isNull("stripe_account_id"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.STRIPE_ACCOUNT_ID, mDataObj.getString("stripe_account_id"));

                if (!mDataObj.isNull("rating"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_RATINGS, mDataObj.getString("rating"));

            }
            if (!response.isNull("product_datils")) {
                JSONArray mDataArray = response.getJSONArray("product_datils");
                for (int i = 0; i < mDataArray.length(); i++) {
                    JSONObject mDataObject = mDataArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    mItemProductModel.add(mModel);
                }
            }

            //Set Data ON Widgets
            CheckVerification();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    //paypal log in
    private void excutePaypalLogInApi() {

        showProgressDialog(mActivity);

        String strURL = ConstantData.PAYPAL_lOGIN_API + "?user_id=" + getUserID();

        Log.e(TAG, "*****Response****" + strURL);

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    parseJsonResponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "*****Response****" + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void parseJsonResponse(String response) {
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                String login_url = mJsonObjectt.getString("login_url");

                Intent mIntent = new Intent(mActivity, OpenPaYpalLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.PAYPAL_LINK);
                mIntent.putExtra(ConstantData.P_LINK, login_url);
                startActivity(mIntent);

            } else {
                showToast(mActivity, mJsonObjectt.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIdVerification().equalsIgnoreCase("1") && getPhoneVerification().equalsIgnoreCase("1") && getEmailVerification().equalsIgnoreCase("1") /* && getPayPalVerification().equalsIgnoreCase("1") */) {
                    callHomeActivityIntent(mActivity);
                } else {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }
}