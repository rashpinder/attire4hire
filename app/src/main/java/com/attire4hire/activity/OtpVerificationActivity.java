package com.attire4hire.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.ButtonBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpVerificationActivity extends BaseActivity {
    Activity mActivity = OtpVerificationActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = OtpVerificationActivity.this.getClass().getSimpleName();

    @BindView(R.id.ed1)
    EdittextRegular ed1;
    @BindView(R.id.ed2)
    EdittextRegular ed2;
    @BindView(R.id.ed3)
    EdittextRegular ed3;
    @BindView(R.id.ed4)
    EdittextRegular ed4;
    @BindView(R.id.ed5)
    EdittextRegular ed5;
    @BindView(R.id.ed6)
    EdittextRegular ed6;
    @BindView(R.id.submitBtn)
    ButtonBold submitBtn;
    @BindView(R.id.resend_code)
    TextviewBold resend_code;
    @BindView(R.id.backIV)
    ImageView backIV;

    String phoneNumber = "", mVerificationId = "", otp = "", code = "";

    PhoneAuthProvider.ForceResendingToken mResendToken;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        ButterKnife.bind(this);

        Intent intent = getIntent();

        phoneNumber = intent.getStringExtra("Phone");

        ed1.setText(null);
        ed2.setText(null);
        ed3.setText(null);
        ed4.setText(null);
        ed5.setText(null);
        ed6.setText(null);

        ed1 = findViewById(R.id.ed1);

        ed1.addTextChangedListener(new GenericTextWatcher(ed1));
        ed2.addTextChangedListener(new GenericTextWatcher(ed2));
        ed3.addTextChangedListener(new GenericTextWatcher(ed3));
        ed4.addTextChangedListener(new GenericTextWatcher(ed4));
        ed5.addTextChangedListener(new GenericTextWatcher(ed5));
        ed6.addTextChangedListener(new GenericTextWatcher(ed6));

//        FirebaseAuth.getInstance().setLanguageCode("fr");

//        mAuth.getFirebaseAuthSettings().setAppVerificationDisabledForTesting(true);

//        FirebaseAppCheck firebaseAppCheck = FirebaseAppCheck.getInstance();
//        firebaseAppCheck.installAppCheckProviderFactory(
//                SafetyNetAppCheckProviderFactory.getInstance());

        if (isNetworkAvailable(mActivity)) {
            sendVerificationCode();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void sendVerificationCode() {
        showProgressDialog(mActivity);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            dismissProgressDialog();
            Log.d(TAG, "onVerificationCompleted:" + credential);

            code = credential.getSmsCode();

            if (code != null) {

                String c1, c2, c3, c4, c5, c6;

                c1 = code.substring(0, 1);
                c2 = code.substring(1, 2);
                c3 = code.substring(2, 3);
                c4 = code.substring(3, 4);
                c5 = code.substring(4, 5);
                c6 = code.substring(5, 6);

                ed1.setText(c1);
                ed2.setText(c2);
                ed3.setText(c3);
                ed4.setText(c4);
                ed5.setText(c5);
                ed6.setText(c6);

//               verifying the code
                if (!isNetworkAvailable(mActivity)) {
                    showToast(mActivity, getString(R.string.internet_connection_error));
                } else {
                    verifyVerificationCode(code);
                }
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            dismissProgressDialog();
            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
                showToast(mActivity, "The SMS quota for this number has been exceeded.");
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            dismissProgressDialog();
            Log.d(TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...
        }
    };

    private void verifyVerificationCode(String otp) {

        showProgressDialog(mActivity);

        if (mVerificationId != null && !mVerificationId.equals("")) {

            //creating the credential
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            //signing the user
            signInWithPhoneAuthCredential(credential);

        } else {
            dismissProgressDialog();
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            dismissProgressDialog();

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();

                            if (Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.Phone_TYPE, "").equalsIgnoreCase("AccountSetUp")) {
                                executePhoneVerificationApi();
                            } else {
                                showReqInfoDialog(mActivity, "", getString(R.string.phone_verified));
                            }

                        } else {

                            dismissProgressDialog();

                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
                            }
                        }
                    }
                });
    }

    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.ed1:
                    if (text.length() == 1) {
                        ed2.requestFocus();

                    }
                    break;

                case R.id.ed2:
                    if (text.length() == 1) {
                        ed3.requestFocus();
                    } else if (text.length() == 0) {
                        ed1.requestFocus();
                    }
                    break;

                case R.id.ed3:
                    if (text.length() == 1) {
                        ed4.requestFocus();
                    } else if (text.length() == 0) {
                        ed2.requestFocus();
                    }
                    break;

                case R.id.ed4:
                    if (text.length() == 1) {
                        ed5.requestFocus();
                    } else if (text.length() == 0) {
                        ed3.requestFocus();
                    }
                    break;

                case R.id.ed5:
                    if (text.length() == 1) {
                        ed6.requestFocus();
                    } else if (text.length() == 0) {
                        ed4.requestFocus();
                    }
                    break;

                case R.id.ed6:
                    if (text.length() == 1) {
                        submitBtn.requestFocus();
                    } else if (text.length() == 0) {
                        ed5.requestFocus();
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    private void validate() {

        otp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString() + ed5.getText().toString() + ed6.getText().toString();

        if (!otp.isEmpty()) {

            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                verifyVerificationCode(otp);
            }

        } else {

            showAlertDialog(mActivity, getString(R.string.please_enter_code));
        }
    }

    public void showReqInfoDialog(Activity mActivity, String strHeading, String strDescriptions) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_request_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtHeadingTV = alertDialog.findViewById(R.id.txtHeadingTV);
        TextView txtDescriptionTV = alertDialog.findViewById(R.id.txtDescriptionTV);

        txtCancelTV.setText("OK");
        txtHeadingTV.setVisibility(View.GONE);

        txtHeadingTV.setText(strHeading);
        txtDescriptionTV.setText(strDescriptions);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, phoneNumber);
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        FirebaseSignOut();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        FirebaseSignOut();
    }

    @OnClick({R.id.submitBtn, R.id.resend_code, R.id.backIV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitBtn:
                performSubmitClick();
                break;

            case R.id.resend_code:
                performResendClick();
                break;

            case R.id.backIV:
                onBackPressed();
                break;
        }
    }

    private void performResendClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            ed1.setText(null);
            ed2.setText(null);
            ed3.setText(null);
            ed4.setText(null);
            ed5.setText(null);
            ed6.setText(null);
            ed1.requestFocus();
            showProgressDialog(mActivity);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks, mResendToken);        // OnVerificationStateChangedCallbacks
        }
    }

    private void performSubmitClick() {
        validate();
    }

    private void FirebaseSignOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }

    private void executePhoneVerificationApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_PHONE_VERIFICATION;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("phone", phoneNumber);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {

                        showReqInfoDialog(mActivity, "", "Your phone number has been verified");

                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}
