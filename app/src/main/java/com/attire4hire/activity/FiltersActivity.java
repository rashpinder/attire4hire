package com.attire4hire.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.savvi.rangedatepicker.CalendarPickerView;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FiltersActivity extends BaseActivity {

    Activity mActivity = FiltersActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = FiltersActivity.this.getClass().getSimpleName();

    @BindView(R.id.rl_occasion_date)
    RelativeLayout rl_occasion_date;

    @BindView(R.id.rl_item_category)
    RelativeLayout rl_item_category;

    @BindView(R.id.rl_product_type)
    RelativeLayout rl_product_type;

    @BindView(R.id.rl_brand)
    RelativeLayout rl_brand;

    @BindView(R.id.rl_color)
    RelativeLayout rl_color;

    @BindView(R.id.rl_condition)
    RelativeLayout rl_condition;

    @BindView(R.id.rl_size)
    RelativeLayout rl_size;

    @BindView(R.id.rl_price_range)
    RelativeLayout rl_price_range;

    @BindView(R.id.rl_location)
    RelativeLayout rl_location;

    @BindView(R.id.rl_occasion)
    RelativeLayout rl_occasion;

    @BindView(R.id.rl_open_for_sale)
    RelativeLayout rl_open_for_sale;

    @BindView(R.id.Iv_OpenForSale)
    ImageView Iv_OpenForSale;

    @BindView(R.id.txtApplyTV)
    TextviewSemiBold txtApplyTV;

    @BindView(R.id.tv_clear)
    TextviewRegular tv_clear;

    List<Date> mSelectedDateList;
    List<Date> mMultiSelectedDateaList;
    ArrayList<String> mSelectedArrayList = new ArrayList<>();
    DatePickerDialog mDatePicker;

    int count = 0;
    private int mYear, mMonth, mDay;
    String OccasionDate = "", OpenForSale = "0", Category = "", Type = "", Brand = "", Colour = "", Condition = "", Size = "", PriceRange_start = "", PriceRange_end = "", Location = "", Occasion = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        ButterKnife.bind(this);
    }

    private void ClearFilterValues() {
        if (mSelectedArrayList != null) {
            mSelectedArrayList.clear();
        }
        if (mMultiSelectedDateaList != null) {
            mMultiSelectedDateaList.clear();
        }
        Attire4hirePrefrences.writeString(mActivity, ConstantData.SEARCH_VALUE, "");

        Attire4hirePrefrences.writeString(mActivity, ConstantData.SIZE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.CONDITION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.COLOUR, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.BRAND, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.ITEM_CATEGORY, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRODUCT_TYPE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION_DATE, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_START, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_END, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.LOCATION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION, "");
        Attire4hirePrefrences.writeString(mActivity, ConstantData.OPEN_FOR_SALE, "");
        Iv_OpenForSale.setImageResource(R.drawable.ic_uncheck);
        count = 0;
    }

    @OnClick({R.id.rl_occasion_date, R.id.rl_item_category, R.id.rl_product_type, R.id.rl_brand, R.id.rl_color, R.id.rl_condition, R.id.rl_size, R.id.rl_price_range, R.id.rl_location, R.id.rl_occasion, R.id.rl_open_for_sale, R.id.txtApplyTV, R.id.tv_clear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_occasion_date:
                performOccasionDateClick();
                break;

            case R.id.rl_item_category:
                performItemCategoryClick();
                break;

            case R.id.rl_product_type:
                performProductTypeClick();
                break;

            case R.id.rl_brand:
                performBrandClick();
                break;

            case R.id.rl_color:
                performColorClick();
                break;

            case R.id.rl_condition:
                performConditionClick();
                break;

            case R.id.rl_size:
                performSizeClick();
                break;

            case R.id.rl_price_range:
                performPriceRangeClick();
                break;

            case R.id.rl_location:
                performLocationClick();
                break;

            case R.id.rl_occasion:
                performOccasionClick();
                break;

            case R.id.rl_open_for_sale:
                performOpenForSaleClick();
                break;

            case R.id.txtApplyTV:
                performApplyClick();
                break;

            case R.id.tv_clear:
                performClearClick();
                break;
        }
    }

    private void performClearClick() {

        showProgressDialog(mActivity);
        ClearFilterValues();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
            }
        }, 1000);
    }

    private void performApplyClick() {

        if (!OccasionDate.equals("") || !OpenForSale.equals("") || !Category.equals("") || !Type.equals("") || !Brand.equals("") || !Colour.equals("") || !Condition.equals("") || !Size.equals("") || !PriceRange_start.equals("") || !PriceRange_end.equals("") || !Location.equals("") || !Occasion.equals("")) {

            Attire4hirePrefrences.writeString(mActivity, ConstantData.SEARCH_VALUE, "Apply");
            onBackPressed();

        } else {
            Toast.makeText(mActivity, "Choose any filter", Toast.LENGTH_SHORT).show();
        }
    }

    private void performOpenForSaleClick() {
        if (count == 0) {
            OpenForSale = "1";
            Iv_OpenForSale.setImageResource(R.drawable.ic_check);
            Attire4hirePrefrences.writeString(mActivity, ConstantData.OPEN_FOR_SALE, OpenForSale);
            count++;
        } else {
            OpenForSale = "0";
            Iv_OpenForSale.setImageResource(R.drawable.ic_uncheck);
            Attire4hirePrefrences.writeString(mActivity, ConstantData.OPEN_FOR_SALE, OpenForSale);
            count = 0;
        }
    }

    private void performOccasionDateClick() {
//        getDate();
        showCalenderPopup();
    }

    private void performLocationClick() {
        startActivity(new Intent(mActivity, LocationActivity.class));
    }

    private void performPriceRangeClick() {
        startActivity(new Intent(mActivity, PriceRangeActivity.class));
    }

    private void performSizeClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "Size");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "Size"));
    }

    private void performConditionClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "Condition");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "Condition"));
    }

    private void performColorClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "Color");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "Color"));
    }

    private void performBrandClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "Brand");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "Brand"));
    }

    private void performProductTypeClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "ProductType");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "ProductType"));
    }

    private void performItemCategoryClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "ItemCategory");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "ItemCategory"));
    }

    private void performOccasionClick() {
        Attire4hirePrefrences.writeString(mActivity, ConstantData.FILTER_VALUE, "Occasion");
        startActivity(new Intent(mActivity, FiltersByActivity.class).putExtra(ConstantData.FILTER_STATUS, "Occasion"));
    }

    private void getDate() {
        Calendar mcurrentDate = Calendar.getInstance();
        mYear = mcurrentDate.get(Calendar.YEAR);

        mMonth = mcurrentDate.get(Calendar.MONTH);

        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        final Calendar[] myCalendar = new Calendar[2];
        myCalendar[0] = Calendar.getInstance();
        myCalendar[1] = Calendar.getInstance();

        if (!Attire4hirePrefrences.readString(mActivity, ConstantData.OCCASION_DATE, "").equals("")) {
            OccasionDate = Attire4hirePrefrences.readString(mActivity, ConstantData.OCCASION_DATE, "");

            mDay = Integer.parseInt(OccasionDate.substring(0, 2));
            mMonth = Integer.parseInt(OccasionDate.substring(3, 5)) - 1;
            mYear = Integer.parseInt(OccasionDate.substring(6, 10));

            myCalendar[0].set(Calendar.YEAR, mDay);
            myCalendar[0].set(Calendar.MONTH, mMonth);
            myCalendar[0].set(Calendar.DAY_OF_MONTH, mYear);

            String myFormat = "dd/MM/yyyy"; //Change as you need
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

            OccasionDate = sdf.format(myCalendar[0].getTime());
        }

        mDatePicker = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                myCalendar[0].set(Calendar.YEAR, selectedyear);
                myCalendar[0].set(Calendar.MONTH, selectedmonth);
                myCalendar[0].set(Calendar.DAY_OF_MONTH, selectedday);

                String myFormat = "dd/MM/yyyy"; //Change as you need
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                //finalDate = sdf.format(myCalendar.getTime());

                OccasionDate = sdf.format(myCalendar[0].getTime());

                Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION_DATE, OccasionDate);

//                PickADate.setText(sdf.format(myCalendar[0].getTime()));

                mDay = selectedday;
                mMonth = selectedmonth;
                mYear = selectedyear;
            }
        }, mYear, mMonth, mDay);
        mDatePicker.getDatePicker().setMinDate(myCalendar[1].getTimeInMillis());
        //mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    /*
     * Show Calendar Popup
     *
     * */
    private void showCalenderPopup() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.calender_popup);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CalendarPickerView calendar = alertDialog.findViewById(R.id.calendar_view);
        ImageView nextClick = alertDialog.findViewById(R.id.rightIV);
        ImageView backClick = alertDialog.findViewById(R.id.leftIV);
        TextView clickBT = alertDialog.findViewById(R.id.tv_set_date);
        clickBT.setText("OK");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        Calendar nextYear = Calendar.getInstance();

        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        //lastYear.add(Calendar.YEAR, 0);
        lastYear.add(Calendar.YEAR, 0);

        Date tomorrow = lastYear.getTime();

        /*
         * CLICK PATICULAR DATE
         * */

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                mSelectedArrayList.clear();
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        /*
         *`
         * BUTTON TO SHOW SELECTED DATES
         * */
        clickBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Date datee = calendar.getSelectedDate();
                mMultiSelectedDateaList = calendar.getSelectedDates();

                for (int i = 0; i < mMultiSelectedDateaList.size(); i++) {
                    String mSelectedDate = convertCalenderTimeToFormat(mMultiSelectedDateaList.get(i).toString());
                    Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                    mSelectedArrayList.add(mSelectedDate);
                }

                Log.e(TAG, "**Selected Dates List Size**" + mSelectedArrayList.size());

                //convert arraylist to string
                OccasionDate = TextUtils.join(",", mSelectedArrayList);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION_DATE, OccasionDate);

                alertDialog.dismiss();
            }
        });

        nextClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backClick.setVisibility(View.VISIBLE);
                c.add(Calendar.MONTH, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                calendar.scrollToDate(resultdate);
            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                c.add(Calendar.MONTH, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                String newDtString = sdf.format(resultdate);
                calendar.scrollToDate(resultdate);
                Log.e("TAG", "mon::" + resultdate);
                if (resultdate.getMonth() < 1) {
                    backClick.setVisibility(View.GONE);
                }
            }
        });

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE);

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                .withSelectedDates(mMultiSelectedDateaList);

        //for showing selected dates
        if (!Attire4hirePrefrences.readString(mActivity, ConstantData.OCCASION_DATE, "").equals("")) {
            OccasionDate = Attire4hirePrefrences.readString(mActivity, ConstantData.OCCASION_DATE, "");

            //convert string to arraylist
            String[] elements = OccasionDate.split(",");
            List<String> fixedLenghtList = Arrays.asList(elements);
            mSelectedArrayList = new ArrayList<String>(fixedLenghtList);
        }

        if (OccasionDate != null) {

            mSelectedDateList = new ArrayList<>();
            try {
                JSONArray mJsonArray = new JSONArray(mSelectedArrayList);
                for (int i = 0; i < mJsonArray.length(); i++) {
                    String strDate = mJsonArray.getString(i);
                    Log.e(TAG, "***Single Date**" + strDate);
                    Date mDate = null;
                    try {
                        mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                        mSelectedDateList.add(mDate);

                        String mSelectedDate = convertCalenderTimeToFormat(mSelectedDateList.get(i).toString());
                        Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                        mSelectedArrayList.add(mSelectedDate);

                        Log.e(TAG, "****Converted Dates***" + mSelectedArrayList.size());

                        calendar.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withSelectedDates(mSelectedDateList);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "***Converted Dates**" + mSelectedDateList.size());
                }
            } catch (Exception e) {
                e.toString();
            }
        }

        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Size = Attire4hirePrefrences.readString(mActivity, ConstantData.SIZE, "");
        Condition = Attire4hirePrefrences.readString(mActivity, ConstantData.CONDITION, "");
        Colour = Attire4hirePrefrences.readString(mActivity, ConstantData.COLOUR, "");
        Brand = Attire4hirePrefrences.readString(mActivity, ConstantData.BRAND, "");
        Category = Attire4hirePrefrences.readString(mActivity, ConstantData.ITEM_CATEGORY, "");
        Type = Attire4hirePrefrences.readString(mActivity, ConstantData.PRODUCT_TYPE, "");
        Occasion = Attire4hirePrefrences.readString(mActivity, ConstantData.OCCASION, "");
        Location = Attire4hirePrefrences.readString(mActivity, ConstantData.LOCATION, "");

        PriceRange_start = Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_START, "");
        PriceRange_end = Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_END, "");

        if (!Attire4hirePrefrences.readString(mActivity, ConstantData.OPEN_FOR_SALE, "").equals("")) {
            if (Attire4hirePrefrences.readString(mActivity, ConstantData.OPEN_FOR_SALE, "").equals("1")) {
                Iv_OpenForSale.setImageResource(R.drawable.ic_check);
                count = 1;
            } else if (Attire4hirePrefrences.readString(mActivity, ConstantData.OPEN_FOR_SALE, "").equals("0")) {
                Iv_OpenForSale.setImageResource(R.drawable.ic_uncheck);
                count = 0;
            }
        }
    }
}
