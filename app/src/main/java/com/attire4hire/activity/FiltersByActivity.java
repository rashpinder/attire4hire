package com.attire4hire.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.FilterAdapter;
import com.attire4hire.interfaces.FilterCallBack;
import com.attire4hire.model.FiltersModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FiltersByActivity extends BaseActivity implements View.OnClickListener {

    Activity mActivity = FiltersByActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = FiltersByActivity.this.getClass().getSimpleName();

    RecyclerView rv_filter;
    FilterAdapter filterAdapter;
    RelativeLayout rlCancelRL;
    TextView tv_app_bar;

    ArrayList<FiltersModel> mFiltersModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters_by);

        intIds();
        performActions();
        GetFilters();
    }

    private void intIds() {
        rv_filter = findViewById(R.id.rv_filter);
        rlCancelRL = findViewById(R.id.rlCancelRL);
        tv_app_bar = findViewById(R.id.tv_app_bar);
    }

    private void performActions() {
        rlCancelRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rlCancelRL:
                onBackPressed();
                break;
        }
    }

    private void GetFilters() {
        if (getIntent() != null) {

            if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("Brand")) {
                tv_app_bar.setText("Brand");
                executeBrandApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("Size")) {
                tv_app_bar.setText("Size");
                executeSizeApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("Condition")) {
                tv_app_bar.setText("Condition");
                executeConditionApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("Color")) {
                tv_app_bar.setText("Colour");
                executeColorApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("ProductType")) {
                tv_app_bar.setText("Product Type");
                executeTypeApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("ItemCategory")) {
                tv_app_bar.setText("Category");
                executeCategoryApi();
            } else if (getIntent().getStringExtra(ConstantData.FILTER_STATUS).equalsIgnoreCase("Occasion")) {
                tv_app_bar.setText("Occasion");
                executeOccasionApi();
            }
        }
    }

    private void executeBrandApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_BRAND;

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setBrand(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeCategoryApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_CATEGORY;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setItemCategory(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeColorApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_COLOR;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setColour(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeSizeApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_SIZE;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setSize(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeTypeApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_TYPE;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setProductType(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeDesignerApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_DESIGNER;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setFilterName(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeConditionApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_CONDITION;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setCondition(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void executeOccasionApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.GET_OCCASION;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            FiltersModel filtersModel = new FiltersModel();

                            JSONObject dataobj = jsonArray.getJSONObject(i);

                            if (!dataobj.isNull("name")) {
                                filtersModel.setOccasion(dataobj.getString("name"));
                            }
                            mFiltersModel.add(filtersModel);

                            SetFilterAdapter();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**'*" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void SetFilterAdapter() {
        filterAdapter = new FilterAdapter(mActivity, mFiltersModel, new FilterCallBack() {
            @Override
            public void CallBack(int position, String Size, String Condition, String Color, String Brand, String ItemCategory, String ProductType, String Occasion) {
                Attire4hirePrefrences.writeString(mActivity, ConstantData.SIZE, Size);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.CONDITION, Condition);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.COLOUR, Color);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.BRAND, Brand);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.ITEM_CATEGORY, ItemCategory);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.PRODUCT_TYPE, ProductType);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.OCCASION, Occasion);
            }
        });
        rv_filter.setAdapter(filterAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rv_filter.setLayoutManager(linearLayoutManager);
    }
}
