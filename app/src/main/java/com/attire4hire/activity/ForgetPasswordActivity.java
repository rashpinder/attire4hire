package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends BaseActivity {
    Activity mActivity = ForgetPasswordActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ForgetPasswordActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.emailLL)
    LinearLayout emailLL;
    @BindView(R.id.emailView)
    View emailView;
    @BindView(R.id.submitBT)
    Button submitBT;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;

    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        //butter knife
        ButterKnife.bind(this);

        if (Attire4hireApplication.getSingletonValues().getEmail() != null && !Attire4hireApplication.getSingletonValues().getEmail().equals("")) {
            email = Attire4hireApplication.getSingletonValues().getEmail();
            emailET.setText(email);
            emailET.setSelection(emailET.getText().length());
        }
    }

    @OnClick({R.id.submitBT, R.id.rlCancelRL})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitBT:
                onForgetPassClick();
                break;
            case R.id.rlCancelRL:
                onBackClick();
                break;
        }
    }

    private void onBackClick() {
        startActivity(new Intent(mActivity, SigninActivity.class));
        mActivity.finish();
    }

    private void onForgetPassClick() {
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeForgetPassApi();
            }
        }
    }

    /*
     * FORGET PASSWORD API
     * */
    private void executeForgetPassApi() {
        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();

        params.put("email", emailET.getText().toString().trim());

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.FORGOT_PASS_API, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (response.getString("status").equals("1")) {
                Attire4hireApplication.getSingletonValues().setEmail(emailET.getText().toString());
//                showDialogFinish(mActivity, getResources().getString(R.string.email_sent));
                showDialogFinish(mActivity, response.getString("message"));
            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Boolean validate() {
        boolean flag = true;
        if (emailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(emailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (emailET.getText().toString().contains(" ")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        }
        return flag;
    }

    private void showDialogFinish(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
                startActivity(new Intent(mActivity, SigninActivity.class));
            }
        });
        alertDialog.show();
    }
}
