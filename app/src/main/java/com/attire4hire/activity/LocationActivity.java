package com.attire4hire.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.adapters.CityAutoCompleteAdapter;
import com.attire4hire.model.CityAutoCompleteModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LocationActivity extends BaseActivity implements View.OnClickListener {

    Activity mActivity = LocationActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = LocationActivity.this.getClass().getSimpleName();
    private RelativeLayout back_btn;
    private AutoCompleteTextView cityET;
    private ImageView imgClearIV;
    CityAutoCompleteAdapter mCityAutoCompleteAdapter;
    ArrayList<CityAutoCompleteModel> mCitiesArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        intIds();
        performActions();
        setCityAutoComplete();
    }

    private void intIds() {
        back_btn = findViewById(R.id.back_btn);
        cityET = findViewById(R.id.cityET);
        imgClearIV = findViewById(R.id.imgClearIV);
    }

    private void HideKey() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void performActions() {
        back_btn.setOnClickListener(this);
        cityET.setOnClickListener(this);
        imgClearIV.setOnClickListener(this);

        if (!Attire4hirePrefrences.readString(mActivity, ConstantData.LOCATION, "").equals("")) {
            cityET.setText(Attire4hirePrefrences.readString(mActivity, ConstantData.LOCATION, ""));
        }

        cityET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do something

                    onBackPressed();
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back_btn:
                onBackPressed();
                break;

            case R.id.cityET:
//                performCityClick();
                break;

            case R.id.imgClearIV:
                clearText();
                break;
        }
    }

    private void performCityClick() {
//        setCityAutoComplete();
    }

    private void clearText() {
        cityET.setText("");
        imgClearIV.setVisibility(View.GONE);
    }

    private void setCityAutoComplete() {
        imgClearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearText();
            }
        });

        if (mCitiesArrayList != null) {
            mCitiesArrayList.clear();
        }

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                CityAutoCompleteModel mModel = new CityAutoCompleteModel();
                if (!mDataObj.isNull("country"))
                    mModel.setCountry(mDataObj.getString("country"));
                if (!mDataObj.isNull("geonameid"))
                    mModel.setGeonameid(mDataObj.getString("geonameid"));
                if (!mDataObj.isNull("name"))
                    mModel.setName(mDataObj.getString("name"));
                if (!mDataObj.isNull("subcountry"))
                    mModel.setSubcountry(mDataObj.getString("subcountry"));

                mCitiesArrayList.add(mModel);
            }

            Log.e(TAG, "***Array Size***" + mCitiesArrayList.size());
        } catch (Exception e) {
            e.toString();
        }

        mCityAutoCompleteAdapter = new CityAutoCompleteAdapter(this, mCitiesArrayList);

        cityET.setThreshold(0);

        cityET.setAdapter(mCityAutoCompleteAdapter);

        cityET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus)
//                    cityET.showDropDown();
            }
        });

        cityET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                cityET.showDropDown();
                return false;
            }
        });

        cityET.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                cityET.showDropDown();
                //do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    imgClearIV.setVisibility(View.VISIBLE);
                    cityET.showDropDown();
                } else {
                    imgClearIV.setVisibility(View.GONE);
                }
            }
        });

        cityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                CityAutoCompleteModel mModel = (CityAutoCompleteModel) arg0.getItemAtPosition(arg2);
                cityET.setText(mModel.getName());
                cityET.setSelection(cityET.getText().toString().trim().length());
                HideKey();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Attire4hirePrefrences.writeString(mActivity, ConstantData.LOCATION, cityET.getText().toString().trim());
        Attire4hirePrefrences.writeString(mActivity, ConstantData.SEARCH_VALUE, "location");
    }
}
