package com.attire4hire.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.ConstantData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.delight.android.webview.AdvancedWebView;

public class OpenPaYpalLinkActivity extends BaseActivity implements AdvancedWebView.Listener {
    /*
     * Initialize Activity
     * */
    Activity mActivity = OpenPaYpalLinkActivity.this;
    /*
     * Get Activity Name
     * */
    String TAG = OpenPaYpalLinkActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;

    private AdvancedWebView mWebView;

    /*
     * Initlize Objects
     * */
    String strLinkUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_pa_ypal_link);

        ButterKnife.bind(this);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(this, this);
        if (getIntent() != null) {
            strLinkUrl = getIntent().getStringExtra(ConstantData.LINK);
            if (strLinkUrl.equals(ConstantData.PAYPAL_LINK)) {
                strLinkUrl = getIntent().getStringExtra(ConstantData.P_LINK);
//                txtTitleTV.setText(getString(R.string.add_paypal_account));
                txtTitleTV.setText(getString(R.string.stripe_account));
            } else if (strLinkUrl.equals(ConstantData.STRIPE_LINK)) {
                strLinkUrl = getIntent().getStringExtra(ConstantData.S_LINK);
                txtTitleTV.setText(getString(R.string.stripe_account));
            }
        }

        mWebView.loadUrl(strLinkUrl);
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
//        if (!mWebView.onBackPressed()) {
//            return;
//        }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
        mProgressBar.setVisibility(View.GONE);
//        if (url.contains("https://dharmani.com/Attire4Hire/Webservice/Stripe/verifyAccount.php?")) {
        if (url.contains("https://attires4hire.com/Admin/Webservice/Stripe/verifyAccount.php?")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                    //set stripe verification 1 to show popup
                    Attire4hireApplication.getSingletonValues().setStripeVerification("1");
                }
            }, 4000);
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }
}
