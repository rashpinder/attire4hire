package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartActivity extends BaseActivity {
    Activity mActivity = CartActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = CartActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.iv_cart_profile)
    ImageView iv_cart_profile;
    @BindView(R.id.tv_product_name)
    TextviewSemiBold tv_product_name;
    @BindView(R.id.rl_delivery_cart)
    RelativeLayout rl_delivery_cart;
    @BindView(R.id.tv_pickup)
    TextviewRegular tv_pickup;
    @BindView(R.id.Iv_pickup)
    ImageView Iv_pickup;
    @BindView(R.id.tv_postage)
    TextviewRegular tv_postage;
    @BindView(R.id.Iv_postage)
    ImageView Iv_postage;
    @BindView(R.id.rl_acc)
    RelativeLayout rl_acc;
    @BindView(R.id.tv_acc_damage_cover)
    TextviewBold tv_acc_damage_cover;
    @BindView(R.id.Iv_acc_damage_cover)
    ImageView Iv_acc_damage_cover;
    @BindView(R.id.rl_fees_cart)
    RelativeLayout rl_fees_cart;
    @BindView(R.id.tv_fees)
    TextviewBold tv_fees;
    @BindView(R.id.tv_rental_fee)
    TextviewRegular tv_rental_fee;
    @BindView(R.id.tv_clean_fee)
    TextviewRegular tv_clean_fee;
    @BindView(R.id.tv_service)
    TextviewRegular tv_service;
    @BindView(R.id.tv_delivery_fee)
    TextviewRegular tv_delivery_fee;
    @BindView(R.id.tv_acc_damage_cover_fee)
    TextviewRegular tv_acc_damage_cover_fee;
    @BindView(R.id.tv_total)
    TextviewRegular tv_total;
    @BindView(R.id.tv_proceed_to_pay)
    TextviewSemiBold tv_proceed_to_pay;
    @BindView(R.id.rl_pickup)
    RelativeLayout rl_pickup;
    @BindView(R.id.rl_postage)
    RelativeLayout rl_postage;
    @BindView(R.id.rl_Acc_damage)
    RelativeLayout rl_Acc_damage;
    @BindView(R.id.feeTextView)
    TextView feeTextView;
    @BindView(R.id.tv_type)
    TextView tv_type;
    @BindView(R.id.tv_delivery)
    TextView tv_delivery;
    @BindView(R.id.Iv_delivery)
    ImageView Iv_delivery;
    @BindView(R.id.tv_total_top)
    TextView tv_total_top;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.cleaningTV)
    TextView cleaningTV;
    @BindView(R.id.cleaningText)
    TextView cleaningText;
    @BindView(R.id.AccidentalTV)
    TextView AccidentalTV;
    @BindView(R.id.AccidentalText)
    TextView AccidentalText;
    @BindView(R.id.view)
    View view;

    TextviewRegular mRentalTV, mCleaningTV, mService, mDeliveryFeeTV;
    int count = 0, i_Delivery, i_Accidental = 0;
    String Accidental_damage = "0", delivery_type = "", CleanFee = "", DeliveryFee = "", RentalFee = "", ServiceFee = "", AccidentalFee = "", TotalFee = "";
    double d_Rental = 0.00, d_Cleaning = 0.00, d_Service = 0.00, d_Delivery = 0.00, d_Accidental = 0.00, d_Total = 0.00;
    ItemProductModel itemProductModel;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    String type = "", RequestId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        //Set Butter Knife
        ButterKnife.bind(this);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            //Set Details Data
            executeOrderApi();
        }
    }

    @OnClick({R.id.cancelRL, R.id.tv_proceed_to_pay, R.id.rl_acc, R.id.rl_delivery_cart, R.id.rl_fees_cart,
             R.id.rl_Acc_damage, R.id.iv_cart_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.tv_proceed_to_pay:
                performProceedtoPayClick();
                break;

            case R.id.rl_acc:
                showAccidentaldamageCover(mActivity, getResources().getString(R.string.acc_damage), getResources().getString(R.string.desc_acc));
                break;

            case R.id.rl_delivery_cart:
                showdelivery(mActivity, getResources().getString(R.string.deli), getResources().getString(R.string.desc_del));
                break;

            case R.id.rl_fees_cart:
                if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                    showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_rental), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                    showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_Item_Buy), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                }
                break;

            case R.id.rl_Acc_damage:
                performAccClick();
                break;

            case R.id.iv_cart_profile:
                performCartProfileClick();
                break;
        }
    }

    private void performCartProfileClick() {
        Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        intent.putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE);
        mActivity.startActivity(intent);
    }

    /* perform proceed to pay click */
    private void performProceedtoPayClick() {
        executeUpdateOrder();
    }

    private void paymentActivityIntent() {
        Intent intent = new Intent(mActivity, PaymentActivity.class);
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PAYMENT_VALUE, "Cart");
        intent.putExtra(ConstantData.PAYMENT_VALUE, "Cart");
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        startActivity(intent);
        finish();
    }

    private void performAccClick() {
        if (count == 0) {
            i_Accidental = 5;
            d_Accidental = 5.00;
            Accidental_damage = "1";
            Iv_acc_damage_cover.setImageResource(R.drawable.ic_check);
            count++;
        } else {
            i_Accidental = 0;
            d_Accidental = 0.00;
            Accidental_damage = "0";
            Iv_acc_damage_cover.setImageResource(R.drawable.ic_uncheck);
            count = 0;
        }

        AccidentalFee = String.format("%.2f", d_Accidental);
        tv_acc_damage_cover_fee.setText(AccidentalFee);

        d_Total = d_Rental + d_Cleaning + d_Service + d_Delivery + d_Accidental;
        TotalFee = String.format("%.2f", d_Total);
        tv_total.setText("£" + TotalFee);
        tv_total_top.setText("Total: " + "£" + TotalFee);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     *
     * AccidentalDamageCover Alert Dialog
     * */
    public void showAccidentaldamageCover(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.popup_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);
        mDesc.setText(strDesc);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Delivery Popup Dialog
     * */
    private void showdelivery(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.delivery_popup_cart);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView btnOk = alertDialog.findViewById(R.id.btnOk);
        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);
        String str_delivery = "<font color=#000000>Your chosen delivery option.</font>";
        mDesc.setText(Html.fromHtml(str_delivery));

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*Show popup of fees*/
    private void showFees(Activity mActivity, String strHeading, String strRental, String strCleaning, String strService, String strDelivery) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.fees_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        mRentalTV = alertDialog.findViewById(R.id.tv_rental);
        mCleaningTV = alertDialog.findViewById(R.id.tv_cleaning);
        mService = alertDialog.findViewById(R.id.tv_service);
        mDeliveryFeeTV = alertDialog.findViewById(R.id.tv_delivery_fee);

        mHeading.setText(strHeading);

        String str_rental = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            str_rental = "<b><font color=#b7006d>Rental Fee:</font></b>" + "<font color=#000000> the cost of renting the item for the selected rental period.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            str_rental = "<b><font color=#b7006d>Purchase Price:</font></b>" + "<font color=#000000> the cost of buying your selected item.</font>";
        }

        String str_cleaning = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            mCleaningTV.setVisibility(View.VISIBLE);
            str_cleaning = "<b><font color=#b7006d>Cleaning Fee:</font></b>" + "<font color=#000000> the cost of dry cleaning determined by the lender.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            mCleaningTV.setVisibility(View.GONE);
        }

        String str_service = "<b><font color=#b7006d>Service Fee:</font></b>" + "<font color=#000000> covers our bank fees, the smooth running of the Attires4Hire platform and other services.</font>";
        String str_delivery = "<b><font color=#b7006d>Delivery Fee:</font></b>" + "<font color=#000000> occurs when you select delivery by post.</font>";

        mRentalTV.setText(Html.fromHtml(str_rental));
        mCleaningTV.setText(Html.fromHtml(str_cleaning));
        mService.setText(Html.fromHtml(str_service));
        mDeliveryFeeTV.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void executeOrderApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ORDER_DETAILS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", itemProductModel.getOrder_id());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                parseOrderResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseOrderResponse(JSONObject response) {
        try {

            if (response.getString("status").equals("1")) {
                try {

                    JSONObject mDataObject = response.getJSONObject("data");

                    itemProductModel = new ItemProductModel();

                    if (!mDataObject.isNull("id") && !mDataObject.getString("id").equals(""))
                        itemProductModel.setOrder_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id") && !mDataObject.getString("user_id").equals(""))
                        itemProductModel.setAnother_User_Id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("request_id") && !mDataObject.getString("request_id").equals("")) {
                        itemProductModel.setRequest_id(mDataObject.getString("request_id"));
                        RequestId = mDataObject.getString("request_id");
                    }
                    if (!mDataObject.isNull("order_number") && !mDataObject.getString("order_number").equals("")) {
                        itemProductModel.setOrder_number(mDataObject.getString("order_number"));
                    }
                    if (!mDataObject.isNull("transaction_number") && !mDataObject.getString("transaction_number").equals("")) {
                        itemProductModel.setTransaction_number(mDataObject.getString("transaction_number"));
                    }
                    if (!mDataObject.isNull("damage_fee") && !mDataObject.getString("damage_fee").equals("")) {
                        itemProductModel.setDamage_fee(mDataObject.getString("damage_fee"));
                        d_Accidental = Double.parseDouble(mDataObject.getString("damage_fee"));
                        AccidentalFee = String.format("%.2f", d_Accidental);
                        tv_acc_damage_cover_fee.setText(AccidentalFee);
                    } else {
                        AccidentalFee = String.format("%.2f", d_Accidental);
                        tv_acc_damage_cover_fee.setText(AccidentalFee);
                    }
                    if (!mDataObject.isNull("accidental_damage") && mDataObject.getString("accidental_damage").equals("1")) {
                        itemProductModel.setAccidental_damage(mDataObject.getString("accidental_damage"));
                        Iv_acc_damage_cover.setImageResource(R.drawable.ic_check);
                        Accidental_damage = "1";
                        count = 1;
                    } else {
                        Accidental_damage = "0";
                        count = 0;
                        Iv_acc_damage_cover.setImageResource(R.drawable.ic_uncheck);
                    }
                    if (!mDataObject.isNull("total_price") && !mDataObject.getString("total_price").equals("")) {
                        itemProductModel.setTotal_price(mDataObject.getString("total_price"));
                        d_Total = Double.parseDouble(mDataObject.getString("total_price"));
                        TotalFee = String.format("%.2f", d_Total);
                        tv_total.setText("£" + TotalFee);
                        tv_total_top.setText("Total: " + "£" + TotalFee);
                    }
                    if (!mDataObject.isNull("status") && !mDataObject.getString("status").equals("")) {
                        itemProductModel.setStatus(mDataObject.getString("status"));
                    }

                    if (!mDataObject.isNull("extension"))
                        itemProductModel.setExtension(mDataObject.getString("extension"));

                    if (!mDataObject.isNull("delivery_type") && !mDataObject.getString("delivery_type").equals("")) {
                        itemProductModel.setDelivery_type(mDataObject.getString("delivery_type"));

                        if (mDataObject.getString("delivery_type").equals("1")) {
                            tv_delivery.setText("Pickup");
                            delivery_type = "1";
                        } else {
                            tv_delivery.setText("Postage");
                            delivery_type = "0";
                        }
                    }

                    if (!mDataObject.isNull("lender_rating_status") && !mDataObject.getString("lender_rating_status").equals(""))
                        itemProductModel.setLender_rating_status(mDataObject.getString("lender_rating_status"));

                    if (!mDataObject.isNull("renter_rating_status") && !mDataObject.getString("renter_rating_status").equals(""))
                        itemProductModel.setRenter_rating_status(mDataObject.getString("renter_rating_status"));

                    if (!mDataObject.isNull("request_type") && !mDataObject.getString("request_type").equals("")) {
                        itemProductModel.setRequest_type(mDataObject.getString("request_type"));
                        type = mDataObject.getString("request_type");
                        if (mDataObject.getString("request_type").equals("rent")) {
                            tv_type.setText("Rental");
                            feeTextView.setText("Rental Fee");
                            tv_clean_fee.setVisibility(View.VISIBLE);
                            cleaningText.setVisibility(View.VISIBLE);
                            cleaningTV.setVisibility(View.VISIBLE);
                            AccidentalTV.setVisibility(View.VISIBLE);
                            AccidentalText.setVisibility(View.VISIBLE);
                            rl_Acc_damage.setVisibility(View.VISIBLE);
                            tv_acc_damage_cover_fee.setVisibility(View.VISIBLE);
                            view.setVisibility(View.VISIBLE);
                        } else {
                            tv_type.setText("Purchase");
                            feeTextView.setText("Purchase Price");
                            tv_clean_fee.setVisibility(View.GONE);
                            cleaningText.setVisibility(View.GONE);
                            cleaningTV.setVisibility(View.GONE);
                            AccidentalTV.setVisibility(View.GONE);
                            AccidentalText.setVisibility(View.GONE);
                            rl_Acc_damage.setVisibility(View.GONE);
                            tv_acc_damage_cover_fee.setVisibility(View.GONE);
                            view.setVisibility(View.GONE);
                        }
                    }
                    if (!mDataObject.isNull("pick_up") && !mDataObject.getString("pick_up").equals(""))
                        itemProductModel.setPick_up(mDataObject.getString("pick_up"));

                    if (!mDataObject.isNull("delivery_fee") && !mDataObject.getString("delivery_fee").equals("")) {
                        itemProductModel.setCart_Delivery_fee(mDataObject.getString("delivery_fee"));
                        d_Delivery = Double.parseDouble(mDataObject.getString("delivery_fee"));
                        DeliveryFee = String.format("%.2f", d_Delivery);
                        tv_delivery_fee.setText(DeliveryFee);
                    } else {
                        DeliveryFee = String.format("%.2f", d_Delivery);
                        tv_delivery_fee.setText(DeliveryFee);
                    }

                    if (!mDataObject.isNull("cleaning_fee") && !mDataObject.getString("cleaning_fee").equals("")) {
                        itemProductModel.setCart_Cleaning_fee(mDataObject.getString("cleaning_fee"));
                        d_Cleaning = Double.parseDouble(mDataObject.getString("cleaning_fee"));
                        CleanFee = String.format("%.2f", d_Cleaning);
                        tv_clean_fee.setText(CleanFee);
                    } else {
                        CleanFee = String.format("%.2f", d_Cleaning);
                        tv_clean_fee.setText(CleanFee);
                    }
                    if (!mDataObject.isNull("rental_fee") && !mDataObject.getString("rental_fee").equals("")) {
                        itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));
                        d_Rental = Double.parseDouble(mDataObject.getString("rental_fee"));
                        RentalFee = String.format("%.2f", d_Rental);
                        tv_rental_fee.setText(RentalFee);
                    } else {
                        RentalFee = String.format("%.2f", d_Rental);
                        tv_rental_fee.setText(RentalFee);
                    }
                    if (!mDataObject.isNull("service_fee") && !mDataObject.getString("service_fee").equals("")) {
                        itemProductModel.setService_fee(mDataObject.getString("service_fee"));
                        d_Service = Double.parseDouble(mDataObject.getString("service_fee"));
                        ServiceFee = String.format("%.2f", d_Service);
                        tv_service.setText(ServiceFee);
                    } else {
                        ServiceFee = String.format("%.2f", d_Service);
                        tv_service.setText(ServiceFee);
                    }

                    JSONObject mDataObject_ProductDetails = mDataObject.getJSONObject("product_detail");

                    if (!mDataObject_ProductDetails.isNull("id"))
                        itemProductModel.setId(mDataObject_ProductDetails.getString("id"));

                    if (!mDataObject_ProductDetails.isNull("name")) {
                        itemProductModel.setName(mDataObject_ProductDetails.getString("name"));
                        tv_product_name.setText(CapitalizedFirstLetter(mDataObject_ProductDetails.getString("name")));
                    }
                    if (!mDataObject_ProductDetails.isNull("user_id")) {
                        itemProductModel.setUser_id(mDataObject_ProductDetails.getString("user_id"));
                    }
                    if (!mDataObject_ProductDetails.isNull("image_id"))
                        itemProductModel.setImage_id(mDataObject_ProductDetails.getString("image_id"));

                    if (!mDataObject_ProductDetails.isNull("category_name"))
                        itemProductModel.setCategory_name(mDataObject_ProductDetails.getString("category_name"));

                    if (!mDataObject_ProductDetails.isNull("type_name"))
                        itemProductModel.setType_name(mDataObject_ProductDetails.getString("type_name"));

                    if (!mDataObject_ProductDetails.isNull("size_name"))
                        itemProductModel.setSize_name(mDataObject_ProductDetails.getString("size_name"));

                    if (!mDataObject_ProductDetails.isNull("color_name"))
                        itemProductModel.setColor_name(mDataObject_ProductDetails.getString("color_name"));

                    if (!mDataObject_ProductDetails.isNull("brand_name"))
                        itemProductModel.setBrand_name(mDataObject_ProductDetails.getString("brand_name"));

                    if (!mDataObject_ProductDetails.isNull("condition_name"))
                        itemProductModel.setCondition_name(mDataObject_ProductDetails.getString("condition_name"));

                    if (!mDataObject_ProductDetails.isNull("ocasion_name"))
                        itemProductModel.setOcasion_name(mDataObject_ProductDetails.getString("ocasion_name"));

                    if (!mDataObject_ProductDetails.isNull("description"))
                        itemProductModel.setDescription(mDataObject_ProductDetails.getString("description"));

                    if (!mDataObject_ProductDetails.isNull("date"))
                        itemProductModel.setDate(mDataObject_ProductDetails.getString("date"));

                    if (!mDataObject_ProductDetails.isNull("retail_price"))
                        itemProductModel.setRetail_price(mDataObject_ProductDetails.getString("retail_price"));

                    if (!mDataObject_ProductDetails.isNull("replacement_value"))
                        itemProductModel.setReplacement_value(mDataObject_ProductDetails.getString("replacement_value"));

                    if (!mDataObject_ProductDetails.isNull("week_4days"))
                        itemProductModel.setWeek_4days(mDataObject_ProductDetails.getString("week_4days"));

                    if (!mDataObject_ProductDetails.isNull("week_8days"))
                        itemProductModel.setWeek_8days(mDataObject_ProductDetails.getString("week_8days"));

                    if (!mDataObject_ProductDetails.isNull("week_12days"))
                        itemProductModel.setWeek_12days(mDataObject_ProductDetails.getString("week_12days"));

                    if (!mDataObject_ProductDetails.isNull("instant_booking"))
                        itemProductModel.setInstant_booking(mDataObject_ProductDetails.getString("instant_booking"));

                    if (!mDataObject_ProductDetails.isNull("open_for_sale"))
                        itemProductModel.setOpen_for_sale(mDataObject_ProductDetails.getString("open_for_sale"));

                    if (!mDataObject_ProductDetails.isNull("cleaning_free"))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));

                    if (!mDataObject_ProductDetails.isNull("drop_person"))
                        itemProductModel.setDrop_person(mDataObject_ProductDetails.getString("drop_person"));

                    if (!mDataObject_ProductDetails.isNull("cleaning_free"))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));

                    if (!mDataObject_ProductDetails.isNull("delivery_free"))
                        itemProductModel.setDelivery_free(mDataObject_ProductDetails.getString("delivery_free"));

                    if (!mDataObject_ProductDetails.isNull("image1") && mDataObject_ProductDetails.getString("image1").contains("http")) {
                        itemProductModel.setImage1(mDataObject_ProductDetails.getString("image1"));

                        if (itemProductModel.getImage1().contains("http")) {
                            Glide.with(mActivity).load(itemProductModel.getImage1())
                                    .placeholder(R.drawable.ic_pp_ph)
                                    .error(R.drawable.ic_pp_ph)
                                    .into(iv_cart_profile);
                        }


//                        if (itemProductModel.getImage1().contains("https")) {
//                            Glide.with(mActivity).load(itemProductModel.getImage1())
//                                    .placeholder(R.drawable.ic_pp_ph)
//                                    .error(R.drawable.ic_pp_ph)
//                                    .into(iv_cart_profile);
//                        } else {
//                            Glide.with(mActivity).load(itemProductModel.getImage1().replace("http://", "https://"))
//                                    .placeholder(R.drawable.ic_pp_ph)
//                                    .error(R.drawable.ic_pp_ph)
//                                    .into(iv_cart_profile);
//                        }
                    }

                    if (!mDataObject_ProductDetails.isNull("image2"))
                        itemProductModel.setImage2(mDataObject_ProductDetails.getString("image2"));

                    if (!mDataObject_ProductDetails.isNull("image3"))
                        itemProductModel.setImage3(mDataObject_ProductDetails.getString("image3"));

                    if (!mDataObject_ProductDetails.isNull("image4"))
                        itemProductModel.setImage4(mDataObject_ProductDetails.getString("image4"));

                    if (!mDataObject_ProductDetails.isNull("image5"))
                        itemProductModel.setImage5(mDataObject_ProductDetails.getString("image5"));

                    if (!mDataObject_ProductDetails.isNull("rating"))
                        itemProductModel.setRatings(mDataObject_ProductDetails.getString("rating"));

                    JSONObject request_detail = mDataObject.getJSONObject("request_detail");

                    if (!request_detail.isNull("booking_dates")) {
                        itemProductModel.setBooking_dates(request_detail.getString("booking_dates"));

                        String input = request_detail.getString("booking_dates");

                        JSONArray jsonArray = new JSONArray(input);
                        String[] strArr = new String[jsonArray.length()];

                        for (int i = 0; i < jsonArray.length(); i++) {
                            strArr[i] = jsonArray.getString(i);
                        }

                        String first_date = strArr[0];
                        String last_date = strArr[strArr.length - 1];

                        tv_date.setText(parseDateToddMMyyyy(first_date) + " – " + parseDateToddMMyyyy(last_date));
                    }

                    mItemProductModel.add(itemProductModel);

                    executeUpdateReadStatusApi(RequestId);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    /*
     * UPDATE READ STATUS API
     * */
    private void executeUpdateReadStatusApi(String request_id) {
//        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();

        params.put("user_id", getUserID());
        params.put("request_id", request_id);

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.UPDATE_READ_STATUS, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
//                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    } else {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeUpdateOrder() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_ORDER_DETAIL;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", itemProductModel.getOrder_id());
            params.put("accidental_damage", Accidental_damage);
            params.put("rental_fee", RentalFee);
            params.put("cleaning_fee", CleanFee);
            params.put("service_fee", ServiceFee);
            if (delivery_type.equals("1")) {
                params.put("delivery_fee", "0");
            } else {
                params.put("delivery_fee", itemProductModel.getDelivery_free());
            }
            params.put("damage_fee", AccidentalFee);
            params.put("total_price", TotalFee);
            params.put("delivery_type", delivery_type);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseAddOrderResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseAddOrderResponse(JSONObject response) {
        dismissProgressDialog();
        try {

            if (response.getString("status").equals("1")) {
                try {

                    if (!response.isNull("order_detail")) {
                        JSONObject mDataObject = response.getJSONObject("order_detail");

                        if (!mDataObject.isNull("id"))
                            itemProductModel.setOrder_id(mDataObject.getString("id"));

                        if (!mDataObject.isNull("user_id"))
                            itemProductModel.setUser_id(mDataObject.getString("user_id"));

                        if (!mDataObject.isNull("request_id"))
                            itemProductModel.setRequest_id(mDataObject.getString("request_id"));

                        if (!mDataObject.isNull("order_number"))
                            itemProductModel.setOrder_number(mDataObject.getString("order_number"));

                        if (!mDataObject.isNull("transaction_number"))
                            itemProductModel.setTransaction_number(mDataObject.getString("transaction_number"));

                        if (!mDataObject.isNull("accidental_damage"))
                            itemProductModel.setAccidental_damage(mDataObject.getString("accidental_damage"));

                        if (!mDataObject.isNull("rental_fee"))
                            itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));

                        if (!mDataObject.isNull("cleaning_fee"))
                            itemProductModel.setCleaning_free(mDataObject.getString("cleaning_fee"));

                        if (!mDataObject.isNull("service_fee"))
                            itemProductModel.setService_fee(mDataObject.getString("service_fee"));

                        if (!mDataObject.isNull("delivery_fee"))
                            itemProductModel.setDelivery_free(mDataObject.getString("delivery_fee"));

                        if (!mDataObject.isNull("damage_fee"))
                            itemProductModel.setDamage_fee(mDataObject.getString("damage_fee"));

                        if (!mDataObject.isNull("total_price"))
                            itemProductModel.setTotal_price(mDataObject.getString("total_price"));

                        if (!mDataObject.isNull("status"))
                            itemProductModel.setStatus(mDataObject.getString("status"));

                        if (!mDataObject.isNull("allow_payment"))
                            itemProductModel.setAllow_payment(mDataObject.getString("allow_payment"));

                        if (!mDataObject.isNull("delivery_date"))
                            itemProductModel.setDelivery_date(mDataObject.getString("delivery_date"));

                        if (!mDataObject.isNull("dispatch_date"))
                            itemProductModel.setDispatch_date(mDataObject.getString("dispatch_date"));
                    }

                    paymentActivityIntent();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (response.getString("status").equals("3")) {

                LogOut();

            } else {

                showAlertDialog(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}