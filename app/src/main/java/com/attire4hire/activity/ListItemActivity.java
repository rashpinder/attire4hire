package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.SlidingImageAdapter;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ImagesModel;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.attire4hire.volley.AppHelper;
import com.attire4hire.volley.VolleyMultipartRequest;
import com.savvi.rangedatepicker.CalendarPickerView;
import com.suke.widget.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ListItemActivity extends BaseActivity {
    Activity mActivity = ListItemActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ListItemActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.layoutCalenderRL)
    RelativeLayout layoutCalenderRL;
    @BindView(R.id.editRentalET)
    EdittextRegular editRentalET;
    @BindView(R.id.editReplacementValueET)
    EdittextRegular editReplacementValueET;
    @BindView(R.id.rlRentalFeeInfoRL)
    RelativeLayout rlRentalFeeInfoRL;
    @BindView(R.id.edit4DaysET)
    EditText edit4DaysET;
    @BindView(R.id.edit8DaysET)
    EditText edit8DaysET;
    @BindView(R.id.edit12DaysET)
    EditText edit12DaysET;
    @BindView(R.id.switchInstantBookingSB)
    SwitchButton switchInstantBookingSB;
    @BindView(R.id.switchSaleSB)
    SwitchButton switchSaleSB;
    @BindView(R.id.editOpenForSaleET)
    EdittextRegular editOpenForSaleET;
    @BindView(R.id.switchCleaningFeeSB)
    SwitchButton switchCleaningFeeSB;
    @BindView(R.id.editCeaningFeeET)
    EdittextRegular editCeaningFeeET;
    @BindView(R.id.rlDeliveryFeeInfoRL)
    RelativeLayout rlDeliveryFeeInfoRL;
    @BindView(R.id.switchDropOffSB)
    SwitchButton switchDropOffSB;
    @BindView(R.id.switchDeliveryFeeSB)
    SwitchButton switchDeliveryFeeSB;
    @BindView(R.id.editDeliveryFeeET)
    EdittextRegular editDeliveryFeeET;
    @BindView(R.id.txtPreviewTV)
    TextviewSemiBold txtPreviewTV;
    @BindView(R.id.txtSubmitTV)
    TextviewSemiBold txtSubmitTV;
    @BindView(R.id.txt4DaysSuggestionTV)
    TextView txt4DaysSuggestionTV;
    @BindView(R.id.txt8DaysSuggestionTV)
    TextView txt8DaysSuggestionTV;
    @BindView(R.id.txt12DaysSuggestionTV)
    TextView txt12DaysSuggestionTV;
    @BindView(R.id.rlAvalabilityInfoRL)
    RelativeLayout rlAvalabilityInfoRL;
    @BindView(R.id.rlRetailsPriceInfoRL)
    RelativeLayout rlRetailsPriceInfoRL;
    @BindView(R.id.rlReplacementValueInfoRL)
    RelativeLayout rlReplacementValueInfoRL;
    @BindView(R.id.rlInstantBookingInfoRL)
    RelativeLayout rlInstantBookingInfoRL;
    @BindView(R.id.rlOpenForSaleInfoRL)
    RelativeLayout rlOpenForSaleInfoRL;
    @BindView(R.id.rlCleaningFeeInfoRL)
    RelativeLayout rlCleaningFeeInfoRL;
    @BindView(R.id.openForSaleTV)
    TextView openForSaleTV;
    @BindView(R.id.cleaningfeeTV)
    TextView cleaningfeeTV;
    @BindView(R.id.deliveryfeeTV)
    TextView deliveryfeeTV;
    @BindView(R.id.Day4TV)
    TextView Day4TV;
    @BindView(R.id.openforsaleTv)
    TextView openforsaleTv;
    @BindView(R.id.cleaningfeeTv)
    TextView cleaningfeeTv;
    @BindView(R.id.deliveryfeeTv)
    TextView deliveryfeeTv;

    /*
     * Initialize Objects
     * */
    ItemProductModel mItemProductModel = new ItemProductModel();
    ArrayList<String> mSelectedArrayList = new ArrayList<>();
    List<Date> mSelectedDateList;
    List<Date> mMultiSelectedDateaList;
    ArrayList<ImagesModel> mImagesArrayList = new ArrayList<>();
    String InstantBooking = "", OpenForSale = "", CleaningFee = "", DropOffInPerson = "", Draft_Id = "", DeliveryFee = "";

    /*
     * Boolean Values
     * */
    boolean IsInstantBooking = false;
    boolean IsOpenForSale = false;
    boolean IsCleaningFee = false;
    boolean IsDropOffInPersion = false;
    boolean IsDeliveryFee = false;

    int rentalPrize = 0, replacementPrize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);
        //ButterKnife
        ButterKnife.bind(this);

        edit12DaysET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_NULL) {

                    //Do something in here
                    edit12DaysET.clearFocus();

                    if (editOpenForSaleET.isEnabled()) {
                        editOpenForSaleET.requestFocus();
                    } else {
                        if (editCeaningFeeET.isEnabled()) {
                            editCeaningFeeET.requestFocus();
                        } else if (editDeliveryFeeET.isEnabled()) {
                            editDeliveryFeeET.requestFocus();
                        } else {
                            openForSaleTV.requestFocus();
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }
        });

        editOpenForSaleET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_NULL) {

                    //Do something in here
                    editOpenForSaleET.clearFocus();

                    if (editCeaningFeeET.isEnabled()) {
                        editCeaningFeeET.requestFocus();
                    } else if (editDeliveryFeeET.isEnabled()) {
                        editDeliveryFeeET.requestFocus();
                    } else {
                        cleaningfeeTV.requestFocus();
                    }

                    return true;
                } else {
                    return false;
                }
            }
        });

        editCeaningFeeET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_NULL) {

                    //Do something in here
                    editCeaningFeeET.clearFocus();

                    if (editCeaningFeeET.isEnabled()) {
                        editDeliveryFeeET.requestFocus();
                    } else {
                        deliveryfeeTv.requestFocus();
                    }

                    return true;
                } else {
                    return false;
                }
            }
        });

        //Get Intent Data:
        getIntentData();
        //Set Switch ChangeListner
        setSwitchChangeListner();
        //Set Edit Text Selection
        setEditTextSelection();

    }

    private void setEditTextSelection() {
        setRetailsValidation();
    }

    private void setRetailsValidation() {

        editRentalET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    //do here your stuff f
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        editReplacementValueET.requestFocus();
                    }
                    return true;
                }
                return false;
            }
        });

        editReplacementValueET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    //do here your stuff f
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }

                    String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("") && !strReplacement.equals("")) {
                        replacementPrize = Integer.parseInt(strReplacement);
                    }

                    if (replacementPrize > rentalPrize) {
                        showAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                    } else {
                        edit4DaysET.requestFocus();
                    }

                    return true;
                }
                return false;
            }
        });

        editReplacementValueET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    }
                }
            }
        });

        editReplacementValueET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editReplacementValueET.length() > 0) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }

                    String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("") && !strReplacement.equals("")) {
                        replacementPrize = Integer.parseInt(strReplacement);
                    }

                    if (replacementPrize > rentalPrize) {
                        showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit4DaysET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });

        edit8DaysET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });

        edit12DaysET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });

        editOpenForSaleET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });
        editCeaningFeeET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });
        editDeliveryFeeET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String strRental = editRentalET.getText().toString().trim().replace("£", "");

                    if (!strRental.equals("")) {
                        rentalPrize = Integer.parseInt(strRental);
                    }
                    if (rentalPrize < 100) {
                        showFocusableAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
                    } else {
                        String strReplacement = editReplacementValueET.getText().toString().trim().replace("£", "");

                        if (!strRental.equals("") && !strReplacement.equals("")) {
                            replacementPrize = Integer.parseInt(strReplacement);
                        }

                        if (replacementPrize > rentalPrize) {
                            showReplacementFocusableAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
                        }
                    }
                }
            }
        });
    }

    public void showFocusableAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                editRentalET.requestFocus();
            }
        });
        alertDialog.show();
    }

    public void showReplacementFocusableAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                editReplacementValueET.requestFocus();
            }
        });
        alertDialog.show();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            mItemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            //Set Validation
            setRentalPriceValidations();

            setData();
        }
    }

    private void setData() {
        if (mItemProductModel.getDraft_Id() != null && mItemProductModel.getDraft_Id().length() > 0) {
            Draft_Id = mItemProductModel.getDraft_Id();
            mItemProductModel.setProduct_id(Draft_Id);
        }

        if (mItemProductModel.getRetail_price() != null && mItemProductModel.getRetail_price().length() > 0) {
            String RetailPrice = mItemProductModel.getRetail_price();
            editRentalET.setText(RetailPrice);
        }

        if (mItemProductModel.getReplacement_value() != null && mItemProductModel.getReplacement_value().length() > 0) {
            String ReplacementValue = mItemProductModel.getReplacement_value();
            editReplacementValueET.setText(ReplacementValue);
        }

        if (mItemProductModel.getWeek_4days() != null && mItemProductModel.getWeek_4days().length() > 0) {
            String Days_4 = mItemProductModel.getWeek_4days();
            edit4DaysET.setText(Days_4);
        }

        if (mItemProductModel.getWeek_8days() != null && mItemProductModel.getWeek_8days().length() > 0) {
            String Days_8 = mItemProductModel.getWeek_8days();
            edit8DaysET.setText(Days_8);
        }

        if (mItemProductModel.getWeek_12days() != null && mItemProductModel.getWeek_12days().length() > 0) {
            String Days_12 = mItemProductModel.getWeek_12days();
            edit12DaysET.setText(Days_12);
        }

        if (mItemProductModel.getInstant_booking() != null && mItemProductModel.getInstant_booking().length() > 0) {
            InstantBooking = mItemProductModel.getInstant_booking();
            switchInstantBookingSB.setChecked(true);
        }

        if (mItemProductModel.getOpen_for_sale() != null && mItemProductModel.getOpen_for_sale().length() > 0) {
            OpenForSale = mItemProductModel.getOpen_for_sale();
            editOpenForSaleET.setText(OpenForSale);
            editOpenForSaleET.setEnabled(true);
            switchSaleSB.setChecked(true);
            openForSaleTV.setTextColor(getResources().getColor(R.color.black));
        } else {
            openForSaleTV.setTextColor(getResources().getColor(R.color.lightGray));
        }

        if (mItemProductModel.getCleaning_free() != null && mItemProductModel.getCleaning_free().length() > 0) {
            CleaningFee = mItemProductModel.getCleaning_free();
            editCeaningFeeET.setText(CleaningFee);
            editCeaningFeeET.setEnabled(true);
            switchCleaningFeeSB.setChecked(true);
            cleaningfeeTV.setTextColor(getResources().getColor(R.color.black));
        } else {
            cleaningfeeTV.setTextColor(getResources().getColor(R.color.lightGray));
        }

        if (mItemProductModel.getDrop_person() != null && mItemProductModel.getDrop_person().length() > 0) {
            DropOffInPerson = mItemProductModel.getDrop_person();
            switchDropOffSB.setChecked(true);
        }

        if (mItemProductModel.getDelivery_free() != null && mItemProductModel.getDelivery_free().length() > 0) {
            DeliveryFee = mItemProductModel.getDelivery_free();
            editDeliveryFeeET.setText(DeliveryFee);
            editDeliveryFeeET.setEnabled(true);
            switchDeliveryFeeSB.setChecked(true);
            deliveryfeeTV.setTextColor(getResources().getColor(R.color.black));
        } else {
            deliveryfeeTV.setTextColor(getResources().getColor(R.color.lightGray));
        }

        //for drafts images
        if (mItemProductModel.getImage1() != null && mItemProductModel.getImage1().contains("http")) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mItemProductModel.getImage1());
            mImagesArrayList.add(mModel);
        }
        //for add item images
        else if (mItemProductModel.getImagesArrayList().size() == 1) {
            if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(0).getImage_path());
                mImagesArrayList.add(mModel);
            }
        }

        //for drafts images
        if (mItemProductModel.getImage2() != null && mItemProductModel.getImage2().contains("http")) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mItemProductModel.getImage2());
            mImagesArrayList.add(mModel);
        }
        //for add item images
        else if (mItemProductModel.getImagesArrayList().size() == 2) {
            if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(0).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(1).getImage_path());
                mImagesArrayList.add(mModel);
            }
        }

        //for drafts images
        if (mItemProductModel.getImage3() != null && mItemProductModel.getImage3().contains("http")) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mItemProductModel.getImage3());
            mImagesArrayList.add(mModel);
        }
        //for add item images
        else if (mItemProductModel.getImagesArrayList().size() == 3) {
            if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(0).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(1).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(2).getImage_path());
                mImagesArrayList.add(mModel);
            }
        }

        //for drafts images
        if (mItemProductModel.getImage4() != null && mItemProductModel.getImage4().contains("http")) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mItemProductModel.getImage4());
            mImagesArrayList.add(mModel);
        }
        //for add item images
        else if (mItemProductModel.getImagesArrayList().size() == 4) {
            if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(0).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(1).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(2).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(3).getImage_path());
                mImagesArrayList.add(mModel);
            }
        }

        //for drafts images
        if (mItemProductModel.getImage5() != null && mItemProductModel.getImage5().contains("http")) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mItemProductModel.getImage5());
            mImagesArrayList.add(mModel);
        }
        //for add item images
        else if (mItemProductModel.getImagesArrayList().size() == 5) {
            if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(0).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(1).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(2).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(3).getImage_path());
                mImagesArrayList.add(mModel);
            }

            if (mItemProductModel.getImagesArrayList().get(4).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(4).getImage_path().contains("http")) {
                ImagesModel mModel = new ImagesModel();
                mModel.setImage_path(mItemProductModel.getImagesArrayList().get(4).getImage_path());
                mImagesArrayList.add(mModel);
            }
        }

        Log.e(TAG, "***ImagesArrayList***: " + mImagesArrayList.size());
    }

    private void setRentalPriceValidations() {
        editRentalET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    setUpDaysPriceAccordingPercentage(editRentalET.getText().toString().trim().replace("£", ""));
                } else {
                    setUpDaysPriceAccordingPercentage("0");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editRentalET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    setUpDaysPriceAccordingPercentage(editRentalET.getText().toString().trim().replace("£", ""));
                    return true;
                }
                return false;
            }
        });
    }

    private void setUpDaysPriceAccordingPercentage(String strRental) {
        if (mItemProductModel.getCondition_name().toLowerCase().equals("good / fair")) {
            double day4 = (Double.parseDouble(strRental) * 10) / 100;
            Log.e(TAG, "**Good Day 4**" + day4);
            double day8 = (Double.parseDouble(strRental) * 15) / 100;
            Log.e(TAG, "**Good Day 8**" + day8);
            double day12 = (Double.parseDouble(strRental) * 20) / 100;
            Log.e(TAG, "**Good Day 12**" + day12);

            String strDay4 = String.format("%.2f", day4);
            String strDay8 = String.format("%.2f", day8);
            String strDay12 = String.format("%.2f", day12);
            txt4DaysSuggestionTV.setText("Suggestion £" + strDay4);
            txt8DaysSuggestionTV.setText("Suggestion £" + strDay8);
            txt12DaysSuggestionTV.setText("Suggestion £" + strDay12);
        }

        if (mItemProductModel.getCondition_name().toLowerCase().equals("good")) {
            double day4 = (Double.parseDouble(strRental) * 10) / 100;
            Log.e(TAG, "**Good Day 4**" + day4);
            double day8 = (Double.parseDouble(strRental) * 15) / 100;
            Log.e(TAG, "**Good Day 8**" + day8);
            double day12 = (Double.parseDouble(strRental) * 20) / 100;
            Log.e(TAG, "**Good Day 12**" + day12);

            String strDay4 = String.format("%.2f", day4);
            String strDay8 = String.format("%.2f", day8);
            String strDay12 = String.format("%.2f", day12);
            txt4DaysSuggestionTV.setText("Suggestion £" + strDay4);
            txt8DaysSuggestionTV.setText("Suggestion £" + strDay8);
            txt12DaysSuggestionTV.setText("Suggestion £" + strDay12);
        }

        if (mItemProductModel.getCondition_name().toLowerCase().equals("excellent")) {
            double day4 = (Double.parseDouble(strRental) * 15) / 100;
            Log.e(TAG, "**Excellent Day 4**" + day4);
            double day8 = (Double.parseDouble(strRental) * 20) / 100;
            Log.e(TAG, "**Excellent Day 8**" + day8);
            double day12 = (Double.parseDouble(strRental) * 25) / 100;
            Log.e(TAG, "**Excellent Day 12**" + day12);

            String strDay4 = String.format("%.2f", day4);
            String strDay8 = String.format("%.2f", day8);
            String strDay12 = String.format("%.2f", day12);
            txt4DaysSuggestionTV.setText("Suggestion £" + strDay4);
            txt8DaysSuggestionTV.setText("Suggestion £" + strDay8);
            txt12DaysSuggestionTV.setText("Suggestion £" + strDay12);
        }
    }

    private void setSwitchChangeListner() {
        switchInstantBookingSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                IsInstantBooking = isChecked;
                if (IsInstantBooking) {
                    InstantBooking = "1";
                } else {
                    InstantBooking = "";
                }
            }
        });

        switchSaleSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                IsOpenForSale = isChecked;
                if (IsOpenForSale) {
                    OpenForSale = "1";
                    openForSaleTV.setTextColor(getResources().getColor(R.color.black));
                    editOpenForSaleET.setEnabled(true);
                } else {
                    OpenForSale = "";
                    editOpenForSaleET.setEnabled(false);
                    openForSaleTV.setTextColor(getResources().getColor(R.color.lightGray));
                    editOpenForSaleET.setText(OpenForSale);
                }
            }
        });

        switchCleaningFeeSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                IsCleaningFee = isChecked;
                if (IsCleaningFee) {
                    CleaningFee = "1";
                    cleaningfeeTV.setTextColor(getResources().getColor(R.color.black));
                    editCeaningFeeET.setEnabled(true);
                } else {
                    CleaningFee = "";
                    cleaningfeeTV.setTextColor(getResources().getColor(R.color.lightGray));
                    editCeaningFeeET.setEnabled(false);
                    editCeaningFeeET.setText(CleaningFee);
                }
            }
        });

        switchDropOffSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                IsDropOffInPersion = isChecked;
                if (IsDropOffInPersion) {
                    DropOffInPerson = "1";
                } else {
                    DropOffInPerson = "";
                }
            }
        });

        switchDeliveryFeeSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                IsDeliveryFee = isChecked;
                if (IsDeliveryFee) {
                    editDeliveryFeeET.setEnabled(true);
                    deliveryfeeTV.setTextColor(getResources().getColor(R.color.black));
                } else {
                    editDeliveryFeeET.setEnabled(false);
                    editDeliveryFeeET.setText("");
                    deliveryfeeTV.setTextColor(getResources().getColor(R.color.lightGray));
                }
            }
        });
    }

    private boolean isValidate() {
        boolean flag = true;
//        if (mMultiSelectedDateaList.size() == 0) {
//            showAlertDialog(mActivity, getString(R.string.enter_date_availability));
//            flag = false;
//        } else

        int rentalPrize = 0;
        int replacementPrize = 0;

        if (editRentalET.getText().toString().length() > 0) {
            rentalPrize = Integer.parseInt(editRentalET.getText().toString().trim().replace("£", ""));
        }
        if (editReplacementValueET.getText().toString().length() > 0) {
            replacementPrize = Integer.parseInt(editReplacementValueET.getText().toString().trim().replace("£", ""));
        }

        if (editRentalET.getText().toString().trim().equals("£") || (editRentalET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_retail_price));
            flag = false;
        } else if (rentalPrize < 100) {
            showAlertDialog(mActivity, getString(R.string.enter_valid_retail_price));
            flag = false;
        } else if (editReplacementValueET.getText().toString().trim().equals("£") || (editReplacementValueET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_replacement_value));
            flag = false;
        } else if (replacementPrize > rentalPrize) {
            showAlertDialog(mActivity, getString(R.string.the_replacement_value_cannot_be_higher));
            flag = false;
        } else if (edit4DaysET.getText().toString().trim().equals("£") || (edit4DaysET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_rental_fee_four_days));
            flag = false;
        } else if (edit8DaysET.getText().toString().trim().equals("£") || (edit8DaysET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_rental_fee_eight_days));
            flag = false;
        } else if (edit12DaysET.getText().toString().trim().equals("£") || (edit12DaysET.getText().toString().trim().equals(""))) {
            showAlertDialog(mActivity, getString(R.string.enter_rental_fee_tweleve_days));
            flag = false;
        } else if (DropOffInPerson.equals("") && editDeliveryFeeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.choose_one_delivery_option));
            flag = false;
        } else if (editOpenForSaleET.getText().toString().trim().equals("") && IsOpenForSale) {
            showAlertDialog(mActivity, getString(R.string.enter_open_for_sale));
            flag = false;
        } else if (editCeaningFeeET.getText().toString().trim().equals("") && IsCleaningFee) {
            showAlertDialog(mActivity, getString(R.string.enter_cleaning_fee));
            flag = false;
        } else if (editDeliveryFeeET.getText().toString().trim().equals("") && IsDeliveryFee) {
            showAlertDialog(mActivity, getString(R.string.enter_delivery_fee));
            flag = false;
        }
        return flag;
    }

    @Override
    public void onBackPressed() {
        showDraftConfirmDialog();
    }

    @OnClick({R.id.cancelRL, R.id.layoutCalenderRL, R.id.rlRentalFeeInfoRL, R.id.rlDeliveryFeeInfoRL, R.id.txtPreviewTV, R.id.txtSubmitTV,
            R.id.rlAvalabilityInfoRL, R.id.rlRetailsPriceInfoRL, R.id.rlReplacementValueInfoRL,
            R.id.rlInstantBookingInfoRL, R.id.rlOpenForSaleInfoRL, R.id.rlCleaningFeeInfoRL

    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.layoutCalenderRL:
                performCalendarClick();
                break;
            case R.id.rlRentalFeeInfoRL:
                performRentalFeeClick();
                break;
            case R.id.rlDeliveryFeeInfoRL:
                performDeliveryFeeClick();
                break;
            case R.id.txtPreviewTV:
                performPreviewClick();
                break;
            case R.id.txtSubmitTV:
                performSubmitClick();
                break;
            case R.id.rlAvalabilityInfoRL:
                showInfromationDialog(mActivity, getString(R.string.heading_Availability), getString(R.string.description_Availability));
                break;
            case R.id.rlRetailsPriceInfoRL:
                showInfromationDialog(mActivity, getString(R.string.heading_Rental_price), getString(R.string.description_Rental_price));
                break;
            case R.id.rlReplacementValueInfoRL:
                showReplacementInformation(mActivity);
                break;
            case R.id.rlInstantBookingInfoRL:
                showInfromationDialog(mActivity, getString(R.string.instant_booking), getString(R.string.description_instant_booking));
                break;
            case R.id.rlOpenForSaleInfoRL:
                showInfromationDialog(mActivity, getString(R.string.open_for_sale), getString(R.string.description_open_for_sale));
                break;
            case R.id.rlCleaningFeeInfoRL:
                showInfromationDialog(mActivity, getString(R.string.cleaning_fee), getString(R.string.description_cleading_fee));
                break;
        }
    }

    private void performCalendarClick() {
        showCalenderPopup();
    }

    private void performRentalFeeClick() {
        showInfromationDialog(mActivity, getString(R.string.rental_fee), getString(R.string.description_rental_fee));
    }

    private void performDeliveryFeeClick() {
        showDeliveryPopup(mActivity, getResources().getString(R.string.deliver));
    }

    private void performPreviewClick() {
        if (isValidate()) {
            updateModelValues();
            showPreviewDialog();
        }
    }

    private void performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                updateModelValues();
                executeAddItemApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void updateModelValues() {
        mItemProductModel.setmSelectedArrayList(mSelectedArrayList);
        mItemProductModel.setRetail_price(editRentalET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setReplacement_value(editReplacementValueET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setWeek_4days(edit4DaysET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setWeek_8days(edit8DaysET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setWeek_12days(edit12DaysET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setInstant_booking(InstantBooking);
        mItemProductModel.setOpen_for_sale(editOpenForSaleET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setCleaning_free(editCeaningFeeET.getText().toString().trim().replace("£", ""));
        mItemProductModel.setDrop_person(DropOffInPerson);
        mItemProductModel.setDelivery_free(editDeliveryFeeET.getText().toString().trim().replace("£", ""));
    }

    private void showPreviewDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_item_preview);

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtSubmitTVPP = alertDialog.findViewById(R.id.txtSubmitTVPP);
        RelativeLayout rlCloseRL = alertDialog.findViewById(R.id.rlCloseRL);
        ViewPager mViewPagerVP = alertDialog.findViewById(R.id.mViewPagerVP);
        CircleIndicator mIndicatorCI = alertDialog.findViewById(R.id.mIndicatorCI);
        TextView txtItemNameTV = alertDialog.findViewById(R.id.txtItemNameTV);
        TextView txtPriceTV = alertDialog.findViewById(R.id.txtPriceTV);
        TextView txtLocationTV = alertDialog.findViewById(R.id.txtLocationTV);
        RatingBar mRatingBarViewRBV = alertDialog.findViewById(R.id.mRatingBarViewRBV);
        TextView txtSizeTV = alertDialog.findViewById(R.id.txtSizeTV);
        TextView txtTypeTV = alertDialog.findViewById(R.id.txtTypeTV);
        TextView txtColorTV = alertDialog.findViewById(R.id.txtColorTV);
        TextView txtBrandDesignTV = alertDialog.findViewById(R.id.txtBrandDesignTV);
        TextView txtConditionsTV = alertDialog.findViewById(R.id.txtConditionsTV);
        TextView txtOccasionTV = alertDialog.findViewById(R.id.txtOccasionTV);
        TextView txtRentalPriceTV = alertDialog.findViewById(R.id.txtRentalPriceTV);
        TextView txtReplacementValueTV = alertDialog.findViewById(R.id.txtReplacementValueTV);
        TextView txt4DaysTV = alertDialog.findViewById(R.id.txt4DaysTV);
        TextView txt8DaysTV = alertDialog.findViewById(R.id.txt8DaysTV);
        TextView txt12DaysTV = alertDialog.findViewById(R.id.txt12DaysTV);
        TextView txtDescriptionTV = alertDialog.findViewById(R.id.txtDescriptionTV);
        TextView txtLocation = alertDialog.findViewById(R.id.txtLocation);

        /*Set Data On Widgets*/
        txtItemNameTV.setText(mItemProductModel.getName());
        txtPriceTV.setText("£" + mItemProductModel.getRetail_price());
        txtSizeTV.setText(mItemProductModel.getSize_name());
        txtTypeTV.setText(mItemProductModel.getType_name());
        txtColorTV.setText(mItemProductModel.getColor_name());
        txtBrandDesignTV.setText(mItemProductModel.getBrand_name());
        txtConditionsTV.setText(mItemProductModel.getCondition_name());
        txtOccasionTV.setText(mItemProductModel.getOcasion_name());
        txtRentalPriceTV.setText(mItemProductModel.getRetail_price());
        txtReplacementValueTV.setText(mItemProductModel.getReplacement_value());
        txt4DaysTV.setText("£" + mItemProductModel.getWeek_4days());
        txt8DaysTV.setText("£" + mItemProductModel.getWeek_8days());
        txt12DaysTV.setText("£" + mItemProductModel.getWeek_12days());
        txtDescriptionTV.setText(mItemProductModel.getDescription());
        txtLocation.setText(mItemProductModel.getLocation());

        /*
         * Set Viewpager Adapter
         * */
        SlidingImageAdapter mSlidingImageAdapter = new SlidingImageAdapter(mActivity, mItemProductModel.getImagesArrayList(), new SlidingImageAdapter.AddClick() {
            @Override
            public void Click(int position) {
                Toast.makeText(mActivity, "Images", Toast.LENGTH_SHORT).show();
            }

        });
        mViewPagerVP.setAdapter(mSlidingImageAdapter);
        mIndicatorCI.setViewPager(mViewPagerVP);

        rlCloseRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtSubmitTVPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                performSubmitClick();
            }
        });
        alertDialog.show();
    }

    /*
     * Show Calendar Popup
     *
     * */
    private void showCalenderPopup() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.calender_popup);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CalendarPickerView calendar = alertDialog.findViewById(R.id.calendar_view);
        ImageView nextClick = alertDialog.findViewById(R.id.rightIV);
        ImageView backClick = alertDialog.findViewById(R.id.leftIV);
        TextView clickBT = alertDialog.findViewById(R.id.tv_set_date);

        clickBT.setVisibility(View.VISIBLE);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, 0);

        Date tomorrow = lastYear.getTime();

        /*
         * CLICK PATICULAR DATE
         * */

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                mSelectedArrayList.clear();
            }

            @Override
            public void onDateUnselected(Date date) {
                mSelectedArrayList.clear();
            }
        });

        /*
         *`
         * BUTTON TO SHOW SELECTED DATES
         * */
        clickBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Date datee = calendar.getSelectedDate();
                mMultiSelectedDateaList = calendar.getSelectedDates();

                for (int i = 0; i < mMultiSelectedDateaList.size(); i++) {
                    String mSelectedDate = convertCalenderTimeToFormat(mMultiSelectedDateaList.get(i).toString());
                    Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                    mSelectedArrayList.add(mSelectedDate);
                }

                Log.e(TAG, "**Selected Dates List Size**" + mSelectedArrayList.size());

                alertDialog.dismiss();
            }
        });

        nextClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backClick.setVisibility(View.VISIBLE);
                c.add(Calendar.MONTH, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                calendar.scrollToDate(resultdate);
            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                c.add(Calendar.MONTH, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());

                //calender current date
                String dayOfTheWeek = (String) DateFormat.format("EEEE", resultdate); // Week day
                String day = (String) DateFormat.format("dd", resultdate); // day
                String monthString = (String) DateFormat.format("MMM", resultdate); // Month
                String monthNumber = (String) DateFormat.format("MM", resultdate); // Month in number
                String year = (String) DateFormat.format("yyyy", resultdate); // Year

                //calender current month
                int CalenderMonth = Integer.parseInt(monthNumber);

                String newDtString = sdf.format(resultdate);
                calendar.scrollToDate(resultdate);
                Log.e("TAG", "mon::" + resultdate);

                //current month
                int month = Calendar.DAY_OF_MONTH;
                int CurrentMonth = month - 1;

                if (CalenderMonth == month) {
                    backClick.setVisibility(View.GONE);
                } else {
                    backClick.setVisibility(View.VISIBLE);
                }
            }
        });

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE);

        calendar.init(tomorrow, nextYear.getTime(), dt1)
                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                .withSelectedDates(mMultiSelectedDateaList);

        Log.e(TAG, "**Dates List Size**: " + mItemProductModel.getDate());

        if (mItemProductModel.getDate() != null && mItemProductModel.getDate().length() > 5) {

            mSelectedDateList = new ArrayList<>();
            try {
                JSONArray mJsonArray = new JSONArray(mItemProductModel.getDate());
                for (int i = 0; i < mJsonArray.length(); i++) {
                    String strDate = mJsonArray.getString(i);
                    Log.e(TAG, "***Single Date**" + strDate);
                    Date mDate = null;
                    try {
                        mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                        mSelectedDateList.add(mDate);

                        String mSelectedDate = convertCalenderTimeToFormat(mSelectedDateList.get(i).toString());
                        Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                        mSelectedArrayList.add(mSelectedDate);

                        Log.e(TAG, "****Converted Dates***" + mSelectedArrayList.size());

                        calendar.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withSelectedDates(mSelectedDateList);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "***Converted Dates**" + mSelectedDateList.size());
                }
            } catch (Exception e) {
                e.toString();
            }
        }
        alertDialog.show();
    }

    /* Add Item API*/
    private void executeAddItemApi() {
        showProgressDialog(mActivity);

        String ApiUrl = ConstantData.ADDITEM_API;
        Log.e(TAG, "Api Url::::" + ApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                ApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                dismissProgressDialog();
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);
                parseResponse(resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(mActivity, getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(mActivity, getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(mActivity, getString(R.string.parse_error));
                        } else {
                            showAlertDialog(mActivity, strBody);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                JSONArray jsonArray = new JSONArray(mItemProductModel.getmSelectedArrayList());
                params.put("user_id", getUserID());
                params.put("name", mItemProductModel.getName());
                params.put("category_name", mItemProductModel.getCategory_name());
                params.put("type_name", mItemProductModel.getType_name());
                params.put("size_name", mItemProductModel.getSize_name());
                params.put("color_name", mItemProductModel.getColor_name());
                params.put("brand_name", mItemProductModel.getBrand_name());
                params.put("condition_name", mItemProductModel.getCondition_name());
                params.put("occasion_name", mItemProductModel.getOcasion_name());
                params.put("description", mItemProductModel.getDescription());
                params.put("location", mItemProductModel.getLocation());
                if (mItemProductModel.getmSelectedArrayList() != null) {
                    params.put("date", jsonArray.toString());
                } else {
                    params.put("date", "");
                }
                params.put("retail_price", mItemProductModel.getRetail_price());
                params.put("replacement_value", mItemProductModel.getReplacement_value());
                params.put("week_4days", mItemProductModel.getWeek_4days());
                params.put("week_8days", mItemProductModel.getWeek_8days());
                params.put("week_12days", mItemProductModel.getWeek_12days());
                params.put("instant_booking", mItemProductModel.getInstant_booking());
                params.put("open_for_sale", mItemProductModel.getOpen_for_sale());
                params.put("cleaning_free", mItemProductModel.getCleaning_free());
                params.put("drop_person", mItemProductModel.getDrop_person());
                params.put("delivery_free", mItemProductModel.getDelivery_free());
                params.put("draft_id", Draft_Id);
                Log.i(TAG, "**ListItemPARAMS**" + params.toString());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView

                if (mItemProductModel.getImagesArrayList().size() == 1) {
                    if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    }
                }

                if (mItemProductModel.getImagesArrayList().size() == 2) {
                    if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    }
                }

                if (mItemProductModel.getImagesArrayList().size() == 3) {
                    if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    }
                }

                if (mItemProductModel.getImagesArrayList().size() == 4) {
                    if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                        params.put("uploads[4]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(3).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                        params.put("uploads[4]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(3).getImage_path())), "image/jpeg"));
                    }
                }

                if (mItemProductModel.getImagesArrayList().size() == 5) {
                    if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(0).getImage_path() != null && mItemProductModel.getImagesArrayList().get(0).getImage_path().contains("http")) {
                        params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(0).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(1).getImage_path() != null && mItemProductModel.getImagesArrayList().get(1).getImage_path().contains("http")) {
                        params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(1).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(2).getImage_path() != null && mItemProductModel.getImagesArrayList().get(2).getImage_path().contains("http")) {
                        params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(2).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                        params.put("uploads[4]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(3).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(3).getImage_path() != null && mItemProductModel.getImagesArrayList().get(3).getImage_path().contains("http")) {
                        params.put("uploads[4]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(3).getImage_path())), "image/jpeg"));
                    }

                    if (mItemProductModel.getImagesArrayList().get(4).getImage_path() != null && !mItemProductModel.getImagesArrayList().get(4).getImage_path().contains("http")) {
                        params.put("uploads[5]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(mItemProductModel.getImagesArrayList().get(4).getImage_path())), "image/jpeg"));
                    } else if (mItemProductModel.getImagesArrayList().get(4).getImage_path() != null && mItemProductModel.getImagesArrayList().get(4).getImage_path().contains("http")) {
                        params.put("uploads[5]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, uriToBitmap(mItemProductModel.getImagesArrayList().get(4).getImage_path())), "image/jpeg"));
                    }
                }
                return params;
            }
        };

        Attire4hireApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    private void parseResponse(String response) {
        dismissProgressDialog();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                showToast(mActivity, mJsonObject.getString("message"));

                Intent intent = new Intent(mActivity, ListingActivity.class);
                startActivity(intent);
                finish();

            } else if (mJsonObject.getString("status").equals("3")) {
                LogOut();
            } else {
                showToast(mActivity, mJsonObject.getString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDraftConfirmDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_add_draft);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtSaveAsDraftTV = alertDialog.findViewById(R.id.txtSaveAsDraftTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity, AddItemActivity.class);
                intent.putExtra(ConstantData.MODEL, mItemProductModel);
                intent.putExtra(ConstantData.ADD_ITEM_TYPE, ConstantData.ACTIVITY_LIST_ITEM);
                startActivity(intent);
                finish();//finishing activity
            }
        });
        txtSaveAsDraftTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                saveAsDraft();
            }
        });
        alertDialog.show();
    }

    /********************************************************************************/

    private void saveAsDraft() {
        if (isNetworkAvailable(mActivity)) {
            updateModelValues();
            executeDraftItemApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    String strImage1 = "";
    String strImage2 = "";
    String strImage3 = "";
    String strImage4 = "";
    String strImage5 = "";

    /*
     * Execute Draft Api
     * */
    private void executeDraftItemApi() {

        //Collect Images:
        if (mImagesArrayList != null) {

            for (int i = 0; i < mImagesArrayList.size(); i++) {
                if (i == 0) {
                    strImage1 = mImagesArrayList.get(i).getImage_path();
                }
                if (i == 1) {
                    strImage2 = mImagesArrayList.get(i).getImage_path();
                }
                if (i == 2) {
                    strImage3 = mImagesArrayList.get(i).getImage_path();
                }
                if (i == 3) {
                    strImage4 = mImagesArrayList.get(i).getImage_path();
                }
                if (i == 4) {
                    strImage5 = mImagesArrayList.get(i).getImage_path();
                }
            }
        }

        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.ADDITEM_DRAFT_API;
        Log.e(TAG, "**API URL**" + mApiUrl);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                mApiUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                dismissProgressDialog();
                String resultResponse = new String(response.data);
                Log.e(TAG, "onResponse: " + resultResponse);
                parseDraftResponse(resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                String strBody = "";
                if (error.networkResponse != null) {
                    try {
                        strBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof AuthFailureError) {
                            showAlertDialog(mActivity, getString(R.string.auth_failed));
                        } else if (error instanceof ServerError) {
                            showAlertDialog(mActivity, getString(R.string.server_error));
                        } else if (error instanceof NetworkError) {
                            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                        } else if (error instanceof ParseError) {
                            showAlertDialog(mActivity, getString(R.string.parse_error));
                        } else {
                            showAlertDialog(mActivity, strBody);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", getUserID());
                params.put("name", mItemProductModel.getName());
                params.put("category_name", mItemProductModel.getCategory_name());
                params.put("type_name", mItemProductModel.getType_name());
                params.put("size_name", mItemProductModel.getSize_name());
                params.put("color_name", mItemProductModel.getColor_name());
                params.put("brand_name", mItemProductModel.getBrand_name());
                params.put("condition_name", mItemProductModel.getCondition_name());
                params.put("occasion_name", mItemProductModel.getOcasion_name());
                params.put("description", mItemProductModel.getDescription());
                params.put("location", mItemProductModel.getLocation());

                JSONArray jsonArray = new JSONArray(mSelectedArrayList);

                if (mSelectedArrayList.size() > 0) {
                    params.put("date", jsonArray.toString());
                } else {
                    params.put("date", "");
                }
                if (editRentalET.getText().toString().trim().length() > 0) {
                    params.put("retail_price", editRentalET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("retail_price", "");
                }
                if (editReplacementValueET.getText().toString().trim().length() > 0) {
                    params.put("replacement_value", editReplacementValueET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("replacement_value", "");
                }
                if (edit4DaysET.getText().toString().trim().length() > 0) {
                    params.put("week_4days", edit4DaysET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("week_4days", "");
                }
                if (edit8DaysET.getText().toString().trim().length() > 0) {
                    params.put("week_8days", edit8DaysET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("week_8days", "");
                }
                if (edit12DaysET.getText().toString().trim().length() > 0) {
                    params.put("week_12days", edit12DaysET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("week_12days", "");
                }

                params.put("instant_booking", "" + InstantBooking);

                if (editOpenForSaleET.getText().toString().trim().length() > 0) {
                    params.put("open_for_sale", editOpenForSaleET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("open_for_sale", "");
                }

                if (editCeaningFeeET.getText().toString().trim().length() > 0) {
                    params.put("cleaning_free", editCeaningFeeET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("cleaning_free", "");
                }

                params.put("drop_person", "" + DropOffInPerson);

                if (editDeliveryFeeET.getText().toString().trim().length() > 0) {
                    params.put("delivery_free", editDeliveryFeeET.getText().toString().trim().replace("£", ""));
                } else {
                    params.put("delivery_free", "");
                }
                params.put("del_DraftID", mItemProductModel.getId());

                //Upload Images
                if (strImage1.contains("http") || strImage1.contains("https"))
                    params.put("uploads[1]", strImage1);
                if (strImage2.contains("http") || strImage2.contains("https"))
                    params.put("uploads[2]", strImage2);
                if (strImage3.contains("http") || strImage3.contains("https"))
                    params.put("uploads[3]", strImage3);
                if (strImage4.contains("http") || strImage4.contains("https"))
                    params.put("uploads[4]", strImage4);
                if (strImage5.contains("http") || strImage5.contains("https"))
                    params.put("uploads[5]", strImage5);

                Log.e(TAG, "**PARAMS**" + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView

                if (strImage1 != null && !strImage1.contains("http") && strImage1.length() > 0)
                    params.put("uploads[1]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(strImage1)), "image/jpeg"));

                if (strImage2 != null && !strImage2.contains("http") && strImage2.length() > 0)
                    params.put("uploads[2]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(strImage2)), "image/jpeg"));

                if (strImage3 != null && !strImage3.contains("http") && strImage3.length() > 0)
                    params.put("uploads[3]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(strImage3)), "image/jpeg"));

                if (strImage4 != null && !strImage4.contains("http") && strImage4.length() > 0)
                    params.put("uploads[4]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(strImage4)), "image/jpeg"));

                if (strImage5 != null && !strImage5.contains("http") && strImage5.length() > 0)
                    params.put("uploads[5]", new DataPart(getAlphaNumericString() + ".jpg", AppHelper.getFileDataFromBitmap(mActivity, getBitmapFromFilePath(strImage5)), "image/jpeg"));

                return params;
            }
        };

        Attire4hireApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    private void parseDraftResponse(String response) {
        dismissProgressDialog();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                showToast(mActivity, getString(R.string.save_as_draft));
                finish();
            } else if (mJsonObject.getString("status").equals("3")) {
                LogOut();

            } else {
                Log.e(TAG, "**ERROR**" + mJsonObject.getString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}