package com.attire4hire.activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.ConstantData;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ReportOrderLenderSideActivity extends BaseActivity {
    Activity mActivity = ReportOrderLenderSideActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = ReportOrderLenderSideActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.iv_5)
    ImageView iv_5;

    @BindView(R.id.iv_6)
    ImageView iv_6;

    @BindView(R.id.ll_5)
    LinearLayout ll_5;

    @BindView(R.id.ll_6)
    LinearLayout ll_6;

    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;

    @BindView(R.id.tvSubmit)
    TextviewRegular tvSubmit;

    @BindView(R.id.et_message)
    EditText et_message;

    @BindView(R.id.rlAttachmentRL)
    RelativeLayout rlAttachmentRL;

    @BindView(R.id.pathTV)
    TextView pathTV;

    ItemProductModel itemProductModel;
    String Message = "", Reason = "";
    String PicturePath = "", mImage = "";

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_order_lender_side);

        ButterKnife.bind(this);

        //Capitalize first letter
        et_message.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_message.getText().toString().length() == 1 && et_message.getTag().toString().equals("true")) {
                    et_message.setTag("false");
                    et_message.setText(et_message.getText().toString().toUpperCase());
                    et_message.setSelection(et_message.getText().toString().length());
                }
                if (et_message.getText().toString().length() == 0) {
                    et_message.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });

        getIntentData();
    }

    @OnClick({R.id.ll_5, R.id.ll_6, R.id.rlCancelRL, R.id.tvSubmit, R.id.rlAttachmentRL})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_5:
                performOrderOptionsClick("5");
                break;

            case R.id.ll_6:
                performOrderOptionsClick("6");
                break;

            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.tvSubmit:
                performSubmitClick();
                break;

            case R.id.rlAttachmentRL:
                performAddAttachmentClick();
                break;
        }
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
        }
    }

    private void performOrderOptionsClick(String type) {
        iv_5.setImageResource(R.drawable.ic_uncheck);
        iv_6.setImageResource(R.drawable.ic_uncheck);

        if (type.equals("5")) {
            iv_5.setImageResource(R.drawable.ic_check);
            Reason = "5";
            Message = getString(R.string.report_order_5);
        } else if (type.equals("6")) {
            iv_6.setImageResource(R.drawable.ic_check);
            Reason = "6";
            Message = getString(R.string.report_order_6);
        }
    }

    /*
     * Add validations
     * */
    private Boolean validate() {
        boolean flag = true;
        if (Reason.equals("")) {
            showAlertDialog(mActivity, getString(R.string.choose_one_option));
            flag = false;
        } else if (et_message.getText().toString().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_describe_issue));
            flag = false;
        }
//        else if (mImage.equals("")) {
//            showAlertDialog(mActivity, getString(R.string.Please_attach_a_supportive_document));
//            flag = false;
//        }
        return flag;
    }

    private void performSubmitClick() {
        if (validate()) {
            if (isNetworkAvailable(mActivity))
                executeAddReportAPI();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    /* Camera Gallery functionality
     * */
    public void performAddAttachmentClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
//                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);

                    mImage = compressImage(getRealPathFromURI(String.valueOf(result.getUri())));

                    PicturePath = result.getUri().getLastPathSegment();
                    pathTV.setText(PicturePath);

                    Log.e(TAG, "**Image**" + mImage);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private void executeAddReportAPI() {
        showProgressDialog(mActivity);

        MultipartBody.Part fileToUpload = null;
        if (mImage != null && !mImage.equals("")) {
            File file = new File(mImage);
            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            fileToUpload = MultipartBody.Part.createFormData("doc", file.getName(), mFile);
        }

        RequestBody UserIdBody = RequestBody.create(MediaType.parse("text/plain"), getUserID());
        RequestBody OrderIdBody = RequestBody.create(MediaType.parse("text/plain"), itemProductModel.getOrder_id());
        RequestBody ReasonBody = RequestBody.create(MediaType.parse("text/plain"), Reason);
        RequestBody MessageBody = RequestBody.create(MediaType.parse("text/plain"), et_message.getText().toString());

        Api api = ApiClient.getApiClient().create(Api.class);
        api.ReportOrder(UserIdBody, OrderIdBody, ReasonBody, MessageBody, fileToUpload).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        Log.e(TAG, "**RESPONSE**" + jsonObjectMM.getString("message"));
                        showAlertDialogFinish(mActivity, jsonObjectMM.getString("message"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}