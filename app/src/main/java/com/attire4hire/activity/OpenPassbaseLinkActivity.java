package com.attire4hire.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.attire4hire.R;
import com.attire4hire.utils.ConstantData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;

public class OpenPassbaseLinkActivity extends AppCompatActivity implements
        EasyPermissions.PermissionCallbacks{
    /*
     * Initialize Activity
     * */
    Activity mActivity = OpenPassbaseLinkActivity.this;
    /*
     * Get Activity Name
     * */
    String TAG = OpenPassbaseLinkActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.mWebViewWV)
    WebView mWebViewWV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;

    /*
     * Initlize Objects
     * */
    String strLinkUrl = "";
    private PermissionRequest mPermissionRequest;

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String[] PERM_CAMERA =
            {Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);

        //butter knife
        ButterKnife.bind(this);

        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.getSettings().setAllowFileAccessFromFileURLs(true);
        mWebViewWV.getSettings().setAllowUniversalAccessFromFileURLs(true);

        mProgressBar.setVisibility(View.GONE);

        //myWebView.setWebViewClient(new WebViewClient());
        mWebViewWV.setWebChromeClient(new WebChromeClient() {
            // Grant permissions for cam
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.i(TAG, "onPermissionRequest");
                mPermissionRequest = request;
                final String[] requestedResources = request.getResources();
                for (String r : requestedResources) {
                    if (r.equals(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
                        // In this sample, we only accept video capture request.
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OpenPassbaseLinkActivity.this)
                                .setTitle("Allow Permission to camera")
                                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        mPermissionRequest.grant(new String[]{PermissionRequest.RESOURCE_VIDEO_CAPTURE});
                                        Log.d(TAG,"Granted");
                                    }
                                })
                                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        mPermissionRequest.deny();
                                        Log.d(TAG,"Denied");
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        break;
                    }
                }
            }

            @Override
            public void onPermissionRequestCanceled(PermissionRequest request) {
                super.onPermissionRequestCanceled(request);
                Toast.makeText(OpenPassbaseLinkActivity.this,"Permission Denied",Toast.LENGTH_SHORT).show();
            }
        });


        if(hasCameraPermission()){
            mWebViewWV.loadUrl("https://verify.passbase.com/dharmani-76f56e11");
//            setContentView(mWebViewWV );
        }else{
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs access to your camera so you can take pictures.",
                    REQUEST_CAMERA_PERMISSION,
                    PERM_CAMERA);
        }

//        getIntentData();
    }

    private boolean hasCameraPermission() {
        return EasyPermissions.hasPermissions(OpenPassbaseLinkActivity.this, PERM_CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    private void getIntentData() {
        if (getIntent() != null) {
            strLinkUrl = getIntent().getStringExtra(ConstantData.LINK);
            Log.e(TAG, "**Link**" + strLinkUrl);
            loadUrlInWebView(strLinkUrl);
        }
    }

    private void loadUrlInWebView(String strLinkUrl) {
        mWebViewWV.setWebViewClient(new MyWebClient());
        mWebViewWV.getSettings().setJavaScriptEnabled(true);
        mWebViewWV.getSettings().setMediaPlaybackRequiresUserGesture(false);
        mWebViewWV.loadUrl(strLinkUrl);
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public class MyWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            mProgressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }
    }

    // To handle "Back" key press event for WebView to go back to previous screen.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebViewWV.canGoBack()) {
            mWebViewWV.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
