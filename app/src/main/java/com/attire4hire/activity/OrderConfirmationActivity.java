package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.extension.ItemDetailsExtensionActivity;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class OrderConfirmationActivity extends BaseActivity implements View.OnClickListener {
    Activity mActivity = OrderConfirmationActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = OrderConfirmationActivity.this.getClass().getSimpleName();

    /*Widgets*/
    TextView mBookedTV, nameTV, order_num, transaction_id, transaction_id2, tv_rental_fee, tv_clean_fee, tv_service,
            tv_delivery_fee, tv_acc_damage_cover_fee, tv_total, tv_cancel, tv_type,
            feeTextView, tv_postage, tv_total_top, txtReportTV;

    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.txtUserNameTV)
    TextviewSemiBold txtUserNameTV;
    @BindView(R.id.txTotalReviewsTV)
    TextviewRegular txTotalReviewsTV;
    @BindView(R.id.txtUserLocationTV)
    TextviewRegular txtUserLocationTV;
    @BindView(R.id.rlContactLedgerRL)
    RelativeLayout rlContactLedgerRL;
    @BindView(R.id.userRatings)
    RatingBar userRatings;
    @BindView(R.id.ll_user_details)
    LinearLayout ll_user_details;
    @BindView(R.id.AccidentalTV)
    TextView AccidentalTV;
    @BindView(R.id.AccidentalText)
    TextView AccidentalText;
    @BindView(R.id.rl_Acc_damage)
    RelativeLayout rl_Acc_damage;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.bookedTV)
    TextView bookedTV;
    @BindView(R.id.bookedIv)
    ImageView bookedIv;
    @BindView(R.id.bookeddateTV)
    TextView bookeddateTV;
    @BindView(R.id.shippedTV)
    TextView shippedTV;
    @BindView(R.id.shippedIv)
    ImageView shippedIv;
    @BindView(R.id.shippeddateTv)
    TextView shippeddateTv;
    @BindView(R.id.receivedTV)
    TextView receivedTV;
    @BindView(R.id.receivedIv)
    ImageView receivedIv;
    @BindView(R.id.receiveddateTv)
    TextView receiveddateTv;
    @BindView(R.id.returnedTV)
    TextView returnedTV;
    @BindView(R.id.returnedIv)
    ImageView returnedIv;
    @BindView(R.id.returneddateTv)
    TextView returneddateTv;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.spaceView)
    View spaceView;
    @BindView(R.id.completedIv)
    ImageView completedIv;
    @BindView(R.id.completeddateTv)
    TextView completeddateTv;

    @BindView(R.id.homeRL)
    RelativeLayout homeRL;

    @BindView(R.id.returnedLL)
    LinearLayout returnedLL;

    @BindView(R.id.tv_request_extension)
    TextView tv_request_extension;

    TextView tv_date;
    TextView cleaningTV;
    TextView cleaningText;
    RelativeLayout cancelRL;
    RelativeLayout mDeliveryRL, mFeesRL, mAccCoverRL;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ImageView iv_cart_profile, Iv_postage, Iv_acc_damage_cover;
    ItemProductModel itemProductModel;
    String LenderBuyStatus = "", LenderRentStatus = "", RenterBuyStatus = "", RenterRentStatus = "";
    String orderid = "", requestId = "", user_id = "", str_Status = "", str_Type = "", user_type = "";
    String first_date = "", last_date = "", hour_diff = "", receive_hour_diff = "", report_status = "";
    SwipeRefreshLayout swipeToRefresh;
    boolean isSwipeRefresh = false;
    Date firstDate = null, currentDate = null;
    double d_Rental = 0.00, d_Cleaning = 0.00, d_Service = 0.00, d_Delivery = 0.00, d_Accidental = 0.00, d_Total = 0.00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);

        ButterKnife.bind(this);

        initialization();
        setData();
        getIntentData();

        /* get current date */
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        String date = sdf.format(c.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            currentDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                executeOrderApi();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeOrderApi();
        }
    }

    private void initialization() {
        tv_type = findViewById(R.id.tv_type);
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        mAccCoverRL = findViewById(R.id.rl_acc_order);
        mDeliveryRL = findViewById(R.id.rl_delivery_order);
        mFeesRL = findViewById(R.id.rl_fees_order);
        mBookedTV = findViewById(R.id.tv_booked);
        cancelRL = findViewById(R.id.cancelRL);
        nameTV = findViewById(R.id.nameTV);
        order_num = findViewById(R.id.order_num);
        transaction_id = findViewById(R.id.transaction_id);
        transaction_id2 = findViewById(R.id.transaction_id2);
        tv_rental_fee = findViewById(R.id.tv_rental_fee);
        tv_clean_fee = findViewById(R.id.tv_clean_fee);
        tv_service = findViewById(R.id.tv_service);
        tv_delivery_fee = findViewById(R.id.tv_delivery_fee);
        tv_acc_damage_cover_fee = findViewById(R.id.tv_acc_damage_cover_fee);
        tv_total = findViewById(R.id.tv_total);
        iv_cart_profile = findViewById(R.id.iv_cart_profile);
        Iv_postage = findViewById(R.id.Iv_postage);
        Iv_acc_damage_cover = findViewById(R.id.Iv_acc_damage_cover);
        tv_cancel = findViewById(R.id.tv_cancel);
        feeTextView = findViewById(R.id.feeTextView);
        tv_postage = findViewById(R.id.tv_postage);
        tv_total_top = findViewById(R.id.tv_total_top);
        tv_date = findViewById(R.id.tv_date);
        cleaningTV = findViewById(R.id.cleaningTV);
        cleaningText = findViewById(R.id.cleaningText);
        txtReportTV = findViewById(R.id.txtReportTV);
    }

    private void setData() {
        tv_request_extension.setOnClickListener(this);
        mAccCoverRL.setOnClickListener(this);
        mDeliveryRL.setOnClickListener(this);
        mFeesRL.setOnClickListener(this);
        mBookedTV.setOnClickListener(this);
        cancelRL.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        rlContactLedgerRL.setOnClickListener(this);
        ll_user_details.setOnClickListener(this);
        iv_cart_profile.setOnClickListener(this);
        homeRL.setOnClickListener(this);
        txtReportTV.setOnClickListener(this);

        shippedIv.setOnClickListener(this);
        receivedIv.setOnClickListener(this);
        returnedIv.setOnClickListener(this);
        completedIv.setOnClickListener(this);
    }

    private void getIntentData() {
        if (getIntent() != null) {

            if (getIntent().getStringExtra(ConstantData.NOTIFICATION) != null) {

                String Notification = getIntent().getStringExtra(ConstantData.NOTIFICATION);

                if (Notification.equals("notification")) {
                    orderid = getIntent().getStringExtra("order_id");
                    requestId = getIntent().getStringExtra("requestId");
                    user_id = getIntent().getStringExtra("fromUserId");
                } else if (Notification.equals("branch")) {
                    orderid = getIntent().getStringExtra(ConstantData.ORDER_ID);
                } else if (Notification.equals(ConstantData.PENALTY)) {
                    orderid = getIntent().getStringExtra(ConstantData.ORDER_ID);
                }
            }

            if ((ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL) != null) {

                itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
                orderid = itemProductModel.getOrder_id();
                requestId = itemProductModel.getRequest_id();
                user_id = itemProductModel.getUser_id();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_acc_order:
                showAccidentaldamageCover(mActivity, getString(R.string.acc_dam), getResources().getString(R.string.acc_descri));
                break;
            case R.id.rl_delivery_order:
                showdelivery(mActivity, getString(R.string.deli), getResources().getString(R.string.deliv));
                break;
            case R.id.rl_fees_order:
                if (str_Type.equalsIgnoreCase("rent")) {
                    showFees(mActivity, getResources().getString(R.string.fee), getResources().getString(R.string.rent_fe), getString(R.string.cle), getResources().getString(R.string.ser), getResources().getString(R.string.de_fee));
                } else if (str_Type.equalsIgnoreCase("buy")) {
                    showFees(mActivity, getResources().getString(R.string.fee), getResources().getString(R.string.desc_Item_Buy), getString(R.string.cle), getResources().getString(R.string.ser), getResources().getString(R.string.de_fee));
                }
                break;
            case R.id.tv_booked:
//                getOrderStatus();
                break;
            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.tv_cancel:
                showDialogCancel(mActivity);
                break;

            case R.id.rlContactLedgerRL:
                performContactLedgerClick();
                break;

            case R.id.ll_user_details:
                performUserDetailsClick();
                break;

            case R.id.iv_cart_profile:
                performCartProfileClick();
                break;

            case R.id.homeRL:
                performHomeClick();
                break;

            case R.id.txtReportTV:
                performReportClick();
                break;

            case R.id.shippedIv:
                executeUpdateOrderStatusApi("3");
                break;

            case R.id.receivedIv:
                executeUpdateOrderStatusApi("4");
                break;

            case R.id.returnedIv:
                executeUpdateOrderStatusApi("5");
                break;

            case R.id.completedIv:
                executeUpdateOrderStatusApi("6");
                break;

            case R.id.tv_request_extension:
                peformRequestExtensionClick();
                break;
        }
    }

    private void peformRequestExtensionClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeCheckExtensionAPI();
        }
    }

    private void performReportClick() {
        if (receive_hour_diff != null && !receive_hour_diff.equals("")) {
            int hours = Integer.parseInt(receive_hour_diff);

            if (hours < 12) {
                if (report_status.equals("1")) {
                    showAlertDialog(mActivity, getString(R.string.already_requested_refund));
                } else {
                    if (user_type.equals("Lender")) {
                        Intent intent = new Intent(mActivity, ReportOrderLenderSideActivity.class);
                        intent.putExtra(ConstantData.MODEL, itemProductModel);
                        mActivity.startActivity(intent);
                    } else {
                        Intent intent = new Intent(mActivity, ReportOrderRenterSideActivity.class);
                        intent.putExtra(ConstantData.MODEL, itemProductModel);
                        mActivity.startActivity(intent);
                    }
                }
            } else {
                showAlertDialog(mActivity, getString(R.string.more_than_12_hours_for_refund_case));
            }
        }
    }

    private void performHomeClick() {
        callHomeActivityIntent(mActivity);
    }

    private void performCartProfileClick() {
        Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        intent.putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE);
        mActivity.startActivity(intent);
    }

    private void performUserDetailsClick() {
        startActivity(new Intent(mActivity, AnotherUserActivity.class).putExtra(Attire4hirePrefrences.ANOTHER_USER_ID, itemProductModel.getAnother_User_Id()));
    }

    public void showDialogCancel(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView noTV = alertDialog.findViewById(R.id.noTV);
        TextView yesTV = alertDialog.findViewById(R.id.yesTV);
        TextView txtMessageTV1 = alertDialog.findViewById(R.id.txtMessageTV1);
        TextView txtMessageTV2 = alertDialog.findViewById(R.id.txtMessageTV2);
        TextView attentionTV = alertDialog.findViewById(R.id.attentionTV);

        if (hour_diff != null && !hour_diff.equals("")) {
            int hours = Integer.parseInt(hour_diff);
            if (user_type.equalsIgnoreCase("Lender")) {

                if (hours < 72) {
                    attentionTV.setVisibility(View.VISIBLE);

                    txtMessageTV1.setText(R.string.less_than_72_hours_in_lender_case_text1);
                    txtMessageTV2.setText(R.string.less_than_72_hours_in_lender_case_text2);
                } else {
                    attentionTV.setVisibility(View.GONE);

                    txtMessageTV1.setText(R.string.more_than_72_hours_in_lender_case_text1);
                    txtMessageTV2.setText(R.string.more_than_72_hours_in_lender_case_text2);
                }

            } else if (user_type.equalsIgnoreCase("Renter")) {

                if (hours < 72) {
                    attentionTV.setVisibility(View.VISIBLE);

                    txtMessageTV1.setText(R.string.less_than_72_hours_in_renter_case_text1);
                    txtMessageTV2.setText(R.string.less_than_72_hours_in_renter_case_text2);
                } else {
                    attentionTV.setVisibility(View.GONE);

                    txtMessageTV1.setText(R.string.more_than_72_hours_in_renter_case_text1);
                    txtMessageTV2.setText(R.string.more_than_72_hours_in_renter_case_text2);
                }
            }
        }

        yesTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                RenterBuyStatus = "7";
                RenterRentStatus = "7";
                LenderBuyStatus = "7";
                LenderRentStatus = "7";
                str_Status = "7";
                executeUpdateOrderStatusApi("7");
            }
        });

        noTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void performContactLedgerClick() {
        executeRequestChatApi();
    }

    private void executeRequestChatApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.REQUEST_CHAT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("owner_id", itemProductModel.getAnother_User_Id());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {

                    if (response.getString("status").equals("1")) {
                        try {

                            if (!response.isNull("room_id"))
                                itemProductModel.setRoom_id(response.getString("room_id"));

                            if (!response.isNull("owner_id"))
                                itemProductModel.setOwner_id(response.getString("owner_id"));

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_detail");

                            if (!mDataObject_UserDetails.isNull("id"))
                                itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name"))
                                itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                            if (!mDataObject_UserDetails.isNull("last_name"))
                                itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                            if (!mDataObject_UserDetails.isNull("email"))
                                itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic"))
                                itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                            if (!mDataObject_UserDetails.isNull("rating"))
                                itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                            mItemProductModel.add(itemProductModel);

                            startActivity(new Intent(mActivity, ChatActivity.class).putExtra(ConstantData.MODEL, itemProductModel));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeUpdateOrderStatusApi(String Status) {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_ORDER_STATUS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", itemProductModel.getOrder_id());
            params.put("status", Status);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                parseUpdateOrderStatusResponse(response);

                manageOrderFlow(str_Type, Status);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseUpdateOrderStatusResponse(JSONObject response) {
        try {

            if (response.getString("status").equals("1")) {
                try {

                    JSONObject mDataObject = response.getJSONObject("order_detail");

//                    itemProductModel = new ItemProductModel();

                    if (!mDataObject.isNull("order_number") && !mDataObject.getString("order_number").equals("")) {
                        itemProductModel.setOrder_number(mDataObject.getString("order_number"));
                    }

                    if (!mDataObject.isNull("status") && !mDataObject.getString("status").equals("")) {
                        itemProductModel.setStatus(mDataObject.getString("status"));
                        str_Status = mDataObject.getString("status");
                    }

                    if (!mDataObject.isNull("dispatch_date") && !mDataObject.getString("dispatch_date").equals("")) {
                        itemProductModel.setDispatch_date(mDataObject.getString("dispatch_date"));
                    }

                    if (!mDataObject.isNull("delivery_date") && !mDataObject.getString("delivery_date").equals("")) {
                        itemProductModel.setDelivery_date(mDataObject.getString("delivery_date"));
                    }

                    if (!mDataObject.isNull("return_date") && !mDataObject.getString("return_date").equals("")) {
                        itemProductModel.setReturn_date(mDataObject.getString("return_date"));
                    }

                    if (!mDataObject.isNull("completed_date") && !mDataObject.getString("completed_date").equals("")) {
                        itemProductModel.setCompleted_date(mDataObject.getString("completed_date"));
                    }

                    if (!mDataObject.isNull("status") && !mDataObject.getString("status").equals("")) {
                        itemProductModel.setStatus(mDataObject.getString("status"));
                    }

                    if (!mDataObject.isNull("receive_hour_diff") && !mDataObject.getString("receive_hour_diff").equals("")) {
                        itemProductModel.setReceive_hour_diff(mDataObject.getString("receive_hour_diff"));
                        receive_hour_diff = mDataObject.getString("receive_hour_diff");
                    }

                    if (!mDataObject.isNull("report_status") && !mDataObject.getString("report_status").equals("")) {
                        itemProductModel.setReport_status(mDataObject.getString("report_status"));
                        report_status = mDataObject.getString("report_status");
                    }

                    mItemProductModel.add(itemProductModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void executeOrderApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ORDER_DETAILS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", orderid);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                parseOrderResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseOrderResponse(JSONObject response) {
        try {

            if (response.getString("status").equals("1")) {
                try {

                    JSONObject mDataObject = response.getJSONObject("data");

                    itemProductModel = new ItemProductModel();

                    if (!mDataObject.isNull("id") && !mDataObject.getString("id").equals(""))
                        itemProductModel.setOrder_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id") && !mDataObject.getString("user_id").equals(""))
                        itemProductModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("request_id") && !mDataObject.getString("request_id").equals(""))
                        itemProductModel.setRequest_id(mDataObject.getString("request_id"));

                    if (!mDataObject.isNull("order_number") && !mDataObject.getString("order_number").equals("")) {
                        itemProductModel.setOrder_number(mDataObject.getString("order_number"));
                        order_num.setText(mDataObject.getString("order_number"));
                    }
                    if (!mDataObject.isNull("transaction_number") && !mDataObject.getString("transaction_number").equals("")) {
                        itemProductModel.setTransaction_number(mDataObject.getString("transaction_number"));
                    }
                    if (!mDataObject.isNull("damage_fee") && !mDataObject.getString("damage_fee").equals("")) {
                        itemProductModel.setDamage_fee(mDataObject.getString("damage_fee"));
                        d_Accidental = Double.parseDouble(mDataObject.getString("damage_fee"));
                        String Accidental = String.format("%.2f", d_Accidental);
                        tv_acc_damage_cover_fee.setText(Accidental);
                    } else {
                        String Accidental = String.format("%.2f", d_Accidental);
                        tv_acc_damage_cover_fee.setText(Accidental);
                    }
                    if (!mDataObject.isNull("accidental_damage") && mDataObject.getString("accidental_damage").equals("1")) {
                        itemProductModel.setAccidental_damage(mDataObject.getString("accidental_damage"));
                        Iv_acc_damage_cover.setImageResource(R.drawable.ic_check);
                    } else {
                        Iv_acc_damage_cover.setImageResource(R.drawable.ic_uncheck);
                    }
                    if (!mDataObject.isNull("total_price") && !mDataObject.getString("total_price").equals("")) {
                        itemProductModel.setTotal_price(mDataObject.getString("total_price"));
                        d_Total = Double.parseDouble(mDataObject.getString("total_price"));
                        String Total = String.format("%.2f", d_Total);
                        tv_total.setText("£" + Total);
                        tv_total_top.setText("Total: " + "£" + Total);
                    }
                    if (!mDataObject.isNull("status") && !mDataObject.getString("status").equals("")) {
                        itemProductModel.setStatus(mDataObject.getString("status"));
                        str_Status = mDataObject.getString("status");
                    }

                    if (!mDataObject.isNull("dispatch_date") && !mDataObject.getString("dispatch_date").equals("")) {
                        itemProductModel.setDispatch_date(mDataObject.getString("dispatch_date"));
                    }

                    if (!mDataObject.isNull("delivery_date") && !mDataObject.getString("delivery_date").equals("")) {
                        itemProductModel.setDelivery_date(mDataObject.getString("delivery_date"));
                    }

                    if (!mDataObject.isNull("return_date") && !mDataObject.getString("return_date").equals("")) {
                        itemProductModel.setReturn_date(mDataObject.getString("return_date"));
                    }

                    if (!mDataObject.isNull("completed_date") && !mDataObject.getString("completed_date").equals("")) {
                        itemProductModel.setCompleted_date(mDataObject.getString("completed_date"));
                    }

                    if (!mDataObject.isNull("delivery_type") && !mDataObject.getString("delivery_type").equals("")) {
                        itemProductModel.setDelivery_type(mDataObject.getString("delivery_type"));

                        if (mDataObject.getString("delivery_type").equals("1")) {
                            tv_postage.setText("Pickup");
                        } else {
                            tv_postage.setText("Postage");
                        }
                    }

                    if (!mDataObject.isNull("lender_rating_status") && !mDataObject.getString("lender_rating_status").equals(""))
                        itemProductModel.setLender_rating_status(mDataObject.getString("lender_rating_status"));

                    if (!mDataObject.isNull("renter_rating_status") && !mDataObject.getString("renter_rating_status").equals(""))
                        itemProductModel.setRenter_rating_status(mDataObject.getString("renter_rating_status"));

                    if (!mDataObject.isNull("request_type") && !mDataObject.getString("request_type").equals("")) {
                        itemProductModel.setRequest_type(mDataObject.getString("request_type"));
                        str_Type = mDataObject.getString("request_type");

                        if (str_Type.equalsIgnoreCase("rent")) {
                            feeTextView.setText("Rental Fee");
                            tv_type.setText("Rental");

                            tv_clean_fee.setVisibility(View.VISIBLE);
                            cleaningText.setVisibility(View.VISIBLE);
                            cleaningTV.setVisibility(View.VISIBLE);
                            tv_cancel.setVisibility(View.VISIBLE);
                            AccidentalTV.setVisibility(View.VISIBLE);
                            AccidentalText.setVisibility(View.VISIBLE);
                            rl_Acc_damage.setVisibility(View.VISIBLE);
                            tv_acc_damage_cover_fee.setVisibility(View.VISIBLE);
                            view.setVisibility(View.VISIBLE);
                        } else if (str_Type.equalsIgnoreCase("buy")) {
                            feeTextView.setText("Purchase Price");
                            tv_type.setText("Purchase");

                            tv_clean_fee.setVisibility(View.GONE);
                            cleaningText.setVisibility(View.GONE);
                            cleaningTV.setVisibility(View.GONE);
                            tv_cancel.setVisibility(View.GONE);
                            AccidentalTV.setVisibility(View.GONE);
                            AccidentalText.setVisibility(View.GONE);
                            rl_Acc_damage.setVisibility(View.GONE);
                            tv_acc_damage_cover_fee.setVisibility(View.GONE);
                            view.setVisibility(View.GONE);
                        }
                    }

                    if (!mDataObject.isNull("delivery_fee") && !mDataObject.getString("delivery_fee").equals("")) {
                        itemProductModel.setCart_Delivery_fee(mDataObject.getString("delivery_fee"));
                        d_Delivery = Double.parseDouble(mDataObject.getString("delivery_fee"));
                        String Delivery = String.format("%.2f", d_Delivery);
                        tv_delivery_fee.setText(Delivery);
                    } else {
                        String Delivery = String.format("%.2f", d_Delivery);
                        tv_delivery_fee.setText(Delivery);
                    }

                    if (!mDataObject.isNull("cleaning_fee") && !mDataObject.getString("cleaning_fee").equals("") && !mDataObject.getString("cleaning_fee").equals(" ")) {
                        itemProductModel.setCart_Cleaning_fee(mDataObject.getString("cleaning_fee"));
                        d_Cleaning = Double.parseDouble(mDataObject.getString("cleaning_fee"));
                        String CleanFee = String.format("%.2f", d_Cleaning);
                        tv_clean_fee.setText(CleanFee);
                    } else {
                        String CleanFee = String.format("%.2f", d_Cleaning);
                        tv_clean_fee.setText(CleanFee);
                    }
                    if (!mDataObject.isNull("rental_fee") && !mDataObject.getString("rental_fee").equals("")) {
                        itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));
                        d_Rental = Double.parseDouble(mDataObject.getString("rental_fee"));
                        String Rental = String.format("%.2f", d_Rental);
                        tv_rental_fee.setText(Rental);
                    } else {
                        String Rental = String.format("%.2f", d_Rental);
                        tv_rental_fee.setText(Rental);
                    }
                    if (!mDataObject.isNull("service_fee") && !mDataObject.getString("service_fee").equals("")) {
                        itemProductModel.setService_fee(mDataObject.getString("service_fee"));
                        d_Service = Double.parseDouble(mDataObject.getString("service_fee"));
                        String Service = String.format("%.2f", d_Service);
                        tv_service.setText(Service);
                    } else {
                        String Service = String.format("%.2f", d_Service);
                        tv_service.setText(Service);
                    }

                    if (!mDataObject.isNull("book_date") && !mDataObject.getString("book_date").equals("")) {
                        itemProductModel.setBook_date(mDataObject.getString("book_date"));
//                        bookeddateTV.setText(getDate(Long.parseLong(mDataObject.getString("book_date"))));
                    }

                    if (!mDataObject.isNull("order_date") && !mDataObject.getString("order_date").equals("")) {
                        itemProductModel.setOrder_date(mDataObject.getString("order_date"));
                        bookeddateTV.setText(mDataObject.getString("order_date"));
                    }

                    if (!mDataObject.isNull("txn_no") && !mDataObject.getString("txn_no").equals("")) {
                        itemProductModel.setTxn_no(mDataObject.getString("txn_no"));
                        transaction_id.setText(mDataObject.getString("txn_no"));
                    }

                    if (!mDataObject.isNull("transaction_number2") && !mDataObject.getString("transaction_number2").equals("")) {
                        itemProductModel.setTxn_no2(mDataObject.getString("transaction_number2"));
                        transaction_id2.setText(mDataObject.getString("transaction_number2"));
                    }

                    if (!mDataObject.isNull("extension") && !mDataObject.getString("extension").equals("")) {
                        itemProductModel.setExtension(mDataObject.getString("extension"));
                    }

                    if (!mDataObject.isNull("hour_diff") && !mDataObject.getString("hour_diff").equals("")) {
                        itemProductModel.setHour_diff(mDataObject.getString("hour_diff"));
                        hour_diff = mDataObject.getString("hour_diff");
                    }

                    if (!mDataObject.isNull("receive_hour_diff") && !mDataObject.getString("receive_hour_diff").equals("")) {
                        itemProductModel.setReceive_hour_diff(mDataObject.getString("receive_hour_diff"));
                        receive_hour_diff = mDataObject.getString("receive_hour_diff");
                    }

                    if (!mDataObject.isNull("report_status") && !mDataObject.getString("report_status").equals("")) {
                        itemProductModel.setReport_status(mDataObject.getString("report_status"));
                        report_status = mDataObject.getString("report_status");
                    }

                    if (!mDataObject.isNull("report_claim") && !mDataObject.getString("report_claim").equals("")) {
                        itemProductModel.setReport_claim(mDataObject.getString("report_claim"));
                    }

                    JSONObject mDataObject_ProductDetails = mDataObject.getJSONObject("product_detail");

                    if (!mDataObject_ProductDetails.isNull("id"))
                        itemProductModel.setId(mDataObject_ProductDetails.getString("id"));

                    if (!mDataObject_ProductDetails.isNull("name")) {
                        itemProductModel.setName(mDataObject_ProductDetails.getString("name"));
                        nameTV.setText(CapitalizedFirstLetter(mDataObject_ProductDetails.getString("name")));
                    }
                    if (!mDataObject_ProductDetails.isNull("user_id")) {
                        itemProductModel.setProduct_owner_id(mDataObject_ProductDetails.getString("user_id"));
                        user_id = mDataObject_ProductDetails.getString("user_id");

                        /* check if user is lender or renter */
                        if (getUserID().equals(user_id)) {
                            user_type = "Lender";
                        } else {
                            user_type = "Renter";
                        }
                    }
                    if (!mDataObject_ProductDetails.isNull("image_id"))
                        itemProductModel.setImage_id(mDataObject_ProductDetails.getString("image_id"));

                    if (!mDataObject_ProductDetails.isNull("category_name"))
                        itemProductModel.setCategory_name(mDataObject_ProductDetails.getString("category_name"));

                    if (!mDataObject_ProductDetails.isNull("type_name"))
                        itemProductModel.setType_name(mDataObject_ProductDetails.getString("type_name"));

                    if (!mDataObject_ProductDetails.isNull("size_name"))
                        itemProductModel.setSize_name(mDataObject_ProductDetails.getString("size_name"));

                    if (!mDataObject_ProductDetails.isNull("color_name"))
                        itemProductModel.setColor_name(mDataObject_ProductDetails.getString("color_name"));

                    if (!mDataObject_ProductDetails.isNull("brand_name"))
                        itemProductModel.setBrand_name(mDataObject_ProductDetails.getString("brand_name"));

                    if (!mDataObject_ProductDetails.isNull("condition_name"))
                        itemProductModel.setCondition_name(mDataObject_ProductDetails.getString("condition_name"));

                    if (!mDataObject_ProductDetails.isNull("ocasion_name"))
                        itemProductModel.setOcasion_name(mDataObject_ProductDetails.getString("ocasion_name"));

                    if (!mDataObject_ProductDetails.isNull("description"))
                        itemProductModel.setDescription(mDataObject_ProductDetails.getString("description"));

                    if (!mDataObject_ProductDetails.isNull("date"))
                        itemProductModel.setDate(mDataObject_ProductDetails.getString("date"));

                    if (!mDataObject_ProductDetails.isNull("retail_price"))
                        itemProductModel.setRetail_price(mDataObject_ProductDetails.getString("retail_price"));

                    if (!mDataObject_ProductDetails.isNull("replacement_value"))
                        itemProductModel.setReplacement_value(mDataObject_ProductDetails.getString("replacement_value"));

                    if (!mDataObject_ProductDetails.isNull("week_4days"))
                        itemProductModel.setWeek_4days(mDataObject_ProductDetails.getString("week_4days"));

                    if (!mDataObject_ProductDetails.isNull("week_8days"))
                        itemProductModel.setWeek_8days(mDataObject_ProductDetails.getString("week_8days"));

                    if (!mDataObject_ProductDetails.isNull("week_12days"))
                        itemProductModel.setWeek_12days(mDataObject_ProductDetails.getString("week_12days"));

                    if (!mDataObject_ProductDetails.isNull("instant_booking"))
                        itemProductModel.setInstant_booking(mDataObject_ProductDetails.getString("instant_booking"));

                    if (!mDataObject_ProductDetails.isNull("open_for_sale"))
                        itemProductModel.setOpen_for_sale(mDataObject_ProductDetails.getString("open_for_sale"));

                    if (!mDataObject_ProductDetails.isNull("cleaning_free"))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));

                    if (!mDataObject_ProductDetails.isNull("drop_person"))
                        itemProductModel.setDrop_person(mDataObject_ProductDetails.getString("drop_person"));

                    if (!mDataObject_ProductDetails.isNull("delivery_free"))
                        itemProductModel.setDelivery_free(mDataObject_ProductDetails.getString("delivery_free"));

                    if (!mDataObject_ProductDetails.isNull("image1") && mDataObject_ProductDetails.getString("image1").contains("http")) {
                        itemProductModel.setImage1(mDataObject_ProductDetails.getString("image1"));

                        if (itemProductModel.getImage1().contains("http")) {
                            Glide.with(mActivity).load(itemProductModel.getImage1())
                                    .placeholder(R.drawable.ic_pp_ph)
                                    .error(R.drawable.ic_pp_ph)
                                    .into(iv_cart_profile);
                        }

//                        if (itemProductModel.getImage1().contains("https")) {
//                            Glide.with(mActivity).load(itemProductModel.getImage1())
//                                    .placeholder(R.drawable.ic_pp_ph)
//                                    .error(R.drawable.ic_pp_ph)
//                                    .into(iv_cart_profile);
//                        } else {
//                            Glide.with(mActivity).load(itemProductModel.getImage1().replace("http://", "https://"))
//                                    .placeholder(R.drawable.ic_pp_ph)
//                                    .error(R.drawable.ic_pp_ph)
//                                    .into(iv_cart_profile);
//                        }
                    }

                    if (!mDataObject_ProductDetails.isNull("image2"))
                        itemProductModel.setImage2(mDataObject_ProductDetails.getString("image2"));

                    if (!mDataObject_ProductDetails.isNull("image3"))
                        itemProductModel.setImage3(mDataObject_ProductDetails.getString("image3"));

                    if (!mDataObject_ProductDetails.isNull("image4"))
                        itemProductModel.setImage4(mDataObject_ProductDetails.getString("image4"));

                    if (!mDataObject_ProductDetails.isNull("image5"))
                        itemProductModel.setImage5(mDataObject_ProductDetails.getString("image5"));

                    if (!mDataObject_ProductDetails.isNull("rating"))
                        itemProductModel.setRatings(mDataObject_ProductDetails.getString("rating"));

                    JSONObject request_detail = mDataObject.getJSONObject("request_detail");

                    if (!request_detail.isNull("booking_dates") && !request_detail.getString("booking_dates").equals("")) {
                        itemProductModel.setBooking_dates(request_detail.getString("booking_dates"));

                        String input = request_detail.getString("booking_dates");

                        JSONArray jsonArray = new JSONArray(input);
                        String[] strArr = new String[jsonArray.length()];

                        for (int i = 0; i < jsonArray.length(); i++) {
                            strArr[i] = jsonArray.getString(i);
                        }

                        first_date = strArr[0];
                        last_date = strArr[strArr.length - 1];

                        tv_date.setText(parseDateToddMMyyyy(first_date) + " – " + parseDateToddMMyyyy(last_date));

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            firstDate = dateFormat.parse(first_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                    if (!mDataObject_UserDetails.isNull("id"))
                        itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                    if (!mDataObject_UserDetails.isNull("first_name"))
                        itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                    if (!mDataObject_UserDetails.isNull("last_name"))
                        itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                    if (!mDataObject_UserDetails.isNull("email"))
                        itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                    if (!mDataObject_UserDetails.isNull("gender"))
                        itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                    if (!mDataObject_UserDetails.isNull("city_name"))
                        itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                    if (!mDataObject_UserDetails.isNull("phone_no"))
                        itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                    if (!mDataObject_UserDetails.isNull("profile_pic"))
                        itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                    if (!mDataObject_UserDetails.isNull("rating"))
                        itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                    if (!mDataObject_UserDetails.isNull("review"))
                        itemProductModel.setReview(mDataObject_UserDetails.getString("review"));

                    setUserDataOnWidgets();

                    mItemProductModel.add(itemProductModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    public static String addDay(String oldDate, int numberOfDays) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(dateFormat.parse(oldDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_YEAR, numberOfDays);
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date newDate = new Date(c.getTimeInMillis());
        String resultDate = dateFormat.format(newDate);
        return resultDate;
    }

    private void setUserDataOnWidgets() {
        if (itemProductModel.getUser_Ratings() != null)
            userRatings.setRating(Float.parseFloat(itemProductModel.getUser_Ratings()));

        if (itemProductModel.getAnother_User_Name() != null)
            txtUserNameTV.setText(itemProductModel.getAnother_User_Name() + " " + itemProductModel.getAnother_User_LastName());

        if (itemProductModel.getAnother_User_CityName() != null)
            txtUserLocationTV.setText(itemProductModel.getAnother_User_CityName());

        if (itemProductModel.getReview().equals("1") || itemProductModel.getReview().equals("0")) {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Review");
        } else {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Reviews");
        }

        if (itemProductModel.getAnother_ProfilePic() != null && itemProductModel.getAnother_ProfilePic().contains("http")) {
            Picasso.with(mActivity).load(itemProductModel.getAnother_ProfilePic())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicIV);
            Log.e(TAG, "**Profileeeeee**" + itemProductModel.getAnother_ProfilePic());
        }

        manageOrderFlow(str_Type, str_Status);
    }

    private void showBooked(Activity mActivity, String strMarked, String Type, String status) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.booking_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView mBookingTV = alertDialog.findViewById(R.id.tv_marked);
        TextView tv_ratings = alertDialog.findViewById(R.id.tv_ratings);

        mBookingTV.setText(strMarked);

        if (user_type.equalsIgnoreCase("Lender")) {

            if (itemProductModel.getRenter_rating_status().equals("true")) {
                mBookingTV.setText("Thank you, your review has been submitted");
                tv_ratings.setText("Ok");
            } else if (itemProductModel.getRenter_rating_status().equals("false")) {
                tv_ratings.setText("Proceed to Review");
            }

        } else if (user_type.equalsIgnoreCase("Renter")) {

            if (itemProductModel.getLender_rating_status().equals("true")) {
                mBookingTV.setText("Thank you, your review has been submitted");
                tv_ratings.setText("Ok");
            } else if (itemProductModel.getLender_rating_status().equals("false")) {
                tv_ratings.setText("Proceed to Review");
            }


        }

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        tv_ratings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (user_type.equalsIgnoreCase("Lender")) {

                    if (itemProductModel.getRenter_rating_status().equals("true")) {
                        alertDialog.dismiss();
                    } else if (itemProductModel.getRenter_rating_status().equals("false")) {
                        startActivity(new Intent(mActivity, ReviewRenterActivity.class).putExtra(ConstantData.MODEL,
                                itemProductModel));
                        alertDialog.dismiss();
                    }

                } else if (user_type.equalsIgnoreCase("Renter")) {

                    if (itemProductModel.getLender_rating_status().equals("true")) {
                        alertDialog.dismiss();
                    } else if (itemProductModel.getLender_rating_status().equals("false")) {
                        startActivity(new Intent(mActivity, LenderReviews.class).putExtra(ConstantData.MODEL, itemProductModel));
                        alertDialog.dismiss();
                    }
                }
            }
        });
        alertDialog.show();
    }

    /*
     *
     * AccidentalDamageCover Alert Dialog
     * */
    public void showAccidentaldamageCover(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.popup_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);
        mDesc.setText(strDesc);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    /*
     *
     * Delivery Popup Dialog
     * */
    private void showdelivery(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.delivery_popup_cart);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);

        String str_delivery = "<font color=#000000>Your chosen delivery option</font>";
        mDesc.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static final Spannable getColoredString(Context context, CharSequence text, int color) {
        Spannable spannable = new SpannableString(text);
        spannable.setSpan(new ForegroundColorSpan(color), 0, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    /*Show popup of fees*/
    private void showFees(Activity mActivity, String strHeading, String strRental, String strCleaning, String strService, String strDelivery) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.fees_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView mRentalTV = alertDialog.findViewById(R.id.tv_rental);
        TextView mCleaningTV = alertDialog.findViewById(R.id.tv_cleaning);
        TextView mService = alertDialog.findViewById(R.id.tv_service);
        TextView mDeliveryFeeTV = alertDialog.findViewById(R.id.tv_delivery_fee);

        mHeading.setText(strHeading);

        String str_rental = "";
        if (str_Type.equalsIgnoreCase("rent")) {
            str_rental = "<b><font color=#b7006d>Rental Fee:</font></b>" + "<font color=#000000> the cost of renting the item for the selected rental period.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            str_rental = "<b><font color=#b7006d>Purchase Price:</font></b>" + "<font color=#000000> the cost of buying your selected item.</font>";
        }

        String str_cleaning = "";

        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            mCleaningTV.setVisibility(View.VISIBLE);
            str_cleaning = "<b><font color=#b7006d>Cleaning Fee:</font></b>" + "<font color=#000000> the cost of dry cleaning determined by the lender.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            mCleaningTV.setVisibility(View.GONE);
        }

        String str_service = "<b><font color=#b7006d>Service Fee:</font></b>" + "<font color=#000000> covers our bank fees, the smooth running of the Attires4Hire platform and other services.</font>";
        String str_delivery = "<b><font color=#b7006d>Delivery Fee:</font></b>" + "<font color=#000000> occurs when you select delivery by post.</font>";

        mRentalTV.setText(Html.fromHtml(str_rental));
        mCleaningTV.setText(Html.fromHtml(str_cleaning));
        mService.setText(Html.fromHtml(str_service));
        mDeliveryFeeTV.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /* method to show cancel button according dates */
    private void HidecancelButton() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = dateFormat.parse(first_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        Date currentDatee = null;
        try {
            currentDatee = currentDate.parse(thisDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (str_Status.equals("1")) {
            if (d.before(currentDatee)) {
                tv_cancel.setVisibility(View.GONE);
            } else {
                if (d.equals(currentDatee)) {
                    tv_cancel.setVisibility(View.GONE);
                } else {
                    if (itemProductModel.getExtension().equals("1")) {
                        tv_cancel.setVisibility(View.GONE);
                    } else {
                        if (itemProductModel.getReport_claim().equals("1")) {
                            tv_cancel.setVisibility(View.GONE);
                        } else {
                            tv_cancel.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        } else {
            tv_cancel.setVisibility(View.GONE);
        }
    }

    /* manage status according to buy/rent and renter/lender*/
    private void manageOrderFlow(String Type, String status) {
        if (str_Type.equalsIgnoreCase("rent")) {
            //hide cancel button after 72 hours
            HidecancelButton();
        } else if (str_Type.equalsIgnoreCase("buy")) {
            tv_cancel.setVisibility(View.GONE);
        }

        bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
        shippedIv.setImageDrawable(getDrawable(R.drawable.ic_clean));
        receivedIv.setImageDrawable(getDrawable(R.drawable.ic_clean));
        returnedIv.setImageDrawable(getDrawable(R.drawable.ic_clean));
        completedIv.setImageDrawable(getDrawable(R.drawable.ic_clean));

        bookedIv.setEnabled(false);
        shippedIv.setEnabled(false);
        receivedIv.setEnabled(false);
        returnedIv.setEnabled(false);
        completedIv.setEnabled(false);

        tv_request_extension.setVisibility(View.GONE);

        if (Type.equalsIgnoreCase("rent")) {

            bookedTV.setText("Booked");
            returnedLL.setVisibility(View.VISIBLE);
            returnedTV.setVisibility(View.VISIBLE);
            returneddateTv.setVisibility(View.VISIBLE);
            returnedIv.setVisibility(View.VISIBLE);
            view3.setVisibility(View.VISIBLE);
            spaceView.setVisibility(View.VISIBLE);

            /* show request a refund button */
            if (user_type.equals("Lender")) {
//                if (status.equals("4") || status.equals("5")) {
//                    txtReportTV.setVisibility(View.VISIBLE);
//                }
                if (status.equals("5")) {
                    txtReportTV.setVisibility(View.VISIBLE);
                }
            } else {
                if (firstDate.equals(currentDate) || firstDate.before(currentDate)) {
                    txtReportTV.setVisibility(View.VISIBLE);
                } else {
                    if (status.equals("4") || status.equals("5")) {
                        txtReportTV.setVisibility(View.VISIBLE);
                    }
//                    if (status.equals("5")) {
//                        txtReportTV.setVisibility(View.VISIBLE);
//                    }
                    else {
                        txtReportTV.setVisibility(View.GONE);
                    }
                }

                /* show extension button after product is received */
                if (status.equals("4")) {
                    if (report_status.equals("1")) {
                        tv_request_extension.setVisibility(View.GONE);
                    } else {
                        tv_request_extension.setVisibility(View.VISIBLE);
                    }
                }
            }

            if (status.equals("1")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                if (user_type.equals("Lender")) {
                    shippedIv.setEnabled(true);
                } else {
                    shippedIv.setEnabled(false);
                }

            } else if (status.equals("3")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                if (user_type.equals("Lender")) {
                    receivedIv.setEnabled(false);
                } else {
                    receivedIv.setEnabled(true);
                }

            } else if (status.equals("4")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                receivedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                /* set received date */
                if (!itemProductModel.getDelivery_date().equals(""))
                    receiveddateTv.setText(itemProductModel.getDelivery_date());

                if (user_type.equals("Lender")) {
                    returnedIv.setEnabled(false);
                    completedIv.setEnabled(true);
                } else {
                    returnedIv.setEnabled(true);
                    completedIv.setEnabled(false);
                }

            } else if (status.equals("5")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                receivedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                returnedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                /* set received date */
                if (!itemProductModel.getDelivery_date().equals(""))
                    receiveddateTv.setText(itemProductModel.getDelivery_date());

                /* set returned date */
                if (!itemProductModel.getReturn_date().equals("")) {
                    returneddateTv.setText(itemProductModel.getReturn_date());
                } else {
                    returneddateTv.setText("");
                }

                if (user_type.equals("Lender")) {
                    completedIv.setEnabled(true);
                } else {
                    completedIv.setEnabled(false);
                    if (itemProductModel.getLender_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                }

            } else if (status.equals("6")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                receivedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                returnedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                completedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                /* set received date */
                if (!itemProductModel.getDelivery_date().equals(""))
                    receiveddateTv.setText(itemProductModel.getDelivery_date());

                /* set returned date */
                if (!itemProductModel.getReturn_date().equals("")) {
                    returneddateTv.setText(itemProductModel.getReturn_date());
                } else {
                    returneddateTv.setText("");
                }

                /* set completed date */
                if (!itemProductModel.getCompleted_date().equals(""))
                    completeddateTv.setText(itemProductModel.getCompleted_date());

                if (user_type.equals("Lender")) {
                    if (itemProductModel.getRenter_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                } else {
                    if (itemProductModel.getLender_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                }

                /* id order is completed then hide request a refund button*/
                txtReportTV.setVisibility(View.GONE);

            } else if (status.equals("7")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_close));
                bookedTV.setText(getResources().getString(R.string.cancelled));

                /* id order is cancelled then hide request a refund button*/
                txtReportTV.setVisibility(View.GONE);
            }

        } else {
            bookedTV.setText("Bought");
            returnedLL.setVisibility(View.GONE);
            returnedTV.setVisibility(View.GONE);
            returneddateTv.setVisibility(View.GONE);
            returnedIv.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            spaceView.setVisibility(View.GONE);

            /* show request a refund button */
            if (user_type.equals("Lender")) {
                txtReportTV.setVisibility(View.GONE);
            } else {
                txtReportTV.setVisibility(View.VISIBLE);
            }

            if (status.equals("2")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                if (user_type.equals("Lender")) {
                    shippedIv.setEnabled(true);
                } else {
                    shippedIv.setEnabled(false);
                }

            } else if (status.equals("3")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                if (user_type.equals("Lender")) {
                    receivedIv.setEnabled(false);
                } else {
                    receivedIv.setEnabled(true);
                }

            } else if (status.equals("4")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                receivedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                /* set received date */
                if (!itemProductModel.getDelivery_date().equals(""))
                    receiveddateTv.setText(itemProductModel.getDelivery_date());

                if (user_type.equals("Lender")) {
                    completedIv.setEnabled(true);
                } else {
                    completedIv.setEnabled(false);
                    if (itemProductModel.getLender_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                }

            } else if (status.equals("6")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                shippedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                receivedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));
                completedIv.setImageDrawable(getDrawable(R.drawable.ic_tick_order));

                /* set shipped date */
                if (!itemProductModel.getDispatch_date().equals(""))
                    shippeddateTv.setText(itemProductModel.getDispatch_date());

                /* set received date */
                if (!itemProductModel.getDelivery_date().equals(""))
                    receiveddateTv.setText(itemProductModel.getDelivery_date());

                /* set completed date */
                if (!itemProductModel.getCompleted_date().equals(""))
                    completeddateTv.setText(itemProductModel.getCompleted_date());

                if (user_type.equals("Lender")) {
                    if (itemProductModel.getRenter_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                } else {
                    if (itemProductModel.getLender_rating_status().equals("false")) {
                        showRatingsStatus(Type, status);
                    }
                }

                /* id order is completed then hide request a refund button*/
                txtReportTV.setVisibility(View.GONE);

            } else if (status.equals("7")) {
                bookedIv.setImageDrawable(getDrawable(R.drawable.ic_close));
                bookedTV.setText(getResources().getString(R.string.cancelled));

                /* id order is cancelled then hide request a refund button*/
                txtReportTV.setVisibility(View.GONE);
            }
        }

        extensionCheckingMethod();
    }

    /* method for managing rewiews */
    private void showRatingsStatus(String Type, String status) {

        if (user_type.equals("Lender")) {
            if (status.equals("6")) {
                if (Type.equalsIgnoreCase("rent")) {
                    showBooked(mActivity, "Rental is completed.", Type, status);
                } else {
                    showBooked(mActivity, "Order is completed.", Type, status);
                }
            }
        } else {
            if (Type.equalsIgnoreCase("rent")) {
                if (status.equals("5")) {
                    showBooked(mActivity, getResources().getString(R.string.thank_you_for_returning_the_item), Type, status);
                } else if (status.equals("6")) {
                    showBooked(mActivity, "Rental is completed.", Type, status);
                }
            } else {
                if (status.equals("4")) {
                    showBooked(mActivity, "Order is Received.", Type, status);
                } else if (status.equals("6")) {
                    showBooked(mActivity, "Order is completed.", Type, status);
                }
            }
        }
    }

    /* method for checking extension if already done or not */
    private void extensionCheckingMethod() {
        if (itemProductModel.getExtension().equals("1")) {
            transaction_id2.setVisibility(View.VISIBLE);
            tv_cancel.setVisibility(View.GONE);
            txtReportTV.setVisibility(View.GONE);
            tv_request_extension.setVisibility(View.GONE);
        } else {
            transaction_id2.setVisibility(View.GONE);
        }
    }

    /* API to check extension */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        mMap.put("request_id", requestId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCheckExtensionAPI() {
        showProgressDialog(mActivity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.CheckExtension(mParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {

                            if (!jsonObjectMM.isNull("dates"))
                                itemProductModel.setBooking_dates(jsonObjectMM.getString("dates"));

                            Intent intent = new Intent(mActivity, ItemDetailsExtensionActivity.class);
                            intent.putExtra(ConstantData.MODEL, itemProductModel);
                            mActivity.startActivity(intent);
                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}
