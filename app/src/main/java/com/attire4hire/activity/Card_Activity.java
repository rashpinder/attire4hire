package com.attire4hire.activity;

import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.utils.ConstantData;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.Year;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Card_Activity extends BaseActivity {
    Activity mActivity = Card_Activity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = Card_Activity.this.getClass().getSimpleName();

    /*Widgets*/

    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    @BindView(R.id.cardNumberET)
    EdittextRegular cardNumberET;

    @BindView(R.id.expiayDateET)
    EdittextRegular expiayDateET;

    @BindView(R.id.expiraryYearET)
    EdittextRegular expiraryYearET;

    @BindView(R.id.cvcET)
    EdittextRegular cvcET;

    @BindView(R.id.bt_verify)
    TextviewSemiBold bt_verify;

    Card card;
    String cardNumber = "", cvc = "";
    String expirayYear = "";
    String expiraydate = "";
    Integer expMonth,expYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_);

        //Set Butter Knife
        ButterKnife.bind(this);

        cardNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Remove spacing char
                String working = s.toString();
                if (working.length() == 4 && before == 0) {
                    working += " ";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
                if (working.length() == 9 && before == 0) {
                    working += " ";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
                if (working.length() == 14 && before == 0) {
                    working += " ";
                    cardNumberET.setText(working);
                    cardNumberET.setSelection(working.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.cancelRL, R.id.bt_verify})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.bt_verify:
                GetCardData();
                break;
        }
    }

    private void GetCardData() {

        if (isValidate()) {

            if (isNetworkAvailable(mActivity)) {
                saveCard();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }

        }
    }

    private void saveCard() {
        cardNumber = cardNumberET.getText().toString().trim();
        expiraydate = expiayDateET.getText().toString().trim();
        expirayYear = expiraryYearET.getText().toString().trim();
        cvc = cvcET.getText().toString().trim();

        card = new Card(cardNumber, Integer.valueOf(expiraydate), Integer.valueOf(expirayYear), cvc);
        // Do not continue token creation.
        if (!card.validateCVC()) {
            showAlertDialog(mActivity, getString(R.string.cvc_invalid));
        }else if (!card.validateExpMonth()) {
            showAlertDialog(mActivity, getString(R.string.expiration_month_invalid));
        }else if (!card.validateExpiryDate()) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid_details));
        }
        else if (!card.validateExpYear()) {
            showAlertDialog(mActivity, getString(R.string.expiration_year_invalid));
        }else if (!card.validateNumber()) {

            showAlertDialog(mActivity, getString(R.string.The_card_invalid));
        }else if (!card.validateCard()) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid_details));
        }
        else {
            CreateToken(card);
        }
    }

    /*
     * To Get Token
     **/

    private void CreateToken(Card card) {
        showProgressDialog(mActivity);
        Stripe stripe = null;
        try {
            stripe = new Stripe(ConstantData.publishable_key);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {

                        // Send token to your server
                        Log.e("Stripe Token", token.getId());

                        executeAddCardApi(token.getId());
                    }

                    public void onError(Exception error) {

                        // Show localized error message

                        dismissProgressDialog();
//                        showAlertDialog(mActivity, error.getLocalizedMessage());
                        showAlertDialog(mActivity, getString(R.string.The_card_invalid_details));
//                        Toast.makeText(getApplicationContext(),
//                                error.getLocalizedMessage(),
//                                Toast.LENGTH_LONG
//                        ).show();
                    }
                }
        );
    }

    public boolean isValidate() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);

        if (!expiayDateET.getText().toString().trim().equals("")) {
            expMonth = Integer.valueOf(expiayDateET.getText().toString().trim());
        }

        if (!expiraryYearET.getText().toString().trim().equals("")) {
            expYear = Integer.valueOf(expiraryYearET.getText().toString().trim());
        }

        boolean flag = true;
        if (cardNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number));
            flag = false;
        } else if (expiayDateET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiray_date));
            flag = false;
        } else if (expMonth < 1 || expMonth > 12) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_expiray));
            flag = false;
        } else if (expiraryYearET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiray_year));
            flag = false;
        } else if (expYear < currentYear) {
            showAlertDialog(mActivity, getString(R.string.please_enter_year_valid_expiray));
            flag = false;
        } else if (cvcET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_cvc));
            flag = false;
        } else if (!(cvcET.getText().length() <= 3)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_3_digit_cvc));
            flag = false;
        }
        return flag;
    }

    private void executeAddCardApi(String Token) {
        String mApiUrl = ConstantData.ADD_CARD_FOR_PAYMENT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("token", Token);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("status");
                    String strMessage = response.getString("message");

                    if (strStatus.equals("1")) {
                        dismissProgressDialog();
//                        onBackPressed();
                        showAlertDialogFinish(mActivity, "Your card has been added successfully.");

                    } else if (strStatus.equals("3")) {
                        LogOut();

                    } else {
                        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}