package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.ListingAdapter;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListingActivity extends BaseActivity {
    /*
     * Initialize Activity
     * */
    Activity mActivity = ListingActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ListingActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;
    @BindView(R.id.btnListItemB)
    Button btnListItemB;

    /*
     * Initialize Objects...
     * */

    ListingAdapter mListingAdapter;
    ArrayList<ItemProductModel> mArrayList = new ArrayList<ItemProductModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        //ButterKnife
        ButterKnife.bind(this);
    }

    @OnClick({R.id.cancelRL, R.id.btnListItemB})
    public void onViewClicked(View mView) {
        switch (mView.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.btnListItemB:
                performListItemClick();
                break;
        }
    }

    private void performListItemClick() {
        if (getIdVerification().equals("1") && getEmailVerification().equals("1") && getPhoneVerification().equals("1") && !getStripeAccountID().equals("")) {
            Intent intent = new Intent(mActivity, AddItemActivity.class);
            startActivity(intent);
        } else {
            if (getIdVerification().equals("1") && getEmailVerification().equals("1") && getPhoneVerification().equals("1") && getStripeAccountID().equals("")) {
                showVerificationAlertDialog(mActivity, getString(R.string.verify_stripe));
            } else
                showVerificationAlertDialog(mActivity, getString(R.string.verify_add_item));
        }
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                intent.putExtra(ConstantData.STRIPE, ConstantData.SHOW_STRIPE);
                startActivity(intent);

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /*
     * Execute Get Products Api
     * */

    private void getProductData() {
        if (isNetworkAvailable(mActivity)) {
            mArrayList.clear();
            executeGetProductsApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Get Product Data
        getProductData();
    }

    private void executeGetProductsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_USER_PRODUCT_LIST;
        Log.e(TAG, "**ApiUrl**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**Param**" + params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    String strStatus = response.getString("status");
                    if (strStatus.equals("1")) {
                        parseResponse(response);
                    } else if (strStatus.equals("3")) {
                        LogOut();
                    } else {
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("data")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    mArrayList.add(mModel);

                    setAdapter();
                }
            }

            if (mArrayList.size() > 0) {
                llNoDataFountTV.setVisibility(View.GONE);
                setAdapter();
            } else {
                llNoDataFountTV.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void setAdapter() {
        mRecyclerViewRV.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mListingAdapter = new ListingAdapter(mActivity, mArrayList);
        mRecyclerViewRV.setAdapter(mListingAdapter);
        mListingAdapter.notifyDataSetChanged();
    }
}