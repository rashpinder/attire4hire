package com.attire4hire.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.RenterReviewAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
//import io.sentry.core.protocol.User;

public class RenterReviewProfileActivity extends BaseActivity implements View.OnClickListener, PaginationInquiriesInterface {

    Activity mActivity = RenterReviewProfileActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = RenterReviewProfileActivity.this.getClass().getSimpleName();

    /*Widgets*/

    @BindView(R.id.renter_review_rv)
    RecyclerView renter_review_rv;
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.write_review_tv)
    TextView write_review_tv;

    @BindView(R.id.tv_NoReviews)
    TextviewBold tv_NoReviews;

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    String strLastPage = "FALSE";
    int page_no = 1;
    boolean isSwipeRefresh = false;

    PaginationInquiriesInterface mInterfaceData;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> tempArrayList = new ArrayList<>();
    RenterReviewAdapter mAdapter;
    String User_id = "";
    ItemProductModel itemProductModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renter_review_profile);

        ButterKnife.bind(this);
        setData();

        mInterfaceData = this;

        getIntentData();

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mItemProductModel.clear();
                tempArrayList.clear();
                GetRenterReviews(page_no);
            }
        });
    }

    private void getIntentData() {

        if (getIntent() != null) {

            if ((ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL) != null){
                itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

                //Set Details Data
                User_id = itemProductModel.getAnother_User_Id();
            }else {
                User_id = getUserID();
            }

        } else {
            User_id = getUserID();
        }
        if (isNetworkAvailable(mActivity)) {
            mItemProductModel.clear();
            GetRenterReviews(page_no);
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void setData() {
        cancelRL.setOnClickListener(this);
        write_review_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.write_review_tv:
                performWriteReviewClick();
                break;
        }
    }

    private void performWriteReviewClick() {
        Intent intent = new Intent(mActivity, ReviewRenterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void GetRenterReviews(final int page_no) {
        if (page_no == 1) {

            if (isSwipeRefresh) {

            } else {
                showProgressDialog(mActivity);
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        String mApiUrl = ConstantData.GET_ALL_REVIEWS;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", User_id);
            params.put("type", "renter");
            params.put("page_no", page_no);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setEnabled(false);
                }

                parseJsonResponse(response.toString());

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(String response) {
        tempArrayList.clear();
        dismissProgressDialog();
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                JSONObject mJsonObject = mJsonObjectt.getJSONObject("data");

                JSONArray mJsonArray = mJsonObject.getJSONArray("all_reviews");

                strLastPage = mJsonObject.getString("last_page");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    itemProductModel = new ItemProductModel();

                    if (!mDataObject.isNull("id"))
                        itemProductModel.setRequest_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id"))
                        itemProductModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("reviewed_user"))
                        itemProductModel.setReviewed_user(mDataObject.getString("reviewed_user"));

                    if (!mDataObject.isNull("product_id"))
                        itemProductModel.setProduct_id(mDataObject.getString("product_id"));

                    if (!mDataObject.isNull("overall_exp"))
                        itemProductModel.setOverall_exp(mDataObject.getString("overall_exp"));

                    if (!mDataObject.isNull("communication"))
                        itemProductModel.setCommunication(mDataObject.getString("communication"));

                    if (!mDataObject.isNull("item_as_desc"))
                        itemProductModel.setItem_as_desc(mDataObject.getString("item_as_desc"));

                    if (!mDataObject.isNull("item_arrive_on_time"))
                        itemProductModel.setItem_arrive_on_time(mDataObject.getString("item_arrive_on_time"));

                    if (!mDataObject.isNull("comments"))
                        itemProductModel.setComments(mDataObject.getString("comments"));

                    if (!mDataObject.isNull("message_date"))
                        itemProductModel.setCreation_date(mDataObject.getString("message_date"));

                    if (!mDataObject.getString("product_detail").equals("")) {

                        JSONObject mDataObject1 = mDataObject.getJSONObject("product_detail");

                        if (!mDataObject1.isNull("id"))
                            itemProductModel.setId(mDataObject1.getString("id"));

                        if (!mDataObject1.isNull("name"))
                            itemProductModel.setName(mDataObject1.getString("name"));

                        if (!mDataObject1.isNull("user_id"))
                            itemProductModel.setUser_id(mDataObject1.getString("user_id"));

                        if (!mDataObject1.isNull("image_id"))
                            itemProductModel.setImage_id(mDataObject1.getString("image_id"));

                        if (!mDataObject1.isNull("category_name"))
                            itemProductModel.setCategory_name(mDataObject1.getString("category_name"));

                        if (!mDataObject1.isNull("type_name"))
                            itemProductModel.setType_name(mDataObject1.getString("type_name"));

                        if (!mDataObject1.isNull("size_name"))
                            itemProductModel.setSize_name(mDataObject1.getString("size_name"));

                        if (!mDataObject1.isNull("color_name"))
                            itemProductModel.setColor_name(mDataObject1.getString("color_name"));

                        if (!mDataObject1.isNull("brand_name"))
                            itemProductModel.setBrand_name(mDataObject1.getString("brand_name"));

                        if (!mDataObject1.isNull("condition_name"))
                            itemProductModel.setCondition_name(mDataObject1.getString("condition_name"));

                        if (!mDataObject1.isNull("ocasion_name"))
                            itemProductModel.setOcasion_name(mDataObject1.getString("ocasion_name"));

                        if (!mDataObject1.isNull("description"))
                            itemProductModel.setDescription(mDataObject1.getString("description"));

                        if (!mDataObject1.isNull("date"))
                            itemProductModel.setDate(mDataObject1.getString("date"));

                        if (!mDataObject1.isNull("retail_price"))
                            itemProductModel.setRetail_price(mDataObject1.getString("retail_price"));

                        if (!mDataObject1.isNull("replacement_value"))
                            itemProductModel.setReplacement_value(mDataObject1.getString("replacement_value"));

                        if (!mDataObject1.isNull("week_4days"))
                            itemProductModel.setWeek_4days(mDataObject1.getString("week_4days"));

                        if (!mDataObject1.isNull("week_8days"))
                            itemProductModel.setWeek_8days(mDataObject1.getString("week_8days"));

                        if (!mDataObject1.isNull("week_12days"))
                            itemProductModel.setWeek_12days(mDataObject1.getString("week_12days"));

                        if (!mDataObject1.isNull("instant_booking"))
                            itemProductModel.setInstant_booking(mDataObject1.getString("instant_booking"));

                        if (!mDataObject1.isNull("open_for_sale"))
                            itemProductModel.setOpen_for_sale(mDataObject1.getString("open_for_sale"));

                        if (!mDataObject1.isNull("cleaning_free"))
                            itemProductModel.setCleaning_free(mDataObject1.getString("cleaning_free"));

                        if (!mDataObject1.isNull("drop_person"))
                            itemProductModel.setDrop_person(mDataObject1.getString("drop_person"));

                        if (!mDataObject1.isNull("favourite"))
                            itemProductModel.setFavourite(mDataObject1.getString("favourite"));

                        if (!mDataObject1.isNull("delivery_free"))
                            itemProductModel.setDelivery_free(mDataObject1.getString("delivery_free"));

                        if (!mDataObject1.isNull("image1"))
                            itemProductModel.setImage1(mDataObject1.getString("image1"));

                        if (!mDataObject1.isNull("image2"))
                            itemProductModel.setImage2(mDataObject1.getString("image2"));

                        if (!mDataObject1.isNull("image3"))
                            itemProductModel.setImage3(mDataObject1.getString("image3"));

                        if (!mDataObject1.isNull("image4"))
                            itemProductModel.setImage4(mDataObject1.getString("image4"));

                        if (!mDataObject1.isNull("image5"))
                            itemProductModel.setImage5(mDataObject1.getString("image5"));
                    }

                    if (!mDataObject.getString("product_detail").equals("")) {

                        JSONObject mDataObject1 = mDataObject.getJSONObject("user_detail");

                        if (!mDataObject1.isNull("first_name"))
                            itemProductModel.setAnother_User_Name(mDataObject1.getString("first_name"));

                        if (!mDataObject1.isNull("last_name"))
                            itemProductModel.setAnother_User_LastName(mDataObject1.getString("last_name"));

                    }

                    if (page_no == 1) {
                        mItemProductModel.add(itemProductModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(itemProductModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {
                    /*
                     * Set Adapter
                     * */
                    setReviewsAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }

            } else {
//                showToast(mActivity, mJsonObjectt.getString("message"));
            }

            if (mItemProductModel.size() > 0) {
                tv_NoReviews.setVisibility(View.GONE);
                /*
                 * Set Adapter
                 * */
                setReviewsAdapter();
            } else {
                tv_NoReviews.setVisibility(View.VISIBLE);
                if (User_id.equals(getUserID())){
                    tv_NoReviews.setText(getResources().getString(R.string.no_reviews));
                }else {
                    tv_NoReviews.setText(getResources().getString(R.string.no_reviews_yet));
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setReviewsAdapter() {
        if (renter_review_rv != null) {
            renter_review_rv.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
            mAdapter = new RenterReviewAdapter(mActivity, mItemProductModel, mInterfaceData);
            renter_review_rv.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void mPaginationInquiriesInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            mProgressBar.setVisibility(View.VISIBLE);

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        GetRenterReviews(page_no);
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }
}
