package com.attire4hire.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.adapters.PayNowPenaltyAdapter;
import com.attire4hire.adapters.RequestAdapter;
import com.attire4hire.model.PenaltyList;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayNowActvity extends BaseActivity {
    Activity mActivity = PayNowActvity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = PayNowActvity.this.getClass().getSimpleName();

    @BindView(R.id.chargesTv)
    TextView chargesTv;

    @BindView(R.id.tvPayNow)
    TextView tvPayNow;

    @BindView(R.id.listRV)
    RecyclerView listRV;

    @BindView(R.id.totalPenaltyTV)
    TextView totalPenaltyTV;

    String penalty_amt = "";
    List<PenaltyList.AllOrder> mPenaltyList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_now_actvity1);

        /* ButterKnife */
        ButterKnife.bind(this);

//        if (Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.PENALTY_PAYMENT, "") != null &&
//                !Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.PENALTY_PAYMENT, "").equals("")) {
//
//            penalty_amt = Attire4hirePrefrences.readString(mActivity, Attire4hirePrefrences.PENALTY_PAYMENT, "");
//            chargesTv.setText(penalty_amt);
//        }

        executeUpdateExtPaymentAPI();
    }

    @OnClick({R.id.tvPayNow, R.id.cancelRL})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvPayNow:
                performPayNowClick();
                break;

            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

    private void performPayNowClick() {
        Intent intent = new Intent(mActivity, PaymentActivity.class);
        Attire4hirePrefrences.writeString(mActivity, ConstantData.PAYMENT_VALUE, "Cart");
        intent.putExtra(ConstantData.PAYMENT_VALUE, "Penalty");
        startActivity(intent);
//        finish();
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        listRV.setLayoutManager(linearLayoutManager);
        PayNowPenaltyAdapter mAdapter = new PayNowPenaltyAdapter(mActivity, mPenaltyList);
        listRV.setAdapter(mAdapter);
    }

    private void executeUpdateExtPaymentAPI() {
        showProgressDialog(mActivity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.getPenaltyList(getUserID()).enqueue(new Callback<PenaltyList>() {
            @Override
            public void onResponse(Call<PenaltyList> call, Response<PenaltyList> response) {
                dismissProgressDialog();

                if (response.body().getStatus().equals("1")) {
                    mPenaltyList = response.body().getData().getAllOrders();
                    totalPenaltyTV.setText("£" + response.body().getData().getTotalPenalty());
                    setAdapter();
                }
            }

            @Override
            public void onFailure(Call<PenaltyList> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }
}