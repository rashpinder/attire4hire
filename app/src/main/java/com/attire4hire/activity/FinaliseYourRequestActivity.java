package com.attire4hire.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FinaliseYourRequestActivity extends BaseActivity {
    Activity mActivity = FinaliseYourRequestActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = FinaliseYourRequestActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.iv_cart_profile)
    ImageView iv_cart_profile;
    @BindView(R.id.tv_product_name)
    TextviewSemiBold tv_product_name;
    @BindView(R.id.rl_delivery_cart)
    RelativeLayout rl_delivery_cart;
    @BindView(R.id.tv_pickup)
    TextviewRegular tv_pickup;
    @BindView(R.id.Iv_pickup)
    ImageView Iv_pickup;
    @BindView(R.id.tv_postage)
    TextviewRegular tv_postage;
    @BindView(R.id.Iv_postage)
    ImageView Iv_postage;
    @BindView(R.id.rl_acc)
    RelativeLayout rl_acc;
    @BindView(R.id.tv_acc_damage_cover)
    TextviewBold tv_acc_damage_cover;
    @BindView(R.id.Iv_acc_damage_cover)
    ImageView Iv_acc_damage_cover;
    @BindView(R.id.rl_fees_cart)
    RelativeLayout rl_fees_cart;
    @BindView(R.id.tv_fees)
    TextviewBold tv_fees;
    @BindView(R.id.tv_rental_fee)
    TextviewRegular tv_rental_fee;
    @BindView(R.id.tv_clean_fee)
    TextviewRegular tv_clean_fee;
    @BindView(R.id.tv_service)
    TextviewRegular tv_service;
    @BindView(R.id.tv_delivery_fee)
    TextviewRegular tv_delivery_fee;
    @BindView(R.id.txtEuroTV)
    TextviewRegular txtEuroTV;
    @BindView(R.id.tv_acc_damage_cover_fee)
    TextviewRegular tv_acc_damage_cover_fee;
    @BindView(R.id.tv_total)
    TextviewRegular tv_total;
    @BindView(R.id.submit_request)
    TextviewSemiBold submit_request;
    @BindView(R.id.rl_pickup)
    RelativeLayout rl_pickup;
    @BindView(R.id.rl_postage)
    RelativeLayout rl_postage;
    @BindView(R.id.rl_Acc_damage)
    RelativeLayout rl_Acc_damage;
    @BindView(R.id.feeTextView)
    TextView feeTextView;
    @BindView(R.id.tv_type)
    TextView tv_type;
    @BindView(R.id.txtDeliveryyFeeTV)
    TextView txtDeliveryyFeeTV;
    @BindView(R.id.tv_total_top)
    TextView tv_total_top;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.cleaningTV)
    TextView cleaningTV;
    @BindView(R.id.cleaningText)
    TextView cleaningText;
    @BindView(R.id.AccidentalTV)
    TextView AccidentalTV;
    @BindView(R.id.AccidentalText)
    TextView AccidentalText;
    @BindView(R.id.view)
    View view;

    TextviewRegular mRentalTV, mCleaningTV, mService, mDeliveryFeeTV;
    int count = 0, i_Delivery, i_Accidental = 0;
    String Accidental_damage = "0", delivery_type = "", first_date = "", last_date = "";
    float f_service = 0, f_Rental = 0, f_Cleaning = 0, f_Delivery = 0;
    double d_Rental = 0.00, d_Cleaning = 0.00, d_Service = 0.00, d_Delivery = 0.00,
            d_Accidental = 0.00, d_Total = 0.00;
    ItemProductModel itemProductModel;
    ArrayList<ItemProductModel> mItemProductModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalise_your_request);
        //Set Butter Knife
        ButterKnife.bind(this);
        getIntentData();
        performPickupClick();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            first_date = getIntent().getStringExtra(ConstantData.FIRST_DATE);
            last_date = getIntent().getStringExtra(ConstantData.LAST_DATE);

            //Set Details Data
            executeCartApi();
        }
    }

    private void setDataOnWidgets() {

        if (itemProductModel.getRequest_type() != null && !itemProductModel.getRequest_type().equals("")) {
            if (itemProductModel.getRequest_type().equals("rent")) {
                tv_type.setText("Rental");

                if (!first_date.equals("") && !last_date.equals("")) {
                    tv_date.setText(parseDateToddMMyyyy(first_date) + " – " + parseDateToddMMyyyy(last_date));
                }

                rl_Acc_damage.setVisibility(View.VISIBLE);
                tv_clean_fee.setVisibility(View.VISIBLE);
                cleaningText.setVisibility(View.VISIBLE);
                cleaningTV.setVisibility(View.VISIBLE);
                AccidentalTV.setVisibility(View.VISIBLE);
                AccidentalText.setVisibility(View.VISIBLE);
                tv_acc_damage_cover_fee.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);

            } else {
                tv_type.setText("Purchase");

                rl_Acc_damage.setVisibility(View.GONE);
                tv_clean_fee.setVisibility(View.GONE);
                cleaningText.setVisibility(View.GONE);
                cleaningTV.setVisibility(View.GONE);
                AccidentalTV.setVisibility(View.GONE);
                AccidentalText.setVisibility(View.GONE);
                tv_acc_damage_cover_fee.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
            }

            if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                feeTextView.setText("Rental Fee");
            } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                feeTextView.setText("Purchase Price");
            }
        }

        if (itemProductModel.getService_fee() != null && !itemProductModel.getService_fee().equals("")) {
            f_service = Float.parseFloat(itemProductModel.getService_fee());
            d_Service = Double.parseDouble(itemProductModel.getService_fee());
            String s = String.format("%.2f", d_Service);
            tv_service.setText("0.00");
        } else {
            String s = String.format("%.2f", d_Service);
            tv_service.setText("0.00");
        }
        if (itemProductModel.getRental_Prize() != null && !itemProductModel.getRental_Prize().equals("")) {
            f_Rental = Float.parseFloat(itemProductModel.getRental_Prize());
            d_Rental = Double.parseDouble(itemProductModel.getRental_Prize());
            String s = String.format("%.2f", d_Rental);
            tv_rental_fee.setText(s);
        } else {
            String s = String.format("%.2f", d_Rental);
            tv_rental_fee.setText(s);
        }

        if (itemProductModel.getCart_Cleaning_fee() != null && !itemProductModel.getCart_Cleaning_fee().equals("")) {
            f_Cleaning = Float.parseFloat(itemProductModel.getCart_Cleaning_fee());
            d_Cleaning = Double.parseDouble(itemProductModel.getCart_Cleaning_fee());
            String s = String.format("%.2f", d_Cleaning);
            tv_clean_fee.setText(s);
        } else {
            String s = String.format("%.2f", d_Cleaning);
            tv_clean_fee.setText(s);
        }

        if (itemProductModel.getCart_Delivery_fee() != null && !itemProductModel.getCart_Delivery_fee().equals("")) {
            f_Delivery = Float.parseFloat(itemProductModel.getCart_Delivery_fee());
            d_Delivery = Double.parseDouble(itemProductModel.getCart_Delivery_fee());
            if (f_Delivery==-1){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.GONE);
                tv_delivery_fee.setText("0.00");
                d_Delivery=0.00;
            }
            if (f_Delivery==0){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
                tv_delivery_fee.setText("0.00");
                d_Delivery=0.00;
            }
            if(d_Delivery==0.0){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
                tv_delivery_fee.setText("0.00");
                d_Delivery=0.00;
            }
            if (d_Delivery!=0.0 && f_Delivery!=0 && f_Delivery!=-1){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
//                tv_delivery_fee.setText("0.00");
//                d_Delivery=0.00;
                String s = String.format("%.2f", d_Delivery);
                tv_delivery_fee.setText(s);
//                tv_delivery_fee.setText("0.00");
//                d_Delivery=0.00;
            }

//            else{
//                rl_postage.setVisibility(View.VISIBLE);
//            }
            String Service_fee = String.format("%.2f", d_Service);
            if(Service_fee.trim().equals("-0.10")){
                tv_service.setText("0.00");
            }else {
                tv_service.setText(Service_fee );
            }
//commenting  d_Delivery = 0.00; for now, for drop person
            if (itemProductModel.getDrop_person().equals("1")) {
                d_Delivery = 0.00;
                String _s = String.format("%.2f", d_Delivery);
                tv_delivery_fee.setText(_s);
                if (f_Delivery==-1){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.GONE);
                    tv_delivery_fee.setText("0.00");
                    d_Delivery=0.00;
                }
                if (f_Delivery==0){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
                    tv_delivery_fee.setText("0.00");
                    d_Delivery=0.00;
                }
                if(d_Delivery==0.0){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
                    tv_delivery_fee.setText("0.00");
                    d_Delivery=0.00;
                }
                if (d_Delivery!=0.0 && f_Delivery!=0 && f_Delivery!=-1){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
//                    tv_delivery_fee.setText("0.00");
//                    d_Delivery=0.00;
                    String sss = String.format("%.2f", d_Delivery);
                    tv_delivery_fee.setText(sss);
//                    tv_delivery_fee.setText("0.00");
//                    d_Delivery=0.00;
                }
            } else {
                String s = String.format("%.2f", d_Delivery);
                if (f_Delivery==-1){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.GONE);
                    tv_delivery_fee.setText("0.00");
                    d_Delivery=0.00;
                }
                if (f_Delivery==0){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
                    tv_delivery_fee.setText("0.00");
                    d_Delivery=0.00;
                }
                if(d_Delivery==0.0){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
                    tv_delivery_fee.setText("0.00");
//                    txtDeliveryyFeeTV.setText("0.00");
                    d_Delivery=0.00;
                }
                if (d_Delivery!=0.0 && f_Delivery!=0 && f_Delivery!=-1){
                    tv_delivery_fee.setVisibility(View.VISIBLE);
                    txtEuroTV.setVisibility(View.VISIBLE);
                    txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                    rl_postage.setVisibility(View.VISIBLE);
//                    tv_delivery_fee.setText("0.00");
//                    d_Delivery=0.00;
                    String ss = String.format("%.2f", d_Delivery);
                    tv_delivery_fee.setText(ss);
//                    tv_delivery_fee.setText("0.00");
//                    d_Delivery=0.00;
                }
//                tv_delivery_fee.setText(s);
            }

        } else {
            f_Delivery = Float.parseFloat(itemProductModel.getCart_Delivery_fee());
            d_Delivery = Double.parseDouble(itemProductModel.getCart_Delivery_fee());
                tv_delivery_fee.setVisibility(View.GONE);
            txtEuroTV.setVisibility(View.GONE);
                txtDeliveryyFeeTV.setVisibility(View.GONE);
            d_Delivery=0.00;
            String s = String.format("%.2f", d_Delivery);
            tv_delivery_fee.setText(s);
            delivery_type = "1";
            Iv_pickup.setImageResource(R.drawable.ic_check);
            Iv_pickup.setClickable(false);
            rl_postage.setVisibility(View.GONE);
            ((LinearLayout.LayoutParams) rl_pickup.getLayoutParams()).setMargins(0, 0, 0, 0);

            if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.10;
            } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.05;
            }

            String Service_fee = String.format("%.2f", d_Service);
            if(Service_fee.trim().equals("-0.10")){
                tv_service.setText("0.00");
            }else {
                tv_service.setText(Service_fee );
            }
        }


        if (itemProductModel.getDrop_person().equals("1")) {
            rl_pickup.setVisibility(View.VISIBLE);

            String Service_fee = String.format("%.2f", d_Service);
            tv_service.setText(Service_fee);
        } else {
            delivery_type = "0";
            Iv_postage.setImageResource(R.drawable.ic_check);
            rl_pickup.setVisibility(View.GONE);
            Iv_postage.setClickable(false);

            if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.10;
            } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.05;
            }

            String Service_fee = String.format("%.2f", d_Service);
            tv_service.setText(Service_fee);
        }

        if (itemProductModel.getName() != null && !itemProductModel.getName().equals("")) {
            tv_product_name.setText(CapitalizedFirstLetter(itemProductModel.getName()));
        }

        if (itemProductModel.getImage1() != null && !itemProductModel.getImage1().equals("")) {

            if (itemProductModel.getImage1().contains("http")) {
                Glide.with(mActivity).load(itemProductModel.getImage1())
                        .placeholder(R.drawable.ic_pp_ph)
                        .error(R.drawable.ic_pp_ph)
                        .into(iv_cart_profile);
            }

//            if (itemProductModel.getImage1().contains("https")) {
//                Glide.with(mActivity).load(itemProductModel.getImage1())
//                        .placeholder(R.drawable.ic_pp_ph)
//                        .error(R.drawable.ic_pp_ph)
//                        .into(iv_cart_profile);
//            } else {
//                Glide.with(mActivity).load(itemProductModel.getImage1().replace("http://", "https://"))
//                        .placeholder(R.drawable.ic_pp_ph)
//                        .error(R.drawable.ic_pp_ph)
//                        .into(iv_cart_profile);
//            }
        }

        d_Accidental = Double.parseDouble(String.valueOf(i_Accidental));
        String s = String.format("%.2f", d_Accidental);
        tv_acc_damage_cover_fee.setText(s);

        d_Total = d_Rental + d_Cleaning + d_Service + d_Delivery + d_Accidental;

        String Total = String.format("%.2f", d_Total);
        tv_total.setText("£" + Total);
        tv_total_top.setText("Total: " + "£" + Total);
    }

    @OnClick({R.id.cancelRL, R.id.submit_request, R.id.rl_acc, R.id.rl_delivery_cart, R.id.rl_fees_cart,
            R.id.rl_pickup, R.id.rl_postage, R.id.rl_Acc_damage, R.id.iv_cart_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.submit_request:
                performProceedtoPayClick();
                break;

            case R.id.rl_acc:
                showAccidentaldamageCover(mActivity, getResources().getString(R.string.acc_damage), getResources().getString(R.string.desc_acc));
                break;

            case R.id.rl_delivery_cart:
                showdelivery(mActivity, getResources().getString(R.string.deli), getResources().getString(R.string.choose_preferred_delivery_option));
                break;

            case R.id.rl_fees_cart:
                if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                    if (itemProductModel.getCart_Delivery_fee() != null && !itemProductModel.getCart_Delivery_fee().equals("")&& !itemProductModel.getCart_Delivery_fee().equals("0")) {
                        showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_rental), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                    }
                    else{
                        showFeesDelivery(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_rental), getString(R.string.clean), getResources().getString(R.string.service_fee_desc));

                    }
                } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                    if (itemProductModel.getCart_Delivery_fee() != null && !itemProductModel.getCart_Delivery_fee().equals("")&& !itemProductModel.getCart_Delivery_fee().equals("0")) {
                        showFees(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_Item_Buy), getString(R.string.clean), getResources().getString(R.string.service_fee_desc), getResources().getString(R.string.del_fee));
                    }
                    else{
                        showFeesDelivery(mActivity, getResources().getString(R.string.fees), getResources().getString(R.string.desc_rental), getString(R.string.clean), getResources().getString(R.string.service_fee_desc));
                    }
                }
                break;

            case R.id.rl_pickup:
                performPickupClick();
                break;

            case R.id.rl_postage:
                performPostageClick();
                break;

            case R.id.rl_Acc_damage:
                performAccClick();
                break;

            case R.id.iv_cart_profile:
                performCartProfileClick();
                break;
        }
    }

    private void performCartProfileClick() {
        Intent intent = new Intent(mActivity, ItemDetailsActivity.class);
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        intent.putExtra(ConstantData.HIDE_BUTTON, ConstantData.HIDE);
        mActivity.startActivity(intent);
    }

    private void performProceedtoPayClick() {
        if (delivery_type.equals("")) {
            showAlertDialog(mActivity, getString(R.string.choose_preferred_delivery_option));
        } else {
            if (isNetworkAvailable(mActivity)) {
                executeAddOrder();
            } else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void performAccClick() {
        if (count == 0) {
            i_Accidental = 5;
            d_Accidental = 5.00;
            Accidental_damage = "1";
            Iv_acc_damage_cover.setImageResource(R.drawable.ic_check);
            count++;
        } else {
            i_Accidental = 0;
            d_Accidental = 0.00;
            Accidental_damage = "0";
            Iv_acc_damage_cover.setImageResource(R.drawable.ic_uncheck);
            count = 0;
        }

        String Accidental = String.format("%.2f", d_Accidental);
        tv_acc_damage_cover_fee.setText(Accidental);

        d_Total = d_Rental + d_Cleaning + d_Service + d_Delivery + d_Accidental;
        String Total = String.format("%.2f", d_Total);
        tv_total.setText("£" + Total);
        tv_total_top.setText("Total: " + "£" + Total);
    }

    private void performPostageClick() {
        delivery_type = "0";
        Iv_postage.setImageResource(R.drawable.ic_check);
        Iv_pickup.setImageResource(R.drawable.ic_uncheck);

        if (!itemProductModel.getCart_Delivery_fee().equals(null) && !itemProductModel.getCart_Delivery_fee().equals("")){
        i_Delivery = Integer.valueOf(itemProductModel.getCart_Delivery_fee());
        d_Delivery = Double.parseDouble(itemProductModel.getCart_Delivery_fee());
            if (f_Delivery==-1){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.GONE);
                tv_delivery_fee.setText("0.00");
                d_Delivery=0.00;
            }
            if (f_Delivery==0){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
                tv_delivery_fee.setText("0.00");
                d_Delivery=0.00;
            }
            if(d_Delivery==0.0){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
                tv_delivery_fee.setText("0.00");
//                    txtDeliveryyFeeTV.setText("0.00");
                d_Delivery=0.00;
            }
            if (d_Delivery!=0.0 && f_Delivery!=0 && f_Delivery!=-1){
                tv_delivery_fee.setVisibility(View.VISIBLE);
                txtEuroTV.setVisibility(View.VISIBLE);
                txtDeliveryyFeeTV.setVisibility(View.VISIBLE);
                rl_postage.setVisibility(View.VISIBLE);
//                tv_delivery_fee.setText("0.00");
//                d_Delivery=0.00;
                String s = String.format("%.2f", d_Delivery);
                tv_delivery_fee.setText(s);
//                tv_delivery_fee.setText("0.00");
//                d_Delivery=0.00;
            }





//        String s = String.format("%.2f", d_Delivery);
//        tv_delivery_fee.setText(s);
        }
        else{
            tv_delivery_fee.setVisibility(View.GONE);
            txtEuroTV.setVisibility(View.GONE);
            txtDeliveryyFeeTV.setVisibility(View.GONE);
            d_Delivery=0.00;
        }

        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.10;
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.05;
        }

        String Service_fee = String.format("%.2f", d_Service);
        tv_service.setText(Service_fee);

        d_Total = d_Rental + d_Cleaning + d_Service + d_Delivery + d_Accidental;
        String Total = String.format("%.2f", d_Total);
        tv_total.setText("£" + Total);
        tv_total_top.setText("Total: " + "£" + Total);
    }

    private void performPickupClick() {
        delivery_type = "1";
        Iv_pickup.setImageResource(R.drawable.ic_check);
        Iv_postage.setImageResource(R.drawable.ic_uncheck);
        d_Delivery = 0.00;
        if (!itemProductModel.getCart_Delivery_fee().equals(null) && !itemProductModel.getCart_Delivery_fee().equals("")) {
            String s = String.format("%.2f", d_Delivery);
            tv_delivery_fee.setText(s);
        }
        else{
            tv_delivery_fee.setVisibility(View.GONE);
            txtEuroTV.setVisibility(View.GONE);
            txtDeliveryyFeeTV.setVisibility(View.GONE);

        }


        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.10;
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            d_Service = (d_Cleaning + d_Delivery + d_Rental) * 0.05;
        }

        String Service_fee = String.format("%.2f", d_Service);
        tv_service.setText(Service_fee);

        d_Total = d_Rental + d_Cleaning + d_Service + d_Delivery + d_Accidental;
        String Total = String.format("%.2f", d_Total);
        tv_total.setText("£" + Total);
        tv_total_top.setText("Total: " + "£" + Total);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (isNetworkAvailable(mActivity)) {
            executeCancelRequest();
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeCancelRequest() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.CANCEL_REQUEST;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("request_id", itemProductModel.getRequest_id());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                dismissProgressDialog();
                try {

                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response);

                    } else if (response.getString("status").equals("3")) {

                        showAlertDialog(mActivity, response.getString("message"));

                    } else {

                        showAlertDialog(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /*
     *
     * AccidentalDamageCover Alert Dialog
     * */
    public void showAccidentaldamageCover(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.popup_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);
        mDesc.setText(strDesc);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Delivery Popup Dialog
     * */
    private void showdelivery(Activity mActivity, String strHeading, String strDesc) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.delivery_popup_cart);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView btnOk = alertDialog.findViewById(R.id.btnOk);

        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);
        String str_delivery = "<font color=#000000>Choose your preferred delivery option.</font>";
        mDesc.setText(Html.fromHtml(str_delivery));

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*Show popup of fees*/
    private void showFees(Activity mActivity, String strHeading, String strRental, String strCleaning, String strService, String strDelivery) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.fees_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        mRentalTV = alertDialog.findViewById(R.id.tv_rental);
        mCleaningTV = alertDialog.findViewById(R.id.tv_cleaning);
        mService = alertDialog.findViewById(R.id.tv_service);
        mDeliveryFeeTV = alertDialog.findViewById(R.id.tv_delivery_fee);

        mHeading.setText(strHeading);

        String str_rental = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            str_rental = "<b><font color=#b7006d>Rental Fee:</font></b>" + "<font color=#000000> the cost of renting the item for the selected rental period.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            str_rental = "<b><font color=#b7006d>Purchase Price:</font></b>" + "<font color=#000000> the cost of buying your selected item.</font>";
        }

        String str_cleaning = "";

        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            mCleaningTV.setVisibility(View.VISIBLE);
            str_cleaning = "<b><font color=#b7006d>Cleaning Fee:</font></b>" + "<font color=#000000> the cost of dry cleaning determined by the lender.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            mCleaningTV.setVisibility(View.GONE);
        }

        String str_service = "<b><font color=#b7006d>Service Fee:</font></b>" + "<font color=#000000> covers our bank fees, the smooth running of the Attires4Hire platform and other services.</font>";
        String str_delivery = "<b><font color=#b7006d>Delivery Fee:</font></b>" + "<font color=#000000> occurs when you select delivery by post.</font>";

        mRentalTV.setText(Html.fromHtml(str_rental));
        mCleaningTV.setText(Html.fromHtml(str_cleaning));
        mService.setText(Html.fromHtml(str_service));
        mDeliveryFeeTV.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*Show popup of fees*/
    private void showFeesDelivery(Activity mActivity, String strHeading, String strRental, String strCleaning, String strService) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.fees_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        mRentalTV = alertDialog.findViewById(R.id.tv_rental);
        mCleaningTV = alertDialog.findViewById(R.id.tv_cleaning);
        mService = alertDialog.findViewById(R.id.tv_service);
        mDeliveryFeeTV = alertDialog.findViewById(R.id.tv_delivery_fee);

        mHeading.setText(strHeading);

        String str_rental = "";
        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            str_rental = "<b><font color=#b7006d>Rental Fee:</font></b>" + "<font color=#000000> the cost of renting the item for the selected rental period.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            str_rental = "<b><font color=#b7006d>Purchase Price:</font></b>" + "<font color=#000000> the cost of buying your selected item.</font>";
        }

        String str_cleaning = "";

        if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
            mCleaningTV.setVisibility(View.VISIBLE);
            str_cleaning = "<b><font color=#b7006d>Cleaning Fee:</font></b>" + "<font color=#000000> the cost of dry cleaning determined by the lender.</font>";
        } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
            mCleaningTV.setVisibility(View.GONE);
        }

        String str_service = "<b><font color=#b7006d>Service Fee:</font></b>" + "<font color=#000000> covers our bank fees, the smooth running of the Attires4Hire platform and other services.</font>";
        String str_delivery = "<b><font color=#b7006d>Delivery Fee:</font></b>" + "<font color=#000000> occurs when you select delivery by post.</font>";

        mRentalTV.setText(Html.fromHtml(str_rental));
        mCleaningTV.setText(Html.fromHtml(str_cleaning));
        mService.setText(Html.fromHtml(str_service));
        mDeliveryFeeTV.setText(Html.fromHtml(str_delivery));
        mDeliveryFeeTV.setVisibility(View.GONE);


        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void executeCartApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_REQUEST_DETAILS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("request_id", itemProductModel.getRequest_id());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (response.getString("status").equals("1")) {
                try {

                    JSONObject mDataObject = response.getJSONObject("data");

                    if (!mDataObject.isNull("request_id") && !mDataObject.getString("request_id").equals(""))
                        itemProductModel.setRequest_id(mDataObject.getString("request_id"));
                    if (!mDataObject.isNull("delivery_fee") && !mDataObject.getString("delivery_fee").equals(""))
                        itemProductModel.setCart_Delivery_fee(mDataObject.getString("delivery_fee"));
                    if (!mDataObject.isNull("cleaning_fee") && !mDataObject.getString("cleaning_fee").equals(""))
                        itemProductModel.setCart_Cleaning_fee(mDataObject.getString("cleaning_fee"));
                    if (!mDataObject.isNull("rental_fee") && !mDataObject.getString("rental_fee").equals(""))
                        itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));
                    if (!mDataObject.isNull("request_type") && !mDataObject.getString("request_type").equals(""))
                        itemProductModel.setRequest_type(mDataObject.getString("request_type"));

                    if (!mDataObject.isNull("service_fee") && !mDataObject.getString("service_fee").equals(""))
                        itemProductModel.setService_fee(mDataObject.getString("service_fee"));

                    JSONObject mDataObject_ProductDetails = mDataObject.getJSONObject("product_detail");

                    if (!mDataObject_ProductDetails.isNull("id") && !mDataObject_ProductDetails.getString("id").equals(""))
                        itemProductModel.setProduct_id(mDataObject_ProductDetails.getString("id"));
                    if (!mDataObject_ProductDetails.isNull("name") && !mDataObject_ProductDetails.getString("name").equals(""))
                        itemProductModel.setName(mDataObject_ProductDetails.getString("name"));
                    if (!mDataObject_ProductDetails.isNull("user_id") && !mDataObject_ProductDetails.getString("user_id").equals(""))
                        itemProductModel.setUser_id(mDataObject_ProductDetails.getString("user_id"));
                    if (!mDataObject_ProductDetails.isNull("image_id") && !mDataObject_ProductDetails.getString("image_id").equals(""))
                        itemProductModel.setImage_id(mDataObject_ProductDetails.getString("image_id"));
                    if (!mDataObject_ProductDetails.isNull("category_name") && !mDataObject_ProductDetails.getString("category_name").equals(""))
                        itemProductModel.setCategory_name(mDataObject_ProductDetails.getString("category_name"));
                    if (!mDataObject_ProductDetails.isNull("type_name") && !mDataObject_ProductDetails.getString("type_name").equals(""))
                        itemProductModel.setType_name(mDataObject_ProductDetails.getString("type_name"));
                    if (!mDataObject_ProductDetails.isNull("size_name") && !mDataObject_ProductDetails.getString("size_name").equals(""))
                        itemProductModel.setSize_name(mDataObject_ProductDetails.getString("size_name"));

                    if (!mDataObject_ProductDetails.isNull("color_name") && !mDataObject_ProductDetails.getString("color_name").equals(""))
                        itemProductModel.setColor_name(mDataObject_ProductDetails.getString("color_name"));
                    if (!mDataObject_ProductDetails.isNull("brand_name") && !mDataObject_ProductDetails.getString("brand_name").equals(""))
                        itemProductModel.setBrand_name(mDataObject_ProductDetails.getString("brand_name"));
                    if (!mDataObject_ProductDetails.isNull("condition_name") && !mDataObject_ProductDetails.getString("condition_name").equals(""))
                        itemProductModel.setCondition_name(mDataObject_ProductDetails.getString("condition_name"));
                    if (!mDataObject_ProductDetails.isNull("ocasion_name") && !mDataObject_ProductDetails.getString("ocasion_name").equals(""))
                        itemProductModel.setOcasion_name(mDataObject_ProductDetails.getString("ocasion_name"));
                    if (!mDataObject_ProductDetails.isNull("description") && !mDataObject_ProductDetails.getString("description").equals(""))
                        itemProductModel.setDescription(mDataObject_ProductDetails.getString("description"));
                    if (!mDataObject_ProductDetails.isNull("date") && !mDataObject_ProductDetails.getString("date").equals(""))
                        itemProductModel.setDate(mDataObject_ProductDetails.getString("date"));
                    if (!mDataObject_ProductDetails.isNull("retail_price") && !mDataObject_ProductDetails.getString("retail_price").equals(""))
                        itemProductModel.setRetail_price(mDataObject_ProductDetails.getString("retail_price"));
                    if (!mDataObject_ProductDetails.isNull("replacement_value") && !mDataObject_ProductDetails.getString("replacement_value").equals(""))
                        itemProductModel.setReplacement_value(mDataObject_ProductDetails.getString("replacement_value"));
                    if (!mDataObject_ProductDetails.isNull("week_4days") && !mDataObject_ProductDetails.getString("week_4days").equals(""))
                        itemProductModel.setWeek_4days(mDataObject_ProductDetails.getString("week_4days"));
                    if (!mDataObject_ProductDetails.isNull("week_8days") && !mDataObject_ProductDetails.getString("week_8days").equals(""))
                        itemProductModel.setWeek_8days(mDataObject_ProductDetails.getString("week_8days"));
                    if (!mDataObject_ProductDetails.isNull("week_12days") && !mDataObject_ProductDetails.getString("week_12days").equals(""))
                        itemProductModel.setWeek_12days(mDataObject_ProductDetails.getString("week_12days"));
                    if (!mDataObject_ProductDetails.isNull("instant_booking") && !mDataObject_ProductDetails.getString("instant_booking").equals(""))
                        itemProductModel.setInstant_booking(mDataObject_ProductDetails.getString("instant_booking"));
                    if (!mDataObject_ProductDetails.isNull("open_for_sale") && !mDataObject_ProductDetails.getString("open_for_sale").equals(""))
                        itemProductModel.setOpen_for_sale(mDataObject_ProductDetails.getString("open_for_sale"));
                    if (!mDataObject_ProductDetails.isNull("cleaning_free") && !mDataObject_ProductDetails.getString("cleaning_free").equals(""))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));
                    if (!mDataObject_ProductDetails.isNull("drop_person") && !mDataObject_ProductDetails.getString("drop_person").equals(""))
                        itemProductModel.setDrop_person(mDataObject_ProductDetails.getString("drop_person"));
                    if (!mDataObject_ProductDetails.isNull("cleaning_free") && !mDataObject_ProductDetails.getString("cleaning_free").equals(""))
                        itemProductModel.setCleaning_free(mDataObject_ProductDetails.getString("cleaning_free"));
                    if (!mDataObject_ProductDetails.isNull("delivery_free") && !mDataObject_ProductDetails.getString("delivery_free").equals(""))
                        itemProductModel.setDelivery_free(mDataObject_ProductDetails.getString("delivery_free"));
                    if (!mDataObject_ProductDetails.isNull("image1") && !mDataObject_ProductDetails.getString("image1").equals(""))
                        itemProductModel.setImage1(mDataObject_ProductDetails.getString("image1"));
                    if (!mDataObject_ProductDetails.isNull("image2") && !mDataObject_ProductDetails.getString("image2").equals(""))
                        itemProductModel.setImage2(mDataObject_ProductDetails.getString("image2"));
                    if (!mDataObject_ProductDetails.isNull("image3") && !mDataObject_ProductDetails.getString("image3").equals(""))
                        itemProductModel.setImage3(mDataObject_ProductDetails.getString("image3"));
                    if (!mDataObject_ProductDetails.isNull("image4") && !mDataObject_ProductDetails.getString("image4").equals(""))
                        itemProductModel.setImage4(mDataObject_ProductDetails.getString("image4"));
                    if (!mDataObject_ProductDetails.isNull("image5") && !mDataObject_ProductDetails.getString("image5").equals(""))
                        itemProductModel.setImage5(mDataObject_ProductDetails.getString("image5"));
                    if (!mDataObject_ProductDetails.isNull("rating") && !mDataObject_ProductDetails.getString("rating").equals(""))
                        itemProductModel.setRatings(mDataObject_ProductDetails.getString("rating"));

                    mItemProductModelList.add(itemProductModel);

                    setDataOnWidgets();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (response.getString("status").equals("3")) {
                LogOut();

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void executeAddOrder() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.ADD_ORDER;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("request_id", itemProductModel.getRequest_id());
            params.put("accidental_damage", Accidental_damage);
            params.put("rental_fee", itemProductModel.getRental_Prize());
            params.put("cleaning_fee", itemProductModel.getCart_Cleaning_fee());
            params.put("service_fee", String.valueOf(d_Service));
            if (delivery_type.equals("1")) {
                params.put("delivery_fee", "0");
            } else {
                params.put("delivery_fee", itemProductModel.getDelivery_free());
            }
            params.put("damage_fee", i_Accidental);
            params.put("total_price", String.valueOf(d_Total));
            params.put("delivery_type", delivery_type);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseAddOrderResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseAddOrderResponse(JSONObject response) {
        dismissProgressDialog();
        try {

            if (response.getString("status").equals("1")) {
                try {

//                    Order_id = response.getString("id");

                    JSONObject mDataObject = response.getJSONObject("order_detail");

                    if (!mDataObject.isNull("id"))
                        itemProductModel.setOrder_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id"))
                        itemProductModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("request_id"))
                        itemProductModel.setRequest_id(mDataObject.getString("request_id"));

                    if (!mDataObject.isNull("order_number"))
                        itemProductModel.setOrder_number(mDataObject.getString("order_number"));

                    if (!mDataObject.isNull("transaction_number"))
                        itemProductModel.setTransaction_number(mDataObject.getString("transaction_number"));

                    if (!mDataObject.isNull("accidental_damage"))
                        itemProductModel.setAccidental_damage(mDataObject.getString("accidental_damage"));

                    if (!mDataObject.isNull("rental_fee"))
                        itemProductModel.setRental_Prize(mDataObject.getString("rental_fee"));

                    if (!mDataObject.isNull("cleaning_fee"))
                        itemProductModel.setCleaning_free(mDataObject.getString("cleaning_fee"));

                    if (!mDataObject.isNull("service_fee"))
                        itemProductModel.setService_fee(mDataObject.getString("service_fee"));

                    if (!mDataObject.isNull("delivery_fee"))
                        itemProductModel.setDelivery_free(mDataObject.getString("delivery_fee"));

                    if (!mDataObject.isNull("damage_fee"))
                        itemProductModel.setDamage_fee(mDataObject.getString("damage_fee"));

                    if (!mDataObject.isNull("total_price"))
                        itemProductModel.setTotal_price(mDataObject.getString("total_price"));

                    if (mDataObject.has("status")) {
                        if (!mDataObject.isNull("status"))
                            itemProductModel.setStatus(mDataObject.getString("status"));
                    }

                    if (!mDataObject.isNull("allow_payment"))
                        itemProductModel.setAllow_payment(mDataObject.getString("allow_payment"));

                    if (!mDataObject.isNull("delivery_date"))
                        itemProductModel.setDelivery_date(mDataObject.getString("delivery_date"));

                    if (!mDataObject.isNull("dispatch_date"))
                        itemProductModel.setDispatch_date(mDataObject.getString("dispatch_date"));

                    showEnqAlertDialog(mActivity, "Your request has been submitted! You will be notified once approved by the Lender!");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (response.getString("status").equals("4")) {

                showAlertPayNowDialog(mActivity, response.getString("message"));

            } else if (response.getString("status").equals("3")) {

                showAlertDialog(mActivity, response.getString("message"));

            } else if (response.getString("status").equals("2")) {

                showAlertDialogFinish(mActivity, response.getString("message"));

            } else {

                showAlertDialog(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
