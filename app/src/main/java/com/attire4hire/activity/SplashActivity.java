package com.attire4hire.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.attire4hire.R;
import com.attire4hire.utils.ConstantData;
import com.google.firebase.messaging.FirebaseMessaging;
import com.onesignal.OSDeviceState;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.ButterKnife;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class SplashActivity extends BaseActivity {
    Activity mActivity = SplashActivity.this;
    String TAG = SplashActivity.this.getClass().getSimpleName();
    ImageView logoIV;

    boolean isNotification = false;
    String user_id = "", notification_type = "", title = "", order_id = "", request_id = "",
            total_price = "", AccidentDemageValue = "", orderNo = "", room_id = "", receiver_id = "", instant_booking = "";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //butter knife
        ButterKnife.bind(this);

        handleSSLHandshake();
        printKeyHash(mActivity);
        printFirebaseToken();

        logoIV = findViewById(R.id.logoIV);
        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.splash_anim);
        animation.setDuration(1000);
        logoIV.startAnimation(animation);

        OSDeviceState device = OneSignal.getDeviceState();
        String pushToken = device.getPushToken();

        Log.e("TAG", "Token:-----" + pushToken);

//        setNotification();
    }

    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }
    }

    private void setNotification() {
        if (getIntent().getExtras() != null) {

            user_id = String.valueOf(getIntent().getExtras().get("user_id"));
            notification_type = String.valueOf(getIntent().getExtras().get("notification_type"));
            order_id = String.valueOf(getIntent().getExtras().get("order_id"));
            request_id = String.valueOf(getIntent().getExtras().get("request_id"));
            total_price = String.valueOf(getIntent().getExtras().get("total_price"));
            AccidentDemageValue = String.valueOf(getIntent().getExtras().get("AccidentDemageValue"));
            orderNo = String.valueOf(getIntent().getExtras().get("orderNo"));
            room_id = String.valueOf(getIntent().getExtras().get("room_id"));
            receiver_id = String.valueOf(getIntent().getExtras().get("receiver_id"));
            instant_booking = String.valueOf(getIntent().getExtras().get("instant_booking"));

            if (notification_type != null) {
                isNotification = true;
                if (notification_type.equalsIgnoreCase("request")) {
                    if (instant_booking.equals("1")) {
                        intent = new Intent(this, Current_BookingsActivity.class);
                    } else {
                        intent = new Intent(this, MyRequestsActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } else if (notification_type.equalsIgnoreCase("user/Accept/Reject")) {
                    intent = new Intent(this, MyInquiriesActivity.class);
                    startActivity(intent);
                    finish();
                } else if (notification_type.equalsIgnoreCase("cancel")) {
                    intent = new Intent(this, MyRequestsActivity.class);
                    startActivity(intent);
                    finish();
                } else if (notification_type.equalsIgnoreCase("order")) {
                    intent = new Intent(this, OrderConfirmationActivity.class);
                    intent.putExtra(ConstantData.NOTIFICATION, "notification");
                    intent.putExtra("order_id", order_id);
                    startActivity(intent);
                    finish();
                } else if (notification_type.equalsIgnoreCase("chat")) {
                    intent = new Intent(this, ChatActivity.class);
                    intent.putExtra(ConstantData.NOTIFICATION, "notification");
                    intent.putExtra("room_id", room_id);
                    intent.putExtra("receiver_id", receiver_id);
                    startActivity(intent);
                    finish();
                } else if (notification_type.equalsIgnoreCase("stripe_account_status")) {
                    Intent mIntent = new Intent(this, AccountSetUpUpdateActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    setUpSplash();
                }
            }

            if (!isNotification) {
                setUpSplash();
            }

        } else {
            setUpSplash();
        }
    }

    private void setUpSplash() {

        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do Work
                        handler.removeCallbacks(this);
                        Looper.myLooper().quit();

                        if (IsLogin().equals("true")) {
                            onIntentMethod();
                        } else {
                            Intent i = new Intent(SplashActivity.this, SocialMediaLoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                }, 2000);

                Looper.loop();
            }
        };
        thread.start();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (IsLogin().equals("true")) {
//                    onIntentMethod();
//                } else {
//                    Intent i = new Intent(SplashActivity.this, SocialMediaLoginActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//            }
//        }, 2000);
    }

    private void printFirebaseToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    String Device_Token = task.getResult();
                    Log.e(TAG, "**Push Token**" + Device_Token);

                });
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashActivity.this, new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String newToken = instanceIdResult.getToken();
//                Log.e(TAG, "newToken:" + newToken);
//
//            }
//        });
    }

    public void onIntentMethod() {
        Intent intent = new Intent(mActivity, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
                Log.e(TAG, "Key Hash=++++++= " + key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }
        return key;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!isNetworkAvailable(mActivity)) {
            setNotification();
        } else {

            //  Branch.sessionBuilder(this).withCallback(branchReferralInitListener).withData(getIntent() != null ? getIntent().getData() : null).init();
            Branch branch = Branch.getInstance(getApplicationContext());
            // Branch init
            branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(JSONObject linkProperties, BranchError error) {
                    if (error == null) {
                        Log.e("BRANCH SDK", linkProperties.toString());
                        // Retrieve deeplink keys from 'referringParams' and evaluate the values to determine where to route the user
                        // Check '+clicked_branch_link' before deciding whether to use your Branch routing logic
                        String order_id = "", user_id = "", type = "";

                        if (IsLogin().equals("true")) {

                            if (linkProperties != null) {
                                try {
                                    if (linkProperties.get("+clicked_branch_link").equals(true)) {

//                                      Toast.makeText(mActivity, "clicked_branch_link", Toast.LENGTH_SHORT).show();

                                        if (!linkProperties.isNull("type") && !linkProperties.get("type").toString().equals("")) {
                                            type = linkProperties.get("type").toString();
                                        }
                                        if (!linkProperties.isNull("order_id") && !linkProperties.get("order_id").toString().equals("")) {
                                            order_id = linkProperties.get("order_id").toString();
                                        }
                                        if (!linkProperties.isNull("user_id") && !linkProperties.get("user_id").toString().equals("")) {
                                            user_id = linkProperties.get("user_id").toString();
                                        }
                                        if (!user_id.equals("") && user_id.equals(getUserID())) {

                                            if (type.equalsIgnoreCase("order")) {
                                                if (!order_id.equals("")) {
                                                    Intent mIntent = new Intent(mActivity, OrderConfirmationActivity.class);
                                                    mIntent.putExtra(ConstantData.ORDER_ID, order_id);
                                                    mIntent.putExtra(ConstantData.NOTIFICATION, "branch");
                                                    mActivity.startActivity(mIntent);
                                                    finish();
                                                }
                                            } else if (type.equalsIgnoreCase("new_request")) {
                                                Intent mIntent = new Intent(mActivity, MyRequestsActivity.class);
                                                mActivity.startActivity(mIntent);
                                                finish();
                                            } else if (type.equalsIgnoreCase("accept/decline")) {
                                                Intent mIntent = new Intent(mActivity, MyInquiriesActivity.class);
                                                mActivity.startActivity(mIntent);
                                                finish();
                                            } else if (type.equalsIgnoreCase("search")) {
                                                onIntentMethod();
                                            }
                                        } else {
                                            onIntentMethod();
                                        }
                                    } else {
//                                      Toast.makeText(mActivity, "not_clicked_branch_link", Toast.LENGTH_SHORT).show();
                                        setNotification();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            Intent i = new Intent(SplashActivity.this, SocialMediaLoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        // do stuff with deep link data (nav to page, display content, etc)
                        setNotification();
                    }
                }
            }, this.getIntent().getData(), this);
        }
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);
//        // if activity is in foreground (or in backstack but partially visible) launching the same
//        // activity will skip onStart, handle this case with reInitSession
//        Branch.sessionBuilder(this).withCallback(branchReferralInitListener).reInit();
//    }
//
//    private Branch.BranchReferralInitListener branchReferralInitListener = new Branch.BranchReferralInitListener() {
//        @Override
//        public void onInitFinished(JSONObject linkProperties, BranchError error) {
//            // do stuff with deep link data (nav to page, display content, etc)
//
//            String order_id = "", user_id = "", type = "";
//
//            if (IsLogin().equals("true")) {
//
//                if (linkProperties != null) {
//                    try {
//                        if (linkProperties.get("+clicked_branch_link").equals(true)) {
//
////                            Toast.makeText(mActivity, "clicked_branch_link", Toast.LENGTH_SHORT).show();
//
//                            if (!linkProperties.isNull("type") && !linkProperties.get("type").toString().equals("")) {
//                                type = linkProperties.get("type").toString();
//                            }
//                            if (!linkProperties.isNull("order_id") && !linkProperties.get("order_id").toString().equals("")) {
//                                order_id = linkProperties.get("order_id").toString();
//                            }
//                            if (!linkProperties.isNull("user_id") && !linkProperties.get("user_id").toString().equals("")) {
//                                user_id = linkProperties.get("user_id").toString();
//                            }
//                            if (!user_id.equals("") && user_id.equals(getUserID())) {
//
//                                if (type.equalsIgnoreCase("order")) {
//                                    if (!order_id.equals("")) {
//                                        Intent mIntent = new Intent(mActivity, OrderConfirmationActivity.class);
//                                        mIntent.putExtra(ConstantData.ORDER_ID, order_id);
//                                        mIntent.putExtra(ConstantData.NOTIFICATION, "branch");
//                                        mActivity.startActivity(mIntent);
//                                        finish();
//                                    }
//                                } else if (type.equalsIgnoreCase("new_request")) {
//                                    Intent mIntent = new Intent(mActivity, MyRequestsActivity.class);
//                                    mActivity.startActivity(mIntent);
//                                    finish();
//                                } else if (type.equalsIgnoreCase("accept")) {
//                                    Toast.makeText(mActivity, "accept", Toast.LENGTH_SHORT).show();
//                                } else if (type.equalsIgnoreCase("decline")) {
//                                    Toast.makeText(mActivity, "reject", Toast.LENGTH_SHORT).show();
//                                }
//
//                            } else {
//                                onIntentMethod();
//                            }
//                        } else {
////                            Toast.makeText(mActivity, "not_clicked_branch_link", Toast.LENGTH_SHORT).show();
//                            setNotification();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else {
//                Intent i = new Intent(SplashActivity.this, SocialMediaLoginActivity.class);
//                startActivity(i);
//                finish();
//            }
//        }
//    };
}
