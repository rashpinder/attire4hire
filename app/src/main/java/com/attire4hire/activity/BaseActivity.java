package com.attire4hire.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.attire4hire.BuildConfig;
import com.attire4hire.R;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    /*
     * Is User Login Or Not
     * */
    public String IsLogin() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.IS_LOGIN, "");
    }

    /*
     * Get User ID
     * */
    public String getUserID() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.USER_ID, "");
    }

    /*
     * Get User Name
     * */
    public String getUserName() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.FIRST_NAME, "") + " " + Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.LAST_NAME, "");
    }

    /*
     * Get User First Name
     * */
    public String getUserFirstName() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.FIRST_NAME, "");
    }

    /*
     * Get User Last Name
     * */
    public String getUserLastName() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.LAST_NAME, "");
    }

    /*
     * Get User Gender
     * */
    public String getUserGender() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.GENDER, "");
    }

    /*
     * Get User City
     * */
    public String getUserCity() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.CITY, "");
    }

    /*
     * Get User Email
     * */
    public String getUserEmail() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.EMAIL, "");
    }

    /*
     * Get User Phone Number
     * */
    public String getUserPhoneNo() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.PHONE_NUMBER, "");
    }

    /*
     * Get User Profile Pic
     * */
    public String getUserProfilePicture() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.PROFILE_PIC, "");
    }

    /*
     * Get User Current Latitude
     * */
    public String getCurrentLatitude() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.CURRENT_LOCATION_LATITUDE, "");
    }

    /*
     * Get User Current Latitude
     * */
    public String getCurrentLongitude() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }

    /*
     * Get User Email Verification
     * */
    public String getEmailVerification() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.EMAIL_VERIFICATION, "");
    }

    /*
     * Get User Phone Verification
     * */
    public String getPhoneVerification() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.PHONE_VERIFICATION, "");
    }

    /*
     * Get User Id Verification
     * */
    public String getIdVerification() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.ID_VERIFICATION, "");
    }

    /*
     * Get Request Count
     * */
    public String getRequestCount() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.REQUEST_COUNT, "");
    }

    /*
     * Get Inquiry Count
     * */
    public String getInquiryCount() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.INQUIRY_COUNT, "");
    }

    /*
     * Get Current Count
     * */
    public String getCurrentCount() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.CURRENT_BOOKING_COUNT, "");
    }

    /*
     * Get Confirmed Count
     * */
    public String getConfirmedCount() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.MY_BOOKING_COUNT, "");
    }

    /*
     * Get Total Count
     * */
    public String getTotalCount() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.TOTAL_COUNT, "");
    }

    /*
     * Get PayPal Verification
     * */
    public String getPayPalVerification() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.PAYPAL_VERIFICATION, "");
    }

    /*
     * Get Stripe Verification
     * */
    public String getStripeVerification() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.STRIPE_VERIFICATION, "");
    }

    /*
     * Get Stripe account id
     * */
    public String getStripeAccountID() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.STRIPE_ACCOUNT_ID, "");
    }

    /*
     * Get facebook id
     * */
    public String getFacebookID() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.FACEBOOK_ID, "");
    }

    /*
     * Get google id
     * */
    public String getGoogleID() {
        return Attire4hirePrefrences.readString(BaseActivity.this, Attire4hirePrefrences.GOOGLE_ID, "");
    }

    /*
     *Hide/Close Keyboard forcefully
     *
     * */
    public void hideCloseKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    Dialog progressDialog;

    /*
     *  Alert Dialog Finish
     * */
    String TAG = BaseActivity.this.getClass().getSimpleName();

    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showAlertDialogFinish(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

    public void showDialogLogout(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtLogoutTV = alertDialog.findViewById(R.id.txtLogoutTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtLogoutTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                SharedPreferences preferences = Attire4hirePrefrences.getPreferences(Objects.requireNonNull(getApplicationContext()));
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();

                disconnectFromFacebook();

                googleLogOut();

                FirebaseAuth.getInstance().signOut();

                Intent intent = new Intent(getApplicationContext(), SocialMediaLoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        alertDialog.show();
    }

    public void showHomeAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                callHomeActivityIntent(mActivity);
            }
        });
        alertDialog.show();
    }

    /*
     * Disconnect with facebook
     * */
    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    /*
     * Google Logout
     * */
    public void googleLogOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        GoogleSignInClient mgoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mgoogleSignInClient.signOut();
    }

    public void LogOut() {
        SharedPreferences preferences = Attire4hirePrefrences.getPreferences(Objects.requireNonNull(getApplicationContext()));
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

        disconnectFromFacebook();

        googleLogOut();

        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(getApplicationContext(), SocialMediaLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(this, strMessage, Toast.LENGTH_SHORT).show();
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (RuntimeException ex) {

            }
        }
    }


    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     * Get Bitmap From Path
     * */

    public Bitmap getBitmapFromFilePath(String path) {
        Bitmap bitmap;
        try {
            File f = new File(path);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), options);

//            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 20, out);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    /**
     * Returns the Uri which can be used to delete/work with images in the photo gallery.
     *
     * @param filePath Path to IMAGE on SD card
     * @return Uri in the format of... content://media/external/images/media/[NUMBER]
     */
    public Uri getUriFromPath(String filePath) {
        long photoId;
        Uri photoUri = MediaStore.Images.Media.getContentUri("external");

        String[] projection = {MediaStore.Images.ImageColumns._ID};
        // TODO This will break if we have no matching item in the MediaStore.
        Cursor cursor = getContentResolver().query(photoUri, projection, MediaStore.Images.ImageColumns.DATA + " LIKE ?", new String[]{filePath}, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(projection[0]);
        photoId = cursor.getLong(columnIndex);

        cursor.close();
        return Uri.parse(photoUri.toString() + "/" + photoId);
    }

    /*
     * Get thumbnail From uri
     * */
    public Bitmap getThumbnailFromUri(Uri uri) throws IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 500) ? (originalSize / 500) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    /*
     *
     * Show Listing Information Popup
     * */
    public void showListing(Activity mActivity, String strHeading) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.listing_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView mAvailabilityTV = alertDialog.findViewById(R.id.tv_avail);
        TextView mRetailTV = alertDialog.findViewById(R.id.tv_retail);
        TextView mReplacementTV = alertDialog.findViewById(R.id.tv_replacement);
        TextView mRentalFeeTV = alertDialog.findViewById(R.id.tv_rental_f);
        TextView mInstantTV = alertDialog.findViewById(R.id.tv_instant_book);
        TextView mOpenSaleTV = alertDialog.findViewById(R.id.tv_open_sale);
        TextView mCleaningFeeTV = alertDialog.findViewById(R.id.tv_clean_fee);

        mHeading.setText(strHeading);

        String str_avail = "<b><font color=#b7006d>Availability: </font></b>" + "<font color=#000000>Block out the dates when your item is unavailable for renting.</font>";
        String str_retail = "<b><font color=#b7006d>Retail Price: </font></b>" + "<font color=#000000>The amount you have purchased the item for.</font>";
        String str_replace = "<b><font color=#b7006d>Replacement value: </font></b>" + "<font color=#000000>In case the item is damaged beyond repair, got lost or stolen. As a guideline: <br>Brand new / worn once: 80-100% of the retail price<br> Good condition, worn couple of times: 50 -60% of the retail price <br>Limited edition or vintage: 60 – 70% of the retail price</font>";
        String str_rental = "<b><font color=#b7006d>Rental fee: </font></b>" + "<font color=#000000>Our app automatically recommends rental fees based on the retail price you have entered for the three different rental periods of 4, 8 and 12 days. However,  it is up to you how much you wish to rent your items for.</font>";
        String str_instant = "<b><font color=#b7006d>Instant Booking: </font></b>" + "<font color=#000000>If you don't want to approve each request, choose Instant Booking. These rentals will be confirmed straight away based on your calendar. It is your responsibility to keep your calendar up to date.</font>";
        String str_sale = "<b><font color=#b7006d>Open for Sale: </font></b>" + "<font color=#000000>If you consider selling your item, select “Open for Sale” and enter the amount you wish to get for it.</font>";
        String str_cleaning = "<b><font color=#b7006d>Cleaning Fee: </font></b>" + "<font color=#000000>The cost of dry cleaning that will be added to the rental price. Please only select dry cleaning when applicable. Leave this blank for bags, footwear and accessories.</font>";

        mAvailabilityTV.setText(Html.fromHtml(str_avail));
        mRetailTV.setText(Html.fromHtml(str_retail));
        mReplacementTV.setText(Html.fromHtml(str_replace));
        mRentalFeeTV.setText(Html.fromHtml(str_rental));
        mInstantTV.setText(Html.fromHtml(str_instant));
        mOpenSaleTV.setText(Html.fromHtml(str_sale));
        mCleaningFeeTV.setText(Html.fromHtml(str_cleaning));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Show Rental Information Popup
     * */
    public void showRentalPopup(Activity mActivity, String strHeading) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.rental_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView mExCondtitonTV = alertDialog.findViewById(R.id.tv_ex_condition);
        TextView mGoodTV = alertDialog.findViewById(R.id.tv_good);
        TextView mDiffAmt = alertDialog.findViewById(R.id.tv_diff_amt);

        mHeading.setText(strHeading);

        String str_exCondition = "<b><font color=#b7006d>Excellent Condition: </font></b>" + "<font color=#000000><br>4 days: 15% of retail price <br> 8 days: 20% of retail price <br> 12 days: 25% of retail price</font>";
        String str_goodCondition = "<b><font color=#b7006d>Good / Fair Condition: </font></b>" + "<font color=#000000><br>4 days: 10% of retail price <br> 8 days: 15% of retail price <br> 12 days: 20% of retail price</font>";
        String str_diff_amt = "<b><font color=#b7006d>You can put different amount if you want</font></b>";

        mExCondtitonTV.setText(Html.fromHtml(str_exCondition));
        mGoodTV.setText(Html.fromHtml(str_goodCondition));
        mDiffAmt.setText(Html.fromHtml(str_diff_amt));


        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Delivery Popup Dialog
     * */
    public void showDeliveryPopup(Activity mActivity, String strHeading) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.delivery_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView mDesc = alertDialog.findViewById(R.id.tv_desc);

        mHeading.setText(strHeading);

        String str_delivery = "<font color=#000000>Choose your preferred options: drop-off in person, offer free postage or charge for delivery.</font>";
        mDesc.setText(Html.fromHtml(str_delivery));

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Generate String Dynamically
     * */
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    public int getRandomColor() {
        int[] mColorsArray = getResources().getIntArray(R.array.androidcolors);

        return mColorsArray[new Random().nextInt(mColorsArray.length)];
    }


    public void shareAppLink() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            String shareMessage = "Check out this cool app I have found! It allows you to rent, lend, buy and sell luxury attires. Download it now!\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    /*
     *
     * Show Listing Information Popup
     * */
    public void showBlockDates(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_block_dates);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        ImageView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);

        TextView txtTextTV = alertDialog.findViewById(R.id.txtTextTV);

        String str_avail = "<b><font color=#b7006d>Availability: </font></b>" + "<font color=#000000>Please block the dates in your calendar when your item is NOT available.</font>";

        txtTextTV.setText(Html.fromHtml(str_avail));


        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Show Information Dialog
     * */
    public void showInfromationDialog(Activity mActivity, String strHeading, String strDescriptions) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_information);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        ImageView imgDismissIV = alertDialog.findViewById(R.id.imgDismissIV);
        TextView txtHeadingTV = alertDialog.findViewById(R.id.txtHeadingTV);
        TextView txtDescriptionTV = alertDialog.findViewById(R.id.txtDescriptionTV);

        txtHeadingTV.setText(strHeading);
        txtDescriptionTV.setText(strDescriptions);

        imgDismissIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showReplacementInformation(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_replacement_value);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView mHeading = alertDialog.findViewById(R.id.tv_heading);
        ImageView imgDismissIV = alertDialog.findViewById(R.id.imgDismissIV);

        TextView txt1TV = alertDialog.findViewById(R.id.txt1TV);
        TextView txt2TV = alertDialog.findViewById(R.id.txt2TV);
        TextView txt3TV = alertDialog.findViewById(R.id.txt3TV);

        String str1 = "<b><font color=#b7006d>Brand New / Worn Once: </font></b>" + "<font color=#000000>80 – 100% of the retail price</font>";
        String str2 = "<b><font color=#b7006d>Good Condition, Worn Couple of Times: </font></b>" + "<font color=#000000>50 – 60% of the retail price</font>";
        String str3 = "<b><font color=#b7006d>Limited Edition or Vintage: </font></b>" + "<font color=#000000>60 – 70% of the retail price</font>";

        txt1TV.setText(Html.fromHtml(str1));
        txt2TV.setText(Html.fromHtml(str2));
        txt3TV.setText(Html.fromHtml(str3));

        imgDismissIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    //convert Calender Time To Format
    public String convertCalenderTimeToFormat(String input) {//Fri Mar 20 00:00:00 GMT 2020
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("E MMM dd hh:mm:ss z yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
        String goal="";
        if(date!=null) {
            goal = outFormat.format(date);
        }
        return goal;
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtTitleTV = alertDialog.findViewById(R.id.txtTitleTV);

        txtTitleTV.setVisibility(View.GONE);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public Bitmap uriToBitmap(String selectedFile) {
        Bitmap image;
        URL url = null;
        try {
            if (selectedFile.contains("http")) {
                url = new URL(selectedFile);
            }

//            if (selectedFile.contains("https")) {
//                url = new URL(selectedFile);
//            } else {
//                url = new URL(selectedFile.replace("http://", "https://"));
//            }
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            image = BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }

        return image;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    //Capitalized first letter of Full Name
    public String CapitalizedFirstLetter(String myString) {
        String output = myString.substring(0, 1).toUpperCase() + myString.substring(1);
//        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase();

        return output;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd/MM/yyyy";
        String outputPattern = "MMM d";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Nullable
    public ExifInterface getExifInterface(Context context, Uri uri) {
        try {
            String path = uri.toString();
            if (path.startsWith("file://")) {
                return new ExifInterface(path);
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (path.startsWith("content://")) {
                    InputStream inputStream = context.getContentResolver().openInputStream(uri);
                    return new ExifInterface(inputStream);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public float getExifAngle(Context context, Uri uri) {
        try {
            ExifInterface exifInterface = getExifInterface(context, uri);
            if (exifInterface == null) {
                return -1f;
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90f;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180f;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270f;
                case ExifInterface.ORIENTATION_NORMAL:
                    return 0f;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    return -1f;
                default:
                    return -1f;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1f;
        }
    }

    private String imgPath = "";

    public Uri setImageUri() {
        // Store image in dcim
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
        Uri imgUri = Uri.fromFile(file);
        getExifAngle(BaseActivity.this, imgUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imgUri);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 20, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.imgPath = file.getAbsolutePath();
        return imgUri;
    }

    public String getImagePath() {
        return imgPath;
    }

    public static String encodeEmoji(String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    public static String decodeEmoji(String message) {
        String myString = null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    public void callHomeActivityIntent(Activity mActivity) {
        Intent mIntent = new Intent(mActivity, HomeActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mIntent);
    }

    /*
    transparent status bar
     */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
    }

    /*
     * concert bitmap to base64
     * */
    public String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    /* compress image */
    public String compressImage(String absolutePath) {

        String filePath = getRealPathFromURI(absolutePath);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bitmap = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int actualWidth, int actualHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > actualHeight || width > actualWidth) {
            final int heightRatio = Math.round((float) height / (float) actualHeight);
            final int widthRatio = Math.round((float) width / (float) actualWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = actualWidth * actualHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = this.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public void showEnqAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                startActivity(new Intent(mActivity, MyInquiriesActivity.class));
                finish();
            }
        });
        alertDialog.show();
    }

    // ADDS 0  e.g - 02 instead of 2
    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    /* dialog to show penalty payment screen */
    public void showAlertPayNowDialog(Activity mActivity, String message) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(message);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity, PayNowActvity.class);
                startActivity(intent);
            }
        });
        alertDialog.show();
    }
}
