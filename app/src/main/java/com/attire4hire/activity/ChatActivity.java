package com.attire4hire.activity;

import static android.view.View.GONE;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.ChatMessagesAdapter;
import com.attire4hire.model.ChatModel;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends BaseActivity implements View.OnClickListener {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = ChatActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = ChatActivity.this.getClass().getSimpleName();

    private RecyclerView rv_chat;
    private ChatMessagesAdapter chatAdapter;
    private EditText input_msg;
    private RelativeLayout rlCancelRL;
    private TextView tv_send, tv_user_name;
    private ImageView iv_profile;
    ItemProductModel mItemProductModel;
    private Socket mSocket;
    private String strRoomID = "";
    boolean isPagination = false;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar msgProgressBarPB;
    ArrayList<ChatModel> mChatModel = new ArrayList<ChatModel>();
    ArrayList<ChatModel> tempArrayList = new ArrayList<>();
    int pageNumber = 1;
    ChatModel chatModel;

    String strLastPage = "FALSE";
    String Receiver_id = "";
    boolean isSwipeRefresh = false;
    String FirstName = "", LastName = "";

    LinearLayoutManager linearLayoutManager;
    String Notification = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        inIds();
        performActions();
        getIntentData();
//        setUpSocketData();

        getMessagesData();
        setAdapter();

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                ++pageNumber;
                executeGetChatListingApi();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpSocketData();

//        mSocket.on("newMessage", onNewMessage);
    }

    private void getIntentData() {

        if (getIntent() != null) {

            if (getIntent().getStringExtra(ConstantData.NOTIFICATION) != null) {

                Notification = getIntent().getStringExtra(ConstantData.NOTIFICATION);

                if (Notification.equals("notification")) {
                    strRoomID = getIntent().getStringExtra("room_id");
                    Receiver_id = getIntent().getStringExtra("receiver_id");
                }
            }

            if ((ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL) != null) {

                mItemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

                strRoomID = mItemProductModel.getRoom_id();
                Receiver_id = mItemProductModel.getAnother_User_Id();
                tv_user_name.setText(mItemProductModel.getAnother_User_Name() + " " + mItemProductModel.getAnother_User_LastName());

                if (mItemProductModel.getAnother_ProfilePic() != null && mItemProductModel.getAnother_ProfilePic().contains("http")) {
                    Picasso.with(mActivity).load(mItemProductModel.getAnother_ProfilePic())
                            .placeholder(R.drawable.ic_pp_ph)
                            .error(R.drawable.ic_pp_ph)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(iv_profile);
                } else {
                    Picasso.with(mActivity).load(R.drawable.ic_pp_ph).into(iv_profile);
                }
            }
        }
    }

    private void inIds() {
        rv_chat = findViewById(R.id.rv_chat);
        rlCancelRL = findViewById(R.id.rlCancelRL);
        tv_send = findViewById(R.id.tv_send);
        input_msg = findViewById(R.id.input_msg);
        mSwipeRefreshLayout = findViewById(R.id.mSwipeRefreshLayout);
        tv_user_name = findViewById(R.id.tv_user_name);
        iv_profile = findViewById(R.id.iv_profile);
        msgProgressBarPB = findViewById(R.id.msgProgressBarPB);
    }

    private void performActions() {
        tv_send.setOnClickListener(this);
        rlCancelRL.setOnClickListener(this);
        iv_profile.setOnClickListener(this);
        tv_user_name.setOnClickListener(this);
    }

    private void setAdapter() {
//        Collections.reverse(mChatModel);
        chatAdapter = new ChatMessagesAdapter(this, mChatModel);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setReverseLayout(true);
        rv_chat.setLayoutManager(linearLayoutManager);
        rv_chat.setItemAnimator(new DefaultItemAnimator());
        rv_chat.setAdapter(chatAdapter);

        if (pageNumber == 1) {
            if (input_msg.hasFocus()) {
                if (mChatModel != null) {
                    rv_chat.scrollToPosition(0);
                }
            }
            if (mChatModel != null) {
                rv_chat.scrollToPosition(0);
            }
        }
    }

    private void setUpSocketData() {
        Attire4hireApplication app = (Attire4hireApplication) getApplication();

        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", strRoomID);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.connect();
    }

    private void getMessagesData() {
        if (isNetworkAvailable(mActivity)) {
            mChatModel.clear();
            tempArrayList.clear();
            executeGetChatListingApi();
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void performPutMessageClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeAddMessageApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void executeGetChatListingApi() {

        if (strLastPage.equals("FALSE")) {
            if (msgProgressBarPB != null) {
                msgProgressBarPB.setVisibility(GONE);
            }
        } else {
            if (pageNumber == 1) {
                if (isSwipeRefresh) {
                } else {
                    showProgressDialog(mActivity);
                }

            } else if (pageNumber > 1) {
                msgProgressBarPB.setVisibility(View.GONE);
            }
        }

        String mApiUrl = ConstantData.GET_CHAT_LISTINGS;
        JSONObject params = new JSONObject();
        try {
            params.put("room_id", strRoomID);
            params.put("page_no", pageNumber);
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        if (isSwipeRefresh) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                        parseResponse(response);

                    } else {
                        if (isSwipeRefresh) {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        dismissProgressDialog();
        tempArrayList.clear();
        try {

            strLastPage = response.getString("last_page");

            if (!response.isNull("all_messages")) {

                JSONArray mJsonArray = response.getJSONArray("all_messages");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);

                    chatModel = new ChatModel();

                    if (!mDataObject.isNull("id")) {
                        chatModel.setMsg_id(mDataObject.getString("id"));
                    }

                    if (!mDataObject.isNull("room_id")) {
                        chatModel.setRoom_id(mDataObject.getString("room_id"));
                        strRoomID = mDataObject.getString("room_id");
                    }
                    if (!mDataObject.isNull("sender_id"))
                        chatModel.setSender_id(mDataObject.getString("sender_id"));
                    if (!mDataObject.isNull("receiver_id"))
                        chatModel.setReceiver_id(mDataObject.getString("receiver_id"));
                    if (!mDataObject.isNull("message"))
                        chatModel.setMessage(mDataObject.getString("message"));
                    if (!mDataObject.isNull("read_status"))
                        chatModel.setRead_status(mDataObject.getString("read_status"));
                    if (!mDataObject.isNull("creation_date"))
                        chatModel.setCreation_date(mDataObject.getString("creation_date"));
                    if (!mDataObject.isNull("profile_pic")) {
                        chatModel.setReceiver_pic(mDataObject.getString("profile_pic"));
                    }

                    //Set Sender and Receiver
                    if (getUserID().equals(mDataObject.getString("sender_id"))) {
                        chatModel.setIntChatType(ConstantData.VIEW_TYPE_SENDER_MESSAGE);
                    } else {
                        chatModel.setIntChatType(ConstantData.VIEW_TYPE_RECIEVER_MESSAGE);
                    }

                    if (!response.getString("receiver_detail").equals("")) {

                        JSONObject mDataObject1 = response.getJSONObject("receiver_detail");

                        if (!mDataObject1.isNull("id")) {
                            chatModel.setId(mDataObject1.getString("id"));
                            Receiver_id = mDataObject.getString("id");
                        }
                        if (!mDataObject1.isNull("first_name")) {
                            chatModel.setReceiver_name(mDataObject1.getString("first_name"));
                            FirstName = mDataObject1.getString("first_name");
                        }

                        if (!mDataObject1.isNull("last_name")) {
                            chatModel.setReceiver_name(mDataObject1.getString("last_name"));
                            LastName = mDataObject1.getString("last_name");
                        }

                        tv_user_name.setText(FirstName + " " + LastName);

                        if (!mDataObject1.isNull("profile_pic") && mDataObject1.getString("profile_pic").contains("http")) {
                            chatModel.setReceiver_pic(mDataObject1.getString("profile_pic"));
                            Picasso.with(mActivity).load(mDataObject1.getString("profile_pic"))
                                    .placeholder(R.drawable.ic_pp_ph)
                                    .error(R.drawable.ic_pp_ph)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(iv_profile);
                        } else {
                            Picasso.with(mActivity).load(R.drawable.ic_pp_ph).into(iv_profile);
                        }
                    }

                    if (pageNumber == 1) {
                        mChatModel.add(chatModel);
                    } else if (pageNumber > 1) {
                        tempArrayList.add(chatModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mChatModel.addAll(tempArrayList);
                }

                if (pageNumber == 1) {
                    /*
                     * Set Adapter
                     * */
                    setAdapter();
                } else {
                    chatAdapter.notifyDataSetChanged();
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void executeAddMessageApi() {
        Calendar calander = Calendar.getInstance();
        Log.i(TAG, "executeAddMessageApi: " + calander.getTime());
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd MMM,yyyy hh:mm a");
        String date = simpledateformat.format(calander.getTime());
        if (date.contains("am")) {
            date = date.substring(0, date.length()-1) + "M" + date.substring(date.length()-1 + 1);
            date = date.substring(0, date.length()-2) + "A" + date.substring(date.length()-2 + 1);

        } else if (date.contains("pm")) {
            date = date.substring(0, date.length()-1) + "M" + date.substring(date.length()-1 + 1);
            date = date.substring(0, date.length()-2) + "P" + date.substring(date.length()-2 + 1);
        }
        String data = input_msg.getText().toString();

        ChatModel mModel = new ChatModel();
        mModel.setIntChatType(ConstantData.VIEW_TYPE_SENDER_MESSAGE);
        mModel.setCreation_date(date);
        mModel.setMessage(data);
        mChatModel.add(0, mModel);
        chatAdapter.notifyDataSetChanged();
        if (mChatModel != null) {
            rv_chat.scrollToPosition(0);
        }
        input_msg.setText("");
        tv_send.setEnabled(false);
        String mApiUrl = ConstantData.ADD_MESSAGE;
        JSONObject params = new JSONObject();
        try {
            params.put("message", data);
            params.put("sender_id", getUserID());
            params.put("receiver_id", Receiver_id);
            params.put("room_id", strRoomID);
            Log.i(TAG, "executeAddMessageApi: "+params);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, response -> {
            tv_send.setEnabled(true);
            Log.e(TAG, "**RESPONSE**" + response);

            if (msgProgressBarPB != null) {
                msgProgressBarPB.setVisibility(GONE);
            }

            try {

                if (response.getString("status").equals("1")) {
                    //perform the sending message attempt.
                    mSocket.emit("newMessage", strRoomID, response.getJSONObject("data"), input_msg.getText().toString());

                    JSONObject mDataObj = response.getJSONObject("data");
//                        ChatModel mModel = new ChatModel();

                    if (!mDataObj.isNull("room_id"))
                        mModel.setRoom_id(mDataObj.getString("room_id"));
                    if (!mDataObj.isNull("sender_id"))
                        mModel.setSender_id(mDataObj.getString("sender_id"));
                    if (!mDataObj.isNull("receiver_id"))
                        mModel.setReceiver_id(mDataObj.getString("receiver_id"));
                    if (!mDataObj.isNull("message"))
                        mModel.setMessage(mDataObj.getString("message"));
                    if (!mDataObj.isNull("read_status"))
                        mModel.setRead_status(mDataObj.getString("read_status"));
                    if (!mDataObj.isNull("creation_date"))
                        mModel.setCreation_date(mDataObj.getString("creation_date"));

                    //Set Sender and Receiver
                    if (getUserID().equals(mDataObj.getString("sender_id"))) {
                        mModel.setIntChatType(ConstantData.VIEW_TYPE_SENDER_MESSAGE);
                    } else {
                        mModel.setIntChatType(ConstantData.VIEW_TYPE_RECIEVER_MESSAGE);
                    }
                } else if (response.getString("status").equals("3")) {
                    LogOut();
                } else {
                    showToast(mActivity, response.getString("message"));
                }

            } catch (Exception e) {
                Log.e(TAG, "**ERROR**" + e.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                tv_send.setEnabled(true);
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    boolean isValidate() {
        boolean flag = true;
        if (input_msg.getText().toString().trim().equals("")) {
            Toast.makeText(mActivity, "Please Write Something!", Toast.LENGTH_SHORT).show();
            flag = false;
        }

        return flag;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.tv_send:
                performPutMessageClick();
                break;

            case R.id.iv_profile:
                performProfilePicClick();
                break;

            case R.id.tv_user_name:
                performNameClick();
                break;
        }
    }

    private void performNameClick() {
        performAnotherUserProfile();
    }

    private void performProfilePicClick() {
        performAnotherUserProfile();
    }

    private void performAnotherUserProfile() {
        startActivity(new Intent(mActivity, AnotherUserActivity.class).putExtra(Attire4hirePrefrences.ANOTHER_USER_ID, mItemProductModel.getAnother_User_Id()));
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };

    String id = "", msg_id = "";
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject mJsonObject = new JSONObject(String.valueOf(args[1]));

                        ChatModel mModel = new ChatModel();

                        if (!mJsonObject.isNull("id")) {
                            mModel.setMsg_id(mJsonObject.getString("id"));
                            if (!id.equals(mJsonObject.getString("id"))) {
                                id = mJsonObject.getString("id");
                            } else {
                                msg_id = mJsonObject.getString("id");
                            }
                        }

                        if (!mJsonObject.isNull("room_id"))
                            mModel.setRoom_id(mJsonObject.getString("room_id"));
                        if (!mJsonObject.isNull("sender_id"))
                            mModel.setSender_id(mJsonObject.getString("sender_id"));
                        if (!mJsonObject.isNull("receiver_id"))
                            mModel.setReceiver_id(mJsonObject.getString("receiver_id"));
                        if (!mJsonObject.isNull("message"))
                            mModel.setMessage(mJsonObject.getString("message"));
                        if (!mJsonObject.isNull("read_status"))
                            mModel.setRead_status(mJsonObject.getString("read_status"));
                        if (!mJsonObject.isNull("creation_date")) {
                            mModel.setCreation_date(mJsonObject.getString("creation_date"));
                        }

                        //Set Sender and Receiver
                        if (getUserID().equals(mJsonObject.getString("sender_id"))) {
                            mModel.setIntChatType(ConstantData.VIEW_TYPE_SENDER_MESSAGE);
                        } else {
                            mModel.setIntChatType(ConstantData.VIEW_TYPE_RECIEVER_MESSAGE);
                        }

                        if (!id.equals(msg_id)) {
                            mChatModel.add(0, mModel);
                            chatAdapter.notifyDataSetChanged();
                            input_msg.setText("");
                            if (mChatModel != null) {
                                rv_chat.scrollToPosition(0);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];

                }
            });
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        String mApiUrl = ConstantData.GET_CHAT_LISTINGS;
        JSONObject params = new JSONObject();
        try {
            params.put("room_id", strRoomID);
            params.put("page_no", pageNumber);
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "**RESPONSE**" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);

        mSocket.emit("leaveChat", strRoomID);

        if (Notification != null && !Notification.equals("")) {
            callHomeActivityIntent(mActivity);
        } else {
            finish();
        }
    }
}
