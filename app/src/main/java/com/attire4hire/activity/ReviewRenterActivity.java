package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.whinc.widget.ratingbar.RatingBar;

import org.json.JSONException;
import org.json.JSONObject;

public class ReviewRenterActivity extends BaseActivity implements View.OnClickListener {
    Activity mActivity = ReviewRenterActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ReviewRenterActivity.this.getClass().getSimpleName();

    RelativeLayout cancelRL;
    RatingBar exp_rating, comm_rating, return_rating, item_looked_rating;
    String str_exp_rating = "", str_comm_rating = "", str_return_rating = "", str_item_looked_rating = "";
    TextView tv_submit, titleId;
    EditText et_exp;
    ItemProductModel itemProductModel;
    String user_id = "";
    String reviewed_user = "";
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_renter);

        //ids
        intIds();

        //Capitalize first letter
        //for review
        et_exp.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_exp.getText().toString().length() == 1 && et_exp.getTag().toString().equals("true")) {
                    et_exp.setTag("false");
                    et_exp.setText(et_exp.getText().toString().toUpperCase());
                    et_exp.setSelection(et_exp.getText().toString().length());
                }
                if (et_exp.getText().toString().length() == 0) {
                    et_exp.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });

        //get Intent Data
        getIntentData();
        setData();
    }

    private void intIds() {
        cancelRL = findViewById(R.id.cancelRL);
        exp_rating = findViewById(R.id.exp_rating);
        comm_rating = findViewById(R.id.comm_rating);
        return_rating = findViewById(R.id.return_rating);
        item_looked_rating = findViewById(R.id.item_looked_rating);
        tv_submit = findViewById(R.id.tv_submit);
        et_exp = findViewById(R.id.et_exp);
        titleId = findViewById(R.id.titleId);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            if (getUserID().equals(itemProductModel.getProduct_owner_id())) {
                //owner, lender
                user_id = itemProductModel.getProduct_owner_id();
                reviewed_user = itemProductModel.getUser_id();

                if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                    titleId.setText("Review your Renter");
                    type = "rent";
                } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                    titleId.setText("Review your Buyer");
                    type = "buy";
                }
            } else {
                //customer, renter
                user_id = itemProductModel.getUser_id();
                reviewed_user = itemProductModel.getProduct_owner_id();

                if (itemProductModel.getRequest_type().equalsIgnoreCase("rent")) {
                    titleId.setText("Review your Renter");
                    type = "rent";
                } else if (itemProductModel.getRequest_type().equalsIgnoreCase("buy")) {
                    titleId.setText("Review your Buyer");
                    type = "buy";
                }
            }
        }
    }

    private void setData() {
        cancelRL.setOnClickListener(this);
        tv_submit.setOnClickListener(this);

        exp_rating.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar ratingBar, int i, int i1) {
                str_exp_rating = String.valueOf(i1);
            }
        });

        comm_rating.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar ratingBar, int i, int i1) {
                str_comm_rating = String.valueOf(i1);
            }
        });

        return_rating.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar ratingBar, int i, int i1) {
                str_return_rating = String.valueOf(i1);
            }
        });

        item_looked_rating.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onChange(RatingBar ratingBar, int i, int i1) {
                str_item_looked_rating = String.valueOf(i1);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;

            case R.id.tv_submit:
                clickSubmitRating();
                break;
        }
    }

    private void clickSubmitRating() {
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeAddReviewApi();
            }
        }
    }

    private Boolean validate() {
        boolean flag = true;
        if (str_exp_rating.equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.overall_experience_renter));
            flag = false;
        } else if (str_comm_rating.equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.communication_renter));
            flag = false;
        } else if (str_return_rating.equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.item_returned_on_time_renter));
            flag = false;
        } else if (str_item_looked_rating.equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.item_looked_after_well_renter));
            flag = false;
        } else if (et_exp.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getResources().getString(R.string.tell_us_your_experience_renter));
            flag = false;
        }
        return flag;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void executeAddReviewApi() {

        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.ADD_REVIEW;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", user_id);
            params.put("order_id", itemProductModel.getOrder_id());
            params.put("product_id", itemProductModel.getId());
            params.put("reviewed_user", reviewed_user);
            params.put("overall_exp", str_exp_rating);
            params.put("communication", str_comm_rating);
            params.put("item_as_desc", str_item_looked_rating);
            params.put("item_arrive_on_time", str_return_rating);
            params.put("comments", et_exp.getText().toString());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

//                        if (type.equals("rent")) {
                            showRatingAlertDialog(mActivity, getString(R.string.lender_review_done));
//                        } else if (type.equals("buy")){
//                            showRatingAlertDialog(mActivity, getString(R.string.render_review_done));
//                        }

                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public void showRatingAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
}
