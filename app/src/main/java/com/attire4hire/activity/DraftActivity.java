package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.DraftAdapter;
import com.attire4hire.interfaces.DraftCallBacks;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DraftActivity extends BaseActivity {
    /*
     * Initlaize Activity...
     * */
    Activity mActivity = DraftActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = DraftActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.llNoDataFountTV)
    LinearLayout llNoDataFountTV;

    /*
     * Initalize Objects...
     * */
    DraftAdapter mDraftAdapter;
    ArrayList<ItemProductModel> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft);
        ButterKnife.bind(this);

        setAdatper();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mArrayList != null) {
            mArrayList.clear();
            //Get Draft Data
            getDraftData();
        }
    }

    private void getDraftData() {
        if (isNetworkAvailable(mActivity))
            executeDraftApi();
        else
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
    }

    @OnClick(R.id.rlCancelRL)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void executeDraftApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_DRAFT_ITEMS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {

                    }

                    if (mArrayList.size() > 0) {
                        llNoDataFountTV.setVisibility(View.GONE);
                        /*
                         * Set Adapter
                         * */
                        setAdatper();
                    } else {
                        llNoDataFountTV.setVisibility(View.VISIBLE);
                        mDraftAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("data")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mItemProductModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mItemProductModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mItemProductModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mItemProductModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mItemProductModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mItemProductModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mItemProductModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("coler_name"))
                        mItemProductModel.setColor_name(mDataObject.getString("coler_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mItemProductModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mItemProductModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mItemProductModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mItemProductModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mItemProductModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("location"))
                        mItemProductModel.setLocation(mDataObject.getString("location"));
                    if (!mDataObject.isNull("date"))
                        mItemProductModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mItemProductModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mItemProductModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mItemProductModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mItemProductModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mItemProductModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mItemProductModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mItemProductModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mItemProductModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mItemProductModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mItemProductModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mItemProductModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mItemProductModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mItemProductModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mItemProductModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mItemProductModel.setImage5(mDataObject.getString("image5"));

                    mArrayList.add(mItemProductModel);

//                    Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                }
                setAdatper();
            }

            if (mArrayList.size() > 0) {
                llNoDataFountTV.setVisibility(View.GONE);
                setAdatper();
            } else {
                llNoDataFountTV.setVisibility(View.VISIBLE);
                mDraftAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void setAdatper() {
        mRecyclerViewRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mDraftAdapter = new DraftAdapter(mActivity, mArrayList, mDraftCallBacks);
        mRecyclerViewRV.setAdapter(mDraftAdapter);
        mDraftAdapter.notifyDataSetChanged();
    }

    DraftCallBacks mDraftCallBacks = new DraftCallBacks() {
        @Override
        public void draftDelete(ItemProductModel mModel, int position) {
            showConfirmDialog(mModel, position);
        }
    };

    private void showConfirmDialog(ItemProductModel mModel, int position) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_draft_item);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //Execute api
                deleteDraftItem(mModel, position);
            }
        });
        alertDialog.show();
    }

    private void deleteDraftItem(ItemProductModel mModel, int position) {
        if (isNetworkAvailable(mActivity))
            executeDeleteDraftApi(mModel, position);
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeDeleteDraftApi(ItemProductModel mModel, int position) {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.DELETE_DRAFT_ITEMS;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("draft_id", mModel.getId());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseDeleteResponse(response, mModel, position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseDeleteResponse(JSONObject response, ItemProductModel mModel, int position) {
        try {

            if (response.getString("status").equals("1")) {
                showToast(mActivity, response.getString("message"));
                mArrayList.remove(position);
                mDraftAdapter.notifyDataSetChanged();

            } else if (response.getString("status").equals("3")) {
                LogOut();

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}