package com.attire4hire.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

public class ReportUserActivity extends BaseActivity implements View.OnClickListener {
    Activity mActivity = ReportUserActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = ReportUserActivity.this.getClass().getSimpleName();

    /*Widgets*/
    ImageView mSpamIV, mOtherIV, mTransIV, mHarassIV;
    LinearLayout mSpamLL, mOtherLL, mTransLL, mHarassLL;
    RelativeLayout rlCancelRL;
    ItemProductModel itemProductModel;
    String Message = "";
    TextviewRegular tvSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_user);

        init();
        setData();
        getIntentData();
    }

    private void setData() {
        mSpamLL.setOnClickListener(this);
        mTransLL.setOnClickListener(this);
        mHarassLL.setOnClickListener(this);
        mOtherLL.setOnClickListener(this);
        rlCancelRL.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
    }

    private void init() {
        mSpamIV = findViewById(R.id.iv_spamm);
        mOtherIV = findViewById(R.id.iv_other);
        mTransIV = findViewById(R.id.iv_trans);
        mHarassIV = findViewById(R.id.iv_harass);
        mSpamLL = findViewById(R.id.ll_spamm);
        mOtherLL = findViewById(R.id.ll_other);
        mTransLL = findViewById(R.id.ll_trans);
        mHarassLL = findViewById(R.id.ll_harass);
        rlCancelRL = findViewById(R.id.rlCancelRL);
        tvSubmit = findViewById(R.id.tvSubmit);
    }

    private void getIntentData() {

        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_spamm:
                mSpamIV.setImageResource(R.drawable.ic_check);
                mOtherIV.setImageResource(R.drawable.ic_uncheck);
                mTransIV.setImageResource(R.drawable.ic_uncheck);
                mHarassIV.setImageResource(R.drawable.ic_uncheck);
                Message = getString(R.string.spamming);
                break;

            case R.id.ll_other:
                mSpamIV.setImageResource(R.drawable.ic_uncheck);
                mOtherIV.setImageResource(R.drawable.ic_check);
                mTransIV.setImageResource(R.drawable.ic_uncheck);
                mHarassIV.setImageResource(R.drawable.ic_uncheck);
                Message = getString(R.string.other);
                break;

            case R.id.ll_trans:
                mSpamIV.setImageResource(R.drawable.ic_uncheck);
                mOtherIV.setImageResource(R.drawable.ic_uncheck);
                mTransIV.setImageResource(R.drawable.ic_check);
                mHarassIV.setImageResource(R.drawable.ic_uncheck);
                Message = getString(R.string.trying_to_complete_transacion_outside_of_the_attires4hire_platform);
                break;

            case R.id.ll_harass:
                mSpamIV.setImageResource(R.drawable.ic_uncheck);
                mOtherIV.setImageResource(R.drawable.ic_uncheck);
                mTransIV.setImageResource(R.drawable.ic_uncheck);
                mHarassIV.setImageResource(R.drawable.ic_check);
                Message = getString(R.string.harassement_or_bullying);
                break;

            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.tvSubmit:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        if (!Message.equals("")) {

            if (isNetworkAvailable(mActivity))
                executeReportUserApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            showAlertDialog(mActivity, getString(R.string.choose_one_option));
        }
    }

    private void executeReportUserApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.ADD_REPORT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("order_id", "0");
            params.put("reported_user", itemProductModel.getAnother_User_Id());
            params.put("message", Message);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                        showAlertDialogFinish(mActivity, getString(R.string.report_submitted));

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}
