package com.attire4hire.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.BrandAutoCompleteAdapter;
import com.attire4hire.adapters.CityAutoCompleteAdapter;
import com.attire4hire.adapters.SlidingEditImageAdapter;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.EdittextSemiBold;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.CityAutoCompleteModel;
import com.attire4hire.model.ImagesModel;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.model.SpinnerModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.ConstantData;
import com.chahinem.pageindicator.PageIndicator;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddItemEditActivity extends BaseActivity {

    private static final int REQUEST_CODE = 732;
    public final int REQUEST_CAMERA = 384;
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /*
     * Initialize Activity
     * */
    Activity mActivity = AddItemEditActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = AddItemEditActivity.this.getClass().getSimpleName();
    /*
     * Widgets
     * */
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.addImagesRL)
    RelativeLayout addImagesRL;
    @BindView(R.id.defaultImg)
    ImageView defaultImg;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;
    @BindView(R.id.indicator)
    PageIndicator indicator;
    @BindView(R.id.nameET)
    EdittextSemiBold nameET;
    @BindView(R.id.category_sp)
    Spinner categorySp;
    @BindView(R.id.size_sp)
    Spinner sizeSp;
    @BindView(R.id.type_sp)
    Spinner typeSp;
    @BindView(R.id.color_sp)
    Spinner colorSp;
    @BindView(R.id.brand_sp)
    Spinner brandSp;
    @BindView(R.id.condition_sp)
    Spinner conditionSp;
    @BindView(R.id.occasion_sp)
    Spinner occasionSp;
    @BindView(R.id.descriptionET)
    EdittextRegular descriptionET;
    @BindView(R.id.txtNextTV)
    TextviewSemiBold txtNextTV;
    @BindView(R.id.cityET)
    AutoCompleteTextView cityET;
    @BindView(R.id.imgClearIV)
    ImageView imgClearIV;
    @BindView(R.id.locationET)
    EdittextRegular locationET;
    @BindView(R.id.imageRL)
    RelativeLayout imageRL;
    @BindView(R.id.brandET)
    AutoCompleteTextView brandET;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    /*
     * Initalize Objects
     * */
    Typeface font;
    String selectedColor, selectedSIze, selectedType, selectedCategory, selectedBrand, selectedCondition, selectedOccasion;
    String RetailPrice = "", ReplacementValue = "", Days_4 = "", Days_8 = "", Days_12 = "", InstantBooking = "", OpenForSale = "", CleaningFee = "", DropOff = "", DeliveryFee = "", Date = "";
    CityAutoCompleteAdapter mCityAutoCompleteAdapter;
    ArrayList<CityAutoCompleteModel> mCitiesArrayList = new ArrayList<>();
    ArrayList<ImagesModel> mImagesArrayList = new ArrayList<>();
    ItemProductModel mItemProductModel = new ItemProductModel();
    SlidingEditImageAdapter mSlidingImageAdapter;
    BrandAutoCompleteAdapter mBrandAutoCompleteAdapter;

    String currentPhotoPath = "";
    private ArrayList<SpinnerModel> brandModelArrayList;
    private ArrayList<SpinnerModel> categoryModelArrayList;
    private ArrayList<SpinnerModel> colorModelArrayList;
    private ArrayList<SpinnerModel> typeModelArrayList;
    private ArrayList<SpinnerModel> sizeModelArrayList;
    private ArrayList<SpinnerModel> occasionModelArrayList;
    private ArrayList<SpinnerModel> conditionModelArrayList;
    private ArrayList<String> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item_edit);

        //Set Butter Knife
        ButterKnife.bind(this);

        //Capitalize first letter
        //for title
        nameET.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (nameET.getText().toString().length() == 1 && nameET.getTag().toString().equals("true")) {
                    nameET.setTag("false");
                    nameET.setText(nameET.getText().toString().toUpperCase());
                    nameET.setSelection(nameET.getText().toString().length());
                }
                if (nameET.getText().toString().length() == 0) {
                    nameET.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });
        //for description
        descriptionET.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (descriptionET.getText().toString().length() == 1 && descriptionET.getTag().toString().equals("true")) {
                    descriptionET.setTag("false");
                    descriptionET.setText(descriptionET.getText().toString().toUpperCase());
                    descriptionET.setSelection(descriptionET.getText().toString().length());
                }
                if (descriptionET.getText().toString().length() == 0) {
                    descriptionET.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });
        //for city
        cityET.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (cityET.getText().toString().length() == 1 && cityET.getTag().toString().equals("true")) {
                    cityET.setTag("false");
                    cityET.setText(cityET.getText().toString().toUpperCase());
                    cityET.setSelection(cityET.getText().toString().length());
                }
                if (cityET.getText().toString().length() == 0) {
                    cityET.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });

        //for brand
        brandET.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (brandET.getText().toString().length() == 1 && brandET.getTag().toString().equals("true")) {
                    brandET.setTag("false");
                    brandET.setText(brandET.getText().toString().toUpperCase());
                    brandET.setSelection(brandET.getText().toString().length());
                }
                if (brandET.getText().toString().length() == 0) {
                    brandET.setTag("true");
                }
            }

            public void afterTextChanged(Editable editable) {

            }
        });


        //Get All Categories:
        getAllCategories();

        //Auto complete
        setCityAutoComplete();
    }

    private void HideKey() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().getSerializableExtra(ConstantData.MODEL) != null) {

            mItemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);
            String mActivityType = getIntent().getStringExtra(ConstantData.ADD_ITEM_TYPE);

            if (mActivityType.equals("AddItem")) {

                if (mItemProductModel.getImage1() != null && mItemProductModel.getImage1().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage1());
                    mImagesArrayList.add(mModel);
                    defaultImg.setVisibility(View.GONE);
                } else {
                    defaultImg.setVisibility(View.VISIBLE);
                }

                if (mItemProductModel.getImage2() != null && mItemProductModel.getImage2().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage2());
                    mImagesArrayList.add(mModel);
                }

                if (mItemProductModel.getImage3() != null && mItemProductModel.getImage3().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage3());
                    mImagesArrayList.add(mModel);
                }

                if (mItemProductModel.getImage4() != null && mItemProductModel.getImage4().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage4());
                    mImagesArrayList.add(mModel);
                }
                if (mItemProductModel.getImage5() != null && mItemProductModel.getImage5().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage5());
                    mImagesArrayList.add(mModel);
                }

                for (int i = 0; i < mImagesArrayList.size(); i++) {
                    mArrayList.add(mImagesArrayList.get(i).getImage_path());
                }

            } else if (mActivityType.equals("AddItemSearch")) {

                if (mItemProductModel.getImage1() != null && mItemProductModel.getImage1().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage1());
                    mImagesArrayList.add(mModel);
                }

                if (mItemProductModel.getImage2() != null && mItemProductModel.getImage2().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage2());
                    mImagesArrayList.add(mModel);
                }

                if (mItemProductModel.getImage3() != null && mItemProductModel.getImage3().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage3());
                    mImagesArrayList.add(mModel);
                }

                if (mItemProductModel.getImage4() != null && mItemProductModel.getImage4().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage4());
                    mImagesArrayList.add(mModel);
                }
                if (mItemProductModel.getImage5() != null && mItemProductModel.getImage5().contains("http")) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mItemProductModel.getImage5());
                    mImagesArrayList.add(mModel);
                }

                for (int i = 0; i < mImagesArrayList.size(); i++) {
                    mArrayList.add(mImagesArrayList.get(i).getImage_path());
                }

            } else if (mActivityType.equals(ConstantData.ACTIVITY_LIST_ITEM)) {
                mImagesArrayList = mItemProductModel.getImagesArrayList();

                for (int i = 0; i < mImagesArrayList.size(); i++) {
                    mArrayList.add(mImagesArrayList.get(i).getImage_path());
                }
            }
            setDataOnWidgets();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void getAllCategories() {
        if (isNetworkAvailable(mActivity))
            executeCategoriesApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeCategoriesApi() {
        showProgressDialog(mActivity);
        String strUrl = ConstantData.ALL_CATEGORY_API;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****Response****" + response);
                dismissProgressDialog();
                categoryModelArrayList = new ArrayList<>();
                sizeModelArrayList = new ArrayList<>();
                typeModelArrayList = new ArrayList<>();
                brandModelArrayList = new ArrayList<>();
                occasionModelArrayList = new ArrayList<>();
                conditionModelArrayList = new ArrayList<>();
                colorModelArrayList = new ArrayList<>();

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.optString("status").equals("1")) {

                        JSONObject dataObj = object.getJSONObject("data");

                        JSONArray category = dataObj.getJSONArray("category");
                        {
                            SpinnerModel categoryModel1 = new SpinnerModel();

                            categoryModel1.setName("");
                            categoryModel1.setId("0");
                            categoryModelArrayList.add(categoryModel1);

                            for (int i = 0; i < category.length(); i++) {

                                SpinnerModel categoryModel = new SpinnerModel();

                                JSONObject dataobj = category.getJSONObject(i);

                                if (!dataobj.isNull("name")) {
                                    categoryModel.setName(dataobj.getString("name"));
                                }
                                categoryModelArrayList.add(categoryModel);

                            }
                            setCategoryAdapter();
                        }
                        JSONArray size = dataObj.getJSONArray("size");
                        {
                            SpinnerModel sizeModell = new SpinnerModel();

                            sizeModell.setName("");
                            sizeModell.setId("0");
                            sizeModelArrayList.add(sizeModell);

                            for (int i = 0; i < size.length(); i++) {

                                SpinnerModel sizeModel = new SpinnerModel();
                                JSONObject dataobj = size.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    sizeModel.setName(dataobj.getString("name"));
                                }
                                sizeModelArrayList.add(sizeModel);

                            }
                            setSizeAdapter();
                        }
                        JSONArray type = dataObj.getJSONArray("type");
                        {
                            SpinnerModel typeModell = new SpinnerModel();

                            typeModell.setName("");
                            typeModell.setId("0");
                            typeModelArrayList.add(typeModell);

                            for (int i = 0; i < type.length(); i++) {

                                SpinnerModel typeModel = new SpinnerModel();
                                JSONObject dataobj = type.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    typeModel.setName(dataobj.getString("name"));
                                }
                                typeModelArrayList.add(typeModel);

                            }
                            setTypeAdapter();
                        }
                        JSONArray color = dataObj.getJSONArray("color");
                        {

                            SpinnerModel colorModell = new SpinnerModel();

                            colorModell.setName("");
                            colorModell.setId("0");
                            colorModelArrayList.add(colorModell);

                            for (int i = 0; i < color.length(); i++) {

                                SpinnerModel colorModel = new SpinnerModel();
                                JSONObject dataobj = color.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    colorModel.setName(dataobj.getString("name"));
                                }
                                colorModelArrayList.add(colorModel);
                            }
                            setColorAdapter();
                        }
                        JSONArray brand = dataObj.getJSONArray("brand/designer");
                        {
                            SpinnerModel brandModell = new SpinnerModel();

                            brandModell.setName("");
                            brandModell.setId("0");
                            brandModelArrayList.add(brandModell);

                            for (int i = 0; i < brand.length(); i++) {

                                SpinnerModel brandModel = new SpinnerModel();
                                JSONObject dataobj = brand.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    brandModel.setName(dataobj.getString("name"));
                                }
                                brandModelArrayList.add(brandModel);
                            }
                            setBrandAdapter();

                            //Brand Auto complete
                            setBrandAutoComplete();
                        }
                        JSONArray occasion = dataObj.getJSONArray("occasion");
                        {
                            SpinnerModel occasionModell = new SpinnerModel();

                            occasionModell.setName("");
                            occasionModell.setId("0");
                            occasionModelArrayList.add(occasionModell);

                            for (int i = 0; i < occasion.length(); i++) {

                                SpinnerModel occasionModel = new SpinnerModel();
                                JSONObject dataobj = occasion.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    occasionModel.setName(dataobj.getString("name"));
                                }
                                occasionModelArrayList.add(occasionModel);
                            }
                            setOccasionAdapter();
                        }
                        JSONArray condition = dataObj.getJSONArray("condition");
                        {
                            SpinnerModel conditionModell = new SpinnerModel();

                            conditionModell.setName("");
                            conditionModell.setId("0");
                            conditionModelArrayList.add(conditionModell);

                            for (int i = 0; i < condition.length(); i++) {

                                SpinnerModel conditionModel = new SpinnerModel();
                                JSONObject dataobj = condition.getJSONObject(i);
                                if (!dataobj.isNull("name")) {
                                    conditionModel.setName(dataobj.getString("name"));
                                }
                                conditionModelArrayList.add(conditionModel);
                            }
                            setConditionAdapter();
                        }
                    }

                    //Get Intent Data
                    getIntentData();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        Attire4hireApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    /*
     *  brand auto complete
     * */
    private void setBrandAutoComplete() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");

        brandET.setTypeface(font);

        mBrandAutoCompleteAdapter = new BrandAutoCompleteAdapter(this, brandModelArrayList);

        brandET.setThreshold(0);

        brandET.setAdapter(mBrandAutoCompleteAdapter);

        brandET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        brandET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (brandET.getText().toString().length() != 0) {
                    if (brandET.hasFocus()) {
                        brandET.showDropDown();
                    }
                }
            }
        });

        brandET.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                //do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
//                    imgClearIV.setVisibility(View.VISIBLE);
                    if (brandET.hasFocus()) {
                        brandET.showDropDown();
                    }
                } else {
                    imgClearIV.setVisibility(View.GONE);
                }
            }
        });

        brandET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                SpinnerModel mModel = (SpinnerModel) arg0.getItemAtPosition(arg2);
                brandET.setText(mModel.getName());
                brandET.setSelection(brandET.getText().toString().trim().length());
                selectedBrand = brandET.getText().toString().trim();
                HideKey();
            }
        });
    }

    /*
     *  category adapter
     * */
    private void setCategoryAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(), "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mCategoryAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, categoryModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);

                    HideKey();

                    TextView itemTextView = v.findViewById(R.id.itemTV);
                    String strName = categoryModelArrayList.get(position).getName();
                    itemTextView.setText(strName);
                    itemTextView.setTypeface(font);
                }
                return v;
            }
        };
        categorySp.setAdapter(mCategoryAd);
        categorySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedCategory = categorySp.getSelectedItem().toString();
                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getCategory_name() != null && mItemProductModel.getCategory_name().length() > 0) {
            for (int i = 0; i < categoryModelArrayList.size(); i++) {
                if (mItemProductModel.getCategory_name().equals(categoryModelArrayList.get(i).getName())) {
                    categorySp.setSelection(i);
                }
            }
        }
    }

    /*
     *  type adapter
     * */
    private void setTypeAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");

        ArrayAdapter<SpinnerModel> mTypeAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, typeModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = typeModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);
                return v;
            }
        };

        typeSp.setAdapter(mTypeAd);
        typeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
                selectedType = typeSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getType_name() != null && mItemProductModel.getType_name().length() > 0) {
            for (int i = 0; i < typeModelArrayList.size(); i++) {
                if (mItemProductModel.getType_name().equals(typeModelArrayList.get(i).getName())) {
                    typeSp.setSelection(i);
                }
            }
        }
    }

    /*
     *  size adapter
     * */
    private void setSizeAdapter() {
        Typeface fontStyle = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mSizeAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, sizeModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = sizeModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(fontStyle);
                return v;
            }
        };
        sizeSp.setAdapter(mSizeAd);
        sizeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(fontStyle);
                ((TextView) view).setTextSize(14);
                selectedSIze = sizeSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getSize_name() != null && mItemProductModel.getSize_name().length() > 0) {
            for (int i = 0; i < sizeModelArrayList.size(); i++) {
                if (mItemProductModel.getSize_name().equals(sizeModelArrayList.get(i).getName())) {
                    sizeSp.setSelection(i);
                }
            }
        }
    }

    /*
     *  Occasion adapter
     * */
    private void setOccasionAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mOcccasionAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, occasionModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);

                String strName = occasionModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);
                return v;
            }

        };
        occasionSp.setAdapter(mOcccasionAd);
        occasionSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
                selectedOccasion = occasionSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getOcasion_name() != null && mItemProductModel.getOcasion_name().length() > 0) {
            for (int i = 0; i < occasionModelArrayList.size(); i++) {
                if (mItemProductModel.getOcasion_name().equals(occasionModelArrayList.get(i).getName())) {
                    occasionSp.setSelection(i);
                }
            }
        }
    }

    /*
     *  brand adapter
     * */
    private void setBrandAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mBrandAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, brandModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = brandModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);
                return v;
            }

        };
        brandSp.setAdapter(mBrandAd);
        brandSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
                selectedBrand = brandSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getBrand_name() != null && mItemProductModel.getBrand_name().length() > 0) {
            for (int i = 0; i < brandModelArrayList.size(); i++) {
                if (mItemProductModel.getBrand_name().equals(brandModelArrayList.get(i).getName())) {
                    brandSp.setSelection(i);
                }
            }
        }
    }

    /*
     *  condition adapter
     * */
    private void setConditionAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mConditionAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, conditionModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = conditionModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);
                return v;
            }

        };
        conditionSp.setAdapter(mConditionAd);
        conditionSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
                selectedCondition = conditionSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getCondition_name() != null && mItemProductModel.getCondition_name().length() > 0) {
            for (int i = 0; i < conditionModelArrayList.size(); i++) {
                if (mItemProductModel.getCondition_name().equals(conditionModelArrayList.get(i).getName())) {
                    conditionSp.setSelection(i);
                }
            }
        }
    }

    /*
     *  color adapter
     * */
    private void setColorAdapter() {
        font = Typeface.createFromAsset(mActivity.getAssets(),
                "ProximaNova-Semibold.ttf");
        ArrayAdapter<SpinnerModel> mColorAd = new ArrayAdapter<SpinnerModel>(this, android.R.layout.simple_spinner_item, colorModelArrayList) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                HideKey();

                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = colorModelArrayList.get(position).getName();
                itemTextView.setText(strName);
                itemTextView.setTypeface(font);
                return v;
            }

        };
        colorSp.setAdapter(mColorAd);
        colorSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextSize(14);
                selectedColor = colorSp.getSelectedItem().toString();
                Log.e("selected", "erty" + selectedColor);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Set Selections
        if (mItemProductModel.getColor_name() != null && mItemProductModel.getColor_name().length() > 0) {
            for (int i = 0; i < colorModelArrayList.size(); i++) {
                if (mItemProductModel.getColor_name().equals(colorModelArrayList.get(i).getName())) {
                    colorSp.setSelection(i);
                }
            }
        }
    }

    private Boolean isValidate() {
        boolean flag = true;

        if (nameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_name));
            flag = false;
        } else if (selectedCategory.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_category));
            flag = false;

        } else if (selectedSIze.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_size));
            flag = false;

        } else if (selectedType.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_type));
            flag = false;

        } else if (selectedColor.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_colour));
            flag = false;

        } else if (brandET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_brand));
            flag = false;

        } else if (selectedCondition.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_condition));
            flag = false;

        } else if (selectedOccasion.equalsIgnoreCase("")) {
            showAlertDialog(mActivity, getString(R.string.enter_occasion));
            flag = false;

        } else if (descriptionET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_desc));
            flag = false;
        } else if (descriptionET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_desc));
            flag = false;
        } else if (cityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_loc));
            flag = false;
        } else if (mImagesArrayList.size() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select));
            flag = false;
        }
        return flag;
    }

    /*
     * Widget Click Listner
     * */
    @OnClick({R.id.cancelRL, R.id.addImagesRL, R.id.txtNextTV, R.id.ll_name, R.id.ll_desrciption, R.id.cityET,
            R.id.defaultImg, R.id.imageRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.addImagesRL:
                perfromImagesClickRL();
                break;
            case R.id.txtNextTV:
                performNextClick();
                break;

            case R.id.ll_name:
                nameET.setEnabled(true);
                break;

            case R.id.ll_desrciption:
                descriptionET.setEnabled(true);
                break;

            case R.id.cityET:
//                setCityAutoComplete();
                break;

            case R.id.defaultImg:
                perfromImagesClickRL();
                break;

            case R.id.imageRL:
                perfromImagesClickRL();
                break;
        }
    }

    private void perfromImagesClickRL() {
        if (mImagesArrayList.size() < 5) {
            performChangeProfilePic();
        } else {
            showToast(mActivity, getString(R.string.you_can_add_max_of_5image));
        }
    }


    public void Pick() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtCameraTV = alertDialog.findViewById(R.id.txtCameraTV);
        TextView txtGalleryTV = alertDialog.findViewById(R.id.txtGalleryTV);
        TextView btnCancelTV = alertDialog.findViewById(R.id.btnCancelTV);

        btnCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        txtCameraTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                openCameraGallery();
            }
        });

        txtGalleryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                performMultiImgClick();
            }
        });
        alertDialog.show();
    }

//    private void Pick() {
//        final CharSequence[] items = {"Camera", "Gallery",
////                "Cancel"
//        };
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
//        builder.setTitle("Choose Images");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//
//                if (items[item].equals("Camera")) {
//                    openCameraGallery();
////                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
////                    intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
////                    startActivityForResult(intent, REQUEST_CAMERA);
//                } else if (items[item].equals("Gallery")) {
//                    performMultiImgClick();
//
//                }
////                else if (items[item].e20quals("Cancel")) {
////                    dialog.dismiss();
////                }
//            }
//        });
//        builder.show();
//    }

    private void openCameraGallery() {
        ImagePicker.Companion.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(720)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(720, 720)    //Final image resolution will be less than 1080 x 1080(Optional)
                .cameraOnly()
                .start();
    }

    private void performMultiImgClick() {
        // start multiple photos selector
        Intent intent = new Intent(AddItemEditActivity.this, ImagesSelectorActivity.class);
        // max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 5);
        // min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100);
        // show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, false);
        // pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mArrayList);
        // start the selector
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void performNextClick() {
        if (isValidate()) {
            mItemProductModel.setBrand_name(brandET.getText().toString().trim());
            mItemProductModel.setColor_name(selectedColor);
            mItemProductModel.setCondition_name(selectedCondition);
            mItemProductModel.setDescription(descriptionET.getText().toString().trim());
            mItemProductModel.setLocation(cityET.getText().toString().trim());
            mItemProductModel.setName(nameET.getText().toString().trim());
            mItemProductModel.setCategory_name(selectedCategory);
            mItemProductModel.setSize_name(selectedSIze);
            mItemProductModel.setType_name(selectedType);
            mItemProductModel.setOcasion_name(selectedOccasion);
            mItemProductModel.setImagesArrayList(mImagesArrayList);

            if (mItemProductModel.getRetail_price() != null && mItemProductModel.getRetail_price().length() > 0) {
                RetailPrice = mItemProductModel.getRetail_price();
                mItemProductModel.setRetail_price(RetailPrice);
            }

            if (mItemProductModel.getReplacement_value() != null && mItemProductModel.getReplacement_value().length() > 0) {
                ReplacementValue = mItemProductModel.getReplacement_value();
                mItemProductModel.setReplacement_value(ReplacementValue);
            }

            if (mItemProductModel.getWeek_4days() != null && mItemProductModel.getWeek_4days().length() > 0) {
                Days_4 = mItemProductModel.getWeek_4days();
                mItemProductModel.setWeek_4days(Days_4);
            }

            if (mItemProductModel.getWeek_8days() != null && mItemProductModel.getWeek_8days().length() > 0) {
                Days_8 = mItemProductModel.getWeek_8days();
                mItemProductModel.setWeek_8days(Days_8);
            }

            if (mItemProductModel.getWeek_12days() != null && mItemProductModel.getWeek_12days().length() > 0) {
                Days_12 = mItemProductModel.getWeek_12days();
                mItemProductModel.setWeek_12days(Days_12);
            }

            if (mItemProductModel.getInstant_booking() != null && mItemProductModel.getInstant_booking().length() > 0 && mItemProductModel.getInstant_booking().equals("1")) {
                InstantBooking = mItemProductModel.getInstant_booking();
                mItemProductModel.setInstant_booking(InstantBooking);
            }

            if (mItemProductModel.getOpen_for_sale() != null && mItemProductModel.getOpen_for_sale().length() > 0) {
                OpenForSale = mItemProductModel.getOpen_for_sale();
                mItemProductModel.setOpen_for_sale(OpenForSale);
            }

            if (mItemProductModel.getCleaning_free() != null && mItemProductModel.getCleaning_free().length() > 0) {
                CleaningFee = mItemProductModel.getCleaning_free();
                mItemProductModel.setCleaning_free(CleaningFee);
            }

            if (mItemProductModel.getDrop_person() != null && mItemProductModel.getDrop_person().length() > 0) {
                DropOff = mItemProductModel.getDrop_person();
                mItemProductModel.setDrop_person(DropOff);
            }

            if (mItemProductModel.getDelivery_free() != null && mItemProductModel.getDelivery_free().length() > 0) {
                DeliveryFee = mItemProductModel.getDelivery_free();
                mItemProductModel.setDelivery_free(DeliveryFee);
            }

            if (mItemProductModel.getDate() != null && mItemProductModel.getDate().length() > 5) {
                Date = mItemProductModel.getDate();
                mItemProductModel.setDate(Date);
            }

            Intent intent = new Intent(mActivity, ListItemEditActivity.class);
            intent.putExtra(ConstantData.MODEL, mItemProductModel);
            startActivity(intent);
            finish();
        }
    }

    /*
     * Get Data on ActivityResult
     * & Set data on widgets
     * */
    private void setDataOnWidgets() {
        nameET.setText(this.mItemProductModel.getName());
        nameET.setSelection(nameET.getText().toString().trim().length());

        setCategoryAdapter();
        setSizeAdapter();
        setTypeAdapter();
        setColorAdapter();
        setBrandAdapter();
        setConditionAdapter();
        setOccasionAdapter();

        descriptionET.setText(mItemProductModel.getDescription());
        descriptionET.setSelection(mItemProductModel.getDescription().length());

        cityET.setText(mItemProductModel.getLocation());
        cityET.setSelection(mItemProductModel.getLocation().length());

      for (int i= 0;i<brandModelArrayList.size();i++){
          if(brandModelArrayList.get(i).getName().toString().equals(mItemProductModel.getBrand_name().toString())){
              brandET.setText(mItemProductModel.getBrand_name());
              brandET.setSelection(mItemProductModel.getBrand_name().length());
              break;
          }else {
              brandET.setText("");
              brandET.setSelection(0);
          }
      }


        setViewPagerAdapter();
    }

    //Set ViewPager Adapter
    private void setViewPagerAdapter() {
        mSlidingImageAdapter = new SlidingEditImageAdapter(mActivity, mImagesArrayList, addClick);
        mViewPagerVP.setAdapter(mSlidingImageAdapter);
        indicator.attachTo(mViewPagerVP);
    }

    SlidingEditImageAdapter.AddClick addClick = new SlidingEditImageAdapter.AddClick() {
        @Override
        public void Click(int position) {
            perfromImagesClickRL();
        }

        @Override
        public void Remove(int position) {
            mImagesArrayList.remove(position);
            if (mArrayList != null && mArrayList.size() != 0) {
                mArrayList.remove(position);
            }
            setViewPagerAdapter();
            if (mArrayList.size() == 0 && mImagesArrayList.size() == 0) {
                defaultImg.setVisibility(View.VISIBLE);
            } else {
                defaultImg.setVisibility(View.GONE);
            }
        }
    };

    /*Set Cities Auto Complete*/
    private void setCityAutoComplete() {
        imgClearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearText();
            }
        });

        if (mCitiesArrayList != null) {
            mCitiesArrayList.clear();
        }

        try {
            //JSONObject mJsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray mJsonArray = new JSONArray(loadJSONFromAsset());
            Log.e(TAG, "***Array Size***" + mJsonArray.length());

            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mDataObj = mJsonArray.getJSONObject(i);
                CityAutoCompleteModel mModel = new CityAutoCompleteModel();
                if (!mDataObj.isNull("country"))
                    mModel.setCountry(mDataObj.getString("country"));
                if (!mDataObj.isNull("geonameid"))
                    mModel.setGeonameid(mDataObj.getString("geonameid"));
                if (!mDataObj.isNull("name"))
                    mModel.setName(mDataObj.getString("name"));
                if (!mDataObj.isNull("subcountry"))
                    mModel.setSubcountry(mDataObj.getString("subcountry"));

                mCitiesArrayList.add(mModel);
            }

            Log.e(TAG, "***Array Size***" + mCitiesArrayList.size());
        } catch (Exception e) {
            e.toString();
        }

        mCityAutoCompleteAdapter = new CityAutoCompleteAdapter(this, mCitiesArrayList);

        cityET.setThreshold(0);

        cityET.setAdapter(mCityAutoCompleteAdapter);

        cityET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (cityET.getText().toString().length() != 0) {
                    if (hasFocus) {
                        cityET.showDropDown();
                    }
                }
            }
        });

        cityET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                scrollView.fullScroll(View.FOCUS_DOWN);
                return false;
            }
        });

        cityET.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                //do nothing
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
//                    imgClearIV.setVisibility(View.VISIBLE);
                    if (cityET.hasFocus()) {
                        cityET.showDropDown();
                    }

                } else {
                    imgClearIV.setVisibility(View.GONE);
                }
            }
        });

        cityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                CityAutoCompleteModel mModel = (CityAutoCompleteModel) arg0.getItemAtPosition(arg2);
                cityET.setText(mModel.getName());
                cityET.setSelection(cityET.getText().toString().trim().length());
                HideKey();
            }
        });
    }

    private void clearText() {
        cityET.setText("");
        imgClearIV.setVisibility(View.GONE);
    }

    private void performImgClick() {
        if (mImagesArrayList.size() == 5) {
            showToast(mActivity, getString(R.string.you_can_add_max_of_5image));
        } else {
            performChangeProfilePic();
        }
    }

    /*******
     * PERMISSIONS
     *********/
    public void performChangeProfilePic() {
        if (checkPermission()) {
            //openCameraGalleryDialog();
            Pick();
        } else {
            requestPermission();
        }
    }

    private File getImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File file = null;
        try {
            file = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        currentPhotoPath = "file:" + file.getPath();
        return file;
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //openCameraGalleryDialog();
                    Pick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            //val fileUri = data?.data
            if (requestCode == REQUEST_CODE) {

                mArrayList = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mArrayList != null;
                defaultImg.setVisibility(View.GONE);

                mImagesArrayList = new ArrayList<>();
                for (int i = 0; i < mArrayList.size(); i++) {
                    ImagesModel mModel = new ImagesModel();
                    mModel.setImage_path(mArrayList.get(i));
                    mModel.setImage_name(mArrayList.get(i));

                    mImagesArrayList.add(mModel);
                }
                setViewPagerAdapter();
            } else {
                if (data != null) {

                    Uri mFileUri = data.getData();

                    showImage(mFileUri);

                } else if (resultCode == ImagePicker.RESULT_ERROR) {
                    Toast.makeText(mActivity, ("Image Not Valid"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void showImage(Uri imageUri) {
        //String file = getRealPathFromURI_API19(mActivity, imageUri);
        String file = getRealPathFromURI_API19(mActivity, imageUri);

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

        Log.e(TAG, "***ImagePath***" + file);
        Log.e(TAG, "***ImageName***" + imageUri.getLastPathSegment());

        mArrayList.add(file);

        defaultImg.setVisibility(View.GONE);

        mImagesArrayList = new ArrayList<>();
        for (int i = 0; i < mArrayList.size(); i++) {
            ImagesModel mModel = new ImagesModel();
            mModel.setImage_path(mArrayList.get(i));
            mModel.setImage_name(mArrayList.get(i));

            mImagesArrayList.add(mModel);
        }
        setViewPagerAdapter();

//        ImagesModel mModel = new ImagesModel();
//        mModel.setImage_path(file);
//        mModel.setImage_name(imageUri.getLastPathSegment());
//        mImagesArrayList.add(mModel);
//        Log.e(TAG, "**Image List Size**: " + mImagesArrayList.size());
    }
}
