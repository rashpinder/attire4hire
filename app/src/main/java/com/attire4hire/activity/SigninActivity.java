package com.attire4hire.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.fonts.ButtonBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends BaseActivity {

    Activity mActivity = SigninActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SigninActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.emailLL)
    LinearLayout emailLL;
    @BindView(R.id.emailView)
    View emailView;
    @BindView(R.id.passwordLL)
    LinearLayout passwordLL;
    @BindView(R.id.passView)
    View passView;
    @BindView(R.id.forgetPassTV)
    TextviewRegular forgetPassTV;
    @BindView(R.id.signInBT)
    ButtonBold signInBT;
    @BindView(R.id.dontHaveAccountTV)
    TextviewRegular dontHaveAccountTV;
    @BindView(R.id.emailET)
    EdittextRegular emailET;
    @BindView(R.id.passET)
    EdittextRegular passET;
    @BindView(R.id.emailIV)
    ImageView emailIV;
    @BindView(R.id.passIV)
    ImageView passIV;
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;
    @BindView(R.id.eyeIV)
    ImageView eyeIV;

    String Device_Token = "", email = "";
    int eyeCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ButterKnife.bind(this);

        getPushToken();

        //password typeface
        passET.setTypeface(Typeface.DEFAULT);
        passET.setTransformationMethod(new PasswordTransformationMethod());
        passET.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        //to show email
        if (Attire4hireApplication.getSingletonValues().getEmail() != null && !Attire4hireApplication.getSingletonValues().getEmail().equals("")) {
            email = Attire4hireApplication.getSingletonValues().getEmail();
            emailET.setText(email);
            emailET.setSelection(emailET.getText().length());
        }

        spannabeText();
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    Device_Token = task.getResult();
                    Log.e(TAG, "**Push Token**" + Device_Token);

                });
    }

    @OnClick({R.id.forgetPassTV, R.id.signInBT, R.id.dontHaveAccountTV, R.id.cancelRL, R.id.eyeIV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgetPassTV:
                performForgotClick();
                break;
            case R.id.signInBT:
                performSignINClick();
                break;
            case R.id.cancelRL:
                onBackPressed();
                break;
            case R.id.eyeIV:
                performEyeClick();
                break;
        }
    }

    private void performEyeClick() {

        if (eyeCount == 0) {
            eyeIV.setImageResource(R.drawable.eye);
            passET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passET.setSelection(passET.getText().length());
            eyeCount++;

        } else if (eyeCount == 1) {
            eyeIV.setImageResource(R.drawable.eye_hide);
            passET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passET.setSelection(passET.getText().length());
            eyeCount = 0;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, SocialMediaLoginActivity.class));
        finish();
    }

    private void performSignINClick() {
        if (validate()) {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSigninApi();
            }
        }
    }

    private void performForgotClick() {
        Attire4hireApplication.getSingletonValues().setEmail(emailET.getText().toString());
        Intent intent = new Intent(mActivity, ForgetPasswordActivity.class);
        startActivity(intent);
        finish();
    }

    /*
     *Sign In Api
     * */
    private void executeSigninApi() {
        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();
        params.put("email", emailET.getText().toString().trim());
        params.put("password", passET.getText().toString().trim());
        params.put("current_latitude", getCurrentLatitude());
        params.put("current_longitude", getCurrentLongitude());
        params.put("device_token", Device_Token);
        params.put("device_type", "1");

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.SIGN_IN, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "**RESPONSE**:" + response);
                Attire4hireApplication.getSingletonValues().setEmail("");
                parseJsonResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /*
     * Sign In Response
     * */
    private void parseJsonResponse(JSONObject response) {
        dismissProgressDialog();
        try {

            if (response.getString("status").equals("1")) {
                JSONObject data = new JSONObject(response.getString("data"));

                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.IS_LOGIN, "true");

                if (!data.isNull("id")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, data.getString("id"));
                }

                if (!data.isNull("first_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, data.getString("first_name"));
                }
                if (!data.isNull("last_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, data.getString("last_name"));
                }
                if (!data.isNull("email")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, data.getString("email"));
                }
                if (!data.isNull("gender")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, data.getString("gender"));
                }
                if (!data.isNull("phone_no")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, data.getString("phone_no"));
                }
                if (!data.isNull("password")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PASSWORD, data.getString("password"));
                }
                if (!data.isNull("role")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ROLE, data.getString("role"));
                }
                if (!data.isNull("profile_pic") && !data.getString("profile_pic").equals("")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, data.getString("profile_pic"));
                }
                if (!data.isNull("created")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CREATED, data.getString("created"));
                }

                if (!data.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, data.getString("email_verification"));

                if (!data.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, data.getString("phone_verification"));

                if (!data.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, data.getString("id_verification"));

                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            } else {

                showAlertDialog(mActivity, response.getString("message"));
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public void spannabeText() {
        String str_signup = "<font color=#FFFFFF>Don't have an account? </font>" + "<b><font color=#FFFFFF>SIGN UP!</font></b>";
        dontHaveAccountTV.setText(str_signup);
        SpannableString ss = new SpannableString((Html.fromHtml(str_signup)));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(mActivity, SignupActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                if (dontHaveAccountTV.isPressed()) {
                    ds.setColor(getResources().getColor(R.color.darkPink));
                } else {
                    ds.setColor(getResources().getColor(R.color.white));
                }
                dontHaveAccountTV.invalidate();
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.white));
                dontHaveAccountTV.setBackground(getDrawable(R.drawable.ripple_effect));
            }
        };
        ss.setSpan(clickableSpan, 23, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        dontHaveAccountTV.setText(ss);
        dontHaveAccountTV.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /*
     * Add validations
     * */
    private Boolean validate() {
        boolean flag = true;
        if (emailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email));
            flag = false;
        } else if (!isValidEmaillId(emailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (emailET.getText().toString().contains(" ")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email));
            flag = false;
        } else if (passET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }
}
