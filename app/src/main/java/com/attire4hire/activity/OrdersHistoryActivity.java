package com.attire4hire.activity;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.OrdersAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.interfaces.PaginationInquiriesInterface;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrdersHistoryActivity extends BaseActivity implements View.OnClickListener, PaginationInquiriesInterface {

    /*
     * Initlaize Activity...
     * */
    Activity mActivity = OrdersHistoryActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = OrdersHistoryActivity.this.getClass().getSimpleName();

    /*Widgets*/
    RecyclerView orders_rv;
    TextviewRegular orders_tv;

    RelativeLayout rlCancelRL;
    TextviewBold tv_NoBookings;
    SwipeRefreshLayout swipeToRefresh;
    ProgressBar mProgressBar;

    String strLastPage = "FALSE";
    int page_no = 1;
    boolean isSwipeRefresh = false;

    PaginationInquiriesInterface mInterfaceData;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> tempArrayList = new ArrayList<>();

    OrdersAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);

        mInterfaceData = this;

        initialization();
        setData();
        SetAdapter();
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mItemProductModel.clear();
                tempArrayList.clear();
                GetOrderHistory(page_no);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isNetworkAvailable(mActivity)) {
            mItemProductModel.clear();
            tempArrayList.clear();
            page_no = 1;
            GetOrderHistory(page_no);
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));

    }

    private void setData() {
        rlCancelRL.setOnClickListener(this);
    }

    private void initialization() {
        orders_rv = findViewById(R.id.orders_rv);
        rlCancelRL = findViewById(R.id.rlCancelRL);
        orders_tv = findViewById(R.id.orders_tv);
        tv_NoBookings = findViewById(R.id.tv_NoBookings);
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        mProgressBar = findViewById(R.id.mProgressBar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
        }
    }

    private void SetAdapter() {
        if (orders_rv != null) {
            orders_rv.setLayoutManager(new LinearLayoutManager(mActivity));
            mAdapter = new OrdersAdapter(mActivity, mItemProductModel, mInterfaceData);
            orders_rv.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void GetOrderHistory(final int page_no) {
        if (page_no == 1) {
            if (isSwipeRefresh) {

            } else {
                showProgressDialog(mActivity);
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }

        String mApiUrl = ConstantData.GET_ALL_ORDERS;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("page_no", page_no);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                if (isSwipeRefresh) {
                    swipeToRefresh.setEnabled(false);
                }
                parseJsonResponse(response.toString());
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(String response) {
        tempArrayList.clear();
        dismissProgressDialog();
        try {

            JSONObject mJsonObjectt = new JSONObject(response);

            if (mJsonObjectt.getString("status").equals("1")) {

                JSONObject mJsonObject = mJsonObjectt.getJSONObject("data");

                JSONArray mJsonArray = mJsonObject.getJSONArray("all_orders");

                strLastPage = mJsonObject.getString("last_page");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();

                    if (!mDataObject.isNull("id"))
                        mModel.setOrder_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("request_id"))
                        mModel.setRequest_id(mDataObject.getString("request_id"));

                    if (!mDataObject.isNull("order_number"))
                        mModel.setOrder_number(mDataObject.getString("order_number"));

                    if (!mDataObject.isNull("transaction_number"))
                        mModel.setTransaction_number(mDataObject.getString("transaction_number"));

                    if (!mDataObject.isNull("accidental_damage"))
                        mModel.setAccidental_damage(mDataObject.getString("accidental_damage"));

                    if (!mDataObject.isNull("rental_fee"))
                        mModel.setRental_Prize(mDataObject.getString("rental_fee"));

                    if (!mDataObject.isNull("cleaning_fee"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_fee"));

                    if (!mDataObject.isNull("service_fee"))
                        mModel.setService_fee(mDataObject.getString("service_fee"));

                    if (!mDataObject.isNull("delivery_fee"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_fee"));

                    if (!mDataObject.isNull("damage_fee"))
                        mModel.setDamage_fee(mDataObject.getString("damage_fee"));

                    if (!mDataObject.isNull("total_price"))
                        mModel.setTotal_price(mDataObject.getString("total_price"));

                    if (!mDataObject.isNull("status"))
                        mModel.setStatus(mDataObject.getString("status"));

                    if (!mDataObject.isNull("allow_payment"))
                        mModel.setAllow_payment(mDataObject.getString("allow_payment"));

                    if (!mDataObject.isNull("dispatch_date"))
                        mModel.setDispatch_date(mDataObject.getString("dispatch_date"));

                    if (!mDataObject.isNull("delivery_date"))
                        mModel.setDelivery_date(mDataObject.getString("delivery_date"));

                    if (!mDataObject.isNull("order_date"))
                        mModel.setOrder_date(mDataObject.getString("order_date"));

                    if (!mDataObject.isNull("type"))
                        mModel.setRequest_type(mDataObject.getString("type"));

                    if (!mDataObject.getString("product_detail").equals("")) {

                        JSONObject mDataObject1 = mDataObject.getJSONObject("product_detail");

                        if (!mDataObject1.isNull("id"))
                            mModel.setId(mDataObject1.getString("id"));

                        if (!mDataObject1.isNull("name"))
                            mModel.setName(mDataObject1.getString("name"));

                        if (!mDataObject1.isNull("user_id"))
                            mModel.setUser_id(mDataObject1.getString("user_id"));

                        if (!mDataObject1.isNull("image_id"))
                            mModel.setImage_id(mDataObject1.getString("image_id"));

                        if (!mDataObject1.isNull("category_name"))
                            mModel.setCategory_name(mDataObject1.getString("category_name"));

                        if (!mDataObject1.isNull("type_name"))
                            mModel.setType_name(mDataObject1.getString("type_name"));

                        if (!mDataObject1.isNull("size_name"))
                            mModel.setSize_name(mDataObject1.getString("size_name"));

                        if (!mDataObject1.isNull("color_name"))
                            mModel.setColor_name(mDataObject1.getString("color_name"));

                        if (!mDataObject1.isNull("brand_name"))
                            mModel.setBrand_name(mDataObject1.getString("brand_name"));

                        if (!mDataObject1.isNull("condition_name"))
                            mModel.setCondition_name(mDataObject1.getString("condition_name"));

                        if (!mDataObject1.isNull("ocasion_name"))
                            mModel.setOcasion_name(mDataObject1.getString("ocasion_name"));

                        if (!mDataObject1.isNull("description"))
                            mModel.setDescription(mDataObject1.getString("description"));

                        if (!mDataObject1.isNull("date"))
                            mModel.setDate(mDataObject1.getString("date"));

                        if (!mDataObject1.isNull("retail_price"))
                            mModel.setRetail_price(mDataObject1.getString("retail_price"));

                        if (!mDataObject1.isNull("replacement_value"))
                            mModel.setReplacement_value(mDataObject1.getString("replacement_value"));

                        if (!mDataObject1.isNull("week_4days"))
                            mModel.setWeek_4days(mDataObject1.getString("week_4days"));

                        if (!mDataObject1.isNull("week_8days"))
                            mModel.setWeek_8days(mDataObject1.getString("week_8days"));

                        if (!mDataObject1.isNull("week_12days"))
                            mModel.setWeek_12days(mDataObject1.getString("week_12days"));

                        if (!mDataObject1.isNull("instant_booking"))
                            mModel.setInstant_booking(mDataObject1.getString("instant_booking"));

                        if (!mDataObject1.isNull("open_for_sale"))
                            mModel.setOpen_for_sale(mDataObject1.getString("open_for_sale"));

                        if (!mDataObject1.isNull("cleaning_free"))
                            mModel.setCleaning_free(mDataObject1.getString("cleaning_free"));

                        if (!mDataObject1.isNull("drop_person"))
                            mModel.setDrop_person(mDataObject1.getString("drop_person"));

                        if (!mDataObject1.isNull("favourite"))
                            mModel.setFavourite(mDataObject1.getString("favourite"));

                        if (!mDataObject1.isNull("delivery_free"))
                            mModel.setDelivery_free(mDataObject1.getString("delivery_free"));

                        if (!mDataObject1.isNull("image1"))
                            mModel.setImage1(mDataObject1.getString("image1"));

                        if (!mDataObject1.isNull("image2"))
                            mModel.setImage2(mDataObject1.getString("image2"));

                        if (!mDataObject1.isNull("image3"))
                            mModel.setImage3(mDataObject1.getString("image3"));

                        if (!mDataObject1.isNull("image4"))
                            mModel.setImage4(mDataObject1.getString("image4"));

                        if (!mDataObject1.isNull("image5"))
                            mModel.setImage5(mDataObject1.getString("image5"));
                    }

                    if (page_no == 1) {
                        mItemProductModel.add(mModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {
                    /*
                     * Set Adapter
                     * */
                    SetAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }
            }
            else {
//                showToast(mActivity, mJsonObjectt.getString("message"));
            }

            if (mItemProductModel.size() > 0) {
                tv_NoBookings.setVisibility(View.GONE);
                /*
                 * Set Adapter
                 * */
                SetAdapter();
            } else {
                tv_NoBookings.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationInquiriesInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            mProgressBar.setVisibility(View.VISIBLE);

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        GetOrderHistory(page_no);
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }
}
