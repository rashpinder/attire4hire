package com.attire4hire.activity;

import androidx.annotation.RequiresApi;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.utils.WordUtils;
import com.attire4hire.fonts.EdittextBold;
import com.attire4hire.fonts.EdittextRegular;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSettingsActivity extends BaseActivity {

    Activity mActivity = AccountSettingsActivity.this;

    /**
     * Getting the Current Class Name
     */

    String TAG = AccountSettingsActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.tv_submit_acc)
    TextView tv_submit_acc;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.et_sort_code)
    EdittextRegular mSortCode;
    @BindView(R.id.et_acc_no)
    EdittextRegular mAccNo;
    @BindView(R.id.et_add_1)
    EdittextRegular mAddress1ET;
    @BindView(R.id.et_add_2)
    EdittextRegular mAddress2ET;
    @BindView(R.id.et_city)
    EdittextRegular mCityET;
    @BindView(R.id.et_country)
    EdittextRegular mCountryET;
    @BindView(R.id.et_postcode)
    EdittextRegular mPostcodeET;
    @BindView(R.id.et_bank_acc)
    EdittextBold mBankAccET;
    @BindView(R.id.et_state)
    EdittextRegular et_state;

    String AccountStatus = "";
    String Auth_Key = "";
    String mAddress2 = "";

    SwipeRefreshLayout swipeToRefresh;
    boolean isSwipeRefresh = false;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        //Set Butter Knife
        ButterKnife.bind(this);

        mAddress1ET.addTextChangedListener(new GenericTextWatcher1(mAddress1ET));
        mAddress2ET.addTextChangedListener(new GenericTextWatcher1(mAddress2ET));
        mCityET.addTextChangedListener(new GenericTextWatcher1(mCityET));
        et_state.addTextChangedListener(new GenericTextWatcher1(et_state));
        mPostcodeET.addTextChangedListener(new GenericTextWatcher1(mPostcodeET));
        mCountryET.addTextChangedListener(new GenericTextWatcher1(mCountryET));

        if (isNetworkAvailable(mActivity)) {
            executeGetAccountApi();
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    public class GenericTextWatcher1 implements TextWatcher {

        private View view;

        private GenericTextWatcher1(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {

                case R.id.et_add_1:
                    try {
                        String inputString = "" + mAddress1ET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + mAddress1ET.getText().toString())) {
                            mAddress1ET.setText("" + firstLetterCapString);
                            mAddress1ET.setSelection(mAddress1ET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_add_2:
                    try {
                        String inputString = "" + mAddress2ET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + mAddress2ET.getText().toString())) {
                            mAddress2ET.setText("" + firstLetterCapString);
                            mAddress2ET.setSelection(mAddress2ET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_city:
                    try {
                        String inputString = "" + mCityET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + mCityET.getText().toString())) {
                            mCityET.setText("" + firstLetterCapString);
                            mCityET.setSelection(mCityET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_state:
                    try {
                        String inputString = "" + et_state.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + et_state.getText().toString())) {
                            et_state.setText("" + firstLetterCapString);
                            et_state.setSelection(et_state.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_postcode:
                    try {
                        String inputString = "" + mPostcodeET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + mPostcodeET.getText().toString())) {
                            mPostcodeET.setText("" + firstLetterCapString);
                            mPostcodeET.setSelection(mPostcodeET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.et_country:
                    try {
                        String inputString = "" + mCountryET.getText().toString();
                        String firstLetterCapString = WordUtils.capitalize(inputString);
                        if (!firstLetterCapString.equals("" + mCountryET.getText().toString())) {
                            mCountryET.setText("" + firstLetterCapString);
                            mCountryET.setSelection(mCountryET.getText().length());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * Add validations*
     */
    private Boolean validate() {
        boolean flag = true;
        if (mAddress1ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_address));
            flag = false;
        } else if (mCityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.enter_city));
            flag = false;
        } else if (et_state.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state));
            flag = false;
        } else if (mPostcodeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_postcode));
            flag = false;
        } else if (mCountryET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_country));
            flag = false;
        }
        return flag;
    }

    @OnClick({R.id.rlCancelRL, R.id.tv_submit_acc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;
            case R.id.tv_submit_acc:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        if (validate()) {
            executeAccountSetUpApi();
        }
    }

    private void executeAccountSetUpApi() {

        mAddress2 = mAddress2ET.getText().toString().trim();

        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.ADD_ACCOUNT_SETUP;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("street_address", mAddress1ET.getText().toString().trim());
            params.put("street_address2", mAddress2);
            params.put("city", mCityET.getText().toString().trim());
            params.put("state", et_state.getText().toString().trim());
            params.put("country", mCountryET.getText().toString().trim());
            params.put("zipcode", mPostcodeET.getText().toString().trim());

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                parseJsonResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                error.printStackTrace();
                Log.e(TAG, "error" + error);
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseJsonResponse(JSONObject response) {
        dismissProgressDialog();
        try {

            if (response.getString("status").equals("1")) {

                JSONObject data = new JSONObject(response.getString("user_detail"));

                if (!data.isNull("id")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, data.getString("id"));
                }
                if (!data.isNull("first_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, data.getString("first_name"));
                }
                if (!data.isNull("last_name")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, data.getString("last_name"));
                }
                if (!data.isNull("email")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, data.getString("email"));
                }
                if (!data.isNull("gender")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GENDER, data.getString("gender"));
                }
                if (!data.isNull("phone_no")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_NUMBER, data.getString("phone_no"));
                }
                if (!data.isNull("password")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PASSWORD, data.getString("password"));
                }
                if (!data.isNull("profile_pic") && !data.getString("profile_pic").equals("")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, data.getString("profile_pic"));
                }
                if (!data.isNull("created")) {
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CREATED, data.getString("created"));
                }
                if (!data.isNull("email_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, data.getString("email_verification"));

                if (!data.isNull("phone_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, data.getString("phone_verification"));

                if (!data.isNull("id_verification"))
                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, data.getString("id_verification"));

                showAlertDialogFinish(mActivity, getString(R.string.thankyou_for_submit));

            } else {
                showToast(mActivity, response.getString("message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeGetAccountApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ACCOUNT_SETUP;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
//                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("data")) {
                JSONObject mDataObj = response.getJSONObject("data");

                ItemProductModel mModel = new ItemProductModel();

                if (!mDataObj.isNull("id")) {
                    mModel.setId(mDataObj.getString("id"));
                }

                if (!mDataObj.isNull("user_id")) {
                    mModel.setUser_id(mDataObj.getString("user_id"));
                }

                if (!mDataObj.isNull("add_line1")) {
                    mModel.setStreet_address(mDataObj.getString("add_line1"));
                    mAddress1ET.setText(mDataObj.getString("add_line1"));
                }

                if (!mDataObj.isNull("add_line2")) {
                    mModel.setStreet_address2(mDataObj.getString("add_line2"));
                    mAddress2ET.setText(mDataObj.getString("add_line2"));
                }

                if (!mDataObj.isNull("account_number")) {
                    mModel.setAccount_number(mDataObj.getString("account_number"));
                    mAccNo.setText(mDataObj.getString("account_number"));
                }

                if (!mDataObj.isNull("city")) {
                    mModel.setCity(mDataObj.getString("city"));
                    mCityET.setText(mDataObj.getString("city"));
                }

                if (!mDataObj.isNull("state")) {
                    mModel.setState(mDataObj.getString("state"));
                    et_state.setText(mDataObj.getString("state"));
                }

                if (!mDataObj.isNull("country")) {
                    mModel.setCountry(mDataObj.getString("country"));
                    mCountryET.setText(mDataObj.getString("country"));
                }

                if (!mDataObj.isNull("pincode")) {
                    mModel.setZipcode(mDataObj.getString("pincode"));
                    mPostcodeET.setText(mDataObj.getString("pincode"));
                }

                if (!mDataObj.isNull("create_date")) {
                    mModel.setCreation_date(mDataObj.getString("create_date"));
                }

                mItemProductModel.add(mModel);
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
