package com.attire4hire.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.adapters.DetailsSlidingImageAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.chahinem.pageindicator.PageIndicator;
import com.savvi.rangedatepicker.CalendarPickerView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ItemDetailsActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = ItemDetailsActivity.this;

    /*
     * Getting the Current Class Name
     */
    String TAG = ItemDetailsActivity.this.getClass().getSimpleName();

    /*
     * Widgets
     * */
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;
    @BindView(R.id.mIndicatorCI)
    PageIndicator mIndicatorCI;
    @BindView(R.id.txtNameTV)
    TextviewBold txtNameTV;
    @BindView(R.id.txtPriceTV)
    TextviewSemiBold txtPriceTV;
    @BindView(R.id.loc)
    ImageView loc;
    @BindView(R.id.txtLocationTV)
    TextviewSemiBold txtLocationTV;
    @BindView(R.id.txtSizeTV)
    TextviewSemiBold txtSizeTV;
    @BindView(R.id.txtTypeTV)
    TextviewSemiBold txtTypeTV;
    @BindView(R.id.txtColorTV)
    TextviewSemiBold txtColorTV;
    @BindView(R.id.txtBrandDesignNameTV)
    TextviewSemiBold txtBrandDesignNameTV;
    @BindView(R.id.txtConditionsTV)
    TextviewSemiBold txtConditionsTV;
    @BindView(R.id.txtOccasionTV)
    TextviewSemiBold txtOccasionTV;
    @BindView(R.id.txtRentalPriceTV)
    TextviewSemiBold txtRentalPriceTV;
    @BindView(R.id.txtReplacementValueTV)
    TextviewSemiBold txtReplacementValueTV;
    @BindView(R.id.imgDay4IV)
    ImageView imgDay4IV;
    @BindView(R.id.imgDay8IV)
    ImageView imgDay8IV;
    @BindView(R.id.imgDay12IV)
    ImageView imgDay12IV;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.rlDescriptionRL)
    RelativeLayout rlDescriptionRL;
    @BindView(R.id.rlDeleteRL)
    RelativeLayout rlDeleteRL;
    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.txtUserNameTV)
    TextviewSemiBold txtUserNameTV;
    @BindView(R.id.txTotalReviewsTV)
    TextviewRegular txTotalReviewsTV;
    @BindView(R.id.txtUserLocationTV)
    TextviewRegular txtUserLocationTV;
    @BindView(R.id.rlContactLedgerRL)
    RelativeLayout rlContactLedgerRL;
    @BindView(R.id.txtAddToFavoriteTV)
    TextviewSemiBold txtAddToFavoriteTV;
    @BindView(R.id.txtRequestToBuyTV)
    TextviewSemiBold txtRequestToBuyTV;
    @BindView(R.id.txtSelectDaysTV)
    TextviewSemiBold txtSelectDaysTV;
    @BindView(R.id.txtRentTV)
    TextviewSemiBold txtRentTV;
    @BindView(R.id.txt4DaysTV)
    TextviewRegular txt4DaysTV;
    @BindView(R.id.txt8DaysTV)
    TextviewRegular txt8DaysTV;
    @BindView(R.id.txt12DaysTV)
    TextviewRegular txt12DaysTV;
    @BindView(R.id.check_availabilityTv)
    TextviewSemiBold check_availabilityTv;
    @BindView(R.id.ll_add_req)
    LinearLayout ll_add_req;
    @BindView(R.id.rl_reqRent)
    RelativeLayout rl_reqRent;
    @BindView(R.id.ll_rental_fee)
    LinearLayout ll_rental_fee;
    @BindView(R.id.ll_user_details)
    LinearLayout ll_user_details;
    @BindView(R.id.ll_rent)
    LinearLayout ll_rent;
    @BindView(R.id.ll_OpenForSale)
    LinearLayout ll_OpenForSale;
    @BindView(R.id.tv_OpenForSalePrice)
    TextviewSemiBold tv_OpenForSalePrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.mRatingbarRB)
    RatingBar mRatingbarRB;
    @BindView(R.id.userRatings)
    RatingBar userRatings;
    @BindView(R.id.purchasePrice)
    LinearLayout purchasePrice;
    @BindView(R.id.buyRentView)
    View buyRentView;

    /*
     * Initalize Objects...
     * */
    ItemProductModel itemProductModel;
    DetailsSlidingImageAdapter mDetailsSlidingImageAdapter;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<String> mSelectDateList = new ArrayList<>();
    List<Date> mSelectedArrayList;
    List<Date> dates;
    String fromDate = "", nextDate = "";
    CalendarPickerView calendarView;
    String strFarvouriteStatus = "", IdVerification = "", Days = "";

    ArrayList<ItemProductModel> mItemProductModelList = new ArrayList<>();
    String HIDE_BUTTON = "", ITEM_STATUS = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        ButterKnife.bind(this);

        getIntentData();

        rlDeleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mActivity, rlDeleteRL);
                popup.getMenuInflater().inflate(R.menu.option_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delete:
                                //your code
                                // EX : call intent if you want to swich to other activity
                                showConfirmDialog();
                                return true;
                            case R.id.edit:
                                //your code
                                Intent mIntent = new Intent(mActivity, AddItemEditActivity.class);
                                mIntent.putExtra(ConstantData.MODEL, itemProductModel);
                                mIntent.putExtra(ConstantData.ADD_ITEM_TYPE, "AddItemSearch");
                                mActivity.startActivity(mIntent);
                                finish();
                                return true;
                            default:
                                return true;
                        }
                    }
                });
                popup.show();//showing popup menu
            }
        });//closing the setOnClickListener method

        IdVerification = getIdVerification();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            /* for hide case*/
            if (getIntent().getStringExtra(ConstantData.HIDE_BUTTON) != null) {
                HIDE_BUTTON = getIntent().getStringExtra(ConstantData.HIDE_BUTTON);
            }
            if (getIntent().getStringExtra(ConstantData.ITEM_STATUS) != null) {
                ITEM_STATUS = getIntent().getStringExtra(ConstantData.ITEM_STATUS);
            }

            //Set Details Data
            getDetailsData();
        }
    }

    private void getDetailsData() {
        if (isNetworkAvailable(mActivity)) {
            if (mItemProductModel != null)
                mItemProductModel.clear();

            if (mSelectDateList != null)
                mSelectDateList.clear();

            Days = "";
            txtSelectDaysTV.setText("");
            imgDay4IV.setImageResource(R.drawable.ic_uncheck);
            imgDay8IV.setImageResource(R.drawable.ic_uncheck);
            imgDay12IV.setImageResource(R.drawable.ic_uncheck);

            executeDetailsApi();
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void setDataOnWidgets() {
        /*
         * Set ViewPager Images:
         * */
        ArrayList<String> mImagesArrayList = new ArrayList<>();
        if (itemProductModel.getImage1() != null && itemProductModel.getImage1().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage1());
        }
        if (itemProductModel.getImage2() != null && itemProductModel.getImage2().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage2());
        }
        if (itemProductModel.getImage3() != null && itemProductModel.getImage3().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage3());
        }
        if (itemProductModel.getImage4() != null && itemProductModel.getImage4().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage4());
        }
        if (itemProductModel.getImage5() != null && itemProductModel.getImage5().contains("http")) {
            mImagesArrayList.add(itemProductModel.getImage5());
        }

        //Set Images
        mDetailsSlidingImageAdapter = new DetailsSlidingImageAdapter(mActivity, mImagesArrayList);
        mViewPagerVP.setAdapter(mDetailsSlidingImageAdapter);
        mIndicatorCI.attachTo(mViewPagerVP);

        /* set data in widgets */
        txtNameTV.setText(CapitalizedFirstLetter(itemProductModel.getName()));
        String strDay41 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_4days()));
        txtPriceTV.setText("£" + strDay41);
        txtSizeTV.setText(itemProductModel.getSize_name());
        txtTypeTV.setText(itemProductModel.getType_name());
        txtColorTV.setText(itemProductModel.getColor_name());

        txtBrandDesignNameTV.setText(itemProductModel.getBrand_name());
        txtConditionsTV.setText(itemProductModel.getCondition_name());
        txtOccasionTV.setText(itemProductModel.getOcasion_name());
        txtRentalPriceTV.setText("£" + itemProductModel.getRetail_price());
        txtLocationTV.setText(itemProductModel.getLocation());

        if (itemProductModel.getAnother_ProfilePic() != null && itemProductModel.getAnother_ProfilePic().contains("http")) {
            Picasso.with(mActivity).load(itemProductModel.getAnother_ProfilePic())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfilePicIV);
        }

        if (itemProductModel.getRatings() != null && itemProductModel.getRatings().length() > 0) {
            mRatingbarRB.setRating(Float.parseFloat(itemProductModel.getRatings()));
        }

        userRatings.setRating(Float.parseFloat(itemProductModel.getUser_Ratings()));

        txtUserNameTV.setText(itemProductModel.getAnother_User_Name() + " " + itemProductModel.getAnother_User_LastName());
        txtUserLocationTV.setText(itemProductModel.getAnother_User_CityName());
        if (itemProductModel.getReview().equals("1") || itemProductModel.getReview().equals("0")) {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Review");
        } else {
            txTotalReviewsTV.setText(itemProductModel.getReview() + " Reviews");
        }

        String strDay4 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_4days()));
        String strDay8 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_8days()));
        String strDay12 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_12days()));

        txt4DaysTV.setText("4 Days" + " £" + strDay4);
        txt8DaysTV.setText("8 Days" + " £" + strDay8);
        txt12DaysTV.setText("12 Days" + " £" + strDay12);

        // if user id matches with the login user
        if (itemProductModel.getAnother_User_Id().equals(getUserID())) {
            rlContactLedgerRL.setVisibility(View.GONE);
            ll_add_req.setVisibility(View.GONE);
            rl_reqRent.setVisibility(View.GONE);
            imgDay4IV.setVisibility(View.GONE);
            imgDay8IV.setVisibility(View.GONE);
            imgDay12IV.setVisibility(View.GONE);
            check_availabilityTv.setVisibility(View.GONE);
            ll_user_details.setVisibility(View.GONE);
            buyRentView.setVisibility(View.GONE);

            if (itemProductModel.getOpen_for_sale().equals("")) {
                txtRequestToBuyTV.setVisibility(View.GONE);
                purchasePrice.setVisibility(View.GONE);

            } else if (itemProductModel.getOpen_for_sale().equals("0")) {
                txtRequestToBuyTV.setVisibility(View.GONE);
                purchasePrice.setVisibility(View.GONE);

            } else {
                purchasePrice.setVisibility(View.VISIBLE);
                txtReplacementValueTV.setText("£" + itemProductModel.getOpen_for_sale());
            }

        } else {
            rlContactLedgerRL.setVisibility(View.VISIBLE);
            ll_user_details.setVisibility(View.VISIBLE);
            ll_add_req.setVisibility(View.VISIBLE);

            if (itemProductModel.getBuyRequestStatus().equals("false")) {
                txtRequestToBuyTV.setVisibility(View.GONE);
                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    imgDay4IV.setVisibility(View.GONE);
                    imgDay8IV.setVisibility(View.GONE);
                    imgDay12IV.setVisibility(View.GONE);
                    check_availabilityTv.setVisibility(View.GONE);
                    rl_reqRent.setVisibility(View.GONE);
                    txtRequestToBuyTV.setVisibility(View.GONE);
                    purchasePrice.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params1);
                } else {
                    imgDay4IV.setVisibility(View.VISIBLE);
                    imgDay8IV.setVisibility(View.VISIBLE);
                    imgDay12IV.setVisibility(View.VISIBLE);
                    check_availabilityTv.setVisibility(View.VISIBLE);
                    rl_reqRent.setVisibility(View.VISIBLE);
                    txtRequestToBuyTV.setVisibility(View.VISIBLE);
                    purchasePrice.setVisibility(View.VISIBLE);
                }

            } else {
                if (itemProductModel.getBuy_Option().equals("false")) {
                    imgDay4IV.setVisibility(View.GONE);
                    imgDay8IV.setVisibility(View.GONE);
                    imgDay12IV.setVisibility(View.GONE);
                    check_availabilityTv.setVisibility(View.GONE);
                    rl_reqRent.setVisibility(View.GONE);
                    txtRequestToBuyTV.setVisibility(View.GONE);
                    purchasePrice.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params1.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params1);
                } else {
                    imgDay4IV.setVisibility(View.VISIBLE);
                    imgDay8IV.setVisibility(View.VISIBLE);
                    imgDay12IV.setVisibility(View.VISIBLE);
                    check_availabilityTv.setVisibility(View.VISIBLE);
                    rl_reqRent.setVisibility(View.VISIBLE);
                    txtRequestToBuyTV.setVisibility(View.VISIBLE);
                    purchasePrice.setVisibility(View.VISIBLE);
                }
            }

            manageOpenForSale();
        }

        if (itemProductModel.getFavourite().equals("1")) {
            txtAddToFavoriteTV.setText("Add to Favorites");
        } else if (itemProductModel.getFavourite().equals("2")) {
            txtAddToFavoriteTV.setText("Remove from Favorites");
        }

        //Get All Dates:
        Log.e(TAG, "***All Dates List***" + itemProductModel.getDate());
    }

    /* manage open for sale according to buy request status */
    private void manageOpenForSale() {

        if (itemProductModel.getBuyRequestStatus().equals("false")) {
            txtRequestToBuyTV.setVisibility(View.GONE);
            purchasePrice.setVisibility(View.GONE);

            if (itemProductModel.getBuy_Option().equals("false")) {
                rl_reqRent.setVisibility(View.GONE);
                txtRequestToBuyTV.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 20);
                txtAddToFavoriteTV.setLayoutParams(params);
            } else {
                rl_reqRent.setVisibility(View.VISIBLE);
            }
        } else {
            if (itemProductModel.getOpen_for_sale().equals("")) {
                txtRequestToBuyTV.setVisibility(View.GONE);
                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    rl_reqRent.setVisibility(View.GONE);
                    txtRequestToBuyTV.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    rl_reqRent.setVisibility(View.VISIBLE);
                }

            } else if (itemProductModel.getOpen_for_sale().equals("0")) {
                txtRequestToBuyTV.setVisibility(View.GONE);

                purchasePrice.setVisibility(View.GONE);

                if (itemProductModel.getBuy_Option().equals("false")) {
                    rl_reqRent.setVisibility(View.GONE);
                    txtRequestToBuyTV.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    rl_reqRent.setVisibility(View.VISIBLE);
                }

            } else {
                purchasePrice.setVisibility(View.VISIBLE);
                txtReplacementValueTV.setText("£" + itemProductModel.getOpen_for_sale());
                if (itemProductModel.getBuy_Option().equals("false")) {
                    txtRequestToBuyTV.setVisibility(View.GONE);
                    rl_reqRent.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 0, 0, 20);
                    txtAddToFavoriteTV.setLayoutParams(params);
                } else {
                    txtRequestToBuyTV.setVisibility(View.VISIBLE);
                    rl_reqRent.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @OnClick({R.id.rlCancelRL, R.id.imgDay4IV, R.id.imgDay8IV, R.id.imgDay12IV, R.id.rlDescriptionRL,
            R.id.rlContactLedgerRL, R.id.txtAddToFavoriteTV, R.id.txtRequestToBuyTV, R.id.txtSelectDaysTV,
            R.id.txtRentTV, R.id.rlDeleteRL, R.id.check_availabilityTv, R.id.ll_user_details})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;
            case R.id.imgDay4IV:
                performDay4Click();
                break;
            case R.id.imgDay8IV:
                performDay8Click();
                break;
            case R.id.imgDay12IV:
                performDay12Click();
                break;
            case R.id.rlDescriptionRL:
                performDescriptionClick();
                break;
            case R.id.rlContactLedgerRL:
                performContactLedgerClick();
                break;
            case R.id.txtAddToFavoriteTV:
                perfromAddToFavoriteClick();
                break;
            case R.id.txtRequestToBuyTV:
                performRequestToBuy();
                break;
            case R.id.txtSelectDaysTV:
                performSelectDaysClick();
                break;
            case R.id.txtRentTV:
                performRequestToRentClick();
                break;
            case R.id.rlDeleteRL:
                showConfirmDialog();
                break;
            case R.id.check_availabilityTv:
                performCalendarClick();
                break;

            case R.id.ll_user_details:
                performUserDetailsClick();
                break;
        }
    }

    private void performUserDetailsClick() {
        startActivity(new Intent(mActivity, AnotherUserActivity.class).putExtra(Attire4hirePrefrences.ANOTHER_USER_ID, itemProductModel.getAnother_User_Id()));
    }

    private void showConfirmDialog() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_draft_item);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);
        TextView txtDeleteTV = alertDialog.findViewById(R.id.txtDeleteTV);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //Execute api
                if (isNetworkAvailable(mActivity))
                    executeDeteleApi();
                else
                    showToast(mActivity, getString(R.string.internet_connection_error));
            }
        });
        alertDialog.show();
    }

    private void performDay4Click() {
        if (!mSelectDateList.isEmpty()) {
            mSelectDateList.clear();
        }
        Days = "4";
        imgDay4IV.setImageResource(R.drawable.ic_check);
        imgDay8IV.setImageResource(R.drawable.ic_uncheck);
        imgDay12IV.setImageResource(R.drawable.ic_uncheck);
        String strDay4 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_4days()));
        txtSelectDaysTV.setText("4 Days" + "/" + " £" + strDay4);
    }

    private void performDay8Click() {
        if (!mSelectDateList.isEmpty()) {
            mSelectDateList.clear();
        }
        Days = "8";
        imgDay4IV.setImageResource(R.drawable.ic_uncheck);
        imgDay8IV.setImageResource(R.drawable.ic_check);
        imgDay12IV.setImageResource(R.drawable.ic_uncheck);
        String strDay8 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_8days()));
        txtSelectDaysTV.setText("8 Days" + "/" + " £" + strDay8);
    }

    private void performDay12Click() {
        if (!mSelectDateList.isEmpty()) {
            mSelectDateList.clear();
        }
        Days = "12";
        imgDay4IV.setImageResource(R.drawable.ic_uncheck);
        imgDay8IV.setImageResource(R.drawable.ic_uncheck);
        imgDay12IV.setImageResource(R.drawable.ic_check);
        String strDay12 = String.format("%.2f", Double.parseDouble(itemProductModel.getWeek_12days()));
        txtSelectDaysTV.setText("12 Days" + "/" + " £" + strDay12);
    }

    private void performDescriptionClick() {
        showInfromationDialog(mActivity, "Description", CapitalizedFirstLetter(itemProductModel.getDescription()));
    }

    private void performContactLedgerClick() {
        executeRequestChatApi();
    }

    private void perfromAddToFavoriteClick() {
        if (strFarvouriteStatus.equals("1")) {
            Add_Favourite("2");
        } else if (strFarvouriteStatus.equals("2")) {
            Add_Favourite("1");
        }
    }

    private void performSelectDaysClick() {

    }

    private void performCalendarClick() {
        if (!Days.equals("")) {
            showCalenderPopup();
        } else {
            showAlertDialog(mActivity, getString(R.string.choose_rental_period));
        }
    }

    private void performRequestToBuy() {

        if (IdVerification.equals("1") && getEmailVerification().equals("1") && getPhoneVerification().equals("1")) {

            RequestToBuy("buy", "", null);

        } else {
            showVerificationAlertDialog(mActivity, getString(R.string.verify_for_buy_rent));
        }
    }

    private void performRequestToRentClick() {

        String SelectDay = txtSelectDaysTV.getText().toString();

        if (SelectDay.isEmpty()) {

            showAlertDialog(mActivity, getString(R.string.choose_rental_period));

        } else if (mSelectDateList.isEmpty()) {

            showAlertDialog(mActivity, getString(R.string.choose_dates));

        } else {

            if (IdVerification.equals("1") && getPhoneVerification().equals("1") && getEmailVerification().equals("1")) {
                Log.i(TAG, "performRequestToRentClick: " + Days + "::" + mSelectDateList);
                RequestToBuy("rent", Days, mSelectDateList);
            } else {
                showVerificationAlertDialog(mActivity, getString(R.string.verify_for_buy_rent));
            }
        }
    }

    private void RequestToBuy(String Type, String Day, List<String> Date) {
        showProgressDialog(mActivity);

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(Date);

        try {
            jsonObject.put("user_id", getUserID());
            jsonObject.put("product_id", itemProductModel.getId());
            jsonObject.put("type", Type);
            jsonObject.put("days", Day);
            jsonObject.put("booking_date", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "**PARAM**" + jsonObject.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.ADD_REQUEST, jsonObject, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "**RESPONSE**" + response);

                dismissProgressDialog();
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

//                        JSONObject mDataObject = response.getJSONObject("data");

                        if (!response.isNull("id") && !response.getString("id").equals("")) {
                            itemProductModel.setRequest_id(response.getString("id"));
                        }

                        mItemProductModelList.add(itemProductModel);

                        Intent intent = new Intent(mActivity, FinaliseYourRequestActivity.class);
                        intent.putExtra(ConstantData.MODEL, itemProductModel);
                        intent.putExtra(ConstantData.FIRST_DATE, fromDate);
                        intent.putExtra(ConstantData.LAST_DATE, nextDate);
                        mActivity.startActivity(intent);
                        finish();
                    } else if (response.getString("status").equals("4")) {
                        showAlertPayNowDialog(mActivity, response.getString("message"));
                    } else {
                        showAlertDialog(mActivity, response.getString("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeDeteleApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.DELETE_PARTICULARITEM;
        JSONObject params = new JSONObject();
        try {
            params.put("id", itemProductModel.getId());
            params.put("user_id", getUserID());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response);

                try {
                    String strStatus = response.getString("status");
                    String strMessage = response.getString("message");
                    if (strStatus.equals("1")) {
                        showAlertDialogFinish(mActivity, "Your item has been deleted.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeDetailsApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_ITEM_DETAILS_API;
        JSONObject params = new JSONObject();
        try {
            params.put("id", itemProductModel.getId());
            params.put("user_id", getUserID());
            Log.i(TAG, "executeDetailsApi: " + "productId" + itemProductModel.getId());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {

            if (!response.isNull("data")) {

                JSONArray mJsonArray = response.getJSONArray("data");

                for (int i = 0; i < 1; i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);

                    if (!mDataObject.isNull("id"))
                        itemProductModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        itemProductModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        itemProductModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        itemProductModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        itemProductModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        itemProductModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name")) {
                        itemProductModel.setColor_name(mDataObject.getString("color_name"));
                        txtColorTV.setText(mDataObject.getString("color_name"));
                    }
                    if (!mDataObject.isNull("favourite")) {
                        itemProductModel.setFavourite(mDataObject.getString("favourite"));
                        strFarvouriteStatus = mDataObject.getString("favourite");
                    }
                    if (!mDataObject.isNull("rent_request_status"))
                        itemProductModel.setRentRequestStatus(mDataObject.getString("rent_request_status"));

                    if (!mDataObject.isNull("buy_request_status"))
                        itemProductModel.setBuyRequestStatus(mDataObject.getString("buy_request_status"));

                    if (!mDataObject.isNull("buy_option"))
                        itemProductModel.setBuy_Option(mDataObject.getString("buy_option"));

                    if (!mDataObject.isNull("brand_name"))
                        itemProductModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        itemProductModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        itemProductModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        itemProductModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        itemProductModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("location"))
                        itemProductModel.setLocation(mDataObject.getString("location"));
                    if (!mDataObject.isNull("date"))
                        itemProductModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        itemProductModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        itemProductModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        itemProductModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        itemProductModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        itemProductModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        itemProductModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        itemProductModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        itemProductModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        itemProductModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        itemProductModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        itemProductModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        itemProductModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        itemProductModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        itemProductModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        itemProductModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        itemProductModel.setRatings(mDataObject.getString("rating"));

                    JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                    if (!mDataObject_UserDetails.isNull("id"))
                        itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                    if (!mDataObject_UserDetails.isNull("first_name"))
                        itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                    if (!mDataObject_UserDetails.isNull("last_name"))
                        itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                    if (!mDataObject_UserDetails.isNull("email"))
                        itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                    if (!mDataObject_UserDetails.isNull("gender"))
                        itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                    if (!mDataObject_UserDetails.isNull("city_name"))
                        itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                    if (!mDataObject_UserDetails.isNull("phone_no"))
                        itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                    if (!mDataObject_UserDetails.isNull("profile_pic"))
                        itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                    if (!mDataObject_UserDetails.isNull("rating"))
                        itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                    if (!mDataObject_UserDetails.isNull("review"))
                        itemProductModel.setReview(mDataObject_UserDetails.getString("review"));

                    mItemProductModel.add(itemProductModel);
                }

                //Set Data On Widgets
                setDataOnWidgets();
            }

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }

    private void showCalenderPopup() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.calender_popup);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        calendarView = alertDialog.findViewById(R.id.calendar_view);
        ImageView nextClick = alertDialog.findViewById(R.id.rightIV);
        ImageView backClick = alertDialog.findViewById(R.id.leftIV);
        TextView clickBT = alertDialog.findViewById(R.id.tv_set_date);

        clickBT.setVisibility(View.VISIBLE);
        clickBT.setText(getString(R.string.choose_your_rental_dates));

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dt1 = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, 0);

        Date tomorrow = lastYear.getTime();
        /*
         * CLICK PATICULAR DATE
         * */

        calendarView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {

            @Override
            public void onDateSelected(Date date) {
                if (!Days.equals("")) {
                    switch (Days) {
                        case "4":
                            GetSelectedDates(date, 1000 * 60 * 60 * 72, fromDate, nextDate);
                            break;
                        case "8":
                            GetSelectedDates(date, 1000 * 60 * 60 * 168, fromDate, nextDate);
                            break;
                        case "12":
                            GetSelectedDates(date, 1000 * 60 * 60 * 264, fromDate, nextDate);
                            break;
                    }

                    if (mSelectedArrayList != null && mSelectedArrayList.size() != 0) {

                        for (int i = 0; i < mSelectedArrayList.size(); i++) {


                            if (dates.contains(mSelectedArrayList.get(i))) {

                                if (dates != null)
                                    dates.clear();

                                showAlertDialog(mActivity, getString(R.string.blocked_dates));

                                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                        .withHighlightedDates(mSelectedArrayList)
                                        .withSelectedDates(null);

                                break;

                            } else {
                                Log.i(TAG, "onDateSelected: " + "2");
                                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                        .withHighlightedDates(mSelectedArrayList)
                                        .withSelectedDates(dates);
                            }
                        }

                    } else {
                        calendarView.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withSelectedDates(dates);
                    }

                } else {
                    showAlertDialog(mActivity, getResources().getString(R.string.choose_rental_period));
                    alertDialog.dismiss();
                }
            }

            @Override
            public void onDateUnselected(Date date) {
                if (mSelectedArrayList != null && mSelectedArrayList.size() > 0) {
                    calendarView.init(tomorrow, nextYear.getTime(), dt1)
                            .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                            .withHighlightedDates(mSelectedArrayList)
                            .withSelectedDates(dates);
                } else {
                    calendarView.init(tomorrow, nextYear.getTime(), dt1)
                            .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                            .withSelectedDates(dates);
                }
            }
        });

        clickBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dates == null) {
                    showAlertDialog(mActivity, getString(R.string.choose_dates));
                } else {

                    for (int i = 0; i < dates.size(); i++) {

                        String mSelectedDate = convertCalenderTimeToFormat(dates.get(i).toString());
                        Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                        mSelectDateList.add(mSelectedDate);

                        calendarView.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withSelectedDates(dates);
                        Log.e(TAG, "***Converted Rental Dates**" + mSelectDateList.size());
                    }
                    dates.clear();
                    alertDialog.dismiss();
                }
            }
        });

        nextClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                backClick.setVisibility(View.VISIBLE);
                c.add(Calendar.MONTH, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date resultdate = new Date(c.getTimeInMillis());
                calendarView.scrollToDate(resultdate);
            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "monMYJJ::" + Calendar.MONTH);
                c.add(Calendar.MONTH, -1);

                Date resultdate = new Date(c.getTimeInMillis());

                //calender current date
                String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", resultdate); // Week day
                String day = (String) android.text.format.DateFormat.format("dd", resultdate); // day
                String monthString = (String) android.text.format.DateFormat.format("MMM", resultdate); // Month
                String monthNumber = (String) android.text.format.DateFormat.format("MM", resultdate); // Month in number
                String year = (String) android.text.format.DateFormat.format("yyyy", resultdate); // Year

                //calender current month
                int CalenderMonth = Integer.parseInt(monthNumber);

                calendarView.scrollToDate(resultdate);
                Log.e("TAG", "mon::" + resultdate);

                //current month
                int month = Calendar.DAY_OF_MONTH;
                int CurrentMonth = month - 1;

                if (CalenderMonth == month) {
                    backClick.setVisibility(View.GONE);
                } else {
                    backClick.setVisibility(View.VISIBLE);
                }
            }
        });

        Log.e(TAG, "**Dates List Size**: " + itemProductModel.getDate());

        if (itemProductModel.getDate() != null && itemProductModel.getDate().length() > 5) {

            mSelectedArrayList = new ArrayList<>();
            try {
                JSONArray mJsonArray = new JSONArray(itemProductModel.getDate());
                for (int i = 0; i < mJsonArray.length(); i++) {
                    String strDate = mJsonArray.getString(i);
                    Log.e(TAG, "***Single Date**" + strDate);
                    Date mDate = null;
                    try {
                        mDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);

                        mSelectedArrayList.add(mDate);

                        if (c.getTime().after(mDate)) {

                            String today = c.getTime().toString().substring(0, 10);
                            String datee = mDate.toString().substring(0, 10);

                            if (today.equals(datee)) {
//                                Toast.makeText(mActivity, "Today", Toast.LENGTH_SHORT).show();
                            } else {
                                mSelectedArrayList.remove(mDate);
//                                Toast.makeText(mActivity, "Old Date", Toast.LENGTH_SHORT).show();
                            }

                        } else if (c.getTime().equals(mDate)) {
//                            Toast.makeText(mActivity, "Today", Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(mActivity, "New Date", Toast.LENGTH_SHORT).show();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.e(TAG, "***Converted Dates**" + mSelectedArrayList.size());
                }

                if (dates != null && dates.size() > 0) {

                    for (int i = 0; i < dates.size(); i++) {
                        String mSelectedDate = convertCalenderTimeToFormat(dates.get(i).toString());
                        Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                        mSelectDateList.add(mSelectedDate);

                        calendarView.init(tomorrow, nextYear.getTime(), dt1)
                                .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                                .withHighlightedDates(mSelectedArrayList)
                                .withSelectedDates(dates);

                    }
                } else {
                    calendarView.init(tomorrow, nextYear.getTime(), dt1)
                            .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                            .withHighlightedDates(mSelectedArrayList);
                }

            } catch (Exception e) {
                e.toString();
            }

        } else {

            if (dates != null && dates.size() > 0) {

                for (int i = 0; i < dates.size(); i++) {
                    String mSelectedDate = convertCalenderTimeToFormat(dates.get(i).toString());
                    Log.e(TAG, "****Converted Dates***" + mSelectedDate);
                    mSelectDateList.add(mSelectedDate);

                    calendarView.init(tomorrow, nextYear.getTime(), dt1)
                            .inMode(CalendarPickerView.SelectionMode.MULTIPLE)
                            .withSelectedDates(dates);
                }

            } else {
                calendarView.init(tomorrow, nextYear.getTime(), dt1)
                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE);
            }
        }

        alertDialog.show();
    }

    private void Add_Favourite(String FavouriteStatus) {
        showProgressDialog(mActivity);
        Map<String, String> params = new HashMap();
        params.put("user_id", getUserID());
        params.put("product_id", itemProductModel.getId());
        params.put("favourite", FavouriteStatus);

        JSONObject parameters = new JSONObject(params);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.ADD_FAVOURITE, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "**RESPONSE**" + response);

                dismissProgressDialog();
                try {
                    if (response.getString("status").equals("1")) {

                        if (strFarvouriteStatus.equals("1")) {
                            txtAddToFavoriteTV.setText("Remove from Favorites");
                            strFarvouriteStatus = "2";

                        } else if (strFarvouriteStatus.equals("2")) {
                            txtAddToFavoriteTV.setText("Add to Favorites");
                            strFarvouriteStatus = "1";
                        }

                        itemProductModel.setFavourite(strFarvouriteStatus);

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));
                    } else if (response.getString("status").equals("3")) {
                        LogOut();

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());


            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void GetSelectedDates(Date date, int MILLIS_IN_DAY, String startDate, String NextDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        fromDate = sdf.format(date);
        //split year, month and days from the date using StringBuffer.
        StringBuffer sBuffer = new StringBuffer(fromDate);

        String dd = sBuffer.substring(0, 2);
        String mon = sBuffer.substring(3, 5);
        String year = sBuffer.substring(6, 10);

        String modifiedFromDate = dd + '/' + mon + '/' + year;
//        int MILLIS_IN_DAY = 1000 * 60 * 60 * 72;
        /* Use SimpleDateFormat to get date in the format
         * as passed in the constructor. This object can be
         * used to covert date in string format to java.util.Date
         * and vice versa.*/
        java.text.SimpleDateFormat dateFormat =
                new java.text.SimpleDateFormat("dd/MM/yyyy");
        java.util.Date dateSelectedFrom = null;
        java.util.Date dateNextDate = null;
        java.util.Date datePreviousDate = null;

        // convert date present in the String to java.util.Date.
        try {
            dateSelectedFrom = dateFormat.parse(modifiedFromDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //get the next date in String.
        nextDate =
                dateFormat.format(dateSelectedFrom.getTime() + MILLIS_IN_DAY);

        //get the next date in java.util.Date.
        try {
            dateNextDate = dateFormat.parse(nextDate);
            System.out.println("Next day's date: " + dateNextDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dates = getDates(fromDate, nextDate);
    }

    private void executeRequestChatApi() {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.REQUEST_CHAT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("owner_id", itemProductModel.getAnother_User_Id());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {

                    if (response.getString("status").equals("1")) {
                        try {

                            if (!response.isNull("room_id"))
                                itemProductModel.setRoom_id(response.getString("room_id"));

                            if (!response.isNull("owner_id"))
                                itemProductModel.setOwner_id(response.getString("owner_id"));

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_detail");

                            if (!mDataObject_UserDetails.isNull("id"))
                                itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name"))
                                itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                            if (!mDataObject_UserDetails.isNull("last_name"))
                                itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                            if (!mDataObject_UserDetails.isNull("email"))
                                itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic"))
                                itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                            if (!mDataObject_UserDetails.isNull("rating"))
                                itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                            mItemProductModel.add(itemProductModel);

                            startActivity(new Intent(mActivity, ChatActivity.class).putExtra(ConstantData.MODEL, itemProductModel));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    public void showVerificationAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity, AccountSetupActivity.class);
                startActivity(intent);
                finish();
            }
        });
        alertDialog.show();
    }
}
