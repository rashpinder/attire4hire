package com.attire4hire.activity;

import static com.attire4hire.activity.SplashActivity.handleSSLHandshake;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.attire4hire.interfaces.DeleteRequestsInterface;
import com.attire4hire.retrofit.Api;
import com.attire4hire.retrofit.ApiClient;
import com.attire4hire.retrofitModelClasses.MyRequestsPojo;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.RequestAdapter;
import com.attire4hire.fonts.TextviewBold;
import com.attire4hire.interfaces.PaginationRequestInterface;
import com.attire4hire.interfaces.RequestStatusCallBack;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class MyRequestsActivity extends BaseActivity implements PaginationRequestInterface {
    Activity mActivity = MyRequestsActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = MyRequestsActivity.this.getClass().getSimpleName();

    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    @BindView(R.id.request_rv)
    RecyclerView request_rv;

    @BindView(R.id.tv_NoRequest)
    TextviewBold tv_NoRequest;

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    String strLastPage = "FALSE", redirection_type = "";

    int page_no = 1;
    boolean isSwipeRefresh = false;

    PaginationRequestInterface mInterfaceData;

    /*Widgets*/
    RequestAdapter mAdapter;

    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();
    ArrayList<ItemProductModel> tempArrayList = new ArrayList<>();

    ArrayList<String> selectedList = new ArrayList<>();

    BottomSheetDialog bottomSheetDialog;

    DeleteRequestsInterface deleteRequestsInterface = new DeleteRequestsInterface() {
        @Override
        public void mDeleteRequestsInterface(int position, String requestId, LinearLayout linearLayout) {
            selectedList.add(requestId);
            performDeleteRequest(position);
        }
    };

    private void performDeleteRequest(int position) {
        bottomSheetDialog = new BottomSheetDialog(mActivity);

        View view = getLayoutInflater().inflate(R.layout.bottomsheet_delete, null);
        BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();
        final LinearLayout deleteLL = view.findViewById(R.id.deleteLL);
        final LinearLayout cancelLL = view.findViewById(R.id.cancelLL);
        deleteLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                executeDeleteRequest(selectedList, position);
            }
        });
        cancelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_requests);

        ButterKnife.bind(this);

        handleSSLHandshake();

        mInterfaceData = this;

        getIntentData();

        setInquiresAdapter();

        swipeToRefresh.setEnabled(false);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mItemProductModel.clear();
                tempArrayList.clear();
//                GetInquiries(page_no);
                executeMyRequests(page_no);
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {

            /* for hide case*/
            if (getIntent().getStringExtra(ConstantData.REDIRECION_TYPE) != null) {
                redirection_type = getIntent().getStringExtra(ConstantData.REDIRECION_TYPE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mItemProductModel != null)
            mItemProductModel.clear();

        if (tempArrayList != null)
            tempArrayList.clear();

        if (isNetworkAvailable(mActivity)) {
            page_no = 1;
//            GetInquiries(page_no);
            executeMyRequests(page_no);
        } else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (redirection_type != null && redirection_type.equalsIgnoreCase(ConstantData.CLOSET)) {
            finish();
        } else {
            callHomeActivityIntent(mActivity);
        }
    }

    private void setInquiresAdapter() {
        if (request_rv != null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
            request_rv.setLayoutManager(linearLayoutManager);
            mAdapter = new RequestAdapter(mActivity, mItemProductModel, requestStatusCallBack, mInterfaceData, deleteRequestsInterface);
            request_rv.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }

    RequestStatusCallBack requestStatusCallBack = new RequestStatusCallBack() {
        @Override
        public void RequestCall(int position, String RequestStatus, View accept, View Reject, View Declined) {
            UpdateRequestStatus(mItemProductModel.get(position).getRequest_id(), RequestStatus, position, accept, Reject, Declined);
        }
    };

    private void UpdateRequestStatus(String RequestId, String Status, int position, View accept, View Reject, View Declined) {
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.UPDATE_REQUEST_STATUS;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("request_id", RequestId);
            params.put("status", Status);
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {
                    if (response.getString("status").equals("1")) {

                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        if (Status.equals("1")) {
                            startActivity(new Intent(mActivity, Current_BookingsActivity.class));
                            mItemProductModel.remove(position);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mItemProductModel.get(position).setRead_status("true");
                            mItemProductModel.get(position).setRequest_status("Declined");
                            mAdapter.notifyItemChanged(position);
                        }

                        if (mItemProductModel.size() > 0) {
                            tv_NoRequest.setVisibility(View.GONE);

                        } else {
                            tv_NoRequest.setVisibility(View.VISIBLE);
                            mAdapter.notifyDataSetChanged();
                        }

                    } else if (response.getString("status").equals("3")) {
                        LogOut();
                    } else {
                        showAlertDialog(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    public void mPaginationRequestInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {

            if (strLastPage.equals("FALSE")) {
                mProgressBar.setVisibility(View.VISIBLE);
            }

            ++page_no;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
//                        GetInquiries(page_no);
                        executeMyRequests(page_no);
                    } else {
                        if (mProgressBar.isShown())
                            mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    //    execute delete chat api
    private void executeDeleteRequest(List<String> list, int position) {
        showProgressDialog(mActivity);

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(list);

        try {
            jsonObject.put("user_id", getUserID());
            jsonObject.put("request_ids", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "**PARAM**" + jsonObject.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.DeleteRequest, jsonObject, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "**RESPONSE**" + response);

                dismissProgressDialog();
                try {
                    if (response.getString("status").equals("1")) {
                        Log.e(TAG, "**RESPONSE**" + response.getString("message"));

                        mItemProductModel.remove(position);
                        mAdapter.notifyDataSetChanged();

                        if (mItemProductModel.size() > 0) {
                            tv_NoRequest.setVisibility(View.GONE);
                        } else {
                            tv_NoRequest.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error.toString());
            }
        });
        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeMyRequests(int page_no) {
        if (page_no == 1) {
            if (isSwipeRefresh) {
            } else {
                showProgressDialog(mActivity);
            }

        } else if (page_no > 1) {
            mProgressBar.setVisibility(View.GONE);
        }
        Api api = ApiClient.getApiClient().create(Api.class);
        api.getAllRequests(getUserID(), page_no).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (page_no == 1) {
                    dismissProgressDialog();
                }

                if (isSwipeRefresh) {
                    swipeToRefresh.setEnabled(false);
                }

                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());
                        parseRetrofitJsonResponse(jsonObjectMM);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    private void parseRetrofitJsonResponse(JSONObject response) {
        tempArrayList.clear();
        try {
            if (response.getString("status").equals("1")) {

                JSONObject mJsonObject = response.getJSONObject("data");

                strLastPage = mJsonObject.getString("last_page");

                JSONArray mJsonArray = mJsonObject.getJSONArray("all_requests");

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mDataObject = mJsonArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();

                    if (!mDataObject.isNull("id"))
                        mModel.setRequest_id(mDataObject.getString("id"));

                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));

                    if (!mDataObject.isNull("product_id"))
                        mModel.setProduct_id(mDataObject.getString("product_id"));

                    if (!mDataObject.isNull("type"))
                        mModel.setRequest_type(mDataObject.getString("type"));

                    if (!mDataObject.isNull("status"))
                        mModel.setRequest_status(mDataObject.getString("status"));

                    if (!mDataObject.isNull("extension"))
                        mModel.setExtension(mDataObject.getString("extension"));

                    if (!mDataObject.isNull("creation_date"))
                        mModel.setCreation_date(mDataObject.getString("creation_date"));

                    if (!mDataObject.isNull("cost"))
                        mModel.setCost(mDataObject.getString("cost"));

                    if (!mDataObject.isNull("start_date"))
                        mModel.setStart_date(mDataObject.getString("start_date"));

                    if (!mDataObject.isNull("end_date"))
                        mModel.setEnd_date(mDataObject.getString("end_date"));

                    if (!mDataObject.isNull("read_status"))
                        mModel.setRead_status(mDataObject.getString("read_status"));

                    if (!mDataObject.getString("user_detail").equals("")) {

                        JSONObject mDataObject_UserDetails = mDataObject.getJSONObject("user_detail");

                        if (!mDataObject_UserDetails.isNull("id"))
                            mModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                        if (!mDataObject_UserDetails.isNull("first_name"))
                            mModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                        if (!mDataObject_UserDetails.isNull("last_name"))
                            mModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                        if (!mDataObject_UserDetails.isNull("email"))
                            mModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                        if (!mDataObject_UserDetails.isNull("gender"))
                            mModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                        if (!mDataObject_UserDetails.isNull("city_name"))
                            mModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                        if (!mDataObject_UserDetails.isNull("phone_no"))
                            mModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                        if (!mDataObject_UserDetails.isNull("profile_pic"))
                            mModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                    }

                    JSONObject mDataObject1 = mDataObject.getJSONObject("product_detail");

                    if (!mDataObject1.isNull("id"))
                        mModel.setId(mDataObject1.getString("id"));

                    if (!mDataObject1.isNull("name"))
                        mModel.setName(mDataObject1.getString("name"));

                    if (!mDataObject1.isNull("user_id"))
                        mModel.setUser_id(mDataObject1.getString("user_id"));

                    if (!mDataObject1.isNull("image_id"))
                        mModel.setImage_id(mDataObject1.getString("image_id"));

                    if (!mDataObject1.isNull("category_name"))
                        mModel.setCategory_name(mDataObject1.getString("category_name"));

                    if (!mDataObject1.isNull("type_name"))
                        mModel.setType_name(mDataObject1.getString("type_name"));

                    if (!mDataObject1.isNull("size_name"))
                        mModel.setSize_name(mDataObject1.getString("size_name"));

                    if (!mDataObject1.isNull("color_name"))
                        mModel.setColor_name(mDataObject1.getString("color_name"));

                    if (!mDataObject1.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject1.getString("brand_name"));

                    if (!mDataObject1.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject1.getString("condition_name"));

                    if (!mDataObject1.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject1.getString("ocasion_name"));

                    if (!mDataObject1.isNull("description"))
                        mModel.setDescription(mDataObject1.getString("description"));

                    if (!mDataObject1.isNull("date"))
                        mModel.setDate(mDataObject1.getString("date"));

                    if (!mDataObject1.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject1.getString("retail_price"));

                    if (!mDataObject1.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject1.getString("replacement_value"));

                    if (!mDataObject1.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject1.getString("week_4days"));

                    if (!mDataObject1.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject1.getString("week_8days"));

                    if (!mDataObject1.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject1.getString("week_12days"));

                    if (!mDataObject1.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject1.getString("instant_booking"));

                    if (!mDataObject1.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject1.getString("open_for_sale"));

                    if (!mDataObject1.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject1.getString("cleaning_free"));

                    if (!mDataObject1.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject1.getString("drop_person"));

                    if (!mDataObject1.isNull("favourite"))
                        mModel.setFavourite(mDataObject1.getString("favourite"));

                    if (!mDataObject1.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject1.getString("delivery_free"));

                    if (!mDataObject1.isNull("image1"))
                        mModel.setImage1(mDataObject1.getString("image1"));

                    if (!mDataObject1.isNull("image2"))
                        mModel.setImage2(mDataObject1.getString("image2"));

                    if (!mDataObject1.isNull("image3"))
                        mModel.setImage3(mDataObject1.getString("image3"));

                    if (!mDataObject1.isNull("image4"))
                        mModel.setImage4(mDataObject1.getString("image4"));

                    if (!mDataObject1.isNull("image5"))
                        mModel.setImage5(mDataObject1.getString("image5"));

                    if (page_no == 1) {
                        mItemProductModel.add(mModel);
                    } else if (page_no > 1) {
                        tempArrayList.add(mModel);
                    }
                }

                if (tempArrayList.size() > 0) {
                    mItemProductModel.addAll(tempArrayList);
                }

                if (page_no == 1) {
                    /*
                     * Set Adapter
                     * */
                    setInquiresAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }

            } else {
//                showToast(mActivity, mJsonObjectt.getString("message"));
            }

            if (mItemProductModel.size() > 0) {
                tv_NoRequest.setVisibility(View.GONE);
                /*
                 * Set Adapter
                 * */
                if (page_no == 1) {
                    setInquiresAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }
            } else {
                mAdapter.notifyDataSetChanged();
                tv_NoRequest.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

