package com.attire4hire.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

public class PriceRangeActivity extends AppCompatActivity implements View.OnClickListener {

    Activity mActivity = PriceRangeActivity.this;

    /**
     * Getting the Current Class Name
     */
    String TAG = PriceRangeActivity.this.getClass().getSimpleName();

    RelativeLayout rlCancelRL;
    TextView tv_min_value, tv_max_value;
    String pricerange_start = "", pricerange_end = "";
    RangeSeekBar range_Seekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_range);

        intIds();
        performActions();
    }

    private void performActions() {
        rlCancelRL.setOnClickListener(this);

        // Set the range
        range_Seekbar.setRangeValues(0, 1000);

        if (!Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_START, "").equals("") && !Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_END, "").equals("")) {

            pricerange_start = Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_START, "");
            pricerange_end = Attire4hirePrefrences.readString(mActivity, ConstantData.PRICE_RANGE_END, "");

            tv_min_value.setText(pricerange_start);
            tv_max_value.setText(pricerange_end);

            range_Seekbar.setSelectedMinValue(Integer.parseInt(pricerange_start));
            range_Seekbar.setSelectedMaxValue(Integer.parseInt(pricerange_end));

//            range_Seekbar.setRangeValues(Float.parseFloat(pricerange_start), Float.parseFloat(pricerange_end));
        }

        range_Seekbar.setOnRangeSeekBarChangeListener(new com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar bar, Object minValue, Object maxValue) {
                tv_min_value.setText(String.valueOf(minValue));
                tv_max_value.setText(String.valueOf(maxValue));

                pricerange_start = tv_min_value.getText().toString();
                pricerange_end = tv_max_value.getText().toString();

                Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_START, pricerange_start);
                Attire4hirePrefrences.writeString(mActivity, ConstantData.PRICE_RANGE_END, pricerange_end);
            }
        });
    }

    private void intIds() {
        rlCancelRL = findViewById(R.id.rlCancelRL);
        range_Seekbar = findViewById(R.id.range_Seekbar);
        tv_min_value = findViewById(R.id.tv_min_value);
        tv_max_value = findViewById(R.id.tv_max_value);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.rlCancelRL:
                onBackPressed();
                break;
        }
    }
}
