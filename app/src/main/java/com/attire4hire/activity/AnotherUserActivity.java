package com.attire4hire.activity;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.R;
import com.attire4hire.adapters.AnotherUserAdapter;
import com.attire4hire.fonts.TextviewRegular;
import com.attire4hire.fonts.TextviewSemiBold;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AnotherUserActivity extends BaseActivity {
    /*
     * Initlaize Activity
     * */
    Activity mActivity = AnotherUserActivity.this;
    /*
     * Initalize TAG
     * */
    String TAG = AnotherUserActivity.this.getClass().getSimpleName();

    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.imgProfilePicIV)
    CircleImageView imgProfilePicIV;
    @BindView(R.id.txtUserNameTV)
    TextviewSemiBold txtUserNameTV;
    @BindView(R.id.txtLocationTV)
    TextviewRegular txtLocationTV;
    @BindView(R.id.ratingbarRB)
    RatingBar ratingbarRB;
    @BindView(R.id.rlCancelRL)
    RelativeLayout rlCancelRL;
    @BindView(R.id.txtItemCountsTV)
    TextviewRegular txtItemCountsTV;
    @BindView(R.id.txtContactLenderTV)
    TextviewSemiBold txtContactLenderTV;
    @BindView(R.id.txtLenderReviewsTV)
    TextviewSemiBold txtLenderReviewsTV;
    @BindView(R.id.txtRenterReviewsTV)
    TextviewSemiBold txtRenterReviewsTV;
    @BindView(R.id.txtReportUserTV)
    TextviewSemiBold txtReportUserTV;
    @BindView(R.id.imgEmailVerifiedIV)
    ImageView imgEmailVerifiedIV;
    @BindView(R.id.imgPhoneNumberVerifiedIV)
    ImageView imgPhoneNumberVerifiedIV;
    @BindView(R.id.imgIdVerifiedIV)
    ImageView imgIdVerifiedIV;
    @BindView(R.id.homeRL)
    RelativeLayout homeRL;

    AnotherUserAdapter mProfileHorizontalRVAdapter;
    ItemProductModel itemProductModel;
    ArrayList<ItemProductModel> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_user);

        ButterKnife.bind(this);

//        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
    }

    private void getIntentData() {

        if (getIntent() != null) {

            String another_user_id = getIntent().getStringExtra(Attire4hirePrefrences.ANOTHER_USER_ID);

            if (isNetworkAvailable(mActivity))
                executeProfileApi(another_user_id);
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void setData() {

        if (itemProductModel.getAnother_Email_Verification().equals("1")) {
            imgEmailVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgEmailVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (itemProductModel.getAnother_Phone_Verification().equals("1")) {
            imgPhoneNumberVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgPhoneNumberVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (itemProductModel.getAnother_Id_Verification().equals("1")) {
            imgIdVerifiedIV.setImageResource(R.drawable.ic_check);
        } else {
            imgIdVerifiedIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (itemProductModel.getAnother_ProfilePic() != null && itemProductModel.getAnother_ProfilePic().contains("http")) {
            Glide.with(mActivity).load(itemProductModel.getAnother_ProfilePic())
                    .placeholder(R.drawable.ic_pp_ph)
                    .error(R.drawable.ic_pp_ph)
                    .into(imgProfilePicIV);
        }

        ratingbarRB.setRating(Float.parseFloat(itemProductModel.getUser_Ratings()));
        txtUserNameTV.setText(itemProductModel.getAnother_User_Name() + " " + itemProductModel.getAnother_User_LastName());
        txtLocationTV.setText(itemProductModel.getAnother_User_CityName());

        txtItemCountsTV.setText("Listed Items: " + mArrayList.size());

        //Set User Items List
        setmAdapter();
    }

    private void setmAdapter() {
        mRecyclerViewRV.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mProfileHorizontalRVAdapter = new AnotherUserAdapter(mActivity, mArrayList);
        mRecyclerViewRV.setAdapter(mProfileHorizontalRVAdapter);
        mProfileHorizontalRVAdapter.notifyDataSetChanged();
    }

    @OnClick({R.id.rlCancelRL, R.id.txtContactLenderTV, R.id.txtLenderReviewsTV, R.id.txtRenterReviewsTV, R.id.txtReportUserTV,
            R.id.homeRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCancelRL:
                onBackPressed();
                break;

            case R.id.txtContactLenderTV:
                performContactLedgerClick();
                break;

            case R.id.txtLenderReviewsTV:
                performLenderReviewsClick();
                break;

            case R.id.txtRenterReviewsTV:
                performRenterReviewsClick();
                break;

            case R.id.txtReportUserTV:
                performReportUserClick();
                break;

            case R.id.homeRL:
                performHomeClick();
                break;
        }
    }

    private void performHomeClick() {
        callHomeActivityIntent(mActivity);
    }

    private void performReportUserClick() {
        Intent intent = new Intent(mActivity, ReportUserActivity.class);
        intent.putExtra(ConstantData.MODEL, itemProductModel);
        startActivity(intent);
    }

    private void performRenterReviewsClick() {
        Intent i = new Intent(mActivity, RenterReviewProfileActivity.class);
        i.putExtra(ConstantData.MODEL, itemProductModel);
        startActivity(i);
    }

    private void performLenderReviewsClick() {
        Intent in = new Intent(mActivity, LenderReviewsProfileActivity.class);
        in.putExtra(ConstantData.MODEL, itemProductModel);
        startActivity(in);
    }

    private void performContactLedgerClick() {
        executeRequestChatApi();
    }

    private void executeRequestChatApi() {
        mArrayList.clear();
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.REQUEST_CHAT;
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("owner_id", itemProductModel.getAnother_User_Id());
        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                try {

                    if (response.getString("status").equals("1")) {
                        try {

                            if (!response.isNull("room_id"))
                                itemProductModel.setRoom_id(response.getString("room_id"));

                            if (!response.isNull("owner_id"))
                                itemProductModel.setOwner_id(response.getString("owner_id"));

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_detail");

                            if (!mDataObject_UserDetails.isNull("id"))
                                itemProductModel.setAnother_User_Id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name"))
                                itemProductModel.setAnother_User_Name(mDataObject_UserDetails.getString("first_name"));

                            if (!mDataObject_UserDetails.isNull("last_name"))
                                itemProductModel.setAnother_User_LastName(mDataObject_UserDetails.getString("last_name"));

                            if (!mDataObject_UserDetails.isNull("email"))
                                itemProductModel.setAnother_User_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                itemProductModel.setAnother_User_Gender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                itemProductModel.setAnother_User_CityName(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                itemProductModel.setAnother_User_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic"))
                                itemProductModel.setAnother_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));

                            if (!mDataObject_UserDetails.isNull("rating"))
                                itemProductModel.setUser_Ratings(mDataObject_UserDetails.getString("rating"));

                            mArrayList.add(itemProductModel);

                            startActivity(new Intent(mActivity, ChatActivity.class).putExtra(ConstantData.MODEL, itemProductModel));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        showToast(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void executeProfileApi(String AnotherUserId) {
        mArrayList.clear();
        showProgressDialog(mActivity);
        String mApiUrl = ConstantData.GET_USER_PROFILE;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", getUserID());
            params.put("viewed_user", AnotherUserId);
        } catch (Exception e) {
            e.toString();
        }
        Log.e(TAG, "**PARAMS**" + params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);

                try {
                    if (response.getString("status").equals("1")) {
                        parseResponse(response);
                    } else if (response.getString("status").equals("3")) {
//                        LogOut();
                        showToast(mActivity, response.getString("message"));
                    } else {
                        showToast(mActivity, response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + error);
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            if (!response.isNull("user_details")) {

                JSONObject mDataObj = response.getJSONObject("user_details");

                itemProductModel = new ItemProductModel();

                if (!mDataObj.isNull("id"))
                    itemProductModel.setAnother_User_Id(mDataObj.getString("id"));

                if (!mDataObj.isNull("first_name"))
                    itemProductModel.setAnother_User_Name(mDataObj.getString("first_name"));

                if (!mDataObj.isNull("last_name"))
                    itemProductModel.setAnother_User_LastName(mDataObj.getString("last_name"));

                if (!mDataObj.isNull("email"))
                    itemProductModel.setAnother_User_Email(mDataObj.getString("email"));

                if (!mDataObj.isNull("gender"))
                    itemProductModel.setAnother_User_Gender(mDataObj.getString("gender"));

                if (!mDataObj.isNull("city_name"))
                    itemProductModel.setAnother_User_CityName(mDataObj.getString("city_name"));

                if (!mDataObj.isNull("phone_no"))
                    itemProductModel.setAnother_User_PhoneNo(mDataObj.getString("phone_no"));

                if (!mDataObj.isNull("profile_pic") && !mDataObj.getString("profile_pic").equals(""))
                    itemProductModel.setAnother_ProfilePic(mDataObj.getString("profile_pic"));

                if (!mDataObj.isNull("email_verification"))
                    itemProductModel.setAnother_Email_Verification(mDataObj.getString("email_verification"));

                if (!mDataObj.isNull("phone_verification"))
                    itemProductModel.setAnother_Phone_Verification(mDataObj.getString("phone_verification"));

                if (!mDataObj.isNull("id_verification"))
                    itemProductModel.setAnother_Id_Verification(mDataObj.getString("id_verification"));

                if (!mDataObj.isNull("rating"))
                    itemProductModel.setUser_Ratings(mDataObj.getString("rating"));
            }

            if (!response.isNull("product_datils")) {
                JSONArray mDataArray = response.getJSONArray("product_datils");
                for (int i = 0; i < mDataArray.length(); i++) {
                    JSONObject mDataObject = mDataArray.getJSONObject(i);
                    ItemProductModel mModel = new ItemProductModel();
                    if (!mDataObject.isNull("id"))
                        mModel.setId(mDataObject.getString("id"));
                    if (!mDataObject.isNull("name"))
                        mModel.setName(mDataObject.getString("name"));
                    if (!mDataObject.isNull("user_id"))
                        mModel.setUser_id(mDataObject.getString("user_id"));
                    if (!mDataObject.isNull("category_name"))
                        mModel.setCategory_name(mDataObject.getString("category_name"));
                    if (!mDataObject.isNull("type_name"))
                        mModel.setType_name(mDataObject.getString("type_name"));
                    if (!mDataObject.isNull("size_name"))
                        mModel.setSize_name(mDataObject.getString("size_name"));
                    if (!mDataObject.isNull("color_name"))
                        mModel.setColor_name(mDataObject.getString("color_name"));
                    if (!mDataObject.isNull("brand_name"))
                        mModel.setBrand_name(mDataObject.getString("brand_name"));
                    if (!mDataObject.isNull("designer_name"))
                        mModel.setDesigner_name(mDataObject.getString("designer_name"));
                    if (!mDataObject.isNull("condition_name"))
                        mModel.setCondition_name(mDataObject.getString("condition_name"));
                    if (!mDataObject.isNull("ocasion_name"))
                        mModel.setOcasion_name(mDataObject.getString("ocasion_name"));
                    if (!mDataObject.isNull("description"))
                        mModel.setDescription(mDataObject.getString("description"));
                    if (!mDataObject.isNull("date"))
                        mModel.setDate(mDataObject.getString("date"));
                    if (!mDataObject.isNull("retail_price"))
                        mModel.setRetail_price(mDataObject.getString("retail_price"));
                    if (!mDataObject.isNull("replacement_value"))
                        mModel.setReplacement_value(mDataObject.getString("replacement_value"));
                    if (!mDataObject.isNull("week_4days"))
                        mModel.setWeek_4days(mDataObject.getString("week_4days"));
                    if (!mDataObject.isNull("week_8days"))
                        mModel.setWeek_8days(mDataObject.getString("week_8days"));
                    if (!mDataObject.isNull("week_12days"))
                        mModel.setWeek_12days(mDataObject.getString("week_12days"));
                    if (!mDataObject.isNull("instant_booking"))
                        mModel.setInstant_booking(mDataObject.getString("instant_booking"));
                    if (!mDataObject.isNull("open_for_sale"))
                        mModel.setOpen_for_sale(mDataObject.getString("open_for_sale"));
                    if (!mDataObject.isNull("cleaning_free"))
                        mModel.setCleaning_free(mDataObject.getString("cleaning_free"));
                    if (!mDataObject.isNull("drop_person"))
                        mModel.setDrop_person(mDataObject.getString("drop_person"));
                    if (!mDataObject.isNull("delivery_free"))
                        mModel.setDelivery_free(mDataObject.getString("delivery_free"));
                    if (!mDataObject.isNull("image1"))
                        mModel.setImage1(mDataObject.getString("image1"));
                    if (!mDataObject.isNull("image2"))
                        mModel.setImage2(mDataObject.getString("image2"));
                    if (!mDataObject.isNull("image3"))
                        mModel.setImage3(mDataObject.getString("image3"));
                    if (!mDataObject.isNull("image4"))
                        mModel.setImage4(mDataObject.getString("image4"));
                    if (!mDataObject.isNull("image5"))
                        mModel.setImage5(mDataObject.getString("image5"));
                    if (!mDataObject.isNull("rating"))
                        mModel.setRatings(mDataObject.getString("rating"));

                    mArrayList.add(mModel);
                }
            }

            //Set Data ON Widgets
            setData();

        } catch (Exception e) {
            Log.e(TAG, "**ERROR**" + e.toString());
        }
    }
}
