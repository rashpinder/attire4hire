package com.attire4hire.activity;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.attire4hire.R;

import java.io.IOException;
import java.io.InputStream;

public class CitiesSearchActivity extends AppCompatActivity {

    Activity mActivity = CitiesSearchActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = CitiesSearchActivity.this.getClass().getSimpleName();

    /*Widgets*/
    RecyclerView inquires_rv;
    ImageView iv_cancel_req;
    EditText editsearchET;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_search);

        inquires_rv = findViewById(R.id.inquires_rv);
        iv_cancel_req = findViewById(R.id.iv_cancel_req);
        editsearchET = findViewById(R.id.editsearchET);
        // Data Test
        loadJSONFromAsset();
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is =getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
