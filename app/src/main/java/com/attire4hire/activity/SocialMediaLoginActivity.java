package com.attire4hire.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.attire4hire.R;
import com.attire4hire.instagramConfig.InstagramApp;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.Attire4hireApplication;
import com.attire4hire.utils.Attire4hirePrefrences;
import com.attire4hire.utils.ConstantData;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SocialMediaLoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    /************************
     *Fused Google Location
     **************/
    public static final int REQUEST_PERMISSION_CODE = 919;
    public String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    public String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    protected String mLastUpdateTime;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public double mLatitude;
    public double mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    /****************************************/

    Activity mActivity = SocialMediaLoginActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SocialMediaLoginActivity.this.getClass().getSimpleName();
    protected static final int RC_SIGN_IN = 12324;
    CallbackManager callbackManager;

    /**
     * Widgets
     */
    @BindView(R.id.fbButton)

    RelativeLayout fbButton;
    String Username = "";
    String lastName = "";
    String gender = "";
    String email = "";
    String token = "";
    String facebook_id = "";
    String google_id = "";
    String phoneNumber = "";
    String instgram_id = "";
    String timePass = "";
    FirebaseAuth mAuth;
    FirebaseUser mFirebaseUser;
    String firstName = "";
    String lastname = "";
    String FacebookProfilePicture = "";
    String GoogleProfilePicture = "";

    TextView mSignInTV;
    @BindView(R.id.emailBT)
    RelativeLayout emailBT;
    @BindView(R.id.googleBT)
    RelativeLayout googleBT;
    @BindView(R.id.instaBt)
    RelativeLayout instaBt;

    @BindView(R.id.byProceedingTV)
    TextView byProceedingTV;

    GoogleSignInClient mGoogleSignInClient;
    private InstagramApp mApp;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private CallbackManager mCallbackManager;
    private URL fbProfilePicture;

    String strFirstName = "", strLastName = "", Device_Token = "";
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    ArrayList<ItemProductModel> mItemProductModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*************
         *Google Location
         **************/

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();

        if (!checkPermission()) {
            requestPermission();
        } else {
            checkLocationSettings();
        }

        updateValuesFromBundle(savedInstanceState);
        /*******************************************/

        setContentView(R.layout.activity_social_media_login);

        ButterKnife.bind(this);
        mSignInTV = findViewById(R.id.tv_sign_in);
        spannabeText();

        //Logout
        disconnectFromFacebook();

        // Initialize your instance of callbackManager//
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //Logout First
        FirebaseAuth.getInstance().signOut();
        signOut();

        addClicksToTermsAndPrivacyString();

        getPushToken();
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    Device_Token = task.getResult();
                    Log.e(TAG, "**Push Token**" + Device_Token);

                });
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.by_proceeding));

        ClickableSpan teremsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {


                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.TERM_AND_SERVICES);
                startActivity(mIntent);

            }
        };

        // Character starting from 32 - 48 is Terms and condition.
        // Character starting from 79 - 93 is privacy policy.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra(ConstantData.LINK, ConstantData.PRIVACY_POLICY);
                startActivity(mIntent);

            }
        };

        SpanString.setSpan(teremsAndCondition, 32, 48, 0);
        SpanString.setSpan(privacy, 79, 93, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 32, 48, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 79, 93, 0);
        SpanString.setSpan(new UnderlineSpan(), 32, 48, 0);
        SpanString.setSpan(new UnderlineSpan(), 79, 93, 0);

        byProceedingTV.setMovementMethod(LinkMovementMethod.getInstance());
        byProceedingTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        byProceedingTV.setSelected(true);
    }

    /*
     * Disconnect with facebook
     * */
    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    private void signIn() {
        signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick({R.id.fbButton, R.id.emailBT, R.id.googleBT, R.id.instaBt, R.id.tv_sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fbButton:
                showProgressDialog(mActivity);
                performFbClick();
                break;
            case R.id.emailBT:
                Intent intent = new Intent(mActivity, SignupActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.googleBT:
                signIn();
                break;
            case R.id.instaBt:
                onInstaBTClick();
                break;
        }

    }

    public void spannabeText() {
        String str_signup = "<font color=#FFFFFF>Already have an account? </font>" + "<b><font color=#FFFFFF>SIGN IN!</font></b>";
        SpannableString ss = new SpannableString((Html.fromHtml(str_signup)));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(mActivity, SigninActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.white));
            }
        };
        ss.setSpan(clickableSpan, 25, 32, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        mSignInTV.setText(ss);
        mSignInTV.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void onInstaBTClick() {
        mApp.authorize();
    }

    // [START signOut]
    private void signOut() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // [START_EXCLUDE]
                            Log.e(TAG, "==Logout Successfully==");
                            // [END_EXCLUDE]
                        }
                    });
        }
    }

    private void performFbClick() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
        callbackManager = CallbackManager.Factory.create();

        FBLogin();
    }

    private void FBLogin() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("email", "public_profile"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                    dismissProgressDialog();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                    dismissProgressDialog();
                }
            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
            dismissProgressDialog();
        }
    }

    private void getFacebookData(LoginResult loginResult) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }

                    Username = fbSocialUserserName;
                    email = fbEmail;
                    facebook_id = fbId;

                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }

                    Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FACEBOOK_IDD, facebook_id);

                    executeFacebookApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture,gender,location");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            email = user.getEmail();
                            Username = user.getDisplayName();
                            facebook_id = user.getUid();
                            FacebookProfilePicture = user.getPhotoUrl().toString();
                            Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FACEBOOK_IDD, facebook_id);

                            executeFacebookApi();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(mActivity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    /*
     *Sign In Api
     * */
    private void executeFacebookApi() {
        showProgressDialog(mActivity);
        String[] firstLastName = {};
//        String strFristName = "", strLastName = "";
        if (Username != null && Username.contains(" ")) {
            firstLastName = Username.split(" ");
            strFirstName = firstLastName[0];
            strLastName = firstLastName[1];
        } else {
            strFirstName = Username;
        }

        if (email == (null)) {
            email = "";
        }
        Map<String, String> params = new HashMap();
        if (strFirstName != null && strFirstName.length() > 0) {
            params.put("first_name", strFirstName);
        }
        if (strLastName != null && strLastName.length() > 0) {
            params.put("last_name", strLastName);
        }

        params.put("email", email);
        params.put("gender", "");
        params.put("phone", "");
        params.put("password", "");
        params.put("facebook_id", facebook_id);
        params.put("device_token", Device_Token);
        params.put("device_type", "1");

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.FACEBOOK_API, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "**RESPONSE**" + response);
                dismissProgressDialog();
                try {

                    if (response.getString("status").equals("1")) {

                        if (!response.getString("id").equals("")) {
                            Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, response.getString("id"));
                        }

                        if (!response.getString("user_details").equals("")) {

                            ItemProductModel mModel = new ItemProductModel();

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_details");

                            if (!mDataObject_UserDetails.isNull("id"))
                                mModel.setUser_id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name")) {
                                mModel.setUser_First_Name(mDataObject_UserDetails.getString("first_name"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObject_UserDetails.getString("first_name"));
                            }
                            if (!mDataObject_UserDetails.isNull("last_name")) {
                                mModel.setUser_Last_Name(mDataObject_UserDetails.getString("last_name"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObject_UserDetails.getString("last_name"));
                            }
                            if (!mDataObject_UserDetails.isNull("email"))
                                mModel.setUser_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                mModel.setGender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                mModel.setUser_City(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                mModel.setUser_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic") && !mDataObject_UserDetails.getString("profile_pic").equals("")) {
                                mModel.setUser_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObject_UserDetails.getString("profile_pic"));
                            } else {
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, FacebookProfilePicture);
                            }

                            if (!mDataObject_UserDetails.isNull("email_verification")) {
                                mModel.setEmail_Verification(mDataObject_UserDetails.getString("email_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObject_UserDetails.getString("email_verification"));
                            }
                            if (!mDataObject_UserDetails.isNull("phone_verification")) {
                                mModel.setPhone_Verification(mDataObject_UserDetails.getString("phone_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, mDataObject_UserDetails.getString("phone_verification"));
                            }

                            if (!mDataObject_UserDetails.isNull("id_verification")) {
                                mModel.setId_Verification(mDataObject_UserDetails.getString("id_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, mDataObject_UserDetails.getString("id_verification"));
                            }

                            if (!mDataObject_UserDetails.isNull("facebook_id")) {
                                mModel.setFacebook_id(mDataObject_UserDetails.getString("facebook_id"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FACEBOOK_ID, mDataObject_UserDetails.getString("facebook_id"));
                            }

                            mItemProductModel.add(mModel);
                        }

                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.IS_LOGIN, "true");
                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LOGIN_TYPE, "social");
                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, email);

                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        showAlertDialog(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
                error.printStackTrace();
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }

    /*
     * Locations
     * */

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);

        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();
                }
                return;
            }

        }
    }

    /*****************Google Location************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i("", "User agreed to make required location settings changes.");
                    startLocationUpdates();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i("", "User chose not to make required location settings changes.");
                    break;
                default:
                    // finish();
            }
        }

//        switch (requestCode) {
//            // Check for the integer request code originally supplied to startResolutionForResult().
//            case REQUEST_CHECK_SETTINGS:
//                switch (resultCode) {
//                    case Activity.RESULT_OK:
//                        Log.i("", "User agreed to make required location settings changes.");
//                        startLocationUpdates();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Log.i("", "User chose not to make required location settings changes.");
//                        break;
//                    default:
//                        // finish();
//                }
//
//            case RC_SIGN_IN:

        else if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

//        break;
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = mCurrentLocation.getLatitude();
        mLongitude = mCurrentLocation.getLongitude();
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            email = account.getEmail();
            google_id = account.getId();
            Username = account.getDisplayName();

            if (account.getPhotoUrl() != null) {
                GoogleProfilePicture = account.getPhotoUrl().toString();
            } else {
                GoogleProfilePicture = "";
            }

            executeGoogleApii();

        } catch (ApiException e) {
            Log.e("MyTAG", "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(mActivity, "Failed", Toast.LENGTH_LONG);

        }
    }

    private void executeGoogleApii() {
        if (phoneNumber == (null)) {
            phoneNumber = "";
        }
        showProgressDialog(mActivity);

        String[] firstLastName = {};

        if (Username != null && Username.contains(" ")) {
            firstLastName = Username.split(" ");
            strFirstName = firstLastName[0];
            strLastName = firstLastName[1];
        } else {
            strFirstName = Username;
        }

        Map<String, String> params = new HashMap();

        if (strFirstName != null && strFirstName.length() > 0) {
            params.put("first_name", strFirstName);
        }
        if (strLastName != null && strLastName.length() > 0) {
            params.put("last_name", strLastName);
        }

        params.put("email", email);
        params.put("gender", "");
        params.put("phone", phoneNumber);
        params.put("password", "");
        params.put("google_id", google_id);
        params.put("device_token", Device_Token);
        params.put("device_type", "1");

        JSONObject parameters = new JSONObject(params);
        Log.e(TAG, "**Params**: " + parameters.toString());
        Log.e(TAG, "**ApiUrl**: " + ConstantData.GOOGLE_API);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, ConstantData.GOOGLE_API, parameters, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "RESPONSE" + response);
                dismissProgressDialog();
                try {

                    if (response.getString("status").equals("1")) {

                        if (!response.getString("id").equals("")) {

                            Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.USER_ID, response.getString("id"));
                        }

                        if (!response.getString("user_details").equals("")) {

                            ItemProductModel mModel = new ItemProductModel();

                            JSONObject mDataObject_UserDetails = response.getJSONObject("user_details");

                            if (!mDataObject_UserDetails.isNull("id"))
                                mModel.setUser_id(mDataObject_UserDetails.getString("id"));

                            if (!mDataObject_UserDetails.isNull("first_name")) {
                                mModel.setUser_First_Name(mDataObject_UserDetails.getString("first_name"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.FIRST_NAME, mDataObject_UserDetails.getString("first_name"));
                            }
                            if (!mDataObject_UserDetails.isNull("last_name")) {
                                mModel.setUser_Last_Name(mDataObject_UserDetails.getString("last_name"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LAST_NAME, mDataObject_UserDetails.getString("last_name"));
                            }
                            if (!mDataObject_UserDetails.isNull("email"))
                                mModel.setUser_Email(mDataObject_UserDetails.getString("email"));

                            if (!mDataObject_UserDetails.isNull("gender"))
                                mModel.setGender(mDataObject_UserDetails.getString("gender"));

                            if (!mDataObject_UserDetails.isNull("city_name"))
                                mModel.setUser_City(mDataObject_UserDetails.getString("city_name"));

                            if (!mDataObject_UserDetails.isNull("phone_no"))
                                mModel.setUser_PhoneNo(mDataObject_UserDetails.getString("phone_no"));

                            if (!mDataObject_UserDetails.isNull("profile_pic") && !mDataObject_UserDetails.getString("profile_pic").equals("")) {
                                mModel.setUser_ProfilePic(mDataObject_UserDetails.getString("profile_pic"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, mDataObject_UserDetails.getString("profile_pic"));
                            } else {
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PROFILE_PIC, GoogleProfilePicture);
                            }
                            if (!mDataObject_UserDetails.isNull("email_verification")) {
                                mModel.setEmail_Verification(mDataObject_UserDetails.getString("email_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL_VERIFICATION, mDataObject_UserDetails.getString("email_verification"));
                            }

                            if (!mDataObject_UserDetails.isNull("phone_verification")) {
                                mModel.setPhone_Verification(mDataObject_UserDetails.getString("phone_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.PHONE_VERIFICATION, mDataObject_UserDetails.getString("phone_verification"));
                            }
                            if (!mDataObject_UserDetails.isNull("id_verification")) {
                                mModel.setId_Verification(mDataObject_UserDetails.getString("id_verification"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.ID_VERIFICATION, mDataObject_UserDetails.getString("id_verification"));
                            }
                            if (!mDataObject_UserDetails.isNull("google_id")) {
                                mModel.setGoogle_id(mDataObject_UserDetails.getString("google_id"));
                                Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.GOOGLE_ID, mDataObject_UserDetails.getString("google_id"));
                            }

                            mItemProductModel.add(mModel);
                        }

                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.IS_LOGIN, "true");
                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.LOGIN_TYPE, "social");

                        Attire4hirePrefrences.writeString(mActivity, Attire4hirePrefrences.EMAIL, email);

                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        showAlertDialog(mActivity, response.getString("message"));
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                Log.e(TAG, "error" + error);
                error.printStackTrace();
                //TODO: handle failure
            }
        });

        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
    }
}