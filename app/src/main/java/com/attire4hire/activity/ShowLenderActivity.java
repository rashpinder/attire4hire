package com.attire4hire.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.attire4hire.R;
import com.attire4hire.model.ItemProductModel;
import com.attire4hire.utils.ConstantData;
import com.whinc.widget.ratingbar.RatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowLenderActivity extends BaseActivity {
    Activity mActivity = ShowLenderActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = ShowLenderActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.cancelRL)
    RelativeLayout cancelRL;

    @BindView(R.id.exp_rating)
    RatingBar exp_rating;

    @BindView(R.id.comm_rating)
    RatingBar comm_rating;

    @BindView(R.id.return_rating)
    RatingBar return_rating;

    @BindView(R.id.item_looked_rating)
    RatingBar item_looked_rating;

    @BindView(R.id.et_exp)
    EditText et_exp;

    @BindView(R.id.nameTV)
    TextView nameTV;

    ItemProductModel itemProductModel;

    String strFullName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_lender);

        //butter knife
        ButterKnife.bind(this);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            itemProductModel = (ItemProductModel) getIntent().getSerializableExtra(ConstantData.MODEL);

            exp_rating.setCount(Integer.parseInt(itemProductModel.getOverall_exp()));

            comm_rating.setCount(Integer.parseInt(itemProductModel.getCommunication()));

            return_rating.setCount(Integer.parseInt(itemProductModel.getItem_arrive_on_time()));

            item_looked_rating.setCount(Integer.parseInt(itemProductModel.getItem_as_desc()));

            if (itemProductModel.getAnother_User_Name() != null && !itemProductModel.getAnother_User_Name().equals("")) {
                strFullName = itemProductModel.getAnother_User_Name() + " " + itemProductModel.getAnother_User_LastName() + "'s ";
            }
            nameTV.setText(strFullName + "Reviews");

            et_exp.setEnabled(false);
            et_exp.setText(CapitalizedFirstLetter(itemProductModel.getComments()));
        }
    }

    @OnClick({R.id.cancelRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelRL:
                onBackPressed();
                break;
        }
    }
}
