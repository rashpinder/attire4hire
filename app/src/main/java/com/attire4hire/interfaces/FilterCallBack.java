package com.attire4hire.interfaces;

import com.attire4hire.model.ItemProductModel;

public interface FilterCallBack {
    public void CallBack(int position, String Size, String Condition, String Color, String Brand, String ItemCategory, String ProductType, String Ocassion);
}
