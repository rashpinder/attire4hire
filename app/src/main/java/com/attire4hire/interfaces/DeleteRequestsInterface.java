package com.attire4hire.interfaces;

import android.widget.LinearLayout;

import java.util.ArrayList;

public interface DeleteRequestsInterface {
    public void mDeleteRequestsInterface(int position, String requestId, LinearLayout linearLayout);
}
