package com.attire4hire.interfaces;

import android.view.View;
import android.widget.TextView;

public interface CancelOrderInterface {
    public void cancelCall(int position, View CancelView, TextView ConfirmedView, TextView NameView);
}
