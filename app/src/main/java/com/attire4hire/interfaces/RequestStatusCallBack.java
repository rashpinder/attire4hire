package com.attire4hire.interfaces;

import android.view.View;

public interface RequestStatusCallBack {
    public void RequestCall(int position, String RequestStatus, View accept, View Reject, View Declined);
}
