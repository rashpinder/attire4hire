package com.attire4hire.interfaces;

import com.attire4hire.model.SearchLocationModel;

public interface LocationCallBack {
    public void getLocationSelection(SearchLocationModel mModel);
}
