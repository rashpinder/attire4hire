package com.attire4hire.interfaces;

import com.attire4hire.model.ItemProductModel;

public interface DraftCallBacks {
    public void draftDelete(ItemProductModel mModel, int position);
}
