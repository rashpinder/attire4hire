package com.attire4hire.interfaces;

import android.view.View;

public interface ExpandableListCallBack {
    public void ExpandableCall(int listPosition, int expandedListPosition, View view);
}
