package com.attire4hire.interfaces;

import com.attire4hire.model.ItemProductModel;

public interface FavouriteCallBack {
    public void favouriteCall(int position,String FavouriteStatus);
}
